(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-others-orders-orders-module"],{

/***/ "./src/app/shared/inmemory-db/users.ts":
/*!*********************************************!*\
  !*** ./src/app/shared/inmemory-db/users.ts ***!
  \*********************************************/
/*! exports provided: UserDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserDB", function() { return UserDB; });
var UserDB = /** @class */ (function () {
    function UserDB() {
        this.users = [
            {
                '_id': '5a7b73f76bed15c94d1e46d4',
                'index': 0,
                'guid': 'c01da2d1-07f8-4acc-a1e3-72dda7310af8',
                'isActive': false,
                'balance': 2838.08,
                'age': 30,
                'name': 'Stefanie Marsh',
                'gender': 'female',
                'company': 'ACIUM',
                'email': 'stefaniemarsh@acium.com',
                'phone': '+1 (857) 535-2066',
                'address': '163 Poplar Avenue, Cliffside, Virginia, 4592',
                'bd': '2015-02-08T04:28:44 -06:00'
            },
            {
                '_id': '5a7b73f7f79f4250b96a355a',
                'index': 1,
                'guid': '3f04aa40-62da-466d-ac14-2b8a5da3d1ce',
                'isActive': true,
                'balance': 3043.81,
                'age': 39,
                'name': 'Elena Bennett',
                'gender': 'female',
                'company': 'FIBRODYNE',
                'email': 'elenabennett@fibrodyne.com',
                'phone': '+1 (994) 570-2070',
                'address': '526 Grace Court, Cherokee, Oregon, 7017',
                'bd': '2017-11-15T09:04:57 -06:00'
            },
            {
                '_id': '5a7b73f78b64a02a67204d6e',
                'index': 2,
                'guid': 'e7d9d61e-b657-4fcf-b069-2eb9bfdc44fa',
                'isActive': true,
                'balance': 1796.92,
                'age': 23,
                'name': 'Joni Cabrera',
                'gender': 'female',
                'company': 'POWERNET',
                'email': 'jonicabrera@powernet.com',
                'phone': '+1 (848) 410-2368',
                'address': '554 Barlow Drive, Alamo, Michigan, 3686',
                'bd': '2017-10-15T12:55:51 -06:00'
            },
            {
                '_id': '5a7b73f7572e59b231149b94',
                'index': 3,
                'guid': '47673d82-ab31-48a1-8a16-2c6701573c67',
                'isActive': false,
                'balance': 2850.27,
                'age': 37,
                'name': 'Gallagher Shaw',
                'gender': 'male',
                'company': 'ZILLAR',
                'email': 'gallaghershaw@zillar.com',
                'phone': '+1 (896) 422-3786',
                'address': '111 Argyle Road, Graball, Idaho, 7272',
                'bd': '2017-11-19T03:38:30 -06:00'
            },
            {
                '_id': '5a7b73f70f9d074552e13090',
                'index': 4,
                'guid': 'bc9c7cd3-04e0-4095-a933-af28efaf3b3e',
                'isActive': false,
                'balance': 3743.48,
                'age': 26,
                'name': 'Blanchard Knapp',
                'gender': 'male',
                'company': 'ACRODANCE',
                'email': 'blanchardknapp@acrodance.com',
                'phone': '+1 (867) 542-2772',
                'address': '707 Malta Street, Yukon, Wyoming, 6861',
                'bd': '2014-05-28T01:33:58 -06:00'
            },
            {
                '_id': '5a7b73f78988bd6e92650473',
                'index': 5,
                'guid': '08cb947c-e49c-4736-9687-0fca0992ec38',
                'isActive': false,
                'balance': 3453.79,
                'age': 34,
                'name': 'Parker Rivas',
                'gender': 'male',
                'company': 'SLAMBDA',
                'email': 'parkerrivas@slambda.com',
                'phone': '+1 (997) 413-2418',
                'address': '543 Roosevelt Place, Tibbie, Minnesota, 6944',
                'bd': '2015-01-05T09:55:23 -06:00'
            },
            {
                '_id': '5a7b73f72488770f90649570',
                'index': 6,
                'guid': '771c85d5-7762-4bae-96fd-09892a9c4374',
                'isActive': false,
                'balance': 3334.73,
                'age': 20,
                'name': 'Alexandria Forbes',
                'gender': 'female',
                'company': 'EQUITOX',
                'email': 'alexandriaforbes@equitox.com',
                'phone': '+1 (869) 521-2533',
                'address': '663 Minna Street, Omar, Alabama, 5265',
                'bd': '2017-03-09T05:48:57 -06:00'
            },
            {
                '_id': '5a7b73f7c576e368b321a705',
                'index': 7,
                'guid': '2455a7ef-a537-46e1-a210-75e5e2187460',
                'isActive': false,
                'balance': 3488.64,
                'age': 37,
                'name': 'Lessie Wise',
                'gender': 'female',
                'company': 'AFFLUEX',
                'email': 'lessiewise@affluex.com',
                'phone': '+1 (820) 404-2967',
                'address': '752 Woodhull Street, Utting, Oklahoma, 2739',
                'bd': '2014-10-21T03:09:34 -06:00'
            },
            {
                '_id': '5a7b73f705f8a9c6e35c8ca2',
                'index': 8,
                'guid': 'a90d65a8-681d-462f-bf08-eceeef366375',
                'isActive': true,
                'balance': 3786.67,
                'age': 36,
                'name': 'Carrie Gates',
                'gender': 'female',
                'company': 'VIRVA',
                'email': 'carriegates@virva.com',
                'phone': '+1 (845) 463-3986',
                'address': '561 Boulevard Court, Rote, Louisiana, 8458',
                'bd': '2017-03-30T02:06:23 -06:00'
            },
            {
                '_id': '5a7b73f7a3e2be2dbb7b093e',
                'index': 9,
                'guid': 'fb3d0f97-91ae-4336-b0b4-19f4a00fe567',
                'isActive': false,
                'balance': 3335.5,
                'age': 33,
                'name': 'Dalton Spears',
                'gender': 'male',
                'company': 'MIRACLIS',
                'email': 'daltonspears@miraclis.com',
                'phone': '+1 (919) 541-3528',
                'address': '167 Lester Court, Glasgow, Arkansas, 6311',
                'bd': '2017-04-01T01:41:12 -06:00'
            },
            {
                '_id': '5a7b73f716de69a9217c1273',
                'index': 10,
                'guid': '129a92fd-848f-48eb-98a1-aebf6e92b079',
                'isActive': false,
                'balance': 3811.15,
                'age': 30,
                'name': 'Delia Merrill',
                'gender': 'female',
                'company': 'COMTEST',
                'email': 'deliamerrill@comtest.com',
                'phone': '+1 (879) 401-2304',
                'address': '761 Polhemus Place, Kidder, Puerto Rico, 5901',
                'bd': '2014-08-29T08:42:59 -06:00'
            },
            {
                '_id': '5a7b73f7ed19007bed2d29fb',
                'index': 11,
                'guid': 'd799b69a-192d-4ee3-9a69-9e8e5afc45b0',
                'isActive': false,
                'balance': 3935.82,
                'age': 28,
                'name': 'Vance Aguilar',
                'gender': 'male',
                'company': 'CYCLONICA',
                'email': 'vanceaguilar@cyclonica.com',
                'phone': '+1 (972) 549-2681',
                'address': '653 Billings Place, Gardners, Connecticut, 7805',
                'bd': '2015-02-21T03:06:14 -06:00'
            },
            {
                '_id': '5a7b73f78d0dc0858a70c44a',
                'index': 12,
                'guid': '8cbb37bb-7644-4993-b48b-df3a69deb339',
                'isActive': true,
                'balance': 3868.95,
                'age': 28,
                'name': 'Adams Harper',
                'gender': 'male',
                'company': 'NORSUP',
                'email': 'adamsharper@norsup.com',
                'phone': '+1 (824) 494-3395',
                'address': '571 Turner Place, Norris, Mississippi, 3829',
                'bd': '2014-01-30T02:05:53 -06:00'
            },
            {
                '_id': '5a7b73f7e929494a8568a885',
                'index': 13,
                'guid': '22ec32d7-0ba9-4366-b6d8-ca16389a2cd9',
                'isActive': false,
                'balance': 3954.41,
                'age': 34,
                'name': 'Bass Sexton',
                'gender': 'male',
                'company': 'CIRCUM',
                'email': 'basssexton@circum.com',
                'phone': '+1 (930) 476-3634',
                'address': '563 Victor Road, Richmond, Kansas, 7742',
                'bd': '2014-05-04T10:16:32 -06:00'
            },
            {
                '_id': '5a7b73f767e97ce3136444fd',
                'index': 14,
                'guid': '031d282f-0be9-49e1-a211-9aa59d449d91',
                'isActive': false,
                'balance': 3287.33,
                'age': 24,
                'name': 'Howard Velez',
                'gender': 'male',
                'company': 'ECOSYS',
                'email': 'howardvelez@ecosys.com',
                'phone': '+1 (920) 556-2885',
                'address': '378 Grimes Road, Websterville, Marshall Islands, 3506',
                'bd': '2015-12-19T08:17:58 -06:00'
            },
            {
                '_id': '5a7b73f7fba076653cc18925',
                'index': 15,
                'guid': 'd76ab6d6-d1db-4286-8516-ce6c9db3972a',
                'isActive': false,
                'balance': 3279.98,
                'age': 21,
                'name': 'Lola Morton',
                'gender': 'female',
                'company': 'PROVIDCO',
                'email': 'lolamorton@providco.com',
                'phone': '+1 (963) 458-2788',
                'address': '991 Ashland Place, Richville, New York, 3529',
                'bd': '2016-11-29T07:58:24 -06:00'
            },
            {
                '_id': '5a7b73f7c6d408bc853be87c',
                'index': 16,
                'guid': '30c2d1c7-770b-4adb-b6df-cc205d748323',
                'isActive': false,
                'balance': 3955.55,
                'age': 37,
                'name': 'Bishop Rutledge',
                'gender': 'male',
                'company': 'DAYCORE',
                'email': 'bishoprutledge@daycore.com',
                'phone': '+1 (886) 539-3156',
                'address': '870 Vanderveer Place, Bridgetown, California, 7593',
                'bd': '2014-11-10T04:47:00 -06:00'
            },
            {
                '_id': '5a7b73f7abe6c78719d2f494',
                'index': 17,
                'guid': '2d8e77a1-4a88-4642-b6a8-693de296661c',
                'isActive': true,
                'balance': 1832.83,
                'age': 23,
                'name': 'Lea Reese',
                'gender': 'female',
                'company': 'GLUID',
                'email': 'leareese@gluid.com',
                'phone': '+1 (866) 413-2199',
                'address': '811 Dunne Place, Vowinckel, Rhode Island, 8646',
                'bd': '2014-03-16T04:30:06 -06:00'
            },
            {
                '_id': '5a7b73f72d64af126b8080be',
                'index': 18,
                'guid': 'e1e8ee63-6d08-48fc-a077-2265cee34f23',
                'isActive': true,
                'balance': 2419.18,
                'age': 23,
                'name': 'Knox Moses',
                'gender': 'male',
                'company': 'BRAINCLIP',
                'email': 'knoxmoses@brainclip.com',
                'phone': '+1 (982) 519-2486',
                'address': '917 Turnbull Avenue, Shasta, Virgin Islands, 7016',
                'bd': '2015-11-09T10:11:15 -06:00'
            },
            {
                '_id': '5a7b73f789b4e9086d34b255',
                'index': 19,
                'guid': '13552b7d-928c-4b92-a2ae-5ccbee807594',
                'isActive': false,
                'balance': 1220.91,
                'age': 22,
                'name': 'Marsha Jacobs',
                'gender': 'female',
                'company': 'COMSTAR',
                'email': 'marshajacobs@comstar.com',
                'phone': '+1 (858) 511-2546',
                'address': '580 Hampton Avenue, Ilchester, New Hampshire, 2191',
                'bd': '2016-02-11T01:34:23 -06:00'
            },
            {
                '_id': '5a7b73f737eea8e94089b7b4',
                'index': 20,
                'guid': 'cf577c87-b40c-4c09-9fac-d04c9a824b86',
                'isActive': false,
                'balance': 2446.07,
                'age': 25,
                'name': 'Bell Emerson',
                'gender': 'male',
                'company': 'MULTIFLEX',
                'email': 'bellemerson@multiflex.com',
                'phone': '+1 (806) 496-2473',
                'address': '238 Oxford Walk, Monument, New Mexico, 1345',
                'bd': '2016-10-07T01:07:21 -06:00'
            },
            {
                '_id': '5a7b73f76bc821dc6ee56ee2',
                'index': 21,
                'guid': 'b6c685c2-a497-4261-9217-622723d5235f',
                'isActive': false,
                'balance': 3694.63,
                'age': 33,
                'name': 'Cecelia Graham',
                'gender': 'female',
                'company': 'ZOXY',
                'email': 'ceceliagraham@zoxy.com',
                'phone': '+1 (933) 429-3129',
                'address': '954 Lawton Street, Terlingua, New Jersey, 6723',
                'bd': '2017-12-01T04:36:13 -06:00'
            },
            {
                '_id': '5a7b73f794c27c4048290cbf',
                'index': 22,
                'guid': '7e887403-8ff5-41b4-9902-bb63ff714fee',
                'isActive': true,
                'balance': 2804.02,
                'age': 29,
                'name': 'Anthony Pennington',
                'gender': 'male',
                'company': 'NAMEGEN',
                'email': 'anthonypennington@namegen.com',
                'phone': '+1 (860) 458-3988',
                'address': '287 Auburn Place, Gardiner, Northern Mariana Islands, 7131',
                'bd': '2018-02-04T11:06:51 -06:00'
            },
            {
                '_id': '5a7b73f720a5781f7d19597a',
                'index': 23,
                'guid': '9e108687-e1ca-4385-bdd5-62ab006f8aa3',
                'isActive': true,
                'balance': 1984.1,
                'age': 36,
                'name': 'Mayo Justice',
                'gender': 'male',
                'company': 'SLOFAST',
                'email': 'mayojustice@slofast.com',
                'phone': '+1 (854) 428-2270',
                'address': '648 Melba Court, Dodge, Pennsylvania, 7596',
                'bd': '2016-12-29T07:28:10 -06:00'
            },
            {
                '_id': '5a7b73f7f0a4c5e6c9807fb2',
                'index': 24,
                'guid': '93b0b383-dd69-4453-be26-f13ae361ce67',
                'isActive': true,
                'balance': 1845.13,
                'age': 22,
                'name': 'Vaughn Salazar',
                'gender': 'male',
                'company': 'ZAGGLE',
                'email': 'vaughnsalazar@zaggle.com',
                'phone': '+1 (986) 415-3294',
                'address': '382 Dewitt Avenue, Goodville, Palau, 711',
                'bd': '2014-10-31T12:32:59 -06:00'
            },
            {
                '_id': '5a7b73f7e6c45298c709371c',
                'index': 25,
                'guid': '5a059bbb-3f6d-47bc-ba2b-c13eeaaa93b4',
                'isActive': false,
                'balance': 3684.79,
                'age': 31,
                'name': 'Calhoun Bradshaw',
                'gender': 'male',
                'company': 'OVERPLEX',
                'email': 'calhounbradshaw@overplex.com',
                'phone': '+1 (964) 594-2363',
                'address': '527 Seton Place, Wedgewood, Wisconsin, 8306',
                'bd': '2016-05-27T10:46:17 -06:00'
            },
            {
                '_id': '5a7b73f79468759d25ecdcf4',
                'index': 26,
                'guid': '68d7f78e-5001-480b-a67d-72b370a5c2de',
                'isActive': false,
                'balance': 1831.14,
                'age': 29,
                'name': 'Dianne Bauer',
                'gender': 'female',
                'company': 'XUMONK',
                'email': 'diannebauer@xumonk.com',
                'phone': '+1 (866) 510-2479',
                'address': '540 Moffat Street, Emison, South Carolina, 7329',
                'bd': '2014-09-02T04:57:23 -06:00'
            },
            {
                '_id': '5a7b73f7346b1bbab11524fa',
                'index': 27,
                'guid': '0729eef8-36c5-4aa2-8e31-f5e2ca19b94b',
                'isActive': false,
                'balance': 1719.77,
                'age': 22,
                'name': 'Hebert Bryan',
                'gender': 'male',
                'company': 'COMTRAIL',
                'email': 'hebertbryan@comtrail.com',
                'phone': '+1 (838) 579-3709',
                'address': '669 Hausman Street, Gerber, Kentucky, 7779',
                'bd': '2017-11-29T12:22:59 -06:00'
            },
            {
                '_id': '5a7b73f75116874002de08de',
                'index': 28,
                'guid': '63014b40-3f1e-40ff-b2f7-f55ef6a5a599',
                'isActive': true,
                'balance': 1973.27,
                'age': 20,
                'name': 'Cash Bean',
                'gender': 'male',
                'company': 'SUPREMIA',
                'email': 'cashbean@supremia.com',
                'phone': '+1 (846) 551-2291',
                'address': '152 Garnet Street, Boling, Nevada, 4867',
                'bd': '2014-01-06T10:18:37 -06:00'
            },
            {
                '_id': '5a7b73f739be4dc1f743993c',
                'index': 29,
                'guid': 'ae498760-b43b-4c9c-8575-820f419984f6',
                'isActive': true,
                'balance': 2118.14,
                'age': 36,
                'name': 'Candy Hopper',
                'gender': 'female',
                'company': 'ACCUFARM',
                'email': 'candyhopper@accufarm.com',
                'phone': '+1 (841) 425-2442',
                'address': '695 Nassau Avenue, Nutrioso, Maryland, 2026',
                'bd': '2016-01-03T02:15:56 -06:00'
            },
            {
                '_id': '5a7b73f70b86f2969d762be2',
                'index': 30,
                'guid': 'f19cb86e-ab4f-4d07-833a-4adb8a19d0af',
                'isActive': false,
                'balance': 3794.89,
                'age': 37,
                'name': 'Fisher Powell',
                'gender': 'male',
                'company': 'ENOMEN',
                'email': 'fisherpowell@enomen.com',
                'phone': '+1 (876) 562-2932',
                'address': '616 Tapscott Avenue, Crucible, Nebraska, 4900',
                'bd': '2018-01-31T05:15:13 -06:00'
            },
            {
                '_id': '5a7b73f7394648a68c2a6ae3',
                'index': 31,
                'guid': 'a88e5389-0b07-4d19-ac6c-718ce9e0de55',
                'isActive': false,
                'balance': 3343.45,
                'age': 38,
                'name': 'Rosemary Sloan',
                'gender': 'female',
                'company': 'PHORMULA',
                'email': 'rosemarysloan@phormula.com',
                'phone': '+1 (924) 517-3289',
                'address': '687 Navy Walk, Edmund, Delaware, 1419',
                'bd': '2018-01-23T11:32:25 -06:00'
            },
            {
                '_id': '5a7b73f77ad97f4e1c2fa65a',
                'index': 32,
                'guid': 'fb915568-2875-49b3-96d7-6b54b2b186a1',
                'isActive': true,
                'balance': 2680.62,
                'age': 30,
                'name': 'Elba Glover',
                'gender': 'female',
                'company': 'APPLICA',
                'email': 'elbaglover@applica.com',
                'phone': '+1 (857) 495-3565',
                'address': '279 Bridgewater Street, Edneyville, Utah, 9246',
                'bd': '2015-10-03T12:24:56 -06:00'
            },
            {
                '_id': '5a7b73f72598106a97fbf7d5',
                'index': 33,
                'guid': 'fac3cd4b-2d42-4b4f-9d6f-0bac689bd47b',
                'isActive': false,
                'balance': 3286.46,
                'age': 37,
                'name': 'Mildred Short',
                'gender': 'female',
                'company': 'NIXELT',
                'email': 'mildredshort@nixelt.com',
                'phone': '+1 (980) 530-3588',
                'address': '434 Elm Place, Coloma, West Virginia, 1990',
                'bd': '2016-03-22T10:13:26 -06:00'
            },
            {
                '_id': '5a7b73f7b88290b05f53faa1',
                'index': 34,
                'guid': 'b1c6a3a3-00bd-4bc6-87df-69eecd909ab5',
                'isActive': false,
                'balance': 1484.16,
                'age': 24,
                'name': 'Karin Schultz',
                'gender': 'female',
                'company': 'PLASMOS',
                'email': 'karinschultz@plasmos.com',
                'phone': '+1 (904) 544-2796',
                'address': '380 Rockaway Avenue, Faxon, American Samoa, 5776',
                'bd': '2016-03-27T09:30:36 -06:00'
            },
            {
                '_id': '5a7b73f7d2f7429d0caec5fe',
                'index': 35,
                'guid': '62c961ac-49b1-4a69-b4bf-13a396ec4fd9',
                'isActive': false,
                'balance': 3450.17,
                'age': 23,
                'name': 'Addie Rose',
                'gender': 'female',
                'company': 'XYQAG',
                'email': 'addierose@xyqag.com',
                'phone': '+1 (838) 549-3147',
                'address': '999 Coleridge Street, Golconda, Vermont, 9575',
                'bd': '2016-10-01T06:50:42 -06:00'
            },
            {
                '_id': '5a7b73f78a4c54ff8334e053',
                'index': 36,
                'guid': '4f2f7ae5-0bd1-4665-b97f-c556e5162349',
                'isActive': false,
                'balance': 1797.89,
                'age': 23,
                'name': 'Janie Ellison',
                'gender': 'female',
                'company': 'SPLINX',
                'email': 'janieellison@splinx.com',
                'phone': '+1 (947) 460-2254',
                'address': '114 Landis Court, Genoa, Indiana, 5198',
                'bd': '2017-07-28T12:45:44 -06:00'
            },
            {
                '_id': '5a7b73f7c87f7e86fcb00055',
                'index': 37,
                'guid': 'b7236378-8129-44b5-bcc6-0369290ffad6',
                'isActive': false,
                'balance': 3776.51,
                'age': 38,
                'name': 'Elisabeth Campbell',
                'gender': 'female',
                'company': 'GOKO',
                'email': 'elisabethcampbell@goko.com',
                'phone': '+1 (849) 430-3377',
                'address': '832 Kermit Place, Lutsen, Georgia, 9145',
                'bd': '2015-04-26T06:40:08 -06:00'
            },
            {
                '_id': '5a7b73f712f9208f145fa6ea',
                'index': 38,
                'guid': '5c955e3a-5f3a-4ead-96ee-80a5de6dc479',
                'isActive': true,
                'balance': 3794.93,
                'age': 27,
                'name': 'Noble Holland',
                'gender': 'male',
                'company': 'NUTRALAB',
                'email': 'nobleholland@nutralab.com',
                'phone': '+1 (888) 573-3730',
                'address': '408 Roosevelt Court, Hiwasse, North Dakota, 281',
                'bd': '2014-03-25T12:24:34 -06:00'
            },
            {
                '_id': '5a7b73f7aa1f371de59df90b',
                'index': 39,
                'guid': '94698a81-61a6-4e23-a952-76a50fba71ef',
                'isActive': true,
                'balance': 2205.55,
                'age': 35,
                'name': 'Laverne Brock',
                'gender': 'female',
                'company': 'ICOLOGY',
                'email': 'lavernebrock@icology.com',
                'phone': '+1 (821) 600-3174',
                'address': '391 Conover Street, Cassel, Tennessee, 6566',
                'bd': '2016-01-27T09:40:41 -06:00'
            },
            {
                '_id': '5a7b73f7c45c697931199945',
                'index': 40,
                'guid': 'a05a215f-be1c-49d1-89ca-c821b118f923',
                'isActive': true,
                'balance': 2397.12,
                'age': 29,
                'name': 'Irene Frost',
                'gender': 'female',
                'company': 'RODEMCO',
                'email': 'irenefrost@rodemco.com',
                'phone': '+1 (918) 539-2612',
                'address': '401 Moore Place, Groton, Arizona, 3415',
                'bd': '2017-09-14T09:46:55 -06:00'
            },
            {
                '_id': '5a7b73f7ef55416e92ebc818',
                'index': 41,
                'guid': '1ae8ceac-e8d0-4417-9f6f-04cd4e4738ad',
                'isActive': false,
                'balance': 3335.51,
                'age': 35,
                'name': 'Beard Hendricks',
                'gender': 'male',
                'company': 'QUONK',
                'email': 'beardhendricks@quonk.com',
                'phone': '+1 (847) 521-3952',
                'address': '576 Bayard Street, Chloride, Federated States Of Micronesia, 8070',
                'bd': '2016-11-01T12:47:26 -06:00'
            },
            {
                '_id': '5a7b73f7cbeecfe6febd672d',
                'index': 42,
                'guid': 'afdf3298-77bd-46b3-ae8d-232f815c5f01',
                'isActive': false,
                'balance': 2205.01,
                'age': 37,
                'name': 'Nelson Shields',
                'gender': 'male',
                'company': 'ARTWORLDS',
                'email': 'nelsonshields@artworlds.com',
                'phone': '+1 (956) 534-3050',
                'address': '581 Maple Street, Needmore, Colorado, 2062',
                'bd': '2014-07-21T08:22:01 -06:00'
            },
            {
                '_id': '5a7b73f71803de25c5f754ad',
                'index': 43,
                'guid': '5b872cad-4388-496b-8ede-5f86990dec00',
                'isActive': true,
                'balance': 1001.05,
                'age': 21,
                'name': 'Luella Duffy',
                'gender': 'female',
                'company': 'KROG',
                'email': 'luelladuffy@krog.com',
                'phone': '+1 (973) 451-2222',
                'address': '349 Bryant Street, Tioga, South Dakota, 6493',
                'bd': '2016-04-27T02:46:46 -06:00'
            },
            {
                '_id': '5a7b73f77f2a05eacb331c74',
                'index': 44,
                'guid': '7d6b7650-10d7-435d-87ca-33a1fe12cd57',
                'isActive': false,
                'balance': 1926.79,
                'age': 27,
                'name': 'Rosa Guthrie',
                'gender': 'female',
                'company': 'COMTOURS',
                'email': 'rosaguthrie@comtours.com',
                'phone': '+1 (814) 528-2701',
                'address': '719 Kathleen Court, Morriston, Guam, 4011',
                'bd': '2015-07-02T08:22:18 -06:00'
            },
            {
                '_id': '5a7b73f7727afbb0fc15653b',
                'index': 45,
                'guid': 'ebbc985b-227e-4954-a8a6-588b2a2bff22',
                'isActive': false,
                'balance': 2464.9,
                'age': 29,
                'name': 'Dillard Carlson',
                'gender': 'male',
                'company': 'COMCUR',
                'email': 'dillardcarlson@comcur.com',
                'phone': '+1 (847) 469-3741',
                'address': '918 Oceanic Avenue, Cochranville, Missouri, 1018',
                'bd': '2016-06-11T11:31:54 -06:00'
            },
            {
                '_id': '5a7b73f71dd7612e967e01ae',
                'index': 46,
                'guid': '63a2ee7f-2141-4ec5-b1e2-fcdcd62f28ed',
                'isActive': false,
                'balance': 3917.74,
                'age': 25,
                'name': 'Faye Walls',
                'gender': 'female',
                'company': 'EMERGENT',
                'email': 'fayewalls@emergent.com',
                'phone': '+1 (964) 527-3791',
                'address': '947 Judge Street, Nescatunga, Maine, 4928',
                'bd': '2014-06-23T12:46:21 -06:00'
            },
            {
                '_id': '5a7b73f7b33c73c425db7ee0',
                'index': 47,
                'guid': '61d40a89-af0c-40ca-8970-c54978134e6b',
                'isActive': true,
                'balance': 2213.18,
                'age': 32,
                'name': 'Norma Hooper',
                'gender': 'female',
                'company': 'PARCOE',
                'email': 'normahooper@parcoe.com',
                'phone': '+1 (827) 503-2742',
                'address': '470 Fenimore Street, Hatteras, Texas, 1582',
                'bd': '2015-01-15T12:22:00 -06:00'
            },
            {
                '_id': '5a7b73f7c30aa4064670cf21',
                'index': 48,
                'guid': '969d77af-b251-4924-82cf-7c787752161d',
                'isActive': false,
                'balance': 3673.94,
                'age': 23,
                'name': 'Lee Wiggins',
                'gender': 'female',
                'company': 'NITRACYR',
                'email': 'leewiggins@nitracyr.com',
                'phone': '+1 (941) 478-3536',
                'address': '958 Flatbush Avenue, Clara, North Carolina, 970',
                'bd': '2018-01-09T11:09:34 -06:00'
            },
            {
                '_id': '5a7b73f7ecd5a4859f2d94dc',
                'index': 49,
                'guid': 'cdf9b8de-a309-4cb7-80bb-f1b830b8b640',
                'isActive': true,
                'balance': 2166.21,
                'age': 27,
                'name': 'Alvarez Lynch',
                'gender': 'male',
                'company': 'KIGGLE',
                'email': 'alvarezlynch@kiggle.com',
                'phone': '+1 (929) 528-3805',
                'address': '901 Stratford Road, Derwood, Iowa, 1402',
                'bd': '2015-01-08T04:28:57 -06:00'
            },
            {
                '_id': '5a7b73f7216c8cabc849eea7',
                'index': 50,
                'guid': 'c4175d6a-1560-468e-b682-701c1549b6b1',
                'isActive': false,
                'balance': 3479.39,
                'age': 39,
                'name': 'Oneal Rosario',
                'gender': 'male',
                'company': 'UBERLUX',
                'email': 'onealrosario@uberlux.com',
                'phone': '+1 (951) 572-3027',
                'address': '267 Rockaway Parkway, Chapin, Montana, 7813',
                'bd': '2014-02-10T05:08:13 -06:00'
            },
            {
                '_id': '5a7b73f78841719bf955b2d9',
                'index': 51,
                'guid': '966c9ce6-9151-47cb-8c71-98c4cd0d2f40',
                'isActive': false,
                'balance': 1625.49,
                'age': 36,
                'name': 'Olsen Stevens',
                'gender': 'male',
                'company': 'EMPIRICA',
                'email': 'olsenstevens@empirica.com',
                'phone': '+1 (871) 403-3377',
                'address': '704 Lamont Court, Saranap, Massachusetts, 3171',
                'bd': '2014-09-17T05:13:13 -06:00'
            },
            {
                '_id': '5a7b73f7b7b8e578dff0f85c',
                'index': 52,
                'guid': '8269a34f-3a02-47d6-bcb1-8f076bb478f0',
                'isActive': true,
                'balance': 1143.73,
                'age': 27,
                'name': 'Marian Henson',
                'gender': 'female',
                'company': 'ENDIPINE',
                'email': 'marianhenson@endipine.com',
                'phone': '+1 (995) 406-2592',
                'address': '803 Ellery Street, Boykin, Alaska, 8624',
                'bd': '2016-08-28T01:22:51 -06:00'
            },
            {
                '_id': '5a7b73f737459ec79c91ca75',
                'index': 53,
                'guid': 'badb9342-10fd-4520-ae66-c246e47add8f',
                'isActive': false,
                'balance': 1458.01,
                'age': 23,
                'name': 'Dudley Dickson',
                'gender': 'male',
                'company': 'POLARIA',
                'email': 'dudleydickson@polaria.com',
                'phone': '+1 (860) 428-3250',
                'address': '833 Revere Place, Rockbridge, Illinois, 4628',
                'bd': '2017-01-19T12:36:59 -06:00'
            },
            {
                '_id': '5a7b73f70ddc6fc11ebf043a',
                'index': 54,
                'guid': '52b1be89-8186-4685-81b7-203c17ed9f89',
                'isActive': true,
                'balance': 2815.76,
                'age': 25,
                'name': 'Earnestine Oneil',
                'gender': 'female',
                'company': 'CYTREK',
                'email': 'earnestineoneil@cytrek.com',
                'phone': '+1 (879) 541-3490',
                'address': '442 Emerald Street, Graniteville, Hawaii, 1302',
                'bd': '2017-07-07T10:34:33 -06:00'
            },
            {
                '_id': '5a7b73f78b816185ccd2b4b3',
                'index': 55,
                'guid': 'e66850ea-546b-4eb5-ae76-d66b0e727f44',
                'isActive': true,
                'balance': 3645.09,
                'age': 21,
                'name': 'Nicholson Mason',
                'gender': 'male',
                'company': 'TELEQUIET',
                'email': 'nicholsonmason@telequiet.com',
                'phone': '+1 (861) 528-3215',
                'address': '261 Aitken Place, Cecilia, Ohio, 1381',
                'bd': '2016-03-20T08:31:34 -06:00'
            },
            {
                '_id': '5a7b73f780f8bf8fbe24d75c',
                'index': 56,
                'guid': '40b999cd-00bf-46e0-9107-b44906d832e0',
                'isActive': false,
                'balance': 2477.66,
                'age': 36,
                'name': 'Linda Shaffer',
                'gender': 'female',
                'company': 'ZORK',
                'email': 'lindashaffer@zork.com',
                'phone': '+1 (828) 524-3011',
                'address': '350 Plymouth Street, Waterford, Washington, 6715',
                'bd': '2017-07-09T05:51:11 -06:00'
            },
            {
                '_id': '5a7b73f741e22fc19ffa6952',
                'index': 57,
                'guid': 'cc2ac19d-7d67-4f60-973a-369160a9c377',
                'isActive': false,
                'balance': 2651.39,
                'age': 20,
                'name': 'Montoya Riggs',
                'gender': 'male',
                'company': 'MARKETOID',
                'email': 'montoyariggs@marketoid.com',
                'phone': '+1 (809) 562-3786',
                'address': '633 Monitor Street, Chicopee, District Of Columbia, 550',
                'bd': '2016-02-05T12:36:05 -06:00'
            },
            {
                '_id': '5a7b73f7de56ead40c26e69a',
                'index': 58,
                'guid': '6e0b06b8-1199-498c-8002-41f4972aa2d2',
                'isActive': false,
                'balance': 3463.92,
                'age': 28,
                'name': 'Walker Duran',
                'gender': 'male',
                'company': 'GEOFORM',
                'email': 'walkerduran@geoform.com',
                'phone': '+1 (868) 502-2553',
                'address': '550 Kensington Walk, Wyano, Virginia, 7703',
                'bd': '2017-08-18T12:39:37 -06:00'
            },
            {
                '_id': '5a7b73f70a04fe142269ea8d',
                'index': 59,
                'guid': 'c6733cd5-1e73-4317-b4bc-1a9e597581a4',
                'isActive': true,
                'balance': 3846.35,
                'age': 26,
                'name': 'Suzanne House',
                'gender': 'female',
                'company': 'SYBIXTEX',
                'email': 'suzannehouse@sybixtex.com',
                'phone': '+1 (892) 533-2739',
                'address': '367 Harwood Place, Twilight, Oregon, 9799',
                'bd': '2016-11-26T11:57:18 -06:00'
            },
            {
                '_id': '5a7b73f7339943d94af3b39d',
                'index': 60,
                'guid': '4ff2c2aa-0573-4be1-a1c8-f684af8a5fbf',
                'isActive': false,
                'balance': 2717.94,
                'age': 26,
                'name': 'Lewis Oconnor',
                'gender': 'male',
                'company': 'EXOZENT',
                'email': 'lewisoconnor@exozent.com',
                'phone': '+1 (954) 582-2660',
                'address': '717 Sutter Avenue, Bartley, Michigan, 1142',
                'bd': '2017-08-21T08:25:00 -06:00'
            },
            {
                '_id': '5a7b73f7d8e266ad1bc5daa8',
                'index': 61,
                'guid': '94667aad-86fc-4a2c-94fb-11b572307c75',
                'isActive': false,
                'balance': 2725.58,
                'age': 39,
                'name': 'Shelley Bonner',
                'gender': 'female',
                'company': 'INDEXIA',
                'email': 'shelleybonner@indexia.com',
                'phone': '+1 (965) 490-3768',
                'address': '896 Clinton Avenue, Canoochee, Idaho, 1154',
                'bd': '2016-04-11T06:08:29 -06:00'
            },
            {
                '_id': '5a7b73f7e74a5af674e4cbdd',
                'index': 62,
                'guid': 'ec68c47e-7cbd-485e-8d54-fab1bb6ea008',
                'isActive': true,
                'balance': 1343.87,
                'age': 29,
                'name': 'Mccall Morales',
                'gender': 'male',
                'company': 'QUILITY',
                'email': 'mccallmorales@quility.com',
                'phone': '+1 (939) 455-2610',
                'address': '325 Crystal Street, Harleigh, Wyoming, 5658',
                'bd': '2014-11-20T07:30:04 -06:00'
            },
            {
                '_id': '5a7b73f7efb231e53a0c94cd',
                'index': 63,
                'guid': '6a8b3f55-406c-4ae8-be59-94a0f8fbd180',
                'isActive': false,
                'balance': 1092.69,
                'age': 37,
                'name': 'Vera Mcpherson',
                'gender': 'female',
                'company': 'CIPROMOX',
                'email': 'veramcpherson@cipromox.com',
                'phone': '+1 (890) 500-3729',
                'address': '771 Beard Street, Rivera, Minnesota, 4726',
                'bd': '2017-07-13T02:47:50 -06:00'
            },
            {
                '_id': '5a7b73f7e345c5dfc5d636e4',
                'index': 64,
                'guid': '46879caf-76e6-46e0-9b8b-bc17667a81ea',
                'isActive': true,
                'balance': 2077.12,
                'age': 36,
                'name': 'Gregory Roth',
                'gender': 'male',
                'company': 'EARWAX',
                'email': 'gregoryroth@earwax.com',
                'phone': '+1 (806) 595-2477',
                'address': '349 Dunham Place, Sardis, Alabama, 3320',
                'bd': '2017-11-08T02:26:23 -06:00'
            },
            {
                '_id': '5a7b73f77f5f9d730fab11e0',
                'index': 65,
                'guid': '9cfb8f58-7acf-4a39-bf2b-c90269c33db0',
                'isActive': true,
                'balance': 3503.58,
                'age': 31,
                'name': 'Russell Carver',
                'gender': 'male',
                'company': 'PREMIANT',
                'email': 'russellcarver@premiant.com',
                'phone': '+1 (849) 521-2335',
                'address': '851 Noble Street, Holcombe, Oklahoma, 311',
                'bd': '2016-07-10T10:08:35 -06:00'
            },
            {
                '_id': '5a7b73f7cab10f461153989c',
                'index': 66,
                'guid': '2562a818-4451-4193-94cd-650d131ff097',
                'isActive': false,
                'balance': 1652.9,
                'age': 21,
                'name': 'Darlene Hurley',
                'gender': 'female',
                'company': 'STELAECOR',
                'email': 'darlenehurley@stelaecor.com',
                'phone': '+1 (868) 492-2270',
                'address': '627 Wilson Street, Loveland, Louisiana, 765',
                'bd': '2017-05-20T12:39:31 -06:00'
            },
            {
                '_id': '5a7b73f7ecccc997e4160a59',
                'index': 67,
                'guid': '0050170f-0283-481d-9633-dc9d134be121',
                'isActive': true,
                'balance': 3692.88,
                'age': 21,
                'name': 'Lela Bailey',
                'gender': 'female',
                'company': 'AQUOAVO',
                'email': 'lelabailey@aquoavo.com',
                'phone': '+1 (917) 449-2329',
                'address': '121 Adams Street, Malo, Arkansas, 7435',
                'bd': '2016-11-06T04:55:46 -06:00'
            }
        ];
    }
    return UserDB;
}());



/***/ }),

/***/ "./src/app/views/others/orders/displayallorders/displayallorders.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/views/others/orders/displayallorders/displayallorders.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-333\">\n  <!-- <span fxFlex></span> -->\n  <mat-form-field class=\"margin-333\" style=\"width: 100%\">\n    <input \n    matInput \n    placeholder=\"Type to search from all columns\" \n    value=\"\"\n    (keyup)='updateFilter($event)'>\n  </mat-form-field>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n  <mat-card-content class=\"p-0\">\n    <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"items\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\"\n          [limit]=\"10\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Jobcard No.\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.jobcard_no }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Car Registration No.\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.carReg_no }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Car Model\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.car_model }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Order date\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.order_date }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Order Status\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                \n                    {{ row?.order_status }}\n                 \n             <!-- <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'Complete' : 'Bidding'}}</mat-chip>\n      -->      </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n           <!--   <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" ><mat-icon>edit</mat-icon></button>\n              <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n            \n             [routerLink]=\"['orders/showorderparts', row.car_model]\" (click)= openOrder()   [routerLink]=\"['/orders/showorderparts', row.order_id]\"\n            -->\n            <div>\n                \n              <div   fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                  <button mat-raised-button color=\"primary\" (click)= openOrder(row.order_id)  >\n                      <mat-icon>open_with</mat-icon>\n                      OPEN\n                  </button>\n                   \n                    </div>  \n                  \n            </div>  \n            </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>\n  </mat-card-content>\n</mat-card>\n"

/***/ }),

/***/ "./src/app/views/others/orders/displayallorders/displayallorders.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/others/orders/displayallorders/displayallorders.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9vcmRlcnMvZGlzcGxheWFsbG9yZGVycy9kaXNwbGF5YWxsb3JkZXJzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/orders/displayallorders/displayallorders.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/others/orders/displayallorders/displayallorders.component.ts ***!
  \************************************************************************************/
/*! exports provided: DisplayallordersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DisplayallordersComponent", function() { return DisplayallordersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _tables_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../tables.service */ "./src/app/views/others/orders/tables.service.ts");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var app_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/services/socket.service */ "./src/app/shared/services/socket.service.ts");
/* harmony import */ var ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-navigation-with-data */ "./node_modules/ngx-navigation-with-data/fesm5/ngx-navigation-with-data.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var DisplayallordersComponent = /** @class */ (function () {
    function DisplayallordersComponent(dialog, snack, tableService, confirmService, loader, router, orderservice, socketService, navCtrl) {
        this.dialog = dialog;
        this.snack = snack;
        this.tableService = tableService;
        this.confirmService = confirmService;
        this.loader = loader;
        this.router = router;
        this.orderservice = orderservice;
        this.socketService = socketService;
        this.navCtrl = navCtrl;
        this.bids = [];
    }
    DisplayallordersComponent.prototype.ngOnInit = function () {
        this.getItems2();
        //this.initIoConnection()
    };
    DisplayallordersComponent.prototype.initIoConnection = function () {
        var _this = this;
        this.socketService.initSocket();
        this.ioConnection = this.socketService.onBid()
            .subscribe(function (bid) {
            _this.bids.push(bid);
        });
        /*
            this.socketService.onEvent(Event.CONNECT)
              .subscribe(() => {
                console.log('connected');
              });
        
            this.socketService.onEvent(Event.DISCONNECT)
              .subscribe(() => {
                console.log('disconnected');
              }); */
    };
    DisplayallordersComponent.prototype.acceptBid = function (message) {
        if (!message) {
            return;
        }
        /* this.socketService.send({
           from: this.user,
           content: message
         }); */
        //  this.messageContent = null;
    };
    DisplayallordersComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    DisplayallordersComponent.prototype.getItems2 = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            _this.orderservice.orders = data;
            _this.orderservice.setOrders(data);
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    DisplayallordersComponent.prototype.getItems = function () {
        var _this = this;
        /* this.getItemSub = this.orderservice.getOrders()
         //this.getItemSub = this.orderservice.postgetOrder()
             .subscribe(data => {
              this.items =this.temp = data;
             // console.log(data+"-------------------fghjklkjhgfdsdfghjkjhdsasdfghjhgfdsasdfghjk");
             })
           //..  console.log (JSON.stringify(this.items )+"----------------------------"+this.getItemSub);
          */
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            console.log(JSON.stringify(data) + "-------------------");
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    DisplayallordersComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var columns = Object.keys(this.temp[0]);
        // Removes last "$$index" from "column"
        columns.splice(columns.length - 1);
        // console.log(columns);
        if (!columns.length)
            return;
        var rows = this.temp.filter(function (d) {
            for (var i = 0; i <= columns.length; i++) {
                var column = columns[i];
                // console.log(d[column]);
                if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
                    return true;
                }
            }
        });
        this.items = rows;
    };
    //JSON.stringify(this.navCtrl.data)
    DisplayallordersComponent.prototype.openOrder = function (id) {
        //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
        //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
        var ite = this.items.find(function (x) { return x.order_id == id; });
        this.router.navigate(['orders/showorderparts', ite.order_id]);
    };
    /*  openPopUp(data: any = {}, isNew?) {
        let title = isNew ? 'Add new member' : 'Update member';
        let dialogRef: MatDialogRef<any> = this.dialog.open(NgxTablePopupComponent, {
          width: '720px',
          disableClose: true,
          data: { title: title, payload: data }
        })
        dialogRef.afterClosed()
          .subscribe(res => {
            if(!res) {
              // If user press cancel
              return;
            }
            this.loader.open();
            if (isNew) {
              this.crudService.addItem(res)
                .subscribe(data => {
                  this.items = data;
                  this.loader.close();
                  this.snack.open('Member Added!', 'OK', { duration: 4000 })
                })
            } else {
              this.crudService.updateItem(data._id, res)
                .subscribe(data => {
                  this.items = data;
                  this.loader.close();
                  this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                })
            }
          })
      }  */
    DisplayallordersComponent.prototype.deleteItem = function (row) {
        var _this = this;
        this.confirmService.confirm({ message: "Delete " + row.name + "?" })
            .subscribe(function (res) {
            if (res) {
                _this.loader.open();
                _this.tableService.removeItem(row)
                    .subscribe(function (data) {
                    _this.items = data;
                    _this.loader.close();
                    _this.snack.open('Member deleted!', 'OK', { duration: 4000 });
                });
            }
        });
    };
    DisplayallordersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-displayallorders',
            template: __webpack_require__(/*! ./displayallorders.component.html */ "./src/app/views/others/orders/displayallorders/displayallorders.component.html"),
            styles: [__webpack_require__(/*! ./displayallorders.component.scss */ "./src/app/views/others/orders/displayallorders/displayallorders.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"],
            providers: [_tables_service__WEBPACK_IMPORTED_MODULE_3__["TableService"], app_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_8__["SocketService"]]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            _tables_service__WEBPACK_IMPORTED_MODULE_3__["TableService"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__["AppLoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_7__["OrderService"],
            app_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_8__["SocketService"],
            ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_9__["NgxNavigationWithDataComponent"]])
    ], DisplayallordersComponent);
    return DisplayallordersComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/orders.module.ts":
/*!******************************************************!*\
  !*** ./src/app/views/others/orders/orders.module.ts ***!
  \******************************************************/
/*! exports provided: OrdersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersModule", function() { return OrdersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _displayallorders_displayallorders_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./displayallorders/displayallorders.component */ "./src/app/views/others/orders/displayallorders/displayallorders.component.ts");
/* harmony import */ var _showorderparts_showorderparts_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./showorderparts/showorderparts.component */ "./src/app/views/others/orders/showorderparts/showorderparts.component.ts");
/* harmony import */ var _orders_routing__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./orders.routing */ "./src/app/views/others/orders/orders.routing.ts");
/* harmony import */ var app_shared_shared_module__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _particularpart_particularpart_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./particularpart/particularpart.component */ "./src/app/views/others/orders/particularpart/particularpart.component.ts");
/* harmony import */ var _printpage_printpage_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./printpage/printpage.component */ "./src/app/views/others/orders/printpage/printpage.component.ts");
/* harmony import */ var ngx_print__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ngx-print */ "./node_modules/ngx-print/fesm5/ngx-print.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var OrdersModule = /** @class */ (function () {
    function OrdersModule() {
    }
    OrdersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__["PerfectScrollbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                ngx_print__WEBPACK_IMPORTED_MODULE_16__["NgxPrintModule"],
                // BrowserAnimationsModule,
                app_shared_shared_module__WEBPACK_IMPORTED_MODULE_13__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_orders_routing__WEBPACK_IMPORTED_MODULE_12__["OrdersRoutes"])
            ],
            declarations: [
                _displayallorders_displayallorders_component__WEBPACK_IMPORTED_MODULE_10__["DisplayallordersComponent"], _showorderparts_showorderparts_component__WEBPACK_IMPORTED_MODULE_11__["ShoworderpartsComponent"], _particularpart_particularpart_component__WEBPACK_IMPORTED_MODULE_14__["ParticularpartComponent"], _printpage_printpage_component__WEBPACK_IMPORTED_MODULE_15__["PrintpageComponent"]
            ]
        })
    ], OrdersModule);
    return OrdersModule;
}());



/***/ }),

/***/ "./src/app/views/others/orders/orders.routing.ts":
/*!*******************************************************!*\
  !*** ./src/app/views/others/orders/orders.routing.ts ***!
  \*******************************************************/
/*! exports provided: OrdersRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrdersRoutes", function() { return OrdersRoutes; });
/* harmony import */ var _displayallorders_displayallorders_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./displayallorders/displayallorders.component */ "./src/app/views/others/orders/displayallorders/displayallorders.component.ts");
/* harmony import */ var _showorderparts_showorderparts_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./showorderparts/showorderparts.component */ "./src/app/views/others/orders/showorderparts/showorderparts.component.ts");
/* harmony import */ var _particularpart_particularpart_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./particularpart/particularpart.component */ "./src/app/views/others/orders/particularpart/particularpart.component.ts");
/* harmony import */ var _printpage_printpage_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./printpage/printpage.component */ "./src/app/views/others/orders/printpage/printpage.component.ts");




var OrdersRoutes = [
    {
        path: '',
        component: _displayallorders_displayallorders_component__WEBPACK_IMPORTED_MODULE_0__["DisplayallordersComponent"],
        data: { title: 'Blank', breadcrumb: 'BLANK' }
    },
    {
        path: 'showorderparts/:id',
        component: _showorderparts_showorderparts_component__WEBPACK_IMPORTED_MODULE_1__["ShoworderpartsComponent"],
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    },
    {
        path: 'particularpart/:id',
        component: _particularpart_particularpart_component__WEBPACK_IMPORTED_MODULE_2__["ParticularpartComponent"],
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    },
    {
        path: 'printpage',
        component: _printpage_printpage_component__WEBPACK_IMPORTED_MODULE_3__["PrintpageComponent"],
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    }
];


/***/ }),

/***/ "./src/app/views/others/orders/particularpart/particularpart.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/particularpart.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<mat-sidenav-container>\n \n\n    <mat-sidenav  #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\" >\n        <!-- Compose button -->\n        <button mat-raised-button class=\"mat-warn full-width\" >{{Order.carReg_no}}</button>\n        <!-- left side buttons           -->\n        <div [perfectScrollbar] class=\"chat-sidebar-scroll\">\n        <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\n          <mat-list-item class=\"primary-imenu-item\"  routerLinkActive=\"open\" *ngFor=\"let item of Order.autorised_parts ; let i = index\" \n          [style.background]=\"selected==i?  '#e91e63':'black'\" (click)=\"selected=i\"    (click)= onclickpart(item.part_id) >\n            <a fxLayout=\"row\">\n                {{item.partQuantity}} &nbsp;\n                <span> {{item.partName}} </span>\n            </a>\n          \n            <mat-divider></mat-divider>\n          </mat-list-item>\n         \n          \n    \n        </mat-nav-list>\n      </div>\n      </mat-sidenav>\n  \n      <div class=\"messages-wrap\">\n<mat-card class=\"p-0\">\n  \n\n    <div>\n        <div  *ngIf=\"(selectedStatusreturn() === 'Closed' || selectedStatusreturn() === 'Complete') ; then thenBlock; else elseBlock\"> </div>\n        <ng-template #thenBlock>\n           \n\n \n            <mat-tab-group>\n                <!--  <div  *ngIf=\"getStatusreturn(row.bidPartId) === 'Closed' ; then thenBlock; else elseBlock\"> </div>\n                  \n                    -->\n            \n            \n                <mat-tab label=\"Bids\">\n                    <mat-card-content  >\n                     \n                                  <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n                            <mat-card-content class=\"p-0\">\n                              <ngx-datatable\n                                    class=\"material ml-0 mr-0\"\n                                    [rows]=\"items\"\n                                    [columnMode]=\"'flex'\"\n                                    [headerHeight]=\"50\"\n                                    [footerHeight]=\"50\"\n                                    [limit]=\"10\"\n                                    [rowHeight]=\"'auto'\">\n                                    <ngx-datatable-column name=\"Supplier Name\" [flexGrow]=\"1\">\n                                      <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                        {{ row?.bidSourceName }}\n                                      </ng-template>\n                                    </ngx-datatable-column>\n                                    <ngx-datatable-column name=\"Supplier Phone No.\" [flexGrow]=\"1\">\n                                      <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                        {{ row?.bidSupplierPhone }}\n                                      </ng-template>\n                                    </ngx-datatable-column>\n                                    <ngx-datatable-column name=\"Amount per Item\" [flexGrow]=\"1\">\n                                      <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                        {{ row?.bidAmount }}\n                                      </ng-template>\n                                    </ngx-datatable-column>\n                                    <ngx-datatable-column name=\"Supplier Status\" [flexGrow]=\"1\">\n                                      <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                        {{ row?.bidSupplyStatus }}\n                                      </ng-template>\n                                    </ngx-datatable-column>\n                                 <!--  <ngx-datatable-column name=\"Order Status\" [flexGrow]=\"1\">\n                                      <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                        <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'Complete' : 'Bidding'}}</mat-chip>\n                                      </ng-template>  \n                                    </ngx-datatable-column>--> \n                                    <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n                                      <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                     <!--   <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" ><mat-icon>edit</mat-icon></button>\n                                        <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n                                      \n                                       [routerLink]=\"['orders/showorderparts', row.car_model]\" (click)= openOrder()   [routerLink]=\"['/orders/showorderparts', row.order_id]\"\n                                    *ngIf=\"row.bidPartId   row.part_status  -->\n                                      <div>\n                                          <div  *ngIf=\"getStatusreturn(row.bidPartId) === 'Closed' ; then thenBlock; else elseBlock\"> </div>\n                                          <ng-template #thenBlock>\n                                              <div fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                                                  <!--   <button  mat-raised-button color=\"primary\" (click)= openOrder(row.order_id) style=\" \n                                                      \n                                                      min-height: 23px !important;\n                                                      min-width: 46px !important;\n                                                      font-size: 12px !important;\n                                                      line-height: 0px; \n                                                \" >\n                                                          *ngIf=\"row.part_status === 'Bidding'\"  \n                                                        Award\n                                                      </button>   --> \n                                                      <mat-chip   \n                                class=\"icon-chip\" \n                                color=\"primary\" \n                                selected=\"true\"(click)=openDialogPurchase(row.bid_id,row.bidSupplierId,row.bidSourceName,row.bidAmount,row.bidPartId,row.bidOrderId) >Award</mat-chip>\n                                                     \n                                                     \n                                                      &nbsp;\n                                        \n                                                      <mat-chip\n                                                      class=\"icon-chip\" \n                                                      color=\"accent\" \n                                                      selected=\"true\" (click)=\"openDialogOffer(row.bid_id,row.bidSupplierId,row.bidSourceName,row.bidAmount,row.bidPartId,row.bidOrderId)\" >Offer</mat-chip>\n                                        \n                                                                                     \n                                                        </div>  \n                        \n                        \n                                                        \n                                            </ng-template>\n                                            <ng-template #elseBlock>\n                                                <div  fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                                                          \n                                                    <mat-chip   \n                              class=\"icon-chip\" \n                              color=\"primary\" \n                              selected=\"true\" >{{getStatusreturn(row.bidPartId)}} </mat-chip>\n                                                   \n                                                                        \n                                                      </div>  \n                                            </ng-template>\n              \n                                          \n                                                                 \n                                      </div>  \n                                      </ng-template>\n                                    </ngx-datatable-column>\n                                  </ngx-datatable>\n                            </mat-card-content>\n                          </mat-card>\n              \n              \n                        <!--\n                        <div class=\"tasktype-item\" *ngFor=\"let t of Order.order_parts\">\n                           \n                            <mat-list-item routerLink=\"/orders/particularpart\" routerLinkActive=\"list-item-active\">\n                                <span>{{t.partName}}</span>\n                                <span fxFlex></span>\n                                <mat-chip mat-sm-chip color=\"primary\" [selected]=\"t.status ? true : false\">{{t.status ? 'completed' : 'pending'}}</mat-chip>\n                                \n                            </mat-list-item>\n                          \n                        </div> -->\n                      \n                    </mat-card-content>\n                  </mat-tab>\n                  <mat-tab label=\"Purchase History\">\n                    <mat-card-content>\n                                          \n                      <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n                        <mat-card-content class=\"p-0\">\n                          <ngx-datatable\n                                class=\"material ml-0 mr-0\"\n                                [rows]=\"itemshistory\"\n                                [columnMode]=\"'flex'\"\n                                [headerHeight]=\"50\"\n                                [footerHeight]=\"50\"\n                                [limit]=\"10\"\n                                [rowHeight]=\"'auto'\">\n                                <ngx-datatable-column name=\"Supplier Name\" [flexGrow]=\"1\">\n                                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    {{ row?.won_supplier_name }}\n                                  </ng-template>\n                                </ngx-datatable-column>\n                                <ngx-datatable-column name=\"Phone No.\" [flexGrow]=\"1\">\n                                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    {{ row?.won_supplier_phone}}\n                                  </ng-template>\n                                </ngx-datatable-column>\n                                <ngx-datatable-column name=\"Amount\" [flexGrow]=\"1\">\n                                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                    {{ row?.part_sale_price }}\n                                  </ng-template>\n                                </ngx-datatable-column>\n                                \n                             \n                              </ngx-datatable>\n                        </mat-card-content>\n                      </mat-card>\n              \n                    </mat-card-content>\n                  </mat-tab>\n                  \n                   \n            \n                \n              </mat-tab-group>\n            \n\n\n                      \n          </ng-template>\n          <ng-template #elseBlock>\n           \n            \n              <mat-tab-group>\n                  <!--  <div  *ngIf=\"getStatusreturn(row.bidPartId) === 'Closed' ; then thenBlock; else elseBlock\"> </div>\n                    \n                      -->\n              \n              \n                  <mat-tab label=\"Bids\">\n                      <mat-card-content  >\n                       \n                                    <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n                              <mat-card-content class=\"p-0\">\n                                <ngx-datatable\n                                      class=\"material ml-0 mr-0\"\n                                      [rows]=\"items\"\n                                      [columnMode]=\"'flex'\"\n                                      [headerHeight]=\"50\"\n                                      [footerHeight]=\"50\"\n                                      [limit]=\"10\"\n                                      [rowHeight]=\"'auto'\">\n                                      <ngx-datatable-column name=\"Supplier Name\" [flexGrow]=\"1\">\n                                        <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                          {{ row?.bidSourceName }}\n                                        </ng-template>\n                                      </ngx-datatable-column>\n                                      <ngx-datatable-column name=\"Supplier Phone No.\" [flexGrow]=\"1\">\n                                        <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                          {{ row?.bidSupplierPhone }}\n                                        </ng-template>\n                                      </ngx-datatable-column>\n                                    \n                                      <ngx-datatable-column name=\"Supplier Status\" [flexGrow]=\"1\">\n                                        <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                          {{ row?.bidSupplyStatus }}\n                                        </ng-template>\n                                      </ngx-datatable-column>\n                                  \n                                      <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n                                        <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                                      \n                                        <div>\n                                            <div  *ngIf=\"getStatusreturn(row.bidPartId) === 'Closed' ; then thenBlock; else elseBlock\"> </div>\n                                            <ng-template #thenBlock>\n                                                <div fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                                                  \n                                                        <mat-chip   \n                                  class=\"icon-chip\" \n                                  color=\"primary\" \n                                  selected=\"true\"(click)=openDialogPurchase(row.bid_id,row.bidSupplierId,row.bidSourceName,row.bidAmount,row.bidPartId,row.bidOrderId) >Award</mat-chip>\n                                                       \n                                                       \n                                                        &nbsp;\n                                          \n                                                        <mat-chip\n                                                        class=\"icon-chip\" \n                                                        color=\"accent\" \n                                                        selected=\"true\" (click)=\"openDialogOffer(row.bid_id,row.bidSupplierId,row.bidSourceName,row.bidAmount,row.bidPartId,row.bidOrderId)\" >Offer</mat-chip>\n                                          \n                                                                                       \n                                                          </div>  \n                          \n                          \n                                                          \n                                              </ng-template>\n                                              <ng-template #elseBlock>\n                                                  <div  fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                                                            \n                                                      <mat-chip   \n                                class=\"icon-chip\" \n                                color=\"primary\" \n                                selected=\"true\" >{{getStatusreturn(row.bidPartId)}} </mat-chip>\n                                                     \n                                                                          \n                                                        </div>  \n                                              </ng-template>\n                \n                                            \n                                                                   \n                                        </div>  \n                                        </ng-template>\n                                      </ngx-datatable-column>\n                                    </ngx-datatable>\n                              </mat-card-content>\n                            </mat-card>\n                \n                      </mat-card-content>\n              \n                    </mat-tab>\n                    <mat-tab disabled color=\"primary\" >\n                        <ng-template mat-tab-label>\n                            <button mat-raised-button color=\"primary\" (click)=\"closeBidding()\" >Close Bidding</button>\n                        </ng-template>\n                    </mat-tab>\n                                 \n                  \n                </mat-tab-group>\n              \n  \n\n          </ng-template>\n\n                         \n    </div>  \n\n\n\n\n\n \n</mat-card>\n\n</div>\n\n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/orders/particularpart/particularpart.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/particularpart.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".spacing {\n  flex: 1 1 auto; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy9vcmRlcnMvcGFydGljdWxhcnBhcnQvcGFydGljdWxhcnBhcnQuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxlQUFjLEVBQ2pCIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvb3RoZXJzL29yZGVycy9wYXJ0aWN1bGFycGFydC9wYXJ0aWN1bGFycGFydC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5zcGFjaW5nIHtcbiAgICBmbGV4OiAxIDEgYXV0bztcbn1cbiJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/orders/particularpart/particularpart.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/particularpart.component.ts ***!
  \********************************************************************************/
/*! exports provided: ParticularpartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ParticularpartComponent", function() { return ParticularpartComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ng2-file-upload */ "./node_modules/ng2-file-upload/index.js");
/* harmony import */ var ng2_file_upload__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _bidpurchase_dialog_bidpurchase_dialog_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bidpurchase-dialog/bidpurchase-dialog.component */ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.ts");
/* harmony import */ var _bidofferdialog_bidofferdialog_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./bidofferdialog/bidofferdialog.component */ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.ts");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ParticularpartComponent = /** @class */ (function () {
    function ParticularpartComponent(media, route, orderservice, dialog, snackBar) {
        this.media = media;
        this.route = route;
        this.orderservice = orderservice;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.isSidenavOpen = true;
        this.console = console;
        this.uploader = new ng2_file_upload__WEBPACK_IMPORTED_MODULE_1__["FileUploader"]({ url: 'upload_url' });
        this.hasBaseDropZoneOver = false;
        this.carsparesarray = { car_parts: [] }; //will be removed
    }
    ParticularpartComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.initc = true;
        //selected=0;
        this.inboxSideNavInit();
        this.productID = this.route.snapshot.params['id'];
        //console.log(this.productID ); 
        this.Order = this.orderservice.giveOrders().find(function (x) { return x.order_id == _this.productID; });
        //  this.Order = JSON.parse(this.productID);
        this.getbids(this.Order.autorised_parts[0].part_id);
        this.selectedid = this.Order.autorised_parts[0].part_id;
        // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,this.Order.order_parts[0].partName)
        //this.getbids("partid:string");
    };
    ParticularpartComponent.prototype.fileOverBase = function (e) {
        this.hasBaseDropZoneOver = e;
    };
    ParticularpartComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    ParticularpartComponent.prototype.onclickpart = function (id) {
        this.getbids(id);
        this.initc = false;
        var ptname = this.Order.autorised_parts.find(function (x) { return x.part_id == id; }).partName;
        this.selectedid = id;
        // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
    };
    ParticularpartComponent.prototype.isItwon = function (id) {
        var stat = this.Order.autorised_parts.find(function (x) { return x.part_id == id; }).part_status;
        if (stat === 'Complete') {
            return true;
        }
        else {
            return false;
        }
    };
    ///row.bidSourceName,row.bidAmount,row.bidPartId,row.bidOrderId
    ParticularpartComponent.prototype.openDialogPurchase = function (bid_id, bidSupplierId, bidSourceName, bidAmount, bidPartId, bidOrderId) {
        var _this = this;
        console.log(bidSourceName + bidAmount);
        var part = this.Order.autorised_parts.find(function (x) { return x.part_id == bidPartId; });
        var dialogRef = this.dialog.open(_bidpurchase_dialog_bidpurchase_dialog_component__WEBPACK_IMPORTED_MODULE_6__["BidpurchaseDialogComponent"], {
            width: '600px',
            data: {
                bid_id: bid_id,
                awardcompanyId: this.Order.order_sourceCompanyid,
                bidSupplierId: bidSupplierId,
                bidOrderId: bidOrderId,
                bidPartId: bidPartId,
                bidamount: bidAmount,
                carmake: this.Order.car_make,
                carmodel: this.Order.car_model,
                caryom: this.Order.car_yom,
                partname: part.partName,
                bidpriceper: bidAmount,
                bidtotalprice: part.partQuantity * bidAmount,
                suppliername: bidSourceName,
                comment: 'super cool',
                quantity: part.partQuantity
            }
        }).afterClosed()
            .subscribe(function (response) {
            _this.Order.autorised_parts.find(function (x) { return x.part_id == bidPartId; }).part_status = 'Complete';
            console.log(response);
        });
    };
    ParticularpartComponent.prototype.openDialogOffer = function (bid_id, bidSupplierId, bidSourceName, bidAmount, bidPartId, bidOrderId) {
        // console.log(bidSourceName+bidAmount);
        var part = this.Order.autorised_parts.find(function (x) { return x.part_id == bidPartId; });
        var dialogRef = this.dialog.open(_bidofferdialog_bidofferdialog_component__WEBPACK_IMPORTED_MODULE_7__["BidofferdialogComponent"], {
            width: '600px',
            data: {
                bid_id: bid_id,
                awardcompanyId: this.Order.order_sourceCompanyid,
                bidSupplierId: bidSupplierId,
                bidOrderId: bidOrderId,
                bidPartId: bidPartId,
                bidamount: bidAmount,
                carmake: this.Order.car_make,
                carmodel: this.Order.car_model,
                caryom: this.Order.car_yom,
                partname: part.partName,
                bidpriceper: bidAmount,
                bidtotalprice: part.partQuantity * bidAmount,
                suppliername: bidSourceName,
                comment: 'super cool',
                quantity: part.partQuantity
            }
        });
    };
    ParticularpartComponent.prototype.getbids = function (partid) {
        var _this = this;
        this.items = this.temp = null;
        this.orderservice.getPartBids(partid).subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    ParticularpartComponent.prototype.getparthistory = function (comclient_number, car_make, car_model, car_yom, partName) {
        var _this = this;
        this.itemshistory = null;
        //    gethistory(comclient_number:string,car_make:string,car_model:string,car_yom:string,partName:string)
        this.orderservice.gethistory(comclient_number, car_make, car_model, car_yom, partName).subscribe(function (data) {
            // refresh the list
            _this.itemshistory = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    ParticularpartComponent.prototype.getStatusreturn = function (vat) {
        var ite = this.Order.autorised_parts.find(function (x) { return x.part_id == vat; }).part_status;
        //console.log( JSON.stringify(ite) +"-------------------"+vat+ite.part_status);
        return ite;
    };
    ParticularpartComponent.prototype.selectedStatusreturn = function () {
        var _this = this;
        var ite = this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; }).part_status;
        //console.log( JSON.stringify(ite) +"-------------------"+vat+ite.part_status);
        return ite;
    };
    ParticularpartComponent.prototype.closeBidding = function () {
        var _this = this;
        this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; });
        //this.items =this.temp =null;
        this.orderservice.closeopenBids(this.selectedid, localStorage.getItem('cnumber'), this.productID, localStorage.getItem('uemail'), "Closed").subscribe(function (data) {
            if (data["result"] == "Success") {
                _this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; }).part_status = 'Closed';
            }
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            // return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        //console.log("-------------------"+this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status);
    };
    ParticularpartComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    ParticularpartComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenav"])
    ], ParticularpartComponent.prototype, "sideNav", void 0);
    ParticularpartComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-particularpart',
            template: __webpack_require__(/*! ./particularpart.component.html */ "./src/app/views/others/orders/particularpart/particularpart.component.html"),
            styles: [__webpack_require__(/*! ./particularpart.component.scss */ "./src/app/views/others/orders/particularpart/particularpart.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_8__["egretAnimations"]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__["ObservableMedia"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialog"], _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBar"]])
    ], ParticularpartComponent);
    return ParticularpartComponent;
}());

// closeopenBids(partid:string,awardcompanyId:string,bidOrderId:string,companyOfficer:string,part_status:string)


/***/ }),

/***/ "./src/app/views/others/orders/printpage/printpage.component.html":
/*!************************************************************************!*\
  !*** ./src/app/views/others/orders/printpage/printpage.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row no-gutters\">\n    <div class=\"col\">\n      <h1> Assistência Nº 1 XXXXX</h1>\n    </div>\n  </div>\n\n  <div class=\"row no-gutters\">\n    <div class=\"col\">\n      <img\n      src=\"../../assets/logotipo.svg\"\n      alt=\"logotipo\"\n      class=\"logotipo\">\n    </div>\n\n    <div class=\"col\">\n      <h2>N Reparações</h2>\n      <p>91 886 73 76</p>    \n      <div class=\"address\">\n        <p>geral@nreparacoes.com</p>\n        <p>Av. Dr. Ribeiro de Magalhães, 1391</p>\n        <p>Centro Comercial 123, Loja BD</p>\n        <p>4610-108 Felgueiras</p>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"row no-gutters items-group\">\n    <div class=\"col-3 label\">\n      <p>Cliente:</p>\n      <p>Contacto:</p>\n    </div>\n\n    <div class=\"col-9\">\n      <p>Manuel Alberto Magalhães Carvalho</p>\n      <p>91 000 00 00</p>\n    </div>\n  </div>\n\n  <div class=\"row no-gutters items-group\">\n    <div class=\"col-3 label\">\n      <p>Equipamento:</p>\n      <p>Cor:</p>\n      <p>Serial:</p>\n      <p>Problema:</p>\n      <p>Observações:</p>\n    </div>\n\n    <div class=\"col-9\">\n      <p>Telemóvel > Apple > Iphone 6</p>\n      <p>Cinza</p>\n      <p>appl2015ewfwejuk23423</p>\n      <p>Não arranca</p>\n      <p>codigo: 1798 - tem de ficar pronto até dia 12/02/2019</p>\n    </div>\n  </div>\n\n  <div class=\"row no-gutters items-group\">\n    <div class=\"col\">\n      <p class=\"label\">Condições Gerais:</p>\n      <ul class=\"disclaimer\">\n        <li>Prazo para levantar 90 dias</li>\n        <li>Garantia 90 dias</li>\n        <li>Esta assistência só é entregue a quem apresentar este documento</li>\n        <li>O preço acresce 10% a cada 30 dias após fim do prazo de levantamento (taxa de conservação)</li>\n        <li>Os dados do cliente, acima mencionados, ficam registados na N Reparações apenas para efeitos de comunicação\n          em relação às suas assistências e facturação\n        </li>\n      </ul>\n    </div>\n  </div>\n\n  <div class=\"row no-gutters\">\n    <div class=\"col\">\n      <p class=\"disclaimer signature-field\">Declaro que li e concordo com o disposto neste documento em 03/02/2019</p>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/views/others/orders/printpage/printpage.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/others/orders/printpage/printpage.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body, h1, h2, h3, h4, p, img {\n  margin: 0; }\n\n/* margin reset*/\n\nh1 {\n  font-size: 18pt;\n  font-weight: 500;\n  margin-bottom: 0.5cm; }\n\nh2 {\n  font-size: 16pt;\n  font-weight: 500; }\n\np {\n  font-size: 12pt; }\n\n.items-group {\n  border-style: solid;\n  border-color: black;\n  border-width: 1pt 1pt 1pt 1pt;\n  border-radius: 5pt;\n  padding: 3pt 3pt 3pt 3pt;\n  margin: 9pt 0 9pt 0; }\n\n.logotipo {\n  width: 6cm;\n  height: 4cm;\n  margin: -0.5cm; }\n\n.label {\n  font-weight: 500; }\n\n.disclaimer, .address p {\n  font-size: 10pt; }\n\n.signature-field {\n  padding-bottom: 24pt;\n  border-bottom: solid black 1pt; }\n\n/*\n.container {\n  display: grid;\n  grid: \"numero numero\"\n        \"logo loja\"\n        \"clt-lbl clt\"\n        \"da-lbl da\";\n  grid-row-gap: 1cm;\n}\n\n.label-group {\n  border-left: solid black 1pt;\n  border-top: solid black 1pt;\n  border-top-left-radius: 10pt;\n  padding: 9pt 0 0 9pt\n}\n\n.description-group {\n  border-top: solid black 1pt;\n  padding: 9pt 0 0 9pt\n}\n\n.numero {\n  grid-area: numero;\n}\n\n.loja {\n  grid-area: loja;\n}\n\n.logotipo {\n  grid-area: logo;\n}\n\n.cliente-label {\n  grid-area: clt-lbl;\n}\n\n.cliente {\n  grid-area: clt;\n}\n\n.detalhes-assistencia-label {\n  grid-area: da-lbl;\n}\n\n.detalhes-assistencia {\n  grid-area: da;\n}\n\n*/\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy9vcmRlcnMvcHJpbnRwYWdlL3ByaW50cGFnZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFxQyxVQUFTLEVBQUc7O0FBQUEsaUJBQWlCOztBQUNsRTtFQUFHLGdCQUFlO0VBQUUsaUJBQWdCO0VBQUUscUJBQW9CLEVBQUc7O0FBQzdEO0VBQUcsZ0JBQWU7RUFBRSxpQkFBZ0IsRUFBRzs7QUFDdkM7RUFBRSxnQkFBZSxFQUFHOztBQUVwQjtFQUNFLG9CQUFtQjtFQUNuQixvQkFBbUI7RUFDbkIsOEJBQTZCO0VBQzdCLG1CQUFrQjtFQUNsQix5QkFBd0I7RUFDeEIsb0JBQW1CLEVBQ3BCOztBQUVEO0VBQ0UsV0FBVTtFQUNWLFlBQVc7RUFDWCxlQUFjLEVBQ2Y7O0FBRUQ7RUFBUyxpQkFBZ0IsRUFBRzs7QUFFNUI7RUFDRSxnQkFBZSxFQUNoQjs7QUFFRDtFQUNFLHFCQUFvQjtFQUNwQiwrQkFBOEIsRUFDL0I7O0FBS0Q7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VBa0RFIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvb3RoZXJzL29yZGVycy9wcmludHBhZ2UvcHJpbnRwYWdlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaHRtbCwgYm9keSwgaDEsIGgyLCBoMywgaDQsIHAsIGltZyB7IG1hcmdpbjogMDt9IC8qIG1hcmdpbiByZXNldCovXG5oMXtmb250LXNpemU6IDE4cHQ7IGZvbnQtd2VpZ2h0OiA1MDA7IG1hcmdpbi1ib3R0b206IDAuNWNtO31cbmgye2ZvbnQtc2l6ZTogMTZwdDsgZm9udC13ZWlnaHQ6IDUwMDt9XG5we2ZvbnQtc2l6ZTogMTJwdDt9XG5cbi5pdGVtcy1ncm91cCB7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGJvcmRlci1jb2xvcjogYmxhY2s7XG4gIGJvcmRlci13aWR0aDogMXB0IDFwdCAxcHQgMXB0O1xuICBib3JkZXItcmFkaXVzOiA1cHQ7XG4gIHBhZGRpbmc6IDNwdCAzcHQgM3B0IDNwdDtcbiAgbWFyZ2luOiA5cHQgMCA5cHQgMDtcbn1cblxuLmxvZ290aXBvIHtcbiAgd2lkdGg6IDZjbTtcbiAgaGVpZ2h0OiA0Y207XG4gIG1hcmdpbjogLTAuNWNtO1xufVxuXG4ubGFiZWwgeyBmb250LXdlaWdodDogNTAwO31cblxuLmRpc2NsYWltZXIsIC5hZGRyZXNzIHAge1xuICBmb250LXNpemU6IDEwcHQ7XG59XG5cbi5zaWduYXR1cmUtZmllbGQge1xuICBwYWRkaW5nLWJvdHRvbTogMjRwdDtcbiAgYm9yZGVyLWJvdHRvbTogc29saWQgYmxhY2sgMXB0O1xufVxuXG5cblxuXG4vKlxuLmNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGdyaWQ7XG4gIGdyaWQ6IFwibnVtZXJvIG51bWVyb1wiXG4gICAgICAgIFwibG9nbyBsb2phXCJcbiAgICAgICAgXCJjbHQtbGJsIGNsdFwiXG4gICAgICAgIFwiZGEtbGJsIGRhXCI7XG4gIGdyaWQtcm93LWdhcDogMWNtO1xufVxuXG4ubGFiZWwtZ3JvdXAge1xuICBib3JkZXItbGVmdDogc29saWQgYmxhY2sgMXB0O1xuICBib3JkZXItdG9wOiBzb2xpZCBibGFjayAxcHQ7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDEwcHQ7XG4gIHBhZGRpbmc6IDlwdCAwIDAgOXB0XG59XG5cbi5kZXNjcmlwdGlvbi1ncm91cCB7XG4gIGJvcmRlci10b3A6IHNvbGlkIGJsYWNrIDFwdDtcbiAgcGFkZGluZzogOXB0IDAgMCA5cHRcbn1cblxuLm51bWVybyB7XG4gIGdyaWQtYXJlYTogbnVtZXJvO1xufVxuXG4ubG9qYSB7XG4gIGdyaWQtYXJlYTogbG9qYTtcbn1cblxuLmxvZ290aXBvIHtcbiAgZ3JpZC1hcmVhOiBsb2dvO1xufVxuXG4uY2xpZW50ZS1sYWJlbCB7XG4gIGdyaWQtYXJlYTogY2x0LWxibDtcbn1cblxuLmNsaWVudGUge1xuICBncmlkLWFyZWE6IGNsdDtcbn1cblxuLmRldGFsaGVzLWFzc2lzdGVuY2lhLWxhYmVsIHtcbiAgZ3JpZC1hcmVhOiBkYS1sYmw7XG59XG5cbi5kZXRhbGhlcy1hc3Npc3RlbmNpYSB7XG4gIGdyaWQtYXJlYTogZGE7XG59XG5cbiovIl19 */"

/***/ }),

/***/ "./src/app/views/others/orders/printpage/printpage.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/others/orders/printpage/printpage.component.ts ***!
  \**********************************************************************/
/*! exports provided: PrintpageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintpageComponent", function() { return PrintpageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrintpageComponent = /** @class */ (function () {
    function PrintpageComponent() {
    }
    PrintpageComponent.prototype.ngOnInit = function () {
        window.print();
    };
    PrintpageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-printpage',
            template: __webpack_require__(/*! ./printpage.component.html */ "./src/app/views/others/orders/printpage/printpage.component.html"),
            styles: [__webpack_require__(/*! ./printpage.component.scss */ "./src/app/views/others/orders/printpage/printpage.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PrintpageComponent);
    return PrintpageComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/showorderparts/showorderparts.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/views/others/orders/showorderparts/showorderparts.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row wrap\">\n    <div fxFlex=\"100\" fxFlex.gt-md=\"300px\" fxFlex.gt-sm=\"50\">\n      <mat-card class=\"profile-sidebar mb-1 pb-0\">\n        <div class=\"propic text-center\">\n          <img src=\"assets/images/car_shell.jpg\" alt=\"\">\n        </div>\n        \n        <div class=\"profile-title text-center mb-1\" style=\"display:flex; flex-direction: column\">\n          <div class=\"main-title\"><b>{{Order.carReg_no}}</b></div>\n         \n          <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n            <label for=\"id1\">Order id: </label>\n            <span fxFlex=\"8px\"></span>\n            <div class=\"subtitle mb-05\">{{Order.order_id}}</div>\n        </div> \n        <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n          <label for=\"id1\">Requisition No: </label>\n          <span fxFlex=\"8px\"></span>\n          <div class=\"subtitle mb-05\">{{Order.requisition_no}}</div>\n      </div> \n\n\n        <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n          <label for=\"id1\">Car Make: </label>\n          <span fxFlex=\"8px\"></span>\n          <div class=\"subtitle mb-05\">{{Order.car_make}}</div>\n      </div> \n      \n      <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n        <label for=\"id1\">Car Model: </label>\n        <span fxFlex=\"8px\"></span>\n        <div class=\"subtitle mb-05\">{{Order.car_model}}</div>\n    </div> \n\n          <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n              <label for=\"id1\">Year of Manufacture: </label>\n              <span fxFlex=\"8px\"></span>\n              <div class=\"subtitle mb-05\">{{Order.car_yom}}</div>\n          </div> \n         \n         \n        </div>\n\n        <div class=\"profile-actions mb-1\" fxLayoutAlign=\"center center\">\n          <button mat-raised-button color=\"primary\" [routerLink]=\"['/orders/particularpart', productID]\">View Part Details</button>\n          \n        </div>\n\n      <!--  <div  class=\"profile-actions mb-1\" fxLayoutAlign=\"center center\">\n        <button mat-raised-button color=\"primary\" printSectionId=\"printcomplete\" ngxPrint>Print Selected</button>\n        </div>\n-->\n        \n        <div class=\"profile-actions mb-1\" fxLayoutAlign=\"center center\">\n    <button mat-raised-button color=\"primary\" (click)=\"openPopup()\">Open Pictures</button>\n        </div>\n        \n        <div class=\"profile-actions mb-1\" fxLayoutAlign=\"center center\">\n     \n<a target=\"_blank\" [routerLink]=\"['/print', orderID ]\">  <button  id=\"printInvoice\" mat-raised-button color=\"primary\" >Print Completed</button>\n </a>\n\n        </div>\n      \n     <!--   <div class=\"profile-nav\">\n          <mat-nav-list>\n            <mat-list-item routerLink=\"/profile/overview\" routerLinkActive=\"list-item-active\">\n              <mat-icon>home</mat-icon>\n              Overview\n            </mat-list-item>\n            <mat-divider></mat-divider>\n            <mat-list-item routerLink=\"/orders/particularpart\" routerLinkActive=\"list-item-active\">\n              <mat-icon>settings</mat-icon>\n              Settings\n            </mat-list-item>\n            <mat-divider></mat-divider>\n            <mat-list-item routerLink=\"/profile/blank\" routerLinkActive=\"list-item-active\">\n              <mat-icon>content_paste</mat-icon>\n              Blank\n            </mat-list-item>\n          </mat-nav-list>\n        </div>  -->\n      </mat-card>\n  \n     \n    </div>\n  \n  <!-- Profile Views    style=\"display: none\" -->\n    <div id=\"print002\" fxFlex=\"100\" fxFlex.gt-sm=\"50\" fxFlex.gt-md=\"calc(100% - 300px)\" >\n      \n        <mat-card class=\"default\">\n       \n            <mat-card-title class=\"mat-bg-primary m-0\">\n              <div class=\"card-title-text\">\n                <span style=\"width: 100%;text-align: center\" > Vehicle Parts </span>\n                <span fxFlex=\"18px\"></span>\n                \n                </div>\n            </mat-card-title>\n\n            <mat-card-content class=\"p-0\">\n\n              <div [perfectScrollbar]  class=\"list-tasktype\">\n                <div class=\"tasktype-item\" *ngFor=\"let t of Order.autorised_parts\">\n                   \n     <ng-container *ngIf=\"t.part_status=='Complete'; else elseTemplate\">\n       \n        <mat-list-item  routerLinkActive=\"list-item-active\">\n           \n                <mat-checkbox color=\"primary\" \n                (change)=\"onChange($event)\" [checked]=\"interestFormGroup.get('interests').value.indexOf(t.part_id) > -1\" [value]=\"t.part_id\">{{t.partName}}</mat-checkbox>\n                <span fxFlex></span>\n                <!--  <mat-chip mat-sm-chip color=\"primary\" [selected]=\"t.status ? true : false\">{{t.status ? 'completed' : 'pending'}}</mat-chip>\n                   --> \n                   <span>{{t.part_status}}</span>\n            </mat-list-item>\n\n     </ng-container>\n     <ng-template #elseTemplate>\n       \n \n        <mat-list-item [routerLink]=\"['/orders/particularpart', productID]\" routerLinkActive=\"list-item-active\">\n                      \n          <span>{{t.partName}}</span>\n            <span fxFlex></span>\n         <!--  <mat-chip mat-sm-chip color=\"primary\" [selected]=\"t.status ? true : false\">{{t.status ? 'completed' : 'pending'}}</mat-chip>\n            --> \n            <span>{{t.part_status}}</span>\n        </mat-list-item>\n\n     </ng-template>\n     \n                                 \n                </div>\n              </div>\n              \n            </mat-card-content>\n          </mat-card>\n\n    </div>\n  </div>\n\n  <div *ngIf=\"somedata\" id=\"printcomplete\" style=\"display: none\" >\n    <!--\n  <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n\n      <div class=\"profile-title text-center mb-1\" style=\"display:flex; flex-direction: column\">\n          <div class=\"main-title\"><b>Part Purchase Summary</b></div>\n          <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n              <label for=\"id1\">Registration No: </label>\n              <span fxFlex=\"8px\"></span>\n              <div class=\"subtitle mb-05\">{{Order.carReg_no}}</div>\n          </div> \n          \n          <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n            <label for=\"id1\">Order id: </label>\n            <span fxFlex=\"8px\"></span>\n            <div class=\"subtitle mb-05\">{{Order.order_id}}</div>\n        </div> \n       \n        <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n          <label for=\"id1\">Car Make: </label>\n          <span fxFlex=\"8px\"></span>\n          <div class=\"subtitle mb-05\">{{Order.car_make}}</div>\n      </div> \n      \n      <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n        <label for=\"id1\">Car Model: </label>\n        <span fxFlex=\"8px\"></span>\n        <div class=\"subtitle mb-05\">{{Order.car_model}}</div>\n    </div> \n\n          <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n              <label for=\"id1\">Year of Manufacture: </label>\n              <span fxFlex=\"8px\"></span>\n              <div class=\"subtitle mb-05\">{{Order.car_yom}}</div>\n          </div> \n         \n         \n        </div>\n\n    <mat-card-content class=\"p-0\">\n      <ngx-datatable\n            class=\"material ml-0 mr-0\"\n            [rows]=\"itemswontoprint\"\n            [columnMode]=\"'flex'\"\n            [headerHeight]=\"50\"\n            [footerHeight]=\"50\"\n            [limit]=\"10\"\n            [rowHeight]=\"'auto'\">\n            <ngx-datatable-column name=\"Part Name.\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ partidtoname(row?.bidPartId) }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Supplier Name.\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.bidSourceName }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Amount\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.bidAmount }}\n              </ng-template>\n            </ngx-datatable-column>\n                    \n \n          </ngx-datatable>\n    </mat-card-content>\n  </mat-card>\n-->\n \n<mat-card class=\"default\">\n       \n    <div class=\"profile-title text-center mb-1\" style=\"display:flex; flex-direction: column\">\n       \n        <mat-card-title class=\"main-title\">\n            <div class=\"card-title-text\">\n              <span style=\"width: 100%;text-align: center\" > Parts Purchase Summary </span>\n              <span fxFlex=\"18px\"></span>\n              \n              </div>\n          </mat-card-title>\n        <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n            <label for=\"id1\">Registration No: </label>\n            <span fxFlex=\"8px\"></span>\n            <div class=\"subtitle mb-05\">{{Order.carReg_no}}</div>\n        </div> \n        \n        <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n          <label for=\"id1\">Order id: </label>\n          <span fxFlex=\"8px\"></span>\n          <div class=\"subtitle mb-05\">{{Order.order_id}}</div>\n      </div> \n     \n      <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n        <label for=\"id1\">Car Make: </label>\n        <span fxFlex=\"8px\"></span>\n        <div class=\"subtitle mb-05\">{{Order.car_make}}</div>\n    </div> \n    \n    <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n      <label for=\"id1\">Car Model: </label>\n      <span fxFlex=\"8px\"></span>\n      <div class=\"subtitle mb-05\">{{Order.car_model}}</div>\n  </div> \n\n        <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n            <label for=\"id1\">Year of Manufacture: </label>\n            <span fxFlex=\"8px\"></span>\n            <div class=\"subtitle mb-05\">{{Order.car_yom}}</div>\n        </div> \n       \n       \n      </div>\n\n  <!--  <mat-card-title class=\"mat-bg-primary m-0\">\n      <div class=\"card-title-text\">\n        <span style=\"width: 100%;text-align: center\" > Parts Purchase Summary </span>\n        <span fxFlex=\"18px\"></span>\n        \n        </div>\n    </mat-card-title>  -->\n    ---------------------------------------------------------- Parts Purchased -----------------------------\n    &nbsp;\n    &nbsp;\n    <mat-card-content class=\"p-0\">\n        \n      <div [perfectScrollbar]  class=\"list-tasktype\">\n        <div class=\"tasktype-item\" *ngFor=\"let t of itemswontoprint\">\n           \n            <mat-list-item [routerLink]=\"['/orders/particularpart', productID]\" routerLinkActive=\"list-item-active\">\n                <span>{{partidtoname(t.bidPartId)}}</span>\n                <span fxFlex></span>\n            \n                <span>{{t.bidSourceName}}</span>\n                <span fxFlex></span>\n                <span>{{t.bidAmount}}</span>                \n            </mat-list-item>\n          \n        </div>\n      </div>\n      \n    </mat-card-content>\n  </mat-card>\n\n\n  </div>"

/***/ }),

/***/ "./src/app/views/others/orders/showorderparts/showorderparts.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/views/others/orders/showorderparts/showorderparts.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "html, body, h1, h2, h3, h4, p, img {\n  margin: 0; }\n\n/* margin reset*/\n\nh1 {\n  font-size: 18pt;\n  font-weight: 500;\n  margin-bottom: 0.5cm; }\n\nh2 {\n  font-size: 16pt;\n  font-weight: 500; }\n\np {\n  font-size: 12pt; }\n\n.items-group {\n  border-style: solid;\n  border-color: black;\n  border-width: 1pt 1pt 1pt 1pt;\n  border-radius: 5pt;\n  padding: 3pt 3pt 3pt 3pt;\n  margin: 9pt 0 9pt 0; }\n\n.logotipo {\n  width: 6cm;\n  height: 4cm;\n  margin: -0.5cm; }\n\n.label {\n  font-weight: 500; }\n\n.disclaimer, .address p {\n  font-size: 10pt; }\n\n.signature-field {\n  padding-bottom: 24pt;\n  border-bottom: solid black 1pt; }\n\n/*\n.container {\n  display: grid;\n  grid: \"numero numero\"\n        \"logo loja\"\n        \"clt-lbl clt\"\n        \"da-lbl da\";\n  grid-row-gap: 1cm;\n}\n\n.label-group {\n  border-left: solid black 1pt;\n  border-top: solid black 1pt;\n  border-top-left-radius: 10pt;\n  padding: 9pt 0 0 9pt\n}\n\n.description-group {\n  border-top: solid black 1pt;\n  padding: 9pt 0 0 9pt\n}\n\n.numero {\n  grid-area: numero;\n}\n\n.loja {\n  grid-area: loja;\n}\n\n.logotipo {\n  grid-area: logo;\n}\n\n.cliente-label {\n  grid-area: clt-lbl;\n}\n\n.cliente {\n  grid-area: clt;\n}\n\n.detalhes-assistencia-label {\n  grid-area: da-lbl;\n}\n\n.detalhes-assistencia {\n  grid-area: da;\n}\n\n*/\n\n#invoice {\n  padding: 30px; }\n\n.invoice {\n  position: relative;\n  background-color: #FFF;\n  min-height: 680px;\n  padding: 15px; }\n\n.invoice header {\n  padding: 10px 0;\n  margin-bottom: 20px;\n  border-bottom: 1px solid #3989c6; }\n\n.invoice .company-details {\n  text-align: right; }\n\n.invoice .company-details .name {\n  margin-top: 0;\n  margin-bottom: 0; }\n\n.invoice .contacts {\n  margin-bottom: 20px; }\n\n.invoice .invoice-to {\n  text-align: left; }\n\n.invoice .invoice-to .to {\n  margin-top: 0;\n  margin-bottom: 0; }\n\n.invoice .invoice-details {\n  text-align: right; }\n\n.invoice .invoice-details .invoice-id {\n  margin-top: 0;\n  color: #3989c6; }\n\n.invoice main {\n  padding-bottom: 50px; }\n\n.invoice main .thanks {\n  margin-top: -100px;\n  font-size: 2em;\n  margin-bottom: 50px; }\n\n.invoice main .notices {\n  padding-left: 6px;\n  border-left: 6px solid #3989c6; }\n\n.invoice main .notices .notice {\n  font-size: 1.2em; }\n\n.invoice table {\n  width: 100%;\n  border-collapse: collapse;\n  border-spacing: 0;\n  margin-bottom: 20px; }\n\n.invoice table td,\n.invoice table th {\n  padding: 15px;\n  background: #eee;\n  border-bottom: 1px solid #fff; }\n\n.invoice table th {\n  white-space: nowrap;\n  font-weight: 400;\n  font-size: 16px; }\n\n.invoice table td h3 {\n  margin: 0;\n  font-weight: 400;\n  color: #3989c6;\n  font-size: 1.2em; }\n\n.invoice table .qty,\n.invoice table .total,\n.invoice table .unit {\n  text-align: right;\n  font-size: 1.2em; }\n\n.invoice table .no {\n  color: #fff;\n  font-size: 1.6em;\n  background: #3989c6; }\n\n.invoice table .unit {\n  background: #ddd; }\n\n.invoice table .total {\n  background: #3989c6;\n  color: #fff; }\n\n.invoice table tbody tr:last-child td {\n  border: none; }\n\n.invoice table tfoot td {\n  background: 0 0;\n  border-bottom: none;\n  white-space: nowrap;\n  text-align: right;\n  padding: 10px 20px;\n  font-size: 1.2em;\n  border-top: 1px solid #aaa; }\n\n.invoice table tfoot tr:first-child td {\n  border-top: none; }\n\n.invoice table tfoot tr:last-child td {\n  color: #3989c6;\n  font-size: 1.4em;\n  border-top: 1px solid #3989c6; }\n\n.invoice table tfoot tr td:first-child {\n  border: none; }\n\n.invoice footer {\n  width: 100%;\n  text-align: center;\n  color: #777;\n  border-top: 1px solid #aaa;\n  padding: 8px 0; }\n\n@media print {\n  .invoice {\n    font-size: 11px !important;\n    overflow: hidden !important; }\n  .invoice footer {\n    position: absolute;\n    bottom: 10px;\n    page-break-after: always; }\n  .invoice > div:last-child {\n    page-break-before: always; } }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy9vcmRlcnMvc2hvd29yZGVycGFydHMvc2hvd29yZGVycGFydHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBcUMsVUFBUyxFQUFHOztBQUFBLGlCQUFpQjs7QUFDbEU7RUFBRyxnQkFBZTtFQUFFLGlCQUFnQjtFQUFFLHFCQUFvQixFQUFHOztBQUM3RDtFQUFHLGdCQUFlO0VBQUUsaUJBQWdCLEVBQUc7O0FBQ3ZDO0VBQUUsZ0JBQWUsRUFBRzs7QUFFcEI7RUFDRSxvQkFBbUI7RUFDbkIsb0JBQW1CO0VBQ25CLDhCQUE2QjtFQUM3QixtQkFBa0I7RUFDbEIseUJBQXdCO0VBQ3hCLG9CQUFtQixFQUNwQjs7QUFFRDtFQUNFLFdBQVU7RUFDVixZQUFXO0VBQ1gsZUFBYyxFQUNmOztBQUVEO0VBQVMsaUJBQWdCLEVBQUc7O0FBRTVCO0VBQ0UsZ0JBQWUsRUFDaEI7O0FBRUQ7RUFDRSxxQkFBb0I7RUFDcEIsK0JBQThCLEVBQy9COztBQUtEOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQWtERTs7QUFDRjtFQUNJLGNBQWEsRUFDaEI7O0FBRUQ7RUFDSSxtQkFBa0I7RUFDbEIsdUJBQXNCO0VBQ3RCLGtCQUFpQjtFQUNqQixjQUNKLEVBQUM7O0FBRUQ7RUFDSSxnQkFBZTtFQUNmLG9CQUFtQjtFQUNuQixpQ0FDSixFQUFDOztBQUVEO0VBQ0ksa0JBQ0osRUFBQzs7QUFFRDtFQUNJLGNBQWE7RUFDYixpQkFDSixFQUFDOztBQUVEO0VBQ0ksb0JBQ0osRUFBQzs7QUFFRDtFQUNJLGlCQUNKLEVBQUM7O0FBRUQ7RUFDSSxjQUFhO0VBQ2IsaUJBQ0osRUFBQzs7QUFFRDtFQUNJLGtCQUNKLEVBQUM7O0FBRUQ7RUFDSSxjQUFhO0VBQ2IsZUFDSixFQUFDOztBQUVEO0VBQ0kscUJBQ0osRUFBQzs7QUFFRDtFQUNJLG1CQUFrQjtFQUNsQixlQUFjO0VBQ2Qsb0JBQ0osRUFBQzs7QUFFRDtFQUNJLGtCQUFpQjtFQUNqQiwrQkFDSixFQUFDOztBQUVEO0VBQ0ksaUJBQ0osRUFBQzs7QUFFRDtFQUNJLFlBQVc7RUFDWCwwQkFBeUI7RUFDekIsa0JBQWlCO0VBQ2pCLG9CQUNKLEVBQUM7O0FBRUQ7O0VBRUksY0FBYTtFQUNiLGlCQUFnQjtFQUNoQiw4QkFDSixFQUFDOztBQUVEO0VBQ0ksb0JBQW1CO0VBQ25CLGlCQUFnQjtFQUNoQixnQkFDSixFQUFDOztBQUVEO0VBQ0ksVUFBUztFQUNULGlCQUFnQjtFQUNoQixlQUFjO0VBQ2QsaUJBQ0osRUFBQzs7QUFFRDs7O0VBR0ksa0JBQWlCO0VBQ2pCLGlCQUNKLEVBQUM7O0FBRUQ7RUFDSSxZQUFXO0VBQ1gsaUJBQWdCO0VBQ2hCLG9CQUNKLEVBQUM7O0FBRUQ7RUFDSSxpQkFDSixFQUFDOztBQUVEO0VBQ0ksb0JBQW1CO0VBQ25CLFlBQ0osRUFBQzs7QUFFRDtFQUNJLGFBQ0osRUFBQzs7QUFFRDtFQUNJLGdCQUFlO0VBQ2Ysb0JBQW1CO0VBQ25CLG9CQUFtQjtFQUNuQixrQkFBaUI7RUFDakIsbUJBQWtCO0VBQ2xCLGlCQUFnQjtFQUNoQiwyQkFDSixFQUFDOztBQUVEO0VBQ0ksaUJBQ0osRUFBQzs7QUFFRDtFQUNJLGVBQWM7RUFDZCxpQkFBZ0I7RUFDaEIsOEJBQ0osRUFBQzs7QUFFRDtFQUNJLGFBQ0osRUFBQzs7QUFFRDtFQUNJLFlBQVc7RUFDWCxtQkFBa0I7RUFDbEIsWUFBVztFQUNYLDJCQUEwQjtFQUMxQixlQUNKLEVBQUM7O0FBRUQ7RUFDSTtJQUNJLDJCQUF5QjtJQUN6Qiw0QkFBMEIsRUFDN0I7RUFDRDtJQUNJLG1CQUFrQjtJQUNsQixhQUFZO0lBQ1oseUJBQ0osRUFBQztFQUNEO0lBQ0ksMEJBQ0osRUFBQyxFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvb3RoZXJzL29yZGVycy9zaG93b3JkZXJwYXJ0cy9zaG93b3JkZXJwYXJ0cy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImh0bWwsIGJvZHksIGgxLCBoMiwgaDMsIGg0LCBwLCBpbWcgeyBtYXJnaW46IDA7fSAvKiBtYXJnaW4gcmVzZXQqL1xuaDF7Zm9udC1zaXplOiAxOHB0OyBmb250LXdlaWdodDogNTAwOyBtYXJnaW4tYm90dG9tOiAwLjVjbTt9XG5oMntmb250LXNpemU6IDE2cHQ7IGZvbnQtd2VpZ2h0OiA1MDA7fVxucHtmb250LXNpemU6IDEycHQ7fVxuXG4uaXRlbXMtZ3JvdXAge1xuICBib3JkZXItc3R5bGU6IHNvbGlkO1xuICBib3JkZXItY29sb3I6IGJsYWNrO1xuICBib3JkZXItd2lkdGg6IDFwdCAxcHQgMXB0IDFwdDtcbiAgYm9yZGVyLXJhZGl1czogNXB0O1xuICBwYWRkaW5nOiAzcHQgM3B0IDNwdCAzcHQ7XG4gIG1hcmdpbjogOXB0IDAgOXB0IDA7XG59XG5cbi5sb2dvdGlwbyB7XG4gIHdpZHRoOiA2Y207XG4gIGhlaWdodDogNGNtO1xuICBtYXJnaW46IC0wLjVjbTtcbn1cblxuLmxhYmVsIHsgZm9udC13ZWlnaHQ6IDUwMDt9XG5cbi5kaXNjbGFpbWVyLCAuYWRkcmVzcyBwIHtcbiAgZm9udC1zaXplOiAxMHB0O1xufVxuXG4uc2lnbmF0dXJlLWZpZWxkIHtcbiAgcGFkZGluZy1ib3R0b206IDI0cHQ7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIGJsYWNrIDFwdDtcbn1cblxuXG5cblxuLypcbi5jb250YWluZXIge1xuICBkaXNwbGF5OiBncmlkO1xuICBncmlkOiBcIm51bWVybyBudW1lcm9cIlxuICAgICAgICBcImxvZ28gbG9qYVwiXG4gICAgICAgIFwiY2x0LWxibCBjbHRcIlxuICAgICAgICBcImRhLWxibCBkYVwiO1xuICBncmlkLXJvdy1nYXA6IDFjbTtcbn1cblxuLmxhYmVsLWdyb3VwIHtcbiAgYm9yZGVyLWxlZnQ6IHNvbGlkIGJsYWNrIDFwdDtcbiAgYm9yZGVyLXRvcDogc29saWQgYmxhY2sgMXB0O1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxMHB0O1xuICBwYWRkaW5nOiA5cHQgMCAwIDlwdFxufVxuXG4uZGVzY3JpcHRpb24tZ3JvdXAge1xuICBib3JkZXItdG9wOiBzb2xpZCBibGFjayAxcHQ7XG4gIHBhZGRpbmc6IDlwdCAwIDAgOXB0XG59XG5cbi5udW1lcm8ge1xuICBncmlkLWFyZWE6IG51bWVybztcbn1cblxuLmxvamEge1xuICBncmlkLWFyZWE6IGxvamE7XG59XG5cbi5sb2dvdGlwbyB7XG4gIGdyaWQtYXJlYTogbG9nbztcbn1cblxuLmNsaWVudGUtbGFiZWwge1xuICBncmlkLWFyZWE6IGNsdC1sYmw7XG59XG5cbi5jbGllbnRlIHtcbiAgZ3JpZC1hcmVhOiBjbHQ7XG59XG5cbi5kZXRhbGhlcy1hc3Npc3RlbmNpYS1sYWJlbCB7XG4gIGdyaWQtYXJlYTogZGEtbGJsO1xufVxuXG4uZGV0YWxoZXMtYXNzaXN0ZW5jaWEge1xuICBncmlkLWFyZWE6IGRhO1xufVxuXG4qL1xuI2ludm9pY2Uge1xuICAgIHBhZGRpbmc6IDMwcHg7XG59XG5cbi5pbnZvaWNlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRjtcbiAgICBtaW4taGVpZ2h0OiA2ODBweDtcbiAgICBwYWRkaW5nOiAxNXB4XG59XG5cbi5pbnZvaWNlIGhlYWRlciB7XG4gICAgcGFkZGluZzogMTBweCAwO1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMzOTg5YzZcbn1cblxuLmludm9pY2UgLmNvbXBhbnktZGV0YWlscyB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHRcbn1cblxuLmludm9pY2UgLmNvbXBhbnktZGV0YWlscyAubmFtZSB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwXG59XG5cbi5pbnZvaWNlIC5jb250YWN0cyB7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweFxufVxuXG4uaW52b2ljZSAuaW52b2ljZS10byB7XG4gICAgdGV4dC1hbGlnbjogbGVmdFxufVxuXG4uaW52b2ljZSAuaW52b2ljZS10byAudG8ge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMFxufVxuXG4uaW52b2ljZSAuaW52b2ljZS1kZXRhaWxzIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodFxufVxuXG4uaW52b2ljZSAuaW52b2ljZS1kZXRhaWxzIC5pbnZvaWNlLWlkIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIGNvbG9yOiAjMzk4OWM2XG59XG5cbi5pbnZvaWNlIG1haW4ge1xuICAgIHBhZGRpbmctYm90dG9tOiA1MHB4XG59XG5cbi5pbnZvaWNlIG1haW4gLnRoYW5rcyB7XG4gICAgbWFyZ2luLXRvcDogLTEwMHB4O1xuICAgIGZvbnQtc2l6ZTogMmVtO1xuICAgIG1hcmdpbi1ib3R0b206IDUwcHhcbn1cblxuLmludm9pY2UgbWFpbiAubm90aWNlcyB7XG4gICAgcGFkZGluZy1sZWZ0OiA2cHg7XG4gICAgYm9yZGVyLWxlZnQ6IDZweCBzb2xpZCAjMzk4OWM2XG59XG5cbi5pbnZvaWNlIG1haW4gLm5vdGljZXMgLm5vdGljZSB7XG4gICAgZm9udC1zaXplOiAxLjJlbVxufVxuXG4uaW52b2ljZSB0YWJsZSB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyLWNvbGxhcHNlOiBjb2xsYXBzZTtcbiAgICBib3JkZXItc3BhY2luZzogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4XG59XG5cbi5pbnZvaWNlIHRhYmxlIHRkLFxuLmludm9pY2UgdGFibGUgdGgge1xuICAgIHBhZGRpbmc6IDE1cHg7XG4gICAgYmFja2dyb3VuZDogI2VlZTtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2ZmZlxufVxuXG4uaW52b2ljZSB0YWJsZSB0aCB7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICBmb250LXdlaWdodDogNDAwO1xuICAgIGZvbnQtc2l6ZTogMTZweFxufVxuXG4uaW52b2ljZSB0YWJsZSB0ZCBoMyB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgY29sb3I6ICMzOTg5YzY7XG4gICAgZm9udC1zaXplOiAxLjJlbVxufVxuXG4uaW52b2ljZSB0YWJsZSAucXR5LFxuLmludm9pY2UgdGFibGUgLnRvdGFsLFxuLmludm9pY2UgdGFibGUgLnVuaXQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIGZvbnQtc2l6ZTogMS4yZW1cbn1cblxuLmludm9pY2UgdGFibGUgLm5vIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBmb250LXNpemU6IDEuNmVtO1xuICAgIGJhY2tncm91bmQ6ICMzOTg5YzZcbn1cblxuLmludm9pY2UgdGFibGUgLnVuaXQge1xuICAgIGJhY2tncm91bmQ6ICNkZGRcbn1cblxuLmludm9pY2UgdGFibGUgLnRvdGFsIHtcbiAgICBiYWNrZ3JvdW5kOiAjMzk4OWM2O1xuICAgIGNvbG9yOiAjZmZmXG59XG5cbi5pbnZvaWNlIHRhYmxlIHRib2R5IHRyOmxhc3QtY2hpbGQgdGQge1xuICAgIGJvcmRlcjogbm9uZVxufVxuXG4uaW52b2ljZSB0YWJsZSB0Zm9vdCB0ZCB7XG4gICAgYmFja2dyb3VuZDogMCAwO1xuICAgIGJvcmRlci1ib3R0b206IG5vbmU7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgZm9udC1zaXplOiAxLjJlbTtcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2FhYVxufVxuXG4uaW52b2ljZSB0YWJsZSB0Zm9vdCB0cjpmaXJzdC1jaGlsZCB0ZCB7XG4gICAgYm9yZGVyLXRvcDogbm9uZVxufVxuXG4uaW52b2ljZSB0YWJsZSB0Zm9vdCB0cjpsYXN0LWNoaWxkIHRkIHtcbiAgICBjb2xvcjogIzM5ODljNjtcbiAgICBmb250LXNpemU6IDEuNGVtO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjMzk4OWM2XG59XG5cbi5pbnZvaWNlIHRhYmxlIHRmb290IHRyIHRkOmZpcnN0LWNoaWxkIHtcbiAgICBib3JkZXI6IG5vbmVcbn1cblxuLmludm9pY2UgZm9vdGVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29sb3I6ICM3Nzc7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNhYWE7XG4gICAgcGFkZGluZzogOHB4IDBcbn1cblxuQG1lZGlhIHByaW50IHtcbiAgICAuaW52b2ljZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogMTFweCFpbXBvcnRhbnQ7XG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW4haW1wb3J0YW50XG4gICAgfVxuICAgIC5pbnZvaWNlIGZvb3RlciB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgYm90dG9tOiAxMHB4O1xuICAgICAgICBwYWdlLWJyZWFrLWFmdGVyOiBhbHdheXNcbiAgICB9XG4gICAgLmludm9pY2U+ZGl2Omxhc3QtY2hpbGQge1xuICAgICAgICBwYWdlLWJyZWFrLWJlZm9yZTogYWx3YXlzXG4gICAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/orders/showorderparts/showorderparts.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/views/others/orders/showorderparts/showorderparts.component.ts ***!
  \********************************************************************************/
/*! exports provided: ShoworderpartsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShoworderpartsComponent", function() { return ShoworderpartsComponent; });
/* harmony import */ var _vehiclepics_vehiclepics_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../vehiclepics/vehiclepics.component */ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ShoworderpartsComponent = /** @class */ (function () {
    function ShoworderpartsComponent(dialog, route, orderservice, router, formBuilder) {
        //  console.log(JSON.stringify(this.navCtrl.data));
        this.dialog = dialog;
        this.route = route;
        this.orderservice = orderservice;
        this.router = router;
        this.formBuilder = formBuilder;
        this.itemswontoprint = [];
        this.somedata = false;
    }
    ShoworderpartsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.orderID = this.productID = this.route.snapshot.params['id'];
        //console.log(this.productID ); 
        //this.Order = JSON.parse(this.productID);
        this.Order = this.orderservice.giveOrders().find(function (x) { return x.order_id == _this.productID; });
        // this.getbids(this.Order.autorised_parts[0].part_id);
        // console.log("-------------------"+JSON.stringify(this.orderservice.giveOrders()));
        //this.Order =  this.getorderfrom(this.productID);
        //  console.log(this.orderservice.giveOrders());
        this.interestFormGroup = this.formBuilder.group({
            interests: this.formBuilder.array([])
        });
    };
    ShoworderpartsComponent.prototype.onChange = function (event) {
        var interests = this.interestFormGroup.get('interests');
        if (event.checked) {
            this.getbids(event.source.value);
            interests.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](event.source.value));
        }
        else {
            var i = interests.controls.findIndex(function (x) { return x.value === event.source.value; });
            interests.removeAt(i);
            this.itemswontoprint.pop(this.itemswontoprint.find(function (x) { return x.bidPartId == event.source.value; }));
        }
    };
    ShoworderpartsComponent.prototype.openPopup = function () {
        var dialogRef = this.dialog.open(_vehiclepics_vehiclepics_component__WEBPACK_IMPORTED_MODULE_0__["VehiclepicsComponent"], {
            width: '80%',
            height: '80%',
            data: this.Order
        });
        //console.log("data is", this.Order.car_imgUrl)
    };
    ShoworderpartsComponent.prototype.openParticular = function () {
        this.router.navigate(['/orders/particularpart', this.Order.order_id]);
    };
    ShoworderpartsComponent.prototype.getorderfrom = function (id) {
        //return this.orderservice.orders.find(x=>x.order_id == id)
    };
    ShoworderpartsComponent.prototype.printPage = function () {
        // window.print();
        // this.router.navigate(['orders/printpage']);
    };
    ShoworderpartsComponent.prototype.getWonBid = function (data) {
        //  itemswon;
        // this.itemswontoprint.push(this.Order.autorised_parts[0]);
        //this.Order.autorised_parts.find(x=>x.part_id == id)
        //console.log("========================= = = =  "+JSON.stringify( this.items));
        // console.log("========================= = = =  "+JSON.stringify(this.items.find(x=>x.bidAmount== "5800")));
        //  console.log("========================= = = =  "+JSON.stringify(data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed")));
        if (data.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won confirmed"; })) {
            //  this.itemswon = [data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed")];
            // var da = ;
            // this.itmw=data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed");
            this.itemswontoprint.push(data.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won confirmed"; }));
            //  console.log("========================= = = =  "+JSON.stringify( this.itemswontoprint));
        }
        else {
            // this.itemswon = [data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won unconfirmed")];
            // this.itmw=data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won unconfirmed");
            this.itemswontoprint.push(data.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won unconfirmed"; }));
        }
        this.somedata = true;
        //this.itemswon = this.items.find(x=>x.bidStatus[this.items.bidStatus.length] == "won unconfirmed");
        //this.items =this.temp =null;
        /*  this.orderservice.closeopenBids(id,localStorage.getItem('cnumber'),this.productID,localStorage.getItem('uemail'),"Bidding").subscribe(
            data => {
              if(data["result"]=="Success"){
                this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status='Bidding'
        
              }
            
             },
             error => {
               console.error("Error saving !");
             //  return Observable.throw(error);
            }
          );  */
    };
    ShoworderpartsComponent.prototype.getbids = function (partid) {
        var _this = this;
        console.log("--------get bids for -----------" + partid);
        // this.items =this.temp =null;
        this.orderservice.getPartBids(partid).subscribe(function (data) {
            // refresh the list
            //  this.items =this.temp = data;
            console.log("--------arrived bids for -----------" + partid);
            //  this.itemswontoprint.push(data)
            _this.getWonBid(data);
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    ShoworderpartsComponent.prototype.partidtoname = function (id) {
        return this.Order.autorised_parts.find(function (x) { return x.part_id == id; }).partName;
    };
    ShoworderpartsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-showorderparts',
            template: __webpack_require__(/*! ./showorderparts.component.html */ "./src/app/views/others/orders/showorderparts/showorderparts.component.html"),
            styles: [__webpack_require__(/*! ./showorderparts.component.scss */ "./src/app/views/others/orders/showorderparts/showorderparts.component.scss")],
            providers: []
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]])
    ], ShoworderpartsComponent);
    return ShoworderpartsComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/tables.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/views/others/orders/tables.service.ts ***!
  \*******************************************************/
/*! exports provided: TableService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TableService", function() { return TableService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _shared_inmemory_db_users__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../shared/inmemory-db/users */ "./src/app/shared/inmemory-db/users.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TableService = /** @class */ (function () {
    function TableService(http) {
        this.http = http;
        var userDB = new _shared_inmemory_db_users__WEBPACK_IMPORTED_MODULE_4__["UserDB"]();
        this.items = userDB.users;
    }
    //******* Implement your APIs ********
    TableService.prototype.getItems = function () {
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.items.slice());
    };
    TableService.prototype.addItem = function (item) {
        item._id = Math.round(Math.random() * 10000000000).toString();
        this.items.unshift(item);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.items.slice()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(1000));
    };
    TableService.prototype.updateItem = function (id, item) {
        this.items = this.items.map(function (i) {
            if (i._id === id) {
                return Object.assign({}, i, item);
            }
            return i;
        });
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.items.slice()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(1000));
    };
    TableService.prototype.removeItem = function (row) {
        var i = this.items.indexOf(row);
        this.items.splice(i, 1);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_2__["of"])(this.items.slice()).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["delay"])(1000));
    };
    TableService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], TableService);
    return TableService;
}());



/***/ })

}]);
//# sourceMappingURL=views-others-orders-orders-module.js.map