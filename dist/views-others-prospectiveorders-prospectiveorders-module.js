(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-others-prospectiveorders-prospectiveorders-module"],{

/***/ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/getquotes/getquotes.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div fxLayout=\"row wrap\">\n  <div fxFlex=\"100\" fxFlex.gt-md=\"300px\" fxFlex.gt-sm=\"50\">\n    <mat-card class=\"profile-sidebar mb-1 pb-0\">\n      <div class=\"propic text-center\">\n        <img src=\"assets/images/car_shell.jpg\" alt=\"\">\n      </div>\n      \n      <div class=\"profile-title text-center mb-1\" style=\"display:flex; flex-direction: column\">\n        <div class=\"main-title\"><b>{{Order.carReg_no}}</b></div>\n       \n      <!--  <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n          <label for=\"id1\">Order id: </label>\n          <span fxFlex=\"8px\"></span>\n          <div class=\"subtitle mb-05\">{{Order.order_id}}</div>\n      </div> \n      <div style=\"white-space:nowrap;display:flex; flex-direction: row \">\n        <label for=\"id1\">Requisition No: </label>\n        <span fxFlex=\"8px\"></span>\n        <div class=\"subtitle mb-05\">{{Order.requisition_no}}</div>\n    </div>   -->\n\n\n      <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n        <label for=\"id1\">Car Make: </label>\n        <span fxFlex=\"8px\"></span>\n        <div class=\"subtitle mb-05\">{{Order.car_make}}</div>\n    </div> \n    \n    <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n      <label for=\"id1\">Car Model: </label>\n      <span fxFlex=\"8px\"></span>\n      <div class=\"subtitle mb-05\">{{Order.car_model}}</div>\n  </div> \n\n        <div style=\"white-space:nowrap; display:flex; flex-direction: row \">\n            <label for=\"id1\">Year of Manufacture: </label>\n            <span fxFlex=\"8px\"></span>\n            <div class=\"subtitle mb-05\">{{Order.car_yom}}</div>\n        </div> \n       \n       \n      </div>\n\n      <div class=\"profile-actions mb-1\" fxLayoutAlign=\"center center\">\n      <!-- <button mat-raised-button color=\"primary\" (click)=\"submit()\">Procure Autorised Parts</button>\n        --> \n      </div>\n\n    </mat-card>\n\n   \n  </div>\n\n<!-- Profile Views -->\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"50\" fxFlex.gt-md=\"calc(100% - 300px)\">\n    \n      <mat-card class=\"default\">\n     \n          <mat-card-title class=\"mat-bg-primary m-0\">\n            <div class=\"card-title-text\">\n              <span style=\"width: 100%;text-align: center\" > Select Parts to Get Quotation </span>\n              <span fxFlex=\"18px\"></span>\n              \n              </div>\n          </mat-card-title>\n\n          <mat-card-content class=\"p-0\">\n\n            <div [perfectScrollbar]  class=\"list-tasktype\">\n              <form [formGroup]=\"interestFormGroup\" (ngSubmit)=\"submit()\">\n              <div class=\"tasktype-item\" *ngFor=\"let t of Order.order_parts; let i = index\">\n                 \n                  <mat-list-item  routerLinkActive=\"list-item-active\">\n                  <!--    <span>{{t.partName}}</span>\n                      <span fxFlex></span>\n                     <mat-chip mat-sm-chip color=\"primary\" [selected]=\"t.status ? true : false\">{{t.status ? 'completed' : 'pending'}}</mat-chip>\n                      \n                      <span>{{t.part_status}}</span>\n                     -->  \n                     <div  style=\"display:flex; flex-direction: row;  align-items: center\">\n                      <mat-checkbox color=\"primary\" \n                      (change)=\"onChange($event)\" [checked]=\"interestFormGroup.get('interests').value.indexOf(t.partName) > -1\" [value]=\"t.partName\">{{t.partName}}</mat-checkbox>\n                      &nbsp;\n                      &nbsp;\n                      &nbsp;\n                      <mat-chip   \n                      class=\"icon-chip\" \n                      color=\"primary\" \n                      selected=\"true\" (click)=\"openDialogQuoate()\" > View Quotations </mat-chip>\n                    </div>\n                  </mat-list-item>\n                \n              </div>\n\n            <!--  <button>Submit</button>  -->\n            <div class=\"profile-actions mb-1\" fxLayoutAlign=\"center center\">\n              <button mat-raised-button color=\"primary\"  type=\"submit\">Request Quotation</button>\n            </div>\n            </form>\n            </div>\n            \n          </mat-card-content>\n        </mat-card>\n\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/getquotes/getquotes.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9wcm9zcGVjdGl2ZW9yZGVycy9nZXRxdW90ZXMvZ2V0cXVvdGVzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/getquotes/getquotes.component.ts ***!
  \*********************************************************************************/
/*! exports provided: GetquotesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetquotesComponent", function() { return GetquotesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _getpreviousquotes_getpreviousquotes_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../getpreviousquotes/getpreviousquotes.component */ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GetquotesComponent = /** @class */ (function () {
    function GetquotesComponent(route, orderservice, router, formBuilder, dialog) {
        //  console.log(JSON.stringify(this.navCtrl.data)); 
        this.route = route;
        this.orderservice = orderservice;
        this.router = router;
        this.formBuilder = formBuilder;
        this.dialog = dialog;
        this.toquoteDatar = {};
    }
    GetquotesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.productID = this.route.snapshot.params['id'];
        //console.log(this.productID ); 
        //this.Order = JSON.parse(this.productID);
        this.Order = this.orderservice.giveprospectivePOrders().find(function (x) { return x.order_id == _this.productID; });
        // console.log("-------------------"+JSON.stringify(this.orderservice.giveprospectivePOrders()));
        //this.Order =  this.getorderfrom(this.productID);
        //  console.log(this.orderservice.giveOrders());
        this.interestFormGroup = this.formBuilder.group({
            interests: this.formBuilder.array([])
        });
    };
    GetquotesComponent.prototype.openParticular = function () {
        // this.router.navigate(['/orders/particularpart',this.Order.order_id]);
    };
    GetquotesComponent.prototype.getorderfrom = function (id) {
        //return this.orderservice.orders.find(x=>x.order_id == id)
    };
    GetquotesComponent.prototype.onChange = function (event) {
        var interests = this.interestFormGroup.get('interests');
        if (event.checked) {
            interests.push(new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](event.source.value));
        }
        else {
            var i = interests.controls.findIndex(function (x) { return x.value === event.source.value; });
            interests.removeAt(i);
        }
    };
    GetquotesComponent.prototype.submit = function () {
        var _this = this;
        if (this.interestFormGroup.value.interests.length >= 1) {
            this.toquoteDatar.order_sourceCompanyid = localStorage.getItem('cnumber');
            this.toquoteDatar.orderId = this.productID;
            this.toquoteDatar.toquoteparts = this.interestFormGroup.value.interests;
            this.subscription = this.orderservice.postAuthorisedParts(this.toquoteDatar).subscribe(function (data) {
                // refresh the list
                // this.items =this.temp = data;
                //   console.log(JSON.stringify( data )+"-------------------");
                _this.subscription.unsubscribe();
                //  this.carsparesarray.car_make = null;
                // this.clearObjectValues(this.carsparesarray);
                //  console.log(data+"----------------------=-===============-------jjjjjjjjj ");
                //  this.basicForm.reset();
                return true;
            }, function (error) {
                console.error("Error saving !");
                //  return Observable.throw(error);
            });
            this.router.navigate(['others']);
            //  console.log(this.interestFormGroup.value.interests);
        }
    };
    GetquotesComponent.prototype.openDialogQuoate = function () {
        //console.log(bidSourceName+bidAmount);
        // var part = this.Order.autorised_parts.find(x=>x.part_id == bidPartId)
        var dialogRef = this.dialog.open(_getpreviousquotes_getpreviousquotes_component__WEBPACK_IMPORTED_MODULE_4__["GetpreviousquotesComponent"], {
            width: '600px',
        });
    };
    GetquotesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-getquotes',
            template: __webpack_require__(/*! ./getquotes.component.html */ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.html"),
            styles: [__webpack_require__(/*! ./getquotes.component.scss */ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"]])
    ], GetquotesComponent);
    return GetquotesComponent;
}());



/***/ }),

/***/ "./src/app/views/others/prospectiveorders/prospectiveorders.module.ts":
/*!****************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/prospectiveorders.module.ts ***!
  \****************************************************************************/
/*! exports provided: ProspectiveordersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProspectiveordersModule", function() { return ProspectiveordersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var app_shared_shared_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _prospectiveorders_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./prospectiveorders.routing */ "./src/app/views/others/prospectiveorders/prospectiveorders.routing.ts");
/* harmony import */ var _viewallporders_viewallporders_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./viewallporders/viewallporders.component */ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.ts");
/* harmony import */ var _getquotes_getquotes_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./getquotes/getquotes.component */ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};










//import { CompanyusersRoutes } from './companyusers.routing';

//import { UsersComponent } from './users/users.component';



//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var ProspectiveordersModule = /** @class */ (function () {
    function ProspectiveordersModule() {
    }
    ProspectiveordersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                //BrowserAnimationsModule,
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__["PerfectScrollbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                app_shared_shared_module__WEBPACK_IMPORTED_MODULE_10__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_prospectiveorders_routing__WEBPACK_IMPORTED_MODULE_11__["ProspectiveordersRoutes"])
            ],
            declarations: [
                _viewallporders_viewallporders_component__WEBPACK_IMPORTED_MODULE_12__["ViewallpordersComponent"],
                _getquotes_getquotes_component__WEBPACK_IMPORTED_MODULE_13__["GetquotesComponent"]
                // UsersComponent
            ]
        })
    ], ProspectiveordersModule);
    return ProspectiveordersModule;
}());



/***/ }),

/***/ "./src/app/views/others/prospectiveorders/prospectiveorders.routing.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/prospectiveorders.routing.ts ***!
  \*****************************************************************************/
/*! exports provided: ProspectiveordersRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProspectiveordersRoutes", function() { return ProspectiveordersRoutes; });
/* harmony import */ var _viewallporders_viewallporders_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./viewallporders/viewallporders.component */ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.ts");
/* harmony import */ var _getquotes_getquotes_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./getquotes/getquotes.component */ "./src/app/views/others/prospectiveorders/getquotes/getquotes.component.ts");
//import { ProspectiveordersComponent } from "./prospectiveorders.component";


//import { UsersComponent } from "./users/users.component";
var ProspectiveordersRoutes = [
    {
        path: '',
        component: _viewallporders_viewallporders_component__WEBPACK_IMPORTED_MODULE_0__["ViewallpordersComponent"],
        data: { title: 'Blank', breadcrumb: 'BLANK' }
    },
    {
        path: 'getquotes/:id',
        component: _getquotes_getquotes_component__WEBPACK_IMPORTED_MODULE_1__["GetquotesComponent"],
        data: { title: 'Blank', breadcrumb: 'BLANK' }
    }
];


/***/ }),

/***/ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-333\">\n    <!-- <span fxFlex></span> -->\n    <mat-form-field class=\"margin-333\" style=\"width: 100%\">\n      <input \n      matInput \n      placeholder=\"Type to search from all columns\" \n      value=\"\"\n      (keyup)='updateFilter($event)'>\n    </mat-form-field>\n  </div>\n  <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n    <mat-card-content class=\"p-0\">\n      <ngx-datatable\n            class=\"material ml-0 mr-0\"\n            [rows]=\"items\"\n            [columnMode]=\"'flex'\"\n            [headerHeight]=\"50\"\n            [footerHeight]=\"50\"\n            [limit]=\"10\"\n            [rowHeight]=\"'auto'\">\n            <ngx-datatable-column name=\"Jobcard No.\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.jobcard_no }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Car Registration No.\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.carReg_no }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Car Model\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.car_model }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Order date\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.order_date }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Order Status\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                  \n                      {{ row?.order_status }}\n                   \n               <!-- <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'Complete' : 'Bidding'}}</mat-chip>\n        -->      </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n             <!--   <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" ><mat-icon>edit</mat-icon></button>\n                <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n              \n               [routerLink]=\"['orders/showorderparts', row.car_model]\" (click)= openOrder()   [routerLink]=\"['/orders/showorderparts', row.order_id]\"\n              -->\n              <div>\n                  \n                <div   fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                    <button mat-raised-button color=\"primary\"  (click)= openOrder(row.order_id) >\n                       \n                        Get Quotes\n                    </button>\n                     \n                      </div>  \n                    \n              </div>  \n              </ng-template>\n            </ngx-datatable-column>\n          </ngx-datatable>\n    </mat-card-content>\n  </mat-card>\n  "

/***/ }),

/***/ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9wcm9zcGVjdGl2ZW9yZGVycy92aWV3YWxscG9yZGVycy92aWV3YWxscG9yZGVycy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ViewallpordersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewallpordersComponent", function() { return ViewallpordersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-navigation-with-data */ "./node_modules/ngx-navigation-with-data/fesm5/ngx-navigation-with-data.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ViewallpordersComponent = /** @class */ (function () {
    function ViewallpordersComponent(dialog, snack, loader, router, orderservice, navCtrl) {
        this.dialog = dialog;
        this.snack = snack;
        this.loader = loader;
        this.router = router;
        this.orderservice = orderservice;
        this.navCtrl = navCtrl;
        this.bids = [];
    }
    ViewallpordersComponent.prototype.ngOnInit = function () {
        this.getItems2();
    };
    ViewallpordersComponent.prototype.getItems2 = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        this.orderservice.getprospectivePOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            //..  this.orderservice.orders = data;
            _this.orderservice.setprospectivePOrders(data);
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    ViewallpordersComponent.prototype.getItems = function () {
        /* this.getItemSub = this.orderservice.getOrders()
         //this.getItemSub = this.orderservice.postgetOrder()
             .subscribe(data => {
              this.items =this.temp = data;
             // console.log(data+"-------------------fghjklkjhgfdsdfghjkjhdsasdfghjhgfdsasdfghjk");
             })
           //..  console.log (JSON.stringify(this.items )+"----------------------------"+this.getItemSub);
          */
        /* this.orderservice.getOrders().subscribe(
                data => {
                   // refresh the list
                   this.items =this.temp = data;
                   console.log(JSON.stringify( data )+"-------------------");
                
                   return true;
                 },
                 error => {
                   console.error("Error saving !");
                 //  return Observable.throw(error);
                }
              );
          */
    };
    ViewallpordersComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var columns = Object.keys(this.temp[0]);
        // Removes last "$$index" from "column"
        columns.splice(columns.length - 1);
        // console.log(columns);
        if (!columns.length)
            return;
        var rows = this.temp.filter(function (d) {
            for (var i = 0; i <= columns.length; i++) {
                var column = columns[i];
                // console.log(d[column]);
                if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
                    return true;
                }
            }
        });
        this.items = rows;
    };
    //JSON.stringify(this.navCtrl.data)
    ViewallpordersComponent.prototype.openOrder = function (id) {
        //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
        //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
        var ite = this.items.find(function (x) { return x.order_id == id; });
        this.router.navigate(['prospectiveorders/getquotes', ite.order_id]);
    };
    ViewallpordersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-viewallporders',
            template: __webpack_require__(/*! ./viewallporders.component.html */ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.html"),
            styles: [__webpack_require__(/*! ./viewallporders.component.scss */ "./src/app/views/others/prospectiveorders/viewallporders/viewallporders.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_2__["AppLoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"],
            ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_5__["NgxNavigationWithDataComponent"]])
    ], ViewallpordersComponent);
    return ViewallpordersComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-others-prospectiveorders-prospectiveorders-module.js.map