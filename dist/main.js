(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./views/others/companyusers/companyusers.module": [
		"./src/app/views/others/companyusers/companyusers.module.ts",
		"views-others-companyusers-companyusers-module"
	],
	"./views/others/compsuppliers/compsuppliers.module": [
		"./src/app/views/others/compsuppliers/compsuppliers.module.ts",
		"views-others-compsuppliers-compsuppliers-module"
	],
	"./views/others/finance/finance.module": [
		"./src/app/views/others/finance/finance.module.ts",
		"views-others-finance-finance-module"
	],
	"./views/others/home/home.module": [
		"./src/app/views/others/home/home.module.ts",
		"views-others-home-home-module"
	],
	"./views/others/manage/manage.module": [
		"./src/app/views/others/manage/manage.module.ts",
		"default~views-others-manage-manage-module~views-others-orders-orders-module",
		"views-others-manage-manage-module"
	],
	"./views/others/orders/orders.module": [
		"./src/app/views/others/orders/orders.module.ts",
		"default~views-others-manage-manage-module~views-others-orders-orders-module",
		"views-others-orders-orders-module"
	],
	"./views/others/procurement/procurement.module": [
		"./src/app/views/others/procurement/procurement.module.ts",
		"views-others-procurement-procurement-module"
	],
	"./views/others/prospectiveorders/prospectiveorders.module": [
		"./src/app/views/others/prospectiveorders/prospectiveorders.module.ts",
		"views-others-prospectiveorders-prospectiveorders-module"
	],
	"./views/others/vehicle/vehicle.module": [
		"./src/app/views/others/vehicle/vehicle.module.ts"
	],
	"./views/sessions/sessions.module": [
		"./src/app/views/sessions/sessions.module.ts",
		"views-sessions-sessions-module"
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		var id = ids[0];
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/services/route-parts.service */ "./src/app/shared/services/route-parts.service.ts");
/* harmony import */ var _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/services/theme.service */ "./src/app/shared/services/theme.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { SocketService } from './shared/services/socket.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(title, router, activeRoute, routePartsService, themeService, renderer) {
        this.title = title;
        this.router = router;
        this.activeRoute = activeRoute;
        this.routePartsService = routePartsService;
        this.themeService = themeService;
        this.renderer = renderer;
        this.appTitle = 'allAutospares';
        this.pageTitle = '';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.changePageTitle();
        //.. this.router.navigate(['sessions/signin'])
    };
    AppComponent.prototype.ngAfterViewInit = function () {
        this.themeService.applyMatTheme(this.renderer);
    };
    AppComponent.prototype.changePageTitle = function () {
        var _this = this;
        this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_2__["NavigationEnd"]; })).subscribe(function (routeChange) {
            var routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);
            if (!routeParts.length)
                return _this.title.setTitle(_this.appTitle);
            // Extract title from parts;
            _this.pageTitle = routeParts
                .reverse()
                .map(function (part) { return part.title; })
                .reduce(function (partA, partI) { return partA + " > " + partI; });
            _this.pageTitle += " | " + _this.appTitle;
            _this.title.setTitle(_this.pageTitle);
        });
    };
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["Title"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_3__["RoutePartsService"],
            _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_4__["ThemeService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: HttpLoaderFactory, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpLoaderFactory", function() { return HttpLoaderFactory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _shared_components_layouts_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shared/components/layouts/print-layout/print-layout.component */ "./src/app/shared/components/layouts/print-layout/print-layout.component.ts");
/* harmony import */ var _views_others_orders_vehiclepics_vehiclepics_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./views/others/orders/vehiclepics/vehiclepics.component */ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.ts");
/* harmony import */ var ngx_image_viewer__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ngx-image-viewer */ "./node_modules/ngx-image-viewer/ngx-image-viewer.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-navigation-with-data */ "./node_modules/ngx-navigation-with-data/fesm5/ngx-navigation-with-data.js");
/* harmony import */ var angular_in_memory_web_api__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! angular-in-memory-web-api */ "./node_modules/angular-in-memory-web-api/index.js");
/* harmony import */ var _shared_inmemory_db_inmemory_db_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./shared/inmemory-db/inmemory-db.service */ "./src/app/shared/inmemory-db/inmemory-db.service.ts");
/* harmony import */ var ngx_print__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-print */ "./node_modules/ngx-print/fesm5/ngx-print.js");
/* harmony import */ var _app_routing__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./app.routing */ "./src/app/app.routing.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _views_others_orders_printer_printer_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./views/others/orders/printer/printer.component */ "./src/app/views/others/orders/printer/printer.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/esm5/ngx-translate-core.js");
/* harmony import */ var _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ngx-translate/http-loader */ "./node_modules/@ngx-translate/http-loader/esm5/ngx-translate-http-loader.js");
/* harmony import */ var _shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");
/* harmony import */ var _shared_services_order_service__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _views_others_orders_particularpart_bidpurchase_dialog_bidpurchase_dialog_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component */ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.ts");
/* harmony import */ var _views_others_orders_particularpart_bidofferdialog_bidofferdialog_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./views/others/orders/particularpart/bidofferdialog/bidofferdialog.component */ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_27___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_27__);
/* harmony import */ var _globals__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./globals */ "./src/app/globals.ts");
/* harmony import */ var _shared_services_auth_role_guard__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./shared/services/auth/role.guard */ "./src/app/shared/services/auth/role.guard.ts");
/* harmony import */ var _views_others_vehicle_vehicledetails_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component */ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.ts");
/* harmony import */ var _views_others_vehicle_vehicledetails_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component */ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.ts");
/* harmony import */ var _views_others_vehicle_vehicledetails_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./views/others/vehicle/vehicledetails/makedialogue/makedialogue.component */ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.ts");
/* harmony import */ var _views_others_companyusers_users_user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./views/others/companyusers/users/user-popup/user-popup.component */ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.ts");
/* harmony import */ var _views_others_compsuppliers_suppliers_supplierpopup_supplierpopup_component__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component */ "./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.ts");
/* harmony import */ var _views_others_vehicle_vehicle_module__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./views/others/vehicle/vehicle.module */ "./src/app/views/others/vehicle/vehicle.module.ts");
/* harmony import */ var _views_others_prospectiveorders_getpreviousquotes_getpreviousquotes_component__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component */ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



// Import your library
// import { ImageViewerModule } from 'ng2-image-viewer';


































//import { BidpurchaseDialogComponent } from './bidpurchase-dialog/bidpurchase-dialog.component';
// AoT requires an exported function for factories
function HttpLoaderFactory(httpClient) {
    return new _ngx_translate_http_loader__WEBPACK_IMPORTED_MODULE_19__["TranslateHttpLoader"](httpClient);
}
var DEFAULT_PERFECT_SCROLLBAR_CONFIG = {
    suppressScrollX: true
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_6__["BrowserAnimationsModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_14__["SharedModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_17__["HttpClientModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_24__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_24__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSlideToggleModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_25__["CommonModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_26__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_27__["NgxDatatableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatInputModule"],
                ngx_print__WEBPACK_IMPORTED_MODULE_12__["NgxPrintModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBarModule"],
                ngx_image_viewer__WEBPACK_IMPORTED_MODULE_2__["ImageViewerModule"].forRoot({ btnShow: {
                        zoomIn: false,
                        zoomOut: false,
                        rotateClockwise: false,
                        rotateCounterClockwise: false,
                        next: true,
                        prev: true
                    }
                }),
                ngx_print__WEBPACK_IMPORTED_MODULE_12__["NgxPrintModule"],
                /*
                  ImageViewerModule,*/
                // MatDialogModule
                _views_others_vehicle_vehicle_module__WEBPACK_IMPORTED_MODULE_35__["VehicleModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_8__["PerfectScrollbarModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_18__["TranslateModule"].forRoot({
                    loader: {
                        provide: _ngx_translate_core__WEBPACK_IMPORTED_MODULE_18__["TranslateLoader"],
                        useFactory: HttpLoaderFactory,
                        deps: [_angular_common_http__WEBPACK_IMPORTED_MODULE_17__["HttpClient"]]
                    }
                }),
                angular_in_memory_web_api__WEBPACK_IMPORTED_MODULE_10__["InMemoryWebApiModule"].forRoot(_shared_inmemory_db_inmemory_db_service__WEBPACK_IMPORTED_MODULE_11__["InMemoryDataService"], { passThruUnknownUrl: true }),
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forRoot(_app_routing__WEBPACK_IMPORTED_MODULE_13__["rootRouterConfig"], { useHash: false })
            ],
            exports: [
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatFormFieldModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_24__["FormsModule"]
                // MatInputModule,
                // MatRippleModule,
            ],
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_15__["AppComponent"], _views_others_orders_particularpart_bidpurchase_dialog_bidpurchase_dialog_component__WEBPACK_IMPORTED_MODULE_22__["BidpurchaseDialogComponent"], _views_others_orders_particularpart_bidofferdialog_bidofferdialog_component__WEBPACK_IMPORTED_MODULE_23__["BidofferdialogComponent"],
                _views_others_companyusers_users_user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_33__["UserPopupComponent"], _views_others_vehicle_vehicledetails_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_32__["MakedialogueComponent"], _views_others_vehicle_vehicledetails_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_31__["ModeldialogueComponent"],
                _views_others_vehicle_vehicledetails_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_30__["PartsdialogueComponent"], _views_others_compsuppliers_suppliers_supplierpopup_supplierpopup_component__WEBPACK_IMPORTED_MODULE_34__["SupplierpopupComponent"], _views_others_prospectiveorders_getpreviousquotes_getpreviousquotes_component__WEBPACK_IMPORTED_MODULE_36__["GetpreviousquotesComponent"],
                _views_others_orders_vehiclepics_vehiclepics_component__WEBPACK_IMPORTED_MODULE_1__["VehiclepicsComponent"], _views_others_orders_printer_printer_component__WEBPACK_IMPORTED_MODULE_16__["PrinterComponent"], _shared_components_layouts_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_0__["PrintLayoutComponent"]],
            entryComponents: [_views_others_orders_particularpart_bidpurchase_dialog_bidpurchase_dialog_component__WEBPACK_IMPORTED_MODULE_22__["BidpurchaseDialogComponent"], _views_others_orders_vehiclepics_vehiclepics_component__WEBPACK_IMPORTED_MODULE_1__["VehiclepicsComponent"], _views_others_orders_particularpart_bidofferdialog_bidofferdialog_component__WEBPACK_IMPORTED_MODULE_23__["BidofferdialogComponent"], _views_others_companyusers_users_user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_33__["UserPopupComponent"], _views_others_vehicle_vehicledetails_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_32__["MakedialogueComponent"], _views_others_vehicle_vehicledetails_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_31__["ModeldialogueComponent"], _views_others_vehicle_vehicledetails_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_30__["PartsdialogueComponent"], _views_others_compsuppliers_suppliers_supplierpopup_supplierpopup_component__WEBPACK_IMPORTED_MODULE_34__["SupplierpopupComponent"], _views_others_prospectiveorders_getpreviousquotes_getpreviousquotes_component__WEBPACK_IMPORTED_MODULE_36__["GetpreviousquotesComponent"]],
            providers: [_globals__WEBPACK_IMPORTED_MODULE_28__["Globals"], ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_9__["NgxNavigationWithDataComponent"],
                _shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_20__["AuthService"], _shared_services_order_service__WEBPACK_IMPORTED_MODULE_21__["OrderService"], _shared_services_auth_role_guard__WEBPACK_IMPORTED_MODULE_29__["RoleGuard"],
                { provide: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["HAMMER_GESTURE_CONFIG"], useClass: _angular_material__WEBPACK_IMPORTED_MODULE_7__["GestureConfig"] },
                { provide: ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_8__["PERFECT_SCROLLBAR_CONFIG"], useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_15__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/app.routing.ts":
/*!********************************!*\
  !*** ./src/app/app.routing.ts ***!
  \********************************/
/*! exports provided: rootRouterConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rootRouterConfig", function() { return rootRouterConfig; });
/* harmony import */ var _shared_components_layouts_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shared/components/layouts/print-layout/print-layout.component */ "./src/app/shared/components/layouts/print-layout/print-layout.component.ts");
/* harmony import */ var _views_others_orders_printer_printer_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./views/others/orders/printer/printer.component */ "./src/app/views/others/orders/printer/printer.component.ts");
/* harmony import */ var _shared_components_layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/components/layouts/admin-layout/admin-layout.component */ "./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts");
/* harmony import */ var _shared_components_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./shared/components/layouts/auth-layout/auth-layout.component */ "./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts");
/* harmony import */ var _shared_services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./shared/services/auth/auth.guard */ "./src/app/shared/services/auth/auth.guard.ts");





var rootRouterConfig = [
    {
        path: '',
        redirectTo: 'others',
        pathMatch: 'full'
    },
    {
        path: '',
        component: _shared_components_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_3__["AuthLayoutComponent"],
        children: [
            {
                path: 'sessions',
                loadChildren: './views/sessions/sessions.module#SessionsModule',
                data: { title: 'Session' }
            },
            {
                path: 'print/:id',
                component: _views_others_orders_printer_printer_component__WEBPACK_IMPORTED_MODULE_1__["PrinterComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            }
        ]
    },
    {
        path: '',
        component: _shared_components_layouts_print_layout_print_layout_component__WEBPACK_IMPORTED_MODULE_0__["PrintLayoutComponent"],
        children: [
            {
                path: 'print/:id',
                component: _views_others_orders_printer_printer_component__WEBPACK_IMPORTED_MODULE_1__["PrinterComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            }
        ]
    },
    {
        path: '',
        component: _shared_components_layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_2__["AdminLayoutComponent"],
        canActivate: [_shared_services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_4__["AuthGuard"]],
        children: [
            {
                path: 'others',
                loadChildren: './views/others/home/home.module#HomeModule',
                data: { title: 'Others', breadcrumb: 'OTHERS' }
            },
            {
                path: 'orders',
                loadChildren: './views/others/orders/orders.module#OrdersModule',
                // canActivate: [RoleGuard],
                data: { role: 'Procurement' }
            },
            {
                path: 'manage',
                loadChildren: './views/others/manage/manage.module#ManageModule',
                //  canActivate: [RoleGuard],
                data: { role: 'Super Admin' }
            },
            {
                path: 'vehicle',
                loadChildren: './views/others/vehicle/vehicle.module#VehicleModule',
                // canActivate: [RoleGuard],
                data: { role: 'Advisor' }
                // data: { title: 'Vehicle', breadcrumb: 'VEHICLE'}
            },
            {
                path: 'finance',
                loadChildren: './views/others/finance/finance.module#FinanceModule',
                data: { title: 'Finance', breadcrumb: 'FINANCE' }
            },
            {
                path: 'companyusers',
                loadChildren: './views/others/companyusers/companyusers.module#CompanyusersModule',
                //  canActivate: [RoleGuard],
                data: { role: 'Admin' }
                // data: { title: 'Vehicle', breadcrumb: 'VEHICLE'}
            },
            {
                path: 'compsuppliers',
                loadChildren: './views/others/compsuppliers/compsuppliers.module#CompsuppliersModule',
                //  canActivate: [RoleGuard],
                data: { role: 'Admin' }
                // data: { title: 'Vehicle', breadcrumb: 'VEHICLE'}
            },
            {
                path: 'prospectiveorders',
                loadChildren: './views/others/prospectiveorders/prospectiveorders.module#ProspectiveordersModule',
                //  canActivate: [RoleGuard],
                data: { role: 'Admin' }
                // data: { title: 'Vehicle', breadcrumb: 'VEHICLE'}
            },
            {
                path: 'procurement',
                loadChildren: './views/others/procurement/procurement.module#ProcurementModule',
                data: { title: 'Procurement', breadcrumb: 'PROCUREMENT' }
            }
        ]
    },
    {
        path: '**',
        redirectTo: 'sessions/404'
    }
];


/***/ }),

/***/ "./src/app/globals.ts":
/*!****************************!*\
  !*** ./src/app/globals.ts ***!
  \****************************/
/*! exports provided: Globals */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Globals", function() { return Globals; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var Globals = /** @class */ (function () {
    function Globals() {
        this.role = 'test';
        this.userdetails = {};
        this.userName = '';
    }
    Globals.prototype.gettypeVehicle = function () {
        return 'link';
    };
    Globals.prototype.gettypeUsers = function () {
        return 'link';
    };
    Globals.prototype.gettypeProcure = function () {
        return 'link';
    };
    Globals.prototype.gettypeDASHBOARD = function () {
        return 'link';
    };
    Globals.prototype.gettypeManage = function () {
        // console.log("1-----------------"+ this.userdetails);
        if (this.userdetails.userAuthority === 'Super Admin') {
            return 'link';
        }
        else {
            return 'dropDown';
        }
    };
    Globals.prototype.gettypeOrders = function () {
        return 'link';
    };
    Globals.prototype.gettypeReports = function () {
        return 'link';
    };
    Globals = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], Globals);
    return Globals;
}());



/***/ }),

/***/ "./src/app/shared/animations/egret-animations.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/animations/egret-animations.ts ***!
  \*******************************************************/
/*! exports provided: egretAnimations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "egretAnimations", function() { return egretAnimations; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var reusable = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animation"])([
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
        opacity: '{{opacity}}',
        transform: 'scale({{scale}}) translate3d({{x}}, {{y}}, {{z}})'
    }),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('{{duration}} {{delay}} cubic-bezier(0.0, 0.0, 0.2, 1)', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])('*'))
], {
    params: {
        duration: '200ms',
        delay: '0ms',
        opacity: '0',
        scale: '1',
        x: '0',
        y: '0',
        z: '0'
    }
});
var egretAnimations = [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('animate', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('void => *', [Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["useAnimation"])(reusable)])]),
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('fadeInOut', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('0', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            opacity: 0,
            display: 'none'
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["state"])('1', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            opacity: 1,
            display: 'block'
        })),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('0 => 1', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms')),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('1 => 0', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('300ms'))
    ]),
];


/***/ }),

/***/ "./src/app/shared/components/breadcrumb/breadcrumb.component.html":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/breadcrumb/breadcrumb.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"breadcrumb-bar\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'simple'\">\r\n  <ul class=\"breadcrumb\">\r\n    <li *ngFor=\"let part of routeParts\"><a routerLink=\"/{{part.url}}\">{{part.breadcrumb | translate}}</a></li>\r\n  </ul>\r\n</div>\r\n\r\n<div class=\"breadcrumb-title\" *ngIf=\"layout.layoutConf.useBreadcrumb && layout.layoutConf.breadcrumb === 'title'\">\r\n  <h1 class=\"bc-title\">{{routeParts[routeParts.length -1]['breadcrumb'] | translate}}</h1>\r\n  <ul class=\"breadcrumb\" *ngIf=\"routeParts.length > 1\">\r\n    <li *ngFor=\"let part of routeParts\"><a routerLink=\"/{{part.url}}\" class=\"text-muted\">{{part.breadcrumb | translate}}</a></li>\r\n  </ul>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/breadcrumb/breadcrumb.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/breadcrumb/breadcrumb.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2JyZWFkY3J1bWIvYnJlYWRjcnVtYi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/components/breadcrumb/breadcrumb.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/components/breadcrumb/breadcrumb.component.ts ***!
  \**********************************************************************/
/*! exports provided: BreadcrumbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BreadcrumbComponent", function() { return BreadcrumbComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/route-parts.service */ "./src/app/shared/services/route-parts.service.ts");
/* harmony import */ var _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../shared/services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BreadcrumbComponent = /** @class */ (function () {
    // public isEnabled: boolean = true;
    function BreadcrumbComponent(router, routePartsService, activeRoute, layout) {
        var _this = this;
        this.router = router;
        this.routePartsService = routePartsService;
        this.activeRoute = activeRoute;
        this.layout = layout;
        this.routerEventSub = this.router.events
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }))
            .subscribe(function (routeChange) {
            _this.routeParts = _this.routePartsService.generateRouteParts(_this.activeRoute.snapshot);
            // generate url from parts
            _this.routeParts.reverse().map(function (item, i) {
                item.breadcrumb = _this.parseText(item);
                item.urlSegments.forEach(function (urlSegment, j) {
                    if (j === 0)
                        return item.url = "" + urlSegment.path;
                    item.url += "/" + urlSegment.path;
                });
                if (i === 0) {
                    return item;
                }
                // prepend previous part to current part
                item.url = _this.routeParts[i - 1].url + "/" + item.url;
                return item;
            });
        });
    }
    BreadcrumbComponent.prototype.ngOnInit = function () { };
    BreadcrumbComponent.prototype.ngOnDestroy = function () {
        if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
        }
    };
    BreadcrumbComponent.prototype.parseText = function (part) {
        part.breadcrumb = part.breadcrumb.replace(/{{([^{}]*)}}/g, function (a, b) {
            var r = part.params[b];
            return typeof r === 'string' ? r : a;
        });
        return part.breadcrumb;
    };
    BreadcrumbComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-breadcrumb',
            template: __webpack_require__(/*! ./breadcrumb.component.html */ "./src/app/shared/components/breadcrumb/breadcrumb.component.html"),
            styles: [__webpack_require__(/*! ./breadcrumb.component.scss */ "./src/app/shared/components/breadcrumb/breadcrumb.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _shared_services_route_parts_service__WEBPACK_IMPORTED_MODULE_2__["RoutePartsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"],
            _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_3__["LayoutService"]])
    ], BreadcrumbComponent);
    return BreadcrumbComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/customizer/customizer.component.html":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/customizer/customizer.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"app-customizer\">\r\n  <div class=\"handle\" *ngIf=\"!isCustomizerOpen\">\r\n    <button \r\n    mat-fab\r\n    color=\"warn\" \r\n    (click)=\"isCustomizerOpen = true\">\r\n      <mat-icon>settings</mat-icon>\r\n    </button>\r\n  </div>\r\n  <mat-card class=\"p-0\" *ngIf=\"isCustomizerOpen\">\r\n    <mat-card-title class=\"mat-bg-warn\">\r\n      <div class=\"card-title-text\">\r\n        <span>Settings</span>\r\n        <span fxFlex></span>\r\n        <button \r\n        class=\"card-control\" \r\n        mat-icon-button\r\n        (click)=\"isCustomizerOpen = false\">\r\n          <mat-icon>close</mat-icon>\r\n        </button>\r\n      </div>\r\n    </mat-card-title>\r\n    <mat-card-content>\r\n      <div class=\"pb-1\">\r\n        <h5 class=\"mt-0\">Layouts</h5>\r\n        <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"selectedLayout\" (change)=\"changeLayoutStyle($event)\">\r\n            <mat-radio-button [value]=\"'top'\"> Top Navigation </mat-radio-button>\r\n            <mat-radio-button [value]=\"'side'\"> Side Navigation </mat-radio-button>\r\n        </mat-radio-group>\r\n      </div>\r\n      <div class=\"pb-1\">\r\n        <mat-checkbox [(ngModel)]=\"isTopbarFixed\" (change)=\"toggleTopbarFixed($event)\" [disabled]=\"selectedLayout === 'top'\" [value]=\"selectedLayout !== 'top'\">Fixed Topbar</mat-checkbox>\r\n      </div>\r\n      <div class=\"pb-1\">\r\n        <mat-checkbox [(ngModel)]=\"layoutConf.breadcrumb\" (change)=\"toggleBreadcrumb($event)\">Use breadcrumb</mat-checkbox>\r\n      </div>\r\n      <div class=\"pb-1\">\r\n          <h6 class=\"mt-0\">Breadcrumb Style</h6>\r\n          <mat-radio-group fxLayout=\"column\" [(ngModel)]=\"layoutConf.breadcrumb\">\r\n              <mat-radio-button [value]=\"'simple'\"> Simple </mat-radio-button>\r\n              <mat-radio-button [value]=\"'title'\"> Simple with title </mat-radio-button>\r\n          </mat-radio-group>\r\n        </div>\r\n      <div class=\"pb-1 pos-rel\">\r\n        <h6 class=\"m-0 pb-1\">Choose a Navigation Style</h6>\r\n        <mat-radio-group \r\n        fxLayout=\"column\" \r\n        [(ngModel)]=\"selectedMenu\" \r\n        (change)=\"changeSidenav($event)\" \r\n        [disabled]=\"selectedLayout === 'top'\">\r\n          <mat-radio-button \r\n          *ngFor=\"let type of sidenavTypes\" \r\n          [value]=\"type.value\">\r\n            {{type.name}}\r\n          </mat-radio-button>\r\n        </mat-radio-group>\r\n      </div>\r\n      <mat-divider></mat-divider>\r\n      \r\n      <div class=\"pb-1 pt-1\">\r\n        <mat-checkbox [(ngModel)]=\"isRTL\" (change)=\"toggleDir($event)\">RTL</mat-checkbox>\r\n      </div>\r\n    </mat-card-content>\r\n  </mat-card>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/customizer/customizer.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/customizer/customizer.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#app-customizer {\n  position: fixed;\n  z-index: 100;\n  bottom: 16px;\n  right: 24px; }\n  #app-customizer .handle {\n    float: right; }\n  #app-customizer .mat-card-content {\n    padding: 1rem 1.5rem 2rem; }\n  .pos-rel {\n  position: relative;\n  z-index: 99; }\n  .pos-rel .olay {\n    position: absolute;\n    width: 100%;\n    height: 100%;\n    background: rgba(255, 255, 255, 0.5);\n    z-index: 100; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2N1c3RvbWl6ZXIvY3VzdG9taXplci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGdCQUFlO0VBQ2YsYUFBWTtFQUNaLGFBQVk7RUFDWixZQUFXLEVBT1o7RUFYRDtJQU1JLGFBQVksRUFDYjtFQVBIO0lBU0ksMEJBQXlCLEVBQzFCO0VBRUg7RUFDRSxtQkFBa0I7RUFDbEIsWUFBVyxFQVFaO0VBVkQ7SUFJSSxtQkFBa0I7SUFDbEIsWUFBVztJQUNYLGFBQVk7SUFDWixxQ0FBbUM7SUFDbkMsYUFBWSxFQUNiIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL2NvbXBvbmVudHMvY3VzdG9taXplci9jdXN0b21pemVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2FwcC1jdXN0b21pemVyIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgei1pbmRleDogMTAwO1xyXG4gIGJvdHRvbTogMTZweDtcclxuICByaWdodDogMjRweDtcclxuICAuaGFuZGxlIHtcclxuICAgIGZsb2F0OiByaWdodDtcclxuICB9XHJcbiAgLm1hdC1jYXJkLWNvbnRlbnQgIHtcclxuICAgIHBhZGRpbmc6IDFyZW0gMS41cmVtIDJyZW07XHJcbiAgfVxyXG59XHJcbi5wb3MtcmVsIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgei1pbmRleDogOTk7XHJcbiAgLm9sYXkge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDI1NSwgMjU1LCAyNTUsIC41KTtcclxuICAgIHotaW5kZXg6IDEwMDtcclxuICB9XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/shared/components/customizer/customizer.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/components/customizer/customizer.component.ts ***!
  \**********************************************************************/
/*! exports provided: CustomizerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomizerComponent", function() { return CustomizerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/navigation.service */ "./src/app/shared/services/navigation.service.ts");
/* harmony import */ var _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/layout.service */ "./src/app/shared/services/layout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CustomizerComponent = /** @class */ (function () {
    function CustomizerComponent(navService, layout) {
        this.navService = navService;
        this.layout = layout;
        this.isCustomizerOpen = false;
        this.sidenavTypes = [{
                name: 'Default Menu',
                value: 'default-menu'
            }, {
                name: 'Separator Menu',
                value: 'separator-menu'
            }, {
                name: 'Icon Menu',
                value: 'icon-menu'
            }];
        this.selectedMenu = 'icon-menu';
        this.isTopbarFixed = false;
        this.isRTL = false;
    }
    CustomizerComponent.prototype.ngOnInit = function () {
        this.layoutConf = this.layout.layoutConf;
        this.selectedLayout = this.layoutConf.navigationPos;
        this.isTopbarFixed = this.layoutConf.topbarFixed;
        this.isRTL = this.layoutConf.dir === 'rtl';
    };
    CustomizerComponent.prototype.changeLayoutStyle = function (data) {
        this.layout.publishLayoutChange({ navigationPos: this.selectedLayout });
    };
    CustomizerComponent.prototype.changeSidenav = function (data) {
        this.navService.publishNavigationChange(data.value);
    };
    CustomizerComponent.prototype.toggleBreadcrumb = function (data) {
        this.layout.publishLayoutChange({ breadcrumb: data.checked });
    };
    CustomizerComponent.prototype.toggleTopbarFixed = function (data) {
        this.layout.publishLayoutChange({ topbarFixed: data.checked });
    };
    CustomizerComponent.prototype.toggleDir = function (data) {
        var dir = data.checked ? 'rtl' : 'ltr';
        this.layout.publishLayoutChange({ dir: dir });
    };
    CustomizerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-customizer',
            template: __webpack_require__(/*! ./customizer.component.html */ "./src/app/shared/components/customizer/customizer.component.html"),
            styles: [__webpack_require__(/*! ./customizer.component.scss */ "./src/app/shared/components/customizer/customizer.component.scss")]
        }),
        __metadata("design:paramtypes", [_shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"],
            _shared_services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"]])
    ], CustomizerComponent);
    return CustomizerComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/header-side/header-side.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/header-side/header-side.component.ts ***!
  \************************************************************************/
/*! exports provided: HeaderSideComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderSideComponent", function() { return HeaderSideComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_theme_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../services/theme.service */ "./src/app/shared/services/theme.service.ts");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/esm5/ngx-translate-core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeaderSideComponent = /** @class */ (function () {
    function HeaderSideComponent(themeService, layout, translate, renderer) {
        this.themeService = themeService;
        this.layout = layout;
        this.translate = translate;
        this.renderer = renderer;
        this.currentLang = 'en';
        this.availableLangs = [{
                name: 'Kenya',
                code: 'en',
            }
            //, {
            //  name: 'Spanish',
            //   code: 'es',
            // }
        ];
    }
    HeaderSideComponent.prototype.ngOnInit = function () {
        this.coname = localStorage.getItem('cname');
        this.egretThemes = this.themeService.egretThemes;
        this.layoutConf = this.layout.layoutConf;
        this.translate.use(this.currentLang);
    };
    HeaderSideComponent.prototype.setLang = function (e) {
        console.log(e);
        this.translate.use(this.currentLang);
    };
    HeaderSideComponent.prototype.changeTheme = function (theme) {
        this.themeService.changeTheme(this.renderer, theme);
    };
    HeaderSideComponent.prototype.toggleNotific = function () {
        //...this.notificPanel.toggle();
    };
    HeaderSideComponent.prototype.toggleSidenav = function () {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    };
    HeaderSideComponent.prototype.toggleCollapse = function () {
        // compact --> full
        if (this.layoutConf.sidebarStyle === 'compact') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            }, { transitionClass: true });
        }
        // * --> compact
        this.layout.publishLayoutChange({
            sidebarStyle: 'compact'
        }, { transitionClass: true });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], HeaderSideComponent.prototype, "notificPanel", void 0);
    HeaderSideComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-side',
            template: __webpack_require__(/*! ./header-side.template.html */ "./src/app/shared/components/header-side/header-side.template.html"),
            styles: ['.element::-webkit-input-placeholder { color: blue;font-weight: bold;}']
        }),
        __metadata("design:paramtypes", [_services_theme_service__WEBPACK_IMPORTED_MODULE_1__["ThemeService"],
            _services_layout_service__WEBPACK_IMPORTED_MODULE_2__["LayoutService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])
    ], HeaderSideComponent);
    return HeaderSideComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/header-side/header-side.template.html":
/*!*************************************************************************!*\
  !*** ./src/app/shared/components/header-side/header-side.template.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-toolbar class=\"topbar topbar-white\">\r\n  <!-- Sidenav toggle button -->\r\n  <button \r\n  *ngIf=\"layoutConf.sidebarStyle !== 'compact'\"\r\n  mat-icon-button\r\n  id=\"sidenavToggle\" \r\n  (click)=\"toggleSidenav()\"\r\n  matTooltip=\"Toggle Hide/Open\">\r\n  <mat-icon>menu</mat-icon>\r\n  </button>\r\n  <!-- Sidenav toggle collapse -->\r\n  <button \r\n  *ngIf=\"layoutConf.sidebarStyle !== 'closed'\"\r\n  mat-icon-button\r\n  id=\"collapseToggle\"\r\n  fxHide.lt-md=\"true\" \r\n  (click)=\"toggleCollapse()\"\r\n  matTooltip=\"Toggle Collapse\"\r\n  class=\"toggle-collapsed\">\r\n  <mat-icon>chevron_left</mat-icon>\r\n  </button>\r\n  <!-- Search form -->\r\n  <div \r\n  fxFlex\r\n  fxHide.lt-sm=\"true\" \r\n  class=\"search-bar\">\r\n    <form class=\"top-search-form\">\r\n    <!-- <mat-icon role=\"img\">search</mat-icon> --> \r\n      <input style=\"color: crimson;\r\n      font-weight: bold;\"  placeholder={{coname}} type=\"text\">\r\n    </form>\r\n  </div>\r\n  <span fxFlex></span>\r\n  <!-- Language Switcher -->\r\n  <mat-select \r\n  placeholder=\"\"\r\n  id=\"langToggle\"\r\n  [style.width]=\"'auto'\"\r\n  name=\"currentLang\"\r\n  [(ngModel)]=\"currentLang\" \r\n  (selectionChange)=\"setLang($event)\">\r\n    <mat-option \r\n    *ngFor=\"let lang of availableLangs\" \r\n    [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n  </mat-select>\r\n  <!-- Theme Switcher -->\r\n <!-- <button \r\n  mat-icon-button\r\n  id=\"schemeToggle\" \r\n  [style.overflow]=\"'visible'\"\r\n  matTooltip=\"Color Schemes\"\r\n  [matMenuTriggerFor]=\"themeMenu\"\r\n  class=\"topbar-button-right\">\r\n    <mat-icon>format_color_fill</mat-icon>\r\n  </button>\r\n  <mat-menu #themeMenu=\"matMenu\">\r\n    <mat-grid-list\r\n    class=\"theme-list\" \r\n    cols=\"2\" \r\n    rowHeight=\"48px\">\r\n      <mat-grid-tile \r\n      *ngFor=\"let theme of egretThemes\"\r\n      (click)=\"changeTheme(theme)\">\r\n        <div mat-menu-item [title]=\"theme.name\">\r\n          <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n          <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n        </div>\r\n      </mat-grid-tile>\r\n    </mat-grid-list>\r\n  </mat-menu>   -->\r\n  <!-- Notification toggle button -->\r\n  <button \r\n  mat-icon-button\r\n  matTooltip=\"Notifications\" \r\n  (click)=\"toggleNotific()\"\r\n  [style.overflow]=\"'visible'\" \r\n  class=\"topbar-button-right\">\r\n    <mat-icon>notifications</mat-icon>\r\n    <span class=\"notification-number mat-bg-warn\">0</span>\r\n  </button>\r\n  <!-- Top left user menu -->\r\n  <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right img-button\">\r\n    <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n  </button>\r\n  <mat-menu #accountMenu=\"matMenu\">\r\n    <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n      <mat-icon>account_box</mat-icon>\r\n      <span>Profile</span>\r\n    </button>\r\n    <button mat-menu-item [routerLink]=\"['/profile/settings']\">\r\n      <mat-icon>settings</mat-icon>\r\n      <span>Account Settings</span>\r\n    </button>\r\n    <button mat-menu-item>\r\n      <mat-icon>notifications_off</mat-icon>\r\n      <span>Disable alerts</span>\r\n    </button>\r\n    <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n      <mat-icon>exit_to_app</mat-icon>\r\n      <span>Sign out</span>\r\n    </button>\r\n  </mat-menu>\r\n</mat-toolbar>"

/***/ }),

/***/ "./src/app/shared/components/header-top/header-top.component.html":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/header-top/header-top.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header-topnav mat-elevation-z2\">\r\n  <div class=\"container\">\r\n    <div class=\"topnav\">\r\n      <!-- App Logo -->\r\n      <div class=\"topbar-branding\">\r\n        <img src=\"assets/images/logo.png\" alt=\"\" class=\"app-logo\">\r\n      </div>\r\n\r\n      <ul class=\"menu\" *ngIf=\"!layoutConf.isMobile\">\r\n        <li *ngFor=\"let item of menuItems; let i = index;\">\r\n          <div *ngIf=\"item.type !== 'separator'\" routerLinkActive=\"open\">\r\n            <a matRipple routerLink=\"/{{item.state}}\" *ngIf=\"item.type === 'link'\">\r\n              <mat-icon>{{item.icon}}</mat-icon> \r\n              {{item.name | translate}}\r\n            </a>\r\n            <div *ngIf=\"item.type === 'dropDown'\">\r\n              <label matRipple for=\"drop-{{i}}\" class=\"toggle\"><mat-icon>{{item.icon}}</mat-icon> {{item.name | translate}}</label>\r\n              <a matRipple><mat-icon>{{item.icon}}</mat-icon> {{item.name | translate}}</a>\r\n              <input type=\"checkbox\" id=\"drop-{{i}}\" />\r\n              <ul>\r\n                <li *ngFor=\"let itemLvL2 of item.sub; let j = index;\" routerLinkActive=\"open\">\r\n                  <a matRipple routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\" \r\n                  *ngIf=\"itemLvL2.type !== 'dropDown'\">\r\n                    <mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>  \r\n                    {{itemLvL2.name | translate}}\r\n                  </a>\r\n                  \r\n                  <div *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n                    <label matRipple for=\"drop-{{i}}{{j}}\" class=\"toggle\">{{itemLvL2.name | translate}}</label>\r\n                    <a matRipple><mat-icon *ngIf=\"itemLvL2.icon\">{{itemLvL2.icon}}</mat-icon>  {{itemLvL2.name | translate}}</a>\r\n                    <input type=\"checkbox\" id=\"drop-{{i}}{{j}}\" />\r\n                    <!-- Level 3 -->\r\n                    <ul>\r\n                      <li *ngFor=\"let itemLvL3 of itemLvL2.sub\" routerLinkActive=\"open\">\r\n                        <a matRipple routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\">\r\n                          <mat-icon *ngIf=\"itemLvL3.icon\">{{itemLvL3.icon}}</mat-icon>\r\n                          {{itemLvL3.name | translate}}\r\n                        </a>\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n                </li>\r\n              </ul>\r\n            </div>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      <span fxFlex></span>\r\n      <!-- End Navigation -->\r\n      \r\n      <!-- Language Switcher -->\r\n      <mat-select \r\n      *ngIf=\"!layoutConf.isMobile\"\r\n      placeholder=\"\"\r\n      id=\"langToggle\"\r\n      [style.width]=\"'auto'\"\r\n      name=\"currentLang\"\r\n      [(ngModel)]=\"currentLang\" \r\n      (selectionChange)=\"setLang()\"\r\n      class=\"topbar-button-right\">\r\n        <mat-option \r\n        *ngFor=\"let lang of availableLangs\" \r\n        [value]=\"lang.code\" ngDefaultControl>{{ lang.name }}</mat-option>\r\n      </mat-select>\r\n      <!-- Theme Switcher -->\r\n    <!--   <button \r\n      mat-icon-button\r\n      id=\"schemeToggle\" \r\n      [style.overflow]=\"'visible'\"\r\n      matTooltip=\"Color Schemes\"\r\n      [matMenuTriggerFor]=\"themeMenu\"\r\n      class=\"topbar-button-right\">\r\n        <mat-icon>format_color_fill</mat-icon>\r\n      </button>\r\n      <mat-menu #themeMenu=\"matMenu\">\r\n        <mat-grid-list\r\n        class=\"theme-list\" \r\n        cols=\"2\" \r\n        rowHeight=\"48px\">\r\n          <mat-grid-tile \r\n          *ngFor=\"let theme of egretThemes\"\r\n          (click)=\"changeTheme(theme)\">\r\n            <div mat-menu-item [title]=\"theme.name\">\r\n              <div [style.background]=\"theme.baseColor\" class=\"egret-swatch\"></div>\r\n              <mat-icon class=\"active-icon\" *ngIf=\"theme.isActive\">check</mat-icon>\r\n            </div>\r\n          </mat-grid-tile>\r\n        </mat-grid-list>\r\n      </mat-menu>  -->\r\n      <!-- Notification toggle button -->\r\n      <button \r\n      mat-icon-button\r\n      matTooltip=\"Notifications\" \r\n      (click)=\"toggleNotific()\"\r\n      [style.overflow]=\"'visible'\" \r\n      class=\"topbar-button-right\">\r\n        <mat-icon>notifications</mat-icon>\r\n        <span class=\"notification-number mat-bg-warn\">0</span>\r\n      </button>\r\n      <!-- Top left user menu -->\r\n      <button mat-icon-button [matMenuTriggerFor]=\"accountMenu\" class=\"topbar-button-right mr-1 img-button\">\r\n        <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n      </button>\r\n      <mat-menu #accountMenu=\"matMenu\">\r\n        <button mat-menu-item [routerLink]=\"['/profile/overview']\">\r\n          <mat-icon>account_box</mat-icon>\r\n          <span>Profile</span>\r\n        </button>\r\n        <button mat-menu-item [routerLink]=\"['/profile/settings']\">\r\n          <mat-icon>settings</mat-icon>\r\n          <span>Account Settings</span>\r\n        </button>\r\n        <button mat-menu-item>\r\n          <mat-icon>notifications_off</mat-icon>\r\n          <span>Disable alerts</span>\r\n        </button>\r\n        <button mat-menu-item [routerLink]=\"['/sessions/signin']\">\r\n          <mat-icon>exit_to_app</mat-icon>\r\n          <span>Sign out</span>\r\n        </button>\r\n      </mat-menu>\r\n      <!-- Mobile screen menu toggle -->\r\n      <button \r\n      mat-icon-button \r\n      class=\"mr-1\" \r\n      (click)=\"toggleSidenav()\" \r\n      *ngIf=\"layoutConf.isMobile\">\r\n        <mat-icon>menu</mat-icon>\r\n      </button>\r\n\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/header-top/header-top.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/components/header-top/header-top.component.ts ***!
  \**********************************************************************/
/*! exports provided: HeaderTopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderTopComponent", function() { return HeaderTopComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/navigation.service */ "./src/app/shared/services/navigation.service.ts");
/* harmony import */ var _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../shared/services/theme.service */ "./src/app/shared/services/theme.service.ts");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/esm5/ngx-translate-core.js");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/layout.service */ "./src/app/shared/services/layout.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HeaderTopComponent = /** @class */ (function () {
    function HeaderTopComponent(layout, navService, themeService, translate, renderer) {
        this.layout = layout;
        this.navService = navService;
        this.themeService = themeService;
        this.translate = translate;
        this.renderer = renderer;
        this.egretThemes = [];
        this.currentLang = 'en';
        this.availableLangs = [{
                name: 'Kenya',
                code: 'en',
            }
            //, {
            //  name: 'Spanish',
            //   code: 'es',
            // }
        ];
    }
    HeaderTopComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutConf = this.layout.layoutConf;
        this.egretThemes = this.themeService.egretThemes;
        this.menuItemSub = this.navService.menuItems$
            .subscribe(function (res) {
            res = res.filter(function (item) { return item.type !== 'icon' && item.type !== 'separator'; });
            var limit = 4;
            var mainItems = res.slice(0, limit);
            if (res.length <= limit) {
                return _this.menuItems = mainItems;
            }
            var subItems = res.slice(limit, res.length - 1);
            mainItems.push({
                name: 'More',
                type: 'dropDown',
                tooltip: 'More',
                icon: 'more_horiz',
                sub: subItems
            });
            _this.menuItems = mainItems;
        });
    };
    HeaderTopComponent.prototype.ngOnDestroy = function () {
        this.menuItemSub.unsubscribe();
    };
    HeaderTopComponent.prototype.setLang = function () {
        this.translate.use(this.currentLang);
    };
    HeaderTopComponent.prototype.changeTheme = function (theme) {
        this.themeService.changeTheme(this.renderer, theme);
    };
    HeaderTopComponent.prototype.toggleNotific = function () {
        this.notificPanel.toggle();
    };
    HeaderTopComponent.prototype.toggleSidenav = function () {
        if (this.layoutConf.sidebarStyle === 'closed') {
            return this.layout.publishLayoutChange({
                sidebarStyle: 'full'
            });
        }
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], HeaderTopComponent.prototype, "notificPanel", void 0);
    HeaderTopComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-header-top',
            template: __webpack_require__(/*! ./header-top.component.html */ "./src/app/shared/components/header-top/header-top.component.html")
        }),
        __metadata("design:paramtypes", [_services_layout_service__WEBPACK_IMPORTED_MODULE_4__["LayoutService"],
            _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"],
            _shared_services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"],
            _angular_core__WEBPACK_IMPORTED_MODULE_0__["Renderer2"]])
    ], HeaderTopComponent);
    return HeaderTopComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts ***!
  \**********************************************************************************/
/*! exports provided: AdminLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminLayoutComponent", function() { return AdminLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/esm5/ngx-translate-core.js");
/* harmony import */ var _services_theme_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/theme.service */ "./src/app/shared/services/theme.service.ts");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import PerfectScrollbar from 'perfect-scrollbar';


var AdminLayoutComponent = /** @class */ (function () {
    function AdminLayoutComponent(router, translate, themeService, layout, media) {
        var _this = this;
        this.router = router;
        this.translate = translate;
        this.themeService = themeService;
        this.layout = layout;
        this.media = media;
        this.isModuleLoading = false;
        // private sidebarPS: PerfectScrollbar;
        // private bodyPS: PerfectScrollbar;
        // private headerFixedBodyPS: PerfectScrollbar;
        this.scrollConfig = {};
        this.layoutConf = {};
        // Close sidenav after route change in mobile
        this.routerEventSub = router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; }))
            .subscribe(function (routeChange) {
            _this.layout.adjustLayout({ route: routeChange.url });
        });
        // Translator init
        var browserLang = translate.getBrowserLang();
        translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
    }
    AdminLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.layoutConf = this.layout.layoutConf;
        // this.layout.adjustLayout();
        // console.log(this.layoutConf)
        // FOR MODULE LOADER FLAG
        this.moduleLoaderSub = this.router.events.subscribe(function (event) {
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouteConfigLoadStart"] || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["ResolveStart"]) {
                _this.isModuleLoading = true;
            }
            if (event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouteConfigLoadEnd"] || event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["ResolveEnd"]) {
                _this.isModuleLoading = false;
            }
        });
    };
    AdminLayoutComponent.prototype.onResize = function (event) {
        this.layout.adjustLayout(event);
    };
    AdminLayoutComponent.prototype.ngAfterViewInit = function () {
        // this.layoutConfSub = this.layout.layoutConf$.subscribe(change => {
        //   // this.initBodyPS(change)
        // })
    };
    // initBodyPS(layoutConf:any = {}) {
    //   if(layoutConf.navigationPos === 'side' && layoutConf.topbarFixed) {
    //     if (this.bodyPS) this.bodyPS.destroy();
    //     if (this.headerFixedBodyPS) this.headerFixedBodyPS.destroy();
    //     this.headerFixedBodyPS = new PerfectScrollbar('.rightside-content-hold', {
    //       suppressScrollX: true
    //     });
    //     this.scrollToTop('.rightside-content-hold');
    //   } else {
    //     if (this.bodyPS) this.bodyPS.destroy();
    //     if (this.headerFixedBodyPS) this.headerFixedBodyPS.destroy();
    //     this.bodyPS = new PerfectScrollbar('.main-content-wrap', {
    //       suppressScrollX: true
    //     });
    //     this.scrollToTop('.main-content-wrap');
    //   }
    // }
    AdminLayoutComponent.prototype.scrollToTop = function (selector) {
        if (document) {
            var element = document.querySelector(selector);
            element.scrollTop = 0;
        }
    };
    AdminLayoutComponent.prototype.ngOnDestroy = function () {
        if (this.moduleLoaderSub) {
            this.moduleLoaderSub.unsubscribe();
        }
        if (this.layoutConfSub) {
            this.layoutConfSub.unsubscribe();
        }
        if (this.routerEventSub) {
            this.routerEventSub.unsubscribe();
        }
    };
    AdminLayoutComponent.prototype.closeSidebar = function () {
        this.layout.publishLayoutChange({
            sidebarStyle: 'closed'
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('window:resize', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], AdminLayoutComponent.prototype, "onResize", null);
    AdminLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin-layout',
            template: __webpack_require__(/*! ./admin-layout.template.html */ "./src/app/shared/components/layouts/admin-layout/admin-layout.template.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            _ngx_translate_core__WEBPACK_IMPORTED_MODULE_3__["TranslateService"],
            _services_theme_service__WEBPACK_IMPORTED_MODULE_4__["ThemeService"],
            _services_layout_service__WEBPACK_IMPORTED_MODULE_5__["LayoutService"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_2__["ObservableMedia"]])
    ], AdminLayoutComponent);
    return AdminLayoutComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/layouts/admin-layout/admin-layout.template.html":
/*!***********************************************************************************!*\
  !*** ./src/app/shared/components/layouts/admin-layout/admin-layout.template.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"app-admin-wrap\" [dir]='layoutConf.dir'>\r\n  <!-- Header for top navigation layout -->\r\n  <!-- ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT -->\r\n  <app-header-top \r\n    *ngIf=\"layoutConf.navigationPos === 'top'\" \r\n    [notificPanel]=\"notificationPanel\">\r\n  </app-header-top>\r\n  <!-- Main Container -->\r\n  <mat-sidenav-container \r\n  [dir]='layoutConf.dir'\r\n  class=\"app-admin-container app-side-nav-container mat-drawer-transition\"\r\n  [ngClass]=\"{\r\n    'navigation-top': layoutConf.navigationPos === 'top',\r\n    'sidebar-full': layoutConf.sidebarStyle === 'full',\r\n    'sidebar-compact': layoutConf.sidebarStyle === 'compact' && layoutConf.navigationPos === 'side',\r\n    'sidebar-compact-big': layoutConf.sidebarStyle === 'compact-big' && layoutConf.navigationPos === 'side',\r\n    'layout-intransition': layoutConf.layoutInTransition,\r\n    'sidebar-opened': layoutConf.sidebarStyle !== 'closed' && layoutConf.navigationPos === 'side',\r\n    'sidebar-closed': layoutConf.sidebarStyle === 'closed',\r\n    'fixed-topbar': layoutConf.topbarFixed && layoutConf.navigationPos === 'side'\r\n  }\">\r\n  <!-- SIDEBAR -->\r\n  <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n  <app-sidebar-side *ngIf=\"layoutConf.navigationPos === 'side'\"></app-sidebar-side>\r\n  \r\n  <!-- Top navigation layout (navigation for mobile screen) -->\r\n  <!-- ONLY REQUIRED FOR **TOP** NAVIGATION MOBILE LAYOUT -->\r\n  <app-sidebar-top *ngIf=\"layoutConf.navigationPos === 'top' && layoutConf.isMobile\"></app-sidebar-top>\r\n\r\n    <!-- App content -->\r\n    <div class=\"main-content-wrap\" [perfectScrollbar]=\"scrollConfig\" [disabled]=\"layoutConf.topbarFixed\">\r\n      <!-- Header for side navigation layout -->\r\n      <!-- ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT -->\r\n      <app-header-side \r\n        *ngIf=\"layoutConf.navigationPos === 'side'\"\r\n        [notificPanel]=\"notificationPanel\">\r\n      </app-header-side>\r\n\r\n      <div class=\"rightside-content-hold\" [perfectScrollbar]=\"scrollConfig\" [disabled]=\"!layoutConf.topbarFixed\">\r\n        <!-- View Loader -->\r\n        <div class=\"view-loader\" *ngIf=\"isModuleLoading\">\r\n          <div class=\"spinner\">\r\n            <div class=\"double-bounce1 mat-bg-accent\"></div>\r\n            <div class=\"double-bounce2 mat-bg-primary\"></div>\r\n          </div>\r\n        </div>\r\n        <!-- Breadcrumb -->\r\n      <!--  <app-breadcrumb></app-breadcrumb>   -->\r\n        <!-- View outlet -->\r\n        <router-outlet></router-outlet>\r\n      </div>\r\n    </div>\r\n    <!-- View overlay for mobile navigation -->\r\n    <div class=\"sidebar-backdrop\"\r\n    [ngClass]=\"{'visible': layoutConf.sidebarStyle !== 'closed' && layoutConf.isMobile}\"\r\n    (click)=\"closeSidebar()\"></div>\r\n    \r\n    <!-- Notificaation bar -->\r\n    <mat-sidenav #notificationPanel mode=\"over\" class=\"\" position=\"end\">\r\n      <div class=\"navigation-hold\" fxLayout=\"column\">\r\n        <app-notifications [notificPanel]=\"notificationPanel\"></app-notifications>\r\n      </div>\r\n    </mat-sidenav>\r\n  </mat-sidenav-container>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/layouts/auth-layout/auth-layout.component.html":
/*!**********************************************************************************!*\
  !*** ./src/app/shared/components/layouts/auth-layout/auth-layout.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>"

/***/ }),

/***/ "./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts ***!
  \********************************************************************************/
/*! exports provided: AuthLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthLayoutComponent", function() { return AuthLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthLayoutComponent = /** @class */ (function () {
    function AuthLayoutComponent() {
    }
    AuthLayoutComponent.prototype.ngOnInit = function () {
    };
    AuthLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-auth-layout',
            template: __webpack_require__(/*! ./auth-layout.component.html */ "./src/app/shared/components/layouts/auth-layout/auth-layout.component.html")
        }),
        __metadata("design:paramtypes", [])
    ], AuthLayoutComponent);
    return AuthLayoutComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/layouts/print-layout/print-layout.component.html":
/*!************************************************************************************!*\
  !*** ./src/app/shared/components/layouts/print-layout/print-layout.component.html ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid \"  [perfectScrollbar]=\"scrollConfig\">\n\n\n<router-outlet></router-outlet>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/shared/components/layouts/print-layout/print-layout.component.scss":
/*!************************************************************************************!*\
  !*** ./src/app/shared/components/layouts/print-layout/print-layout.component.scss ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9jb21wb25lbnRzL2xheW91dHMvcHJpbnQtbGF5b3V0L3ByaW50LWxheW91dC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/shared/components/layouts/print-layout/print-layout.component.ts":
/*!**********************************************************************************!*\
  !*** ./src/app/shared/components/layouts/print-layout/print-layout.component.ts ***!
  \**********************************************************************************/
/*! exports provided: PrintLayoutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrintLayoutComponent", function() { return PrintLayoutComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PrintLayoutComponent = /** @class */ (function () {
    function PrintLayoutComponent() {
    }
    PrintLayoutComponent.prototype.ngOnInit = function () {
    };
    PrintLayoutComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-print-layout',
            template: __webpack_require__(/*! ./print-layout.component.html */ "./src/app/shared/components/layouts/print-layout/print-layout.component.html"),
            styles: [__webpack_require__(/*! ./print-layout.component.scss */ "./src/app/shared/components/layouts/print-layout/print-layout.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], PrintLayoutComponent);
    return PrintLayoutComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/notifications/notifications.component.html":
/*!******************************************************************************!*\
  !*** ./src/app/shared/components/notifications/notifications.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center mat-bg-primary pt-1 pb-1\">\r\n  <h6 class=\"m-0\">Notifications</h6>\r\n</div>\r\n<mat-nav-list class=\"notification-list\" role=\"list\">\r\n  <!-- Notification item -->\r\n  <mat-list-item *ngFor=\"let n of notifications\" class=\"notific-item\" role=\"listitem\" routerLinkActive=\"open\">\r\n    <mat-icon [color]=\"n.color\" class=\"notific-icon mr-1\">{{n.icon}}</mat-icon>\r\n    <a [routerLink]=\"[n.route || '/dashboard']\">\r\n      <div class=\"mat-list-text\">\r\n        <h4 class=\"message\">{{n.message}}</h4>\r\n        <small class=\"time text-muted\">{{n.time}}</small>\r\n      </div>\r\n    </a>\r\n  </mat-list-item>\r\n</mat-nav-list>\r\n<div class=\"text-center mt-1\" *ngIf=\"notifications.length\">\r\n  <small><a href=\"#\" (click)=\"clearAll($event)\">Clear all notifications</a></small>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/notifications/notifications.component.ts":
/*!****************************************************************************!*\
  !*** ./src/app/shared/components/notifications/notifications.component.ts ***!
  \****************************************************************************/
/*! exports provided: NotificationsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NotificationsComponent", function() { return NotificationsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(router) {
        this.router = router;
        // Dummy notifications
        this.notifications = [{
                message: 'New contact added',
                icon: 'assignment_ind',
                time: '1 min ago',
                route: '/inbox',
                color: 'primary'
            }, {
                message: 'New message',
                icon: 'chat',
                time: '4 min ago',
                route: '/chat',
                color: 'accent'
            }, {
                message: 'Server rebooted',
                icon: 'settings_backup_restore',
                time: '12 min ago',
                route: '/charts',
                color: 'warn'
            }];
    }
    NotificationsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.router.events.subscribe(function (routeChange) {
            if (routeChange instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]) {
                _this.notificPanel.close();
            }
        });
    };
    NotificationsComponent.prototype.clearAll = function (e) {
        e.preventDefault();
        this.notifications = [];
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], NotificationsComponent.prototype, "notificPanel", void 0);
    NotificationsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-notifications',
            template: __webpack_require__(/*! ./notifications.component.html */ "./src/app/shared/components/notifications/notifications.component.html")
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], NotificationsComponent);
    return NotificationsComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/sidebar-side/sidebar-side.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/shared/components/sidebar-side/sidebar-side.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar-panel\">\r\n  <div id=\"scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n    <!-- App Logo -->\r\n    <div class=\"branding default-bg\">\r\n      <img src=\"assets/images/logo-withoutname.png\" alt=\"\" class=\"app-logo\">\r\n      <!-- Two different logos for dark and light themes -->\r\n      <img \r\n      src=\"assets/images/logo-text-white.png\" \r\n      alt=\"\" \r\n      class=\"app-logo-text\"\r\n      *ngIf=\"themeService.activatedTheme?.name?.indexOf('dark') !== -1\">\r\n      <img \r\n      src=\"assets/images/logo-text.png\" \r\n      alt=\"\" \r\n      class=\"app-logo-text\"\r\n      *ngIf=\"themeService.activatedTheme?.name?.indexOf('dark') === -1\">\r\n    </div>\r\n\r\n    <!-- Sidebar user -->\r\n    <div class=\"app-user\">\r\n      <div class=\"app-user-photo\">\r\n        <img src=\"assets/images/face-7.jpg\" alt=\"\">\r\n      </div>\r\n      <span class=\"app-user-name mb-05\">\r\n        <mat-icon class=\"icon-xs text-muted\">lock</mat-icon> \r\n        {{userName}}\r\n      </span>\r\n      <!-- Small buttons -->\r\n      <div class=\"app-user-controls\">\r\n        <button \r\n        class=\"text-muted\"\r\n        mat-icon-button \r\n        mat-xs-button\r\n        [matMenuTriggerFor]=\"appUserMenu\">\r\n          <mat-icon>settings</mat-icon>\r\n        </button>\r\n        <button \r\n        class=\"text-muted\"\r\n        mat-icon-button \r\n        mat-xs-button\r\n        matTooltip=\"Inbox\"\r\n        routerLink=\"/inbox\">\r\n          <mat-icon>email</mat-icon>\r\n        </button>\r\n        <button \r\n        class=\"text-muted\"\r\n        mat-icon-button \r\n        mat-xs-button\r\n        matTooltip=\"Sign Out\"\r\n        \r\n        (click)=\"logout()\"\r\n        >\r\n\r\n        <!--routerLink=\"/sessions/signin\"-->\r\n          <mat-icon>exit_to_app</mat-icon>\r\n        </button>\r\n        <mat-menu #appUserMenu=\"matMenu\">\r\n            <button mat-menu-item routerLink=\"/profile/overview\">\r\n              <mat-icon>account_box</mat-icon>\r\n              <span>Profile</span>\r\n            </button>\r\n            <button mat-menu-item routerLink=\"/profile/settings\">\r\n              <mat-icon>settings</mat-icon>\r\n              <span>Account Settings</span>\r\n            </button>\r\n            <button mat-menu-item routerLink=\"/calendar\">\r\n              <mat-icon>date_range</mat-icon>\r\n              <span>Calendar</span>\r\n            </button>\r\n            <button mat-menu-item routerLink=\"/sessions/signin\">\r\n              <mat-icon>exit_to_app</mat-icon>\r\n              <span>Sign out</span>\r\n            </button>\r\n          </mat-menu>\r\n      </div>\r\n    </div>\r\n    <!-- Navigation -->\r\n    <app-sidenav [items]=\"menuItems\" [hasIconMenu]=\"hasIconTypeMenuItem\" [iconMenuTitle]=\"iconTypeMenuTitle\"></app-sidenav>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/sidebar-side/sidebar-side.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/shared/components/sidebar-side/sidebar-side.component.ts ***!
  \**************************************************************************/
/*! exports provided: SidebarSideComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarSideComponent", function() { return SidebarSideComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/navigation.service */ "./src/app/shared/services/navigation.service.ts");
/* harmony import */ var _services_theme_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/theme.service */ "./src/app/shared/services/theme.service.ts");
/* harmony import */ var app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");
/* harmony import */ var app_globals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





// import PerfectScrollbar from 'perfect-scrollbar';
var SidebarSideComponent = /** @class */ (function () {
    function SidebarSideComponent(navService, themeService, authService, globals) {
        this.navService = navService;
        this.themeService = themeService;
        this.authService = authService;
        this.globals = globals;
    }
    SidebarSideComponent.prototype.ngOnInit = function () {
        var _this = this;
        // console.log("-------------------- side bar ng on inti");
        this.navService.publishNavigationChange(this.globals.userdetails.userDepartment);
        this.userName = this.globals.userName;
        this.iconTypeMenuTitle = this.navService.iconTypeMenuTitle;
        this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
            _this.menuItems = menuItem;
            //Checks item list has any icon type.
            _this.hasIconTypeMenuItem = !!_this.menuItems.filter(function (item) { return item.type === 'icon'; }).length;
        });
    };
    SidebarSideComponent.prototype.logout = function () {
        // logout()
        this.authService.logout();
    };
    SidebarSideComponent.prototype.ngAfterViewInit = function () {
        // setTimeout(() => {
        //   this.sidebarPS = new PerfectScrollbar('#scroll-area', {
        //     suppressScrollX: true
        //   })
        // })
    };
    SidebarSideComponent.prototype.ngOnDestroy = function () {
        // if(this.sidebarPS) {
        //   this.sidebarPS.destroy();
        // }
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    };
    SidebarSideComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-side',
            template: __webpack_require__(/*! ./sidebar-side.component.html */ "./src/app/shared/components/sidebar-side/sidebar-side.component.html")
        }),
        __metadata("design:paramtypes", [_shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"],
            _services_theme_service__WEBPACK_IMPORTED_MODULE_2__["ThemeService"],
            app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            app_globals__WEBPACK_IMPORTED_MODULE_4__["Globals"]])
    ], SidebarSideComponent);
    return SidebarSideComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/sidebar-top/sidebar-top.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/shared/components/sidebar-top/sidebar-top.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidebar-panel\">\r\n  <div id=\"sidebar-top-scroll-area\" [perfectScrollbar] class=\"navigation-hold\" fxLayout=\"column\">\r\n    <app-sidenav [items]=\"menuItems\"></app-sidenav>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/components/sidebar-top/sidebar-top.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/shared/components/sidebar-top/sidebar-top.component.ts ***!
  \************************************************************************/
/*! exports provided: SidebarTopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidebarTopComponent", function() { return SidebarTopComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../shared/services/navigation.service */ "./src/app/shared/services/navigation.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import PerfectScrollbar from 'perfect-scrollbar';

var SidebarTopComponent = /** @class */ (function () {
    function SidebarTopComponent(navService) {
        this.navService = navService;
    }
    SidebarTopComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.menuItemsSub = this.navService.menuItems$.subscribe(function (menuItem) {
            _this.menuItems = menuItem.filter(function (item) { return item.type !== 'icon' && item.type !== 'separator'; });
        });
    };
    SidebarTopComponent.prototype.ngAfterViewInit = function () {
        // setTimeout(() => {
        //   this.sidebarPS = new PerfectScrollbar('#sidebar-top-scroll-area', {
        //     suppressScrollX: true
        //   })
        // })
    };
    SidebarTopComponent.prototype.ngOnDestroy = function () {
        // if(this.sidebarPS) {
        //   this.sidebarPS.destroy();
        // }
        if (this.menuItemsSub) {
            this.menuItemsSub.unsubscribe();
        }
    };
    SidebarTopComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidebar-top',
            template: __webpack_require__(/*! ./sidebar-top.component.html */ "./src/app/shared/components/sidebar-top/sidebar-top.component.html")
        }),
        __metadata("design:paramtypes", [_shared_services_navigation_service__WEBPACK_IMPORTED_MODULE_1__["NavigationService"]])
    ], SidebarTopComponent);
    return SidebarTopComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/sidenav/sidenav.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/components/sidenav/sidenav.component.ts ***!
  \****************************************************************/
/*! exports provided: SidenavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SidenavComponent", function() { return SidenavComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SidenavComponent = /** @class */ (function () {
    function SidenavComponent() {
        this.menuItems = [];
    }
    SidenavComponent.prototype.ngOnInit = function () { };
    // Only for demo purpose
    SidenavComponent.prototype.addMenuItem = function () {
        this.menuItems.push({
            name: 'ITEM',
            type: 'dropDown',
            tooltip: 'Item',
            icon: 'done',
            state: 'material',
            sub: [
                { name: 'SUBITEM', state: 'cards' },
                { name: 'SUBITEM', state: 'buttons' }
            ]
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('items'),
        __metadata("design:type", Array)
    ], SidenavComponent.prototype, "menuItems", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('hasIconMenu'),
        __metadata("design:type", Boolean)
    ], SidenavComponent.prototype, "hasIconTypeMenuItem", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])('iconMenuTitle'),
        __metadata("design:type", String)
    ], SidenavComponent.prototype, "iconTypeMenuTitle", void 0);
    SidenavComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-sidenav',
            template: __webpack_require__(/*! ./sidenav.template.html */ "./src/app/shared/components/sidenav/sidenav.template.html")
        }),
        __metadata("design:paramtypes", [])
    ], SidenavComponent);
    return SidenavComponent;
}());



/***/ }),

/***/ "./src/app/shared/components/sidenav/sidenav.template.html":
/*!*****************************************************************!*\
  !*** ./src/app/shared/components/sidenav/sidenav.template.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"sidenav-hold\">\r\n  <div class=\"icon-menu\" *ngIf=\"hasIconTypeMenuItem\">\r\n    <!-- Icon menu separator -->\r\n    <div class=\"mt-1 mb-1 nav-item-sep\">\r\n      <mat-divider [ngStyle]=\"{margin: '0 -24px'}\"></mat-divider>\r\n      <span class=\"text-muted icon-menu-title\">{{iconTypeMenuTitle}}</span>\r\n    </div>\r\n    <!-- Icon menu items -->\r\n    <div class=\"icon-menu-item\" *ngFor=\"let item of menuItems\">\r\n      <button *ngIf=\"!item.disabled && item.type === 'icon'\" mat-raised-button [matTooltip]=\"item.tooltip\" routerLink=\"/{{item.state}}\"\r\n        routerLinkActive=\"mat-bg-primary\">\r\n        <mat-icon>{{item.icon}}</mat-icon>\r\n      </button>\r\n    </div>\r\n  </div>\r\n\r\n  <ul appDropdown class=\"sidenav\">\r\n    <li *ngFor=\"let item of menuItems\" appDropdownLink routerLinkActive=\"open\">\r\n      <div class=\"nav-item-sep\" *ngIf=\"item.type === 'separator'\">\r\n        <mat-divider></mat-divider>\r\n        <span class=\"text-muted\">{{item.name | translate}}</span>\r\n      </div>\r\n      <div *ngIf=\"!item.disabled && item.type !== 'separator' && item.type !== 'icon'\" class=\"lvl1\">\r\n        <a routerLink=\"/{{item.state}}\" appDropdownToggle matRipple *ngIf=\"item.type === 'link'\">\r\n          <mat-icon>{{item.icon}}</mat-icon>\r\n          <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n          <span fxFlex></span>\r\n          <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\" *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n        </a>\r\n        <a [href]=\"item.state\" appDropdownToggle matRipple *ngIf=\"item.type === 'extLink'\" target=\"_blank\">\r\n          <mat-icon>{{item.icon}}</mat-icon>\r\n          <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n          <span fxFlex></span>\r\n          <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\" *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n        </a>\r\n\r\n        <!-- DropDown -->\r\n        <a *ngIf=\"item.type === 'dropDown'\" appDropdownToggle matRipple>\r\n          <mat-icon>{{item.icon}}</mat-icon>\r\n          <span class=\"item-name lvl1\">{{item.name | translate}}</span>\r\n          <span fxFlex></span>\r\n          <span class=\"menuitem-badge mat-bg-{{ badge.color }}\" [ngStyle]=\"{background: badge.color}\" *ngFor=\"let badge of item.badges\">{{ badge.value }}</span>\r\n          <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\r\n        </a>\r\n        <!-- LEVEL 2 -->\r\n        <ul class=\"submenu lvl2\" appDropdown *ngIf=\"item.type === 'dropDown'\">\r\n          <li *ngFor=\"let itemLvL2 of item.sub\" appDropdownLink routerLinkActive=\"open\">\r\n\r\n            <a routerLink=\"{{item.state ? '/'+item.state : ''}}/{{itemLvL2.state}}\" appDropdownToggle *ngIf=\"itemLvL2.type !== 'dropDown'\"\r\n              matRipple>\r\n              <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\r\n              <span fxFlex></span>\r\n            </a>\r\n\r\n            <a *ngIf=\"itemLvL2.type === 'dropDown'\" appDropdownToggle matRipple>\r\n              <span class=\"item-name lvl2\">{{itemLvL2.name | translate}}</span>\r\n              <span fxFlex></span>\r\n              <mat-icon class=\"menu-caret\">keyboard_arrow_right</mat-icon>\r\n            </a>\r\n\r\n            <!-- LEVEL 3 -->\r\n            <ul class=\"submenu lvl3\" appDropdown *ngIf=\"itemLvL2.type === 'dropDown'\">\r\n              <li *ngFor=\"let itemLvL3 of itemLvL2.sub\" appDropdownLink routerLinkActive=\"open\">\r\n                <a routerLink=\"{{item.state ? '/'+item.state : ''}}{{itemLvL2.state ? '/'+itemLvL2.state : ''}}/{{itemLvL3.state}}\" appDropdownToggle\r\n                  matRipple>\r\n                  <span class=\"item-name lvl3\">{{itemLvL3.name | translate}}</span>\r\n                </a>\r\n              </li>\r\n            </ul>\r\n\r\n          </li>\r\n        </ul>\r\n      </div>\r\n    </li>\r\n  </ul>\r\n</div>"

/***/ }),

/***/ "./src/app/shared/directives/dropdown-anchor.directive.ts":
/*!****************************************************************!*\
  !*** ./src/app/shared/directives/dropdown-anchor.directive.ts ***!
  \****************************************************************/
/*! exports provided: DropdownAnchorDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownAnchorDirective", function() { return DropdownAnchorDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dropdown-link.directive */ "./src/app/shared/directives/dropdown-link.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var DropdownAnchorDirective = /** @class */ (function () {
    function DropdownAnchorDirective(navlink) {
        this.navlink = navlink;
    }
    DropdownAnchorDirective.prototype.onClick = function (e) {
        this.navlink.toggle();
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], DropdownAnchorDirective.prototype, "onClick", null);
    DropdownAnchorDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appDropdownToggle]'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__["DropdownLinkDirective"])),
        __metadata("design:paramtypes", [_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_1__["DropdownLinkDirective"]])
    ], DropdownAnchorDirective);
    return DropdownAnchorDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/dropdown-link.directive.ts":
/*!**************************************************************!*\
  !*** ./src/app/shared/directives/dropdown-link.directive.ts ***!
  \**************************************************************/
/*! exports provided: DropdownLinkDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DropdownLinkDirective", function() { return DropdownLinkDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _dropdown_directive__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dropdown.directive */ "./src/app/shared/directives/dropdown.directive.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var DropdownLinkDirective = /** @class */ (function () {
    function DropdownLinkDirective(nav) {
        this.nav = nav;
    }
    Object.defineProperty(DropdownLinkDirective.prototype, "open", {
        get: function () {
            return this._open;
        },
        set: function (value) {
            this._open = value;
            if (value) {
                this.nav.closeOtherLinks(this);
            }
        },
        enumerable: true,
        configurable: true
    });
    DropdownLinkDirective.prototype.ngOnInit = function () {
        this.nav.addLink(this);
    };
    DropdownLinkDirective.prototype.ngOnDestroy = function () {
        this.nav.removeGroup(this);
    };
    DropdownLinkDirective.prototype.toggle = function () {
        this.open = !this.open;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Object)
    ], DropdownLinkDirective.prototype, "group", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostBinding"])('class.open'),
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], DropdownLinkDirective.prototype, "open", null);
    DropdownLinkDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appDropdownLink]'
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_dropdown_directive__WEBPACK_IMPORTED_MODULE_1__["AppDropdownDirective"])),
        __metadata("design:paramtypes", [_dropdown_directive__WEBPACK_IMPORTED_MODULE_1__["AppDropdownDirective"]])
    ], DropdownLinkDirective);
    return DropdownLinkDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/dropdown.directive.ts":
/*!*********************************************************!*\
  !*** ./src/app/shared/directives/dropdown.directive.ts ***!
  \*********************************************************/
/*! exports provided: AppDropdownDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppDropdownDirective", function() { return AppDropdownDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppDropdownDirective = /** @class */ (function () {
    function AppDropdownDirective(router) {
        this.router = router;
        this.navlinks = [];
    }
    AppDropdownDirective.prototype.closeOtherLinks = function (openLink) {
        this.navlinks.forEach(function (link) {
            if (link !== openLink) {
                link.open = false;
            }
        });
    };
    AppDropdownDirective.prototype.addLink = function (link) {
        this.navlinks.push(link);
    };
    AppDropdownDirective.prototype.removeGroup = function (link) {
        var index = this.navlinks.indexOf(link);
        if (index !== -1) {
            this.navlinks.splice(index, 1);
        }
    };
    AppDropdownDirective.prototype.getUrl = function () {
        return this.router.url;
    };
    AppDropdownDirective.prototype.ngOnInit = function () {
        var _this = this;
        this._router = this.router.events.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["filter"])(function (event) { return event instanceof _angular_router__WEBPACK_IMPORTED_MODULE_1__["NavigationEnd"]; })).subscribe(function (event) {
            _this.navlinks.forEach(function (link) {
                if (link.group) {
                    var routeUrl = _this.getUrl();
                    var currentUrl = routeUrl.split('/');
                    if (currentUrl.indexOf(link.group) > 0) {
                        link.open = true;
                        _this.closeOtherLinks(link);
                    }
                }
            });
        });
    };
    AppDropdownDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[appDropdown]'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AppDropdownDirective);
    return AppDropdownDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/egret-side-nav-toggle.directive.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/directives/egret-side-nav-toggle.directive.ts ***!
  \**********************************************************************/
/*! exports provided: EgretSideNavToggleDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EgretSideNavToggleDirective", function() { return EgretSideNavToggleDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var EgretSideNavToggleDirective = /** @class */ (function () {
    function EgretSideNavToggleDirective(media, sideNav) {
        this.media = media;
        this.sideNav = sideNav;
    }
    EgretSideNavToggleDirective.prototype.ngOnInit = function () {
        this.initSideNav();
    };
    EgretSideNavToggleDirective.prototype.ngOnDestroy = function () {
        if (this.screenSizeWatcher) {
            this.screenSizeWatcher.unsubscribe();
        }
    };
    EgretSideNavToggleDirective.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.sideNav.opened = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    EgretSideNavToggleDirective.prototype.initSideNav = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    EgretSideNavToggleDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({
            selector: '[EgretSideNavToggle]'
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Host"])()), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Self"])()), __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Optional"])()),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__["ObservableMedia"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]])
    ], EgretSideNavToggleDirective);
    return EgretSideNavToggleDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/font-size.directive.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/directives/font-size.directive.ts ***!
  \**********************************************************/
/*! exports provided: FontSizeDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FontSizeDirective", function() { return FontSizeDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};

var FontSizeDirective = /** @class */ (function () {
    function FontSizeDirective(fontSize, el) {
        this.fontSize = fontSize;
        this.el = el;
    }
    FontSizeDirective.prototype.ngOnInit = function () {
        this.el.nativeElement.fontSize = this.fontSize;
    };
    FontSizeDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({ selector: '[fontSize]' }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"])('fontSize')),
        __metadata("design:paramtypes", [String, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], FontSizeDirective);
    return FontSizeDirective;
}());



/***/ }),

/***/ "./src/app/shared/directives/scroll-to.directive.ts":
/*!**********************************************************!*\
  !*** ./src/app/shared/directives/scroll-to.directive.ts ***!
  \**********************************************************/
/*! exports provided: ScrollToDirective */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollToDirective", function() { return ScrollToDirective; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};

var ScrollToDirective = /** @class */ (function () {
    function ScrollToDirective(elmID, el) {
        this.elmID = elmID;
        this.el = el;
    }
    ScrollToDirective.prototype.ngOnInit = function () { };
    ScrollToDirective.prototype.currentYPosition = function () {
        // Firefox, Chrome, Opera, Safari
        if (self.pageYOffset)
            return self.pageYOffset;
        // Internet Explorer 6 - standards mode
        if (document.documentElement && document.documentElement.scrollTop)
            return document.documentElement.scrollTop;
        // Internet Explorer 6, 7 and 8
        if (document.body.scrollTop)
            return document.body.scrollTop;
        return 0;
    };
    ;
    ScrollToDirective.prototype.elmYPosition = function (eID) {
        var elm = document.getElementById(eID);
        var y = elm.offsetTop;
        var node = elm;
        while (node.offsetParent && node.offsetParent != document.body) {
            node = node.offsetParent;
            y += node.offsetTop;
        }
        return y;
    };
    ;
    ScrollToDirective.prototype.smoothScroll = function () {
        if (!this.elmID)
            return;
        var startY = this.currentYPosition();
        var stopY = this.elmYPosition(this.elmID);
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY);
            return;
        }
        var speed = Math.round(distance / 50);
        if (speed >= 20)
            speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for (var i = startY; i < stopY; i += step) {
                setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
                leapY += step;
                if (leapY > stopY)
                    leapY = stopY;
                timer++;
            }
            return;
        }
        for (var i = startY; i > stopY; i -= step) {
            setTimeout("window.scrollTo(0, " + leapY + ")", timer * speed);
            leapY -= step;
            if (leapY < stopY)
                leapY = stopY;
            timer++;
        }
        return false;
    };
    ;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ScrollToDirective.prototype, "smoothScroll", null);
    ScrollToDirective = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Directive"])({ selector: '[scrollTo]' }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Attribute"])('scrollTo')),
        __metadata("design:paramtypes", [String, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"]])
    ], ScrollToDirective);
    return ScrollToDirective;
}());



/***/ }),

/***/ "./src/app/shared/helpers/url.helper.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/helpers/url.helper.ts ***!
  \**********************************************/
/*! exports provided: getQueryParam */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getQueryParam", function() { return getQueryParam; });
function getQueryParam(prop) {
    var params = {};
    var search = decodeURIComponent(window.location.href.slice(window.location.href.indexOf('?') + 1));
    var definitions = search.split('&');
    definitions.forEach(function (val, key) {
        var parts = val.split('=', 2);
        params[parts[0]] = parts[1];
    });
    return (prop && prop in params) ? params[prop] : params;
}


/***/ }),

/***/ "./src/app/shared/inmemory-db/chat-db.ts":
/*!***********************************************!*\
  !*** ./src/app/shared/inmemory-db/chat-db.ts ***!
  \***********************************************/
/*! exports provided: ChatDB */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatDB", function() { return ChatDB; });
var ChatDB = /** @class */ (function () {
    function ChatDB() {
    }
    ChatDB.user = [
        {
            id: "7863a6802ez0e277a0f98534",
            name: "John Doe",
            avatar: "assets/images/face-1.jpg",
            status: "online",
            chatInfo: [
                {
                    chatId: "89564a680b3249760ea21fe77",
                    contactId: "323sa680b3249760ea21rt47",
                    contactName: "Frank Powell",
                    unread: 4,
                    lastChatTime: "2017-06-12T02:10:18.931Z"
                },
                {
                    chatId: "3289564a680b2134760ea21fe7753",
                    contactId: "14663a3406eb47ffa63d4fec9429cb71",
                    contactName: "Betty Diaz",
                    unread: 0,
                    lastChatTime: "2017-06-12T02:10:18.931Z"
                }
            ]
        }
    ];
    ChatDB.contacts = [
        {
            id: "323sa680b3249760ea21rt47",
            name: "Frank Powell",
            avatar: "assets/images/faces/13.jpg",
            status: "online",
            mood: ""
        },
        {
            id: "14663a3406eb47ffa63d4fec9429cb71",
            name: "Betty Diaz",
            avatar: "assets/images/faces/12.jpg",
            status: "online",
            mood: ""
        },
        {
            id: "43bd9bc59d164b5aea498e3ae1c24c3c",
            name: "Brian Stephens",
            avatar: "assets/images/faces/3.jpg",
            status: "online",
            mood: ""
        },
        {
            id: "3fc8e01f3ce649d1caf884fbf4f698e4",
            name: "Jacqueline Day",
            avatar: "assets/images/faces/16.jpg",
            status: "offline",
            mood: ""
        },
        {
            id: "e929b1d790ab49968ed8e34648553df4",
            name: "Arthur Mendoza",
            avatar: "assets/images/faces/10.jpg",
            status: "online",
            mood: ""
        },
        {
            id: "d6caf04bba614632b5fecf91aebf4564",
            name: "Jeremy Lee",
            avatar: "assets/images/faces/9.jpg",
            status: "offline",
            mood: ""
        },
        {
            id: "be0fb188c8e242f097fafa24632107e4",
            name: "Johnny Newman",
            avatar: "assets/images/faces/5.jpg",
            status: "offline",
            mood: ""
        },
        {
            id: "dea902191b964a68ba5f2d93cff37e13",
            name: "Jeffrey Little",
            avatar: "assets/images/faces/15.jpg",
            status: "online",
            mood: ""
        },
        {
            id: "0bf58f5ccc4543a9f8747350b7bda3c7",
            name: "Barbara Romero",
            avatar: "assets/images/faces/4.jpg",
            status: "offline",
            mood: ""
        },
        {
            id: "c5d7498bbcb84d81fc72168871ac6a6e",
            name: "Daniel James",
            avatar: "assets/images/faces/2.jpg",
            status: "offline",
            mood: ""
        },
        {
            id: "97bfbdd9413e46efdaca2010400fe18c",
            name: "Alice Sanders",
            avatar: "assets/images/faces/17.jpg",
            status: "offline",
            mood: ""
        }
    ];
    ChatDB.chatCollection = [
        {
            id: "89564a680b3249760ea21fe77",
            chats: [
                {
                    contactId: "323sa680b3249760ea21rt47",
                    text: "Do you ever find yourself falling into the “discount trap?”",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "7863a6802ez0e277a0f98534",
                    text: "Giving away your knowledge or product just to gain clients?",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "323sa680b3249760ea21rt47",
                    text: "Yes",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "7863a6802ez0e277a0f98534",
                    text: "Don’t feel bad. It happens to a lot of us",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "323sa680b3249760ea21rt47",
                    text: "Do you ever find yourself falling into the “discount trap?”",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "7863a6802ez0e277a0f98534",
                    text: "Giving away your knowledge or product just to gain clients?",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "323sa680b3249760ea21rt47",
                    text: "Yes",
                    time: "2018-02-32T08:45:28.291Z"
                },
                {
                    contactId: "7863a6802ez0e277a0f98534",
                    text: "Don’t feel bad. It happens to a lot of us",
                    time: "2018-02-32T08:45:28.291Z"
                }
            ]
        },
        {
            id: "3289564a680b2134760ea21fe7753",
            chats: [
                {
                    contactId: "14663a3406eb47ffa63d4fec9429cb71",
                    text: "Do you ever find yourself falling into the “discount trap?”",
                    time: "2018-03-32T08:45:28.291Z"
                },
                {
                    contactId: "7863a6802ez0e277a0f98534",
                    text: "Giving away your knowledge or product just to gain clients?",
                    time: "2018-03-32T08:45:28.291Z"
                },
                {
                    contactId: "14663a3406eb47ffa63d4fec9429cb71",
                    text: "Yes",
                    time: "2018-03-32T08:45:28.291Z"
                },
                {
                    contactId: "7863a6802ez0e277a0f98534",
                    text: "Don’t feel bad. It happens to a lot of us",
                    time: "2018-03-32T08:45:28.291Z"
                }
            ]
        }
    ];
    return ChatDB;
}());



/***/ }),

/***/ "./src/app/shared/inmemory-db/inmemory-db.service.ts":
/*!***********************************************************!*\
  !*** ./src/app/shared/inmemory-db/inmemory-db.service.ts ***!
  \***********************************************************/
/*! exports provided: InMemoryDataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InMemoryDataService", function() { return InMemoryDataService; });
/* harmony import */ var _chat_db__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./chat-db */ "./src/app/shared/inmemory-db/chat-db.ts");

var InMemoryDataService = /** @class */ (function () {
    function InMemoryDataService() {
    }
    InMemoryDataService.prototype.createDb = function () {
        return {
            'contacts': _chat_db__WEBPACK_IMPORTED_MODULE_0__["ChatDB"].contacts,
            'chat-collections': _chat_db__WEBPACK_IMPORTED_MODULE_0__["ChatDB"].chatCollection,
            'chat-user': _chat_db__WEBPACK_IMPORTED_MODULE_0__["ChatDB"].user
        };
    };
    return InMemoryDataService;
}());



/***/ }),

/***/ "./src/app/shared/pipes/excerpt.pipe.ts":
/*!**********************************************!*\
  !*** ./src/app/shared/pipes/excerpt.pipe.ts ***!
  \**********************************************/
/*! exports provided: ExcerptPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExcerptPipe", function() { return ExcerptPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ExcerptPipe = /** @class */ (function () {
    function ExcerptPipe() {
    }
    ExcerptPipe.prototype.transform = function (text, limit) {
        if (limit === void 0) { limit = 5; }
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    };
    ExcerptPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'excerpt' })
    ], ExcerptPipe);
    return ExcerptPipe;
}());



/***/ }),

/***/ "./src/app/shared/pipes/get-value-by-key.pipe.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/pipes/get-value-by-key.pipe.ts ***!
  \*******************************************************/
/*! exports provided: GetValueByKeyPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetValueByKeyPipe", function() { return GetValueByKeyPipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var GetValueByKeyPipe = /** @class */ (function () {
    function GetValueByKeyPipe() {
    }
    GetValueByKeyPipe.prototype.transform = function (value, id, property) {
        var filteredObj = value.find(function (item) {
            if (item.id !== undefined) {
                return item.id === id;
            }
            return false;
        });
        if (filteredObj) {
            return filteredObj[property];
        }
    };
    GetValueByKeyPipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({
            name: "getValueByKey",
            pure: false
        })
    ], GetValueByKeyPipe);
    return GetValueByKeyPipe;
}());



/***/ }),

/***/ "./src/app/shared/pipes/relative-time.pipe.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/pipes/relative-time.pipe.ts ***!
  \****************************************************/
/*! exports provided: RelativeTimePipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RelativeTimePipe", function() { return RelativeTimePipe; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var RelativeTimePipe = /** @class */ (function () {
    function RelativeTimePipe() {
    }
    RelativeTimePipe.prototype.transform = function (value) {
        if (!(value instanceof Date))
            value = new Date(value);
        var seconds = Math.floor(((new Date()).getTime() - value.getTime()) / 1000);
        var interval = Math.floor(seconds / 31536000);
        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    };
    RelativeTimePipe = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Pipe"])({ name: 'relativeTime' })
    ], RelativeTimePipe);
    return RelativeTimePipe;
}());



/***/ }),

/***/ "./src/app/shared/services/app-confirm/app-confirm.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/shared/services/app-confirm/app-confirm.component.ts ***!
  \**********************************************************************/
/*! exports provided: AppComfirmComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComfirmComponent", function() { return AppComfirmComponent; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var AppComfirmComponent = /** @class */ (function () {
    function AppComfirmComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    AppComfirmComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirm',
            template: "<h1 matDialogTitle>{{ data.title }}</h1>\n    <div mat-dialog-content>{{ data.message }}</div>\n    <div mat-dialog-actions>\n    <button \n    type=\"button\" \n    mat-raised-button\n    color=\"primary\" \n    (click)=\"dialogRef.close(true)\">OK</button>\n    &nbsp;\n    <span fxFlex></span>\n    <button \n    type=\"button\"\n    color=\"accent\"\n    mat-raised-button \n    (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>",
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_0__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialogRef"], Object])
    ], AppComfirmComponent);
    return AppComfirmComponent;
}());



/***/ }),

/***/ "./src/app/shared/services/app-confirm/app-confirm.service.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/services/app-confirm/app-confirm.service.ts ***!
  \********************************************************************/
/*! exports provided: AppConfirmService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppConfirmService", function() { return AppConfirmService; });
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_confirm_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-confirm.component */ "./src/app/shared/services/app-confirm/app-confirm.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppConfirmService = /** @class */ (function () {
    function AppConfirmService(dialog) {
        this.dialog = dialog;
    }
    AppConfirmService.prototype.confirm = function (data) {
        if (data === void 0) { data = {}; }
        data.title = data.title || 'Confirm';
        data.message = data.message || 'Are you sure?';
        var dialogRef;
        dialogRef = this.dialog.open(_app_confirm_component__WEBPACK_IMPORTED_MODULE_2__["AppComfirmComponent"], {
            width: '380px',
            disableClose: true,
            data: { title: data.title, message: data.message }
        });
        return dialogRef.afterClosed();
    };
    AppConfirmService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_0__["MatDialog"]])
    ], AppConfirmService);
    return AppConfirmService;
}());



/***/ }),

/***/ "./src/app/shared/services/app-loader/app-loader.component.css":
/*!*********************************************************************!*\
  !*** ./src/app/shared/services/app-loader/app-loader.component.css ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".mat-dialog-content {\r\n  min-height: 122px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL3NlcnZpY2VzL2FwcC1sb2FkZXIvYXBwLWxvYWRlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQWtCO0NBQ25CIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL3NlcnZpY2VzL2FwcC1sb2FkZXIvYXBwLWxvYWRlci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1hdC1kaWFsb2ctY29udGVudCB7XHJcbiAgbWluLWhlaWdodDogMTIycHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/shared/services/app-loader/app-loader.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/shared/services/app-loader/app-loader.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"text-center\">\r\n    <h6 class=\"m-0 pb-1\">{{ title }}</h6>\r\n    <div mat-dialog-content>\r\n        <mat-spinner [style.margin]=\"'auto'\"></mat-spinner>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/shared/services/app-loader/app-loader.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/shared/services/app-loader/app-loader.component.ts ***!
  \********************************************************************/
/*! exports provided: AppLoaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoaderComponent", function() { return AppLoaderComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppLoaderComponent = /** @class */ (function () {
    function AppLoaderComponent(dialogRef) {
        this.dialogRef = dialogRef;
    }
    AppLoaderComponent.prototype.ngOnInit = function () {
    };
    AppLoaderComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-app-loader',
            template: __webpack_require__(/*! ./app-loader.component.html */ "./src/app/shared/services/app-loader/app-loader.component.html"),
            styles: [__webpack_require__(/*! ./app-loader.component.css */ "./src/app/shared/services/app-loader/app-loader.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]])
    ], AppLoaderComponent);
    return AppLoaderComponent;
}());



/***/ }),

/***/ "./src/app/shared/services/app-loader/app-loader.service.ts":
/*!******************************************************************!*\
  !*** ./src/app/shared/services/app-loader/app-loader.service.ts ***!
  \******************************************************************/
/*! exports provided: AppLoaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppLoaderService", function() { return AppLoaderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _app_loader_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-loader.component */ "./src/app/shared/services/app-loader/app-loader.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AppLoaderService = /** @class */ (function () {
    function AppLoaderService(dialog) {
        this.dialog = dialog;
    }
    AppLoaderService.prototype.open = function (title) {
        if (title === void 0) { title = 'Please wait'; }
        this.dialogRef = this.dialog.open(_app_loader_component__WEBPACK_IMPORTED_MODULE_2__["AppLoaderComponent"], { disableClose: true, backdropClass: 'light-backdrop' });
        this.dialogRef.updateSize('200px');
        this.dialogRef.componentInstance.title = title;
        return this.dialogRef.afterClosed();
    };
    AppLoaderService.prototype.close = function () {
        if (this.dialogRef)
            this.dialogRef.close();
    };
    AppLoaderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"]])
    ], AppLoaderService);
    return AppLoaderService;
}());



/***/ }),

/***/ "./src/app/shared/services/auth/auth.guard.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/auth/auth.guard.ts ***!
  \****************************************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./auth.service */ "./src/app/shared/services/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthGuard = /** @class */ (function () {
    function AuthGuard(router, authservice) {
        /*
            if (authservice.isAuth){
              this.isAuthenticated = true;
                  }else{
                    this.isAuthenticated = false;
                  }  */
        this.router = router;
        this.authservice = authservice;
        this.isAuthenticated = false; // Set this value dynamically
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        if (this.authservice.isAuth()) {
            return true;
        }
        this.router.navigate(['/sessions/signin']);
        return false;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/shared/services/auth/auth.service.ts":
/*!******************************************************!*\
  !*** ./src/app/shared/services/auth/auth.service.ts ***!
  \******************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jwt-decode */ "./node_modules/jwt-decode/lib/index.js");
/* harmony import */ var jwt_decode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jwt_decode__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_globals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/globals */ "./src/app/globals.ts");
var __assign = (undefined && undefined.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthService = /** @class */ (function () {
    //user =null;
    function AuthService(http, router, globals) {
        this.http = http;
        this.router = router;
        this.globals = globals;
        this._url = "https://allautospares.com:447/";
        // private _url: string ="http://localhost:3000/";
        this.userdetails = {};
    }
    AuthService.prototype.clear = function () {
        localStorage.clear();
    };
    AuthService.prototype.setUserdetails = function (val) {
        this.userdetails = this.getDecodedAccessToken(val);
        this.globals.userdetails = this.userdetails;
        this.globals.userName = this.userdetails.userName;
        localStorage.setItem('token', val);
        // console.log("======================== "+JSON.stringify(this.userdetails.userAuthority));
    };
    AuthService.prototype.giveUserdetails = function () {
        // console.log(JSON.stringify(this.orders));
        return this.userdetails;
    };
    AuthService.prototype.giveUserName = function () {
        //  console.log(JSON.stringify(this.userdetails ));
        return this.userdetails.userName;
    };
    AuthService.prototype.getDecodedAccessToken = function (token) {
        try {
            return jwt_decode__WEBPACK_IMPORTED_MODULE_2__(token);
        }
        catch (Error) {
            return null;
        }
    };
    AuthService.prototype.registerUser = function (authData) {
        this.user = {
            email: authData.email,
            userId: Math.round(Math.random() * 10000).toString()
        };
    };
    /* login(authData: AuthData) {
         this.user = {
             email: authData.email,
             userId: Math.round(Math.random() * 10000).toString()
         };
 
     } */
    AuthService.prototype.logout = function () {
        this.userdetails = null;
        // this.clear();
        localStorage.removeItem('token');
        this.router.navigate(['/sessions/signin']);
    };
    AuthService.prototype.removetoken = function () {
        localStorage.removeItem('token');
    };
    AuthService.prototype.getUser = function () {
        return __assign({}, this.user);
    };
    AuthService.prototype.isAuth = function () {
        // console.log("-------------------++++  "+this.userdetails != null);
        //  return this.userdetails != null;
        /// this.userdetails != null;
        // return true;
        return localStorage.getItem('token') != null;
    };
    AuthService.prototype.isAdminAuth = function () {
        // console.log("-------------------++++  "+this.userdetails != null);
        //  return this.userdetails != null;
        /// this.userdetails != null;
        // return true;
        //if(this.globals.userdetails.userAuthority==='Admin'){
        return this.globals.userdetails.userAuthority;
        // }
        //return localStorage.getItem('token') != null;
    };
    AuthService.prototype.userLogin = function (userlog) {
        //  /corporate/register
        return this.http.post(this._url + 'all/users/login', userlog);
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"], app_globals__WEBPACK_IMPORTED_MODULE_4__["Globals"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/shared/services/auth/role.guard.ts":
/*!****************************************************!*\
  !*** ./src/app/shared/services/auth/role.guard.ts ***!
  \****************************************************/
/*! exports provided: RoleGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoleGuard", function() { return RoleGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/globals */ "./src/app/globals.ts");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./auth.service */ "./src/app/shared/services/auth/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RoleGuard = /** @class */ (function () {
    function RoleGuard(_authService, _router, globals) {
        this._authService = _authService;
        this._router = _router;
        this.globals = globals;
    }
    RoleGuard.prototype.canActivate = function (next, state) {
        //const user = this._authService.decode();
        // console.log(this.globals.userdetails.userAuthority+" ======================== "+next.data.role);
        if (this.globals.userdetails.userAuthority === next.data.role) {
            return true;
        }
        else {
            // navigate to not found page
            this._router.navigate(['/sessions/404']);
            return false;
        }
    };
    RoleGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], app_globals__WEBPACK_IMPORTED_MODULE_2__["Globals"]])
    ], RoleGuard);
    return RoleGuard;
}());



/***/ }),

/***/ "./src/app/shared/services/layout.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/services/layout.service.ts ***!
  \***************************************************/
/*! exports provided: LayoutService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LayoutService", function() { return LayoutService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _helpers_url_helper__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../helpers/url.helper */ "./src/app/shared/helpers/url.helper.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LayoutService = /** @class */ (function () {
    function LayoutService(router) {
        this.router = router;
        this.layoutConfSubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.layoutConf);
        this.layoutConf$ = this.layoutConfSubject.asObservable();
        this.fullWidthRoutes = ['shop'];
        this.setAppLayout();
    }
    LayoutService.prototype.setAppLayout = function () {
        //******** SET YOUR LAYOUT OPTIONS HERE *********
        this.layoutConf = {
            "navigationPos": "side",
            "sidebarStyle": "full",
            "dir": "ltr",
            "useBreadcrumb": true,
            "topbarFixed": false,
            "breadcrumb": "title" // simple, title
        };
        //******* Only for demo purpose ***
        this.setLayoutFromQuery();
        //**********************
    };
    LayoutService.prototype.publishLayoutChange = function (lc, opt) {
        var _this = this;
        if (opt === void 0) { opt = {}; }
        var duration = opt.duration || 250;
        if (!opt.transitionClass) {
            this.layoutConf = Object.assign(this.layoutConf, lc);
            return this.layoutConfSubject.next(this.layoutConf);
        }
        this.layoutConf = Object.assign(this.layoutConf, lc, { layoutInTransition: true });
        this.layoutConfSubject.next(this.layoutConf);
        setTimeout(function () {
            _this.layoutConf = Object.assign(_this.layoutConf, { layoutInTransition: false });
            _this.layoutConfSubject.next(_this.layoutConf);
        }, duration);
    };
    LayoutService.prototype.setLayoutFromQuery = function () {
        var layoutConfString = Object(_helpers_url_helper__WEBPACK_IMPORTED_MODULE_3__["getQueryParam"])('layout');
        try {
            this.layoutConf = JSON.parse(layoutConfString);
        }
        catch (e) { }
    };
    LayoutService.prototype.adjustLayout = function (options) {
        var _this = this;
        if (options === void 0) { options = {}; }
        var sidebarStyle;
        this.isMobile = this.isSm();
        this.currentRoute = options.route || this.currentRoute;
        sidebarStyle = this.isMobile ? 'closed' : this.layoutConf.sidebarStyle;
        if (this.currentRoute) {
            this.fullWidthRoutes.forEach(function (route) {
                if (_this.currentRoute.indexOf(route) !== -1) {
                    sidebarStyle = 'closed';
                }
            });
        }
        this.publishLayoutChange({
            isMobile: this.isMobile,
            sidebarStyle: sidebarStyle
        });
    };
    LayoutService.prototype.isSm = function () {
        return window.matchMedia("(max-width: 959px)").matches;
    };
    LayoutService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LayoutService);
    return LayoutService;
}());



/***/ }),

/***/ "./src/app/shared/services/navigation.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/shared/services/navigation.service.ts ***!
  \*******************************************************/
/*! exports provided: NavigationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavigationService", function() { return NavigationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var app_globals__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//iconMenu2 IMenuItem
var NavigationService = /** @class */ (function () {
    function NavigationService(globals) {
        this.globals = globals;
        /*
        setIconMenu(){
          ///this.globals.userdetails.userAuthority
        if('Advisor'==='Advisor'){
          this.iconMenu=[
            {
              name: 'DASHBOARD',
              type: 'link',
              tooltip: 'Dashboard',
              icon: 'dashboard',
              state: 'others'
            },
            {
              name: 'Vehicle',
              type: 'dropDown',
              tooltip: 'Vehicle',
              icon: 'format_line_spacing',
              state: 'vehicle',
              sub: [
                { name: 'New Vehicle', state: 'newvehicle' },
               // { name: 'Company Users', state: 'users' },
               // { name: 'Register Supplier', state: 'suppliers' },
                { name: 'Vehicle Details', state: 'vehicledetails' }
              ]
            }
            
          ]
        }else if( this.globals.userdetails.userAuthority==='Procurement' ){
          this.iconMenu=[
            {
              name: 'DASHBOARD',
              type: 'link',
              tooltip: 'Dashboard',
              icon: 'dashboard',
              state: 'others'
            },
            {
              name: 'Procure',
              type: 'link',
              tooltip: 'Procure',
              icon: 'format_line_spacing',
              state: 'procurement'
            },
            {
              name: 'Orders',
              type: 'link',
              tooltip: 'Documentation',
              icon: 'library_books',
              state: 'orders'
            }
          ]
        
        } else if(this.globals.userdetails.userAuthority==='Admin'){
          this.iconMenu=[
            {
              name: 'DASHBOARD',
              type: 'link',
              tooltip: 'Dashboard',
              icon: 'dashboard',
              state: 'others'
            },
            
            {
              name: 'Users',
              type: 'link',
              tooltip: 'Users',
              icon: 'person',
              state: 'companyusers'
            },
            {
              name: 'Vehicle',
              type: 'dropDown',
              tooltip: 'Vehicle',
              icon: 'format_line_spacing',
              state: 'vehicle',
              sub: [
                { name: 'New Vehicle', state: 'newvehicle' },
               // { name: 'Company Users', state: 'users' },
               // { name: 'Register Supplier', state: 'suppliers' },
                { name: 'Vehicle Details', state: 'vehicledetails' }
              ]
            },
            {
              name: 'Procure',
              type: 'link',
              tooltip: 'Procure',
              icon: 'format_line_spacing',
              state: 'procurement'
            },
            {
              name: 'Orders',
              type: 'link',
              tooltip: 'Documentation',
              icon: 'library_books',
              state: 'orders'
            }
        
        
        
          ]
        } else if(this.globals.userdetails.userAuthority==='Super Admin'){
          this.iconMenu=[
        
            {
              name: 'DASHBOARD',
              type: 'link',
              tooltip: 'Dashboard',
              icon: 'dashboard',
              state: 'others'
            },
            
            {
              name: 'Users',
              type: 'link',
              tooltip: 'Users',
              icon: 'person',
              state: 'companyusers'
            },
            {
              name: 'Vehicle',
              type: 'dropDown',
              tooltip: 'Vehicle',
              icon: 'format_line_spacing',
              state: 'vehicle',
              sub: [
                { name: 'New Vehicle', state: 'newvehicle' },
               // { name: 'Company Users', state: 'users' },
               // { name: 'Register Supplier', state: 'suppliers' },
                { name: 'Vehicle Details', state: 'vehicledetails' }
              ]
            },
            {
              name: 'Procure',
              type: 'link',
              tooltip: 'Procure',
              icon: 'format_line_spacing',
              state: 'procurement'
            },
            {
              name: 'Orders',
              type: 'link',
              tooltip: 'Documentation',
              icon: 'library_books',
              state: 'orders'
            },
            {
              name: 'Manage',
              type: 'dropDown',
              tooltip: 'Manage',
              icon: 'format_line_spacing',
              state: 'manage',
              sub: [
                { name: 'Add Company', state: 'addcompany' },
                { name: 'Company Users', state: 'users' },
                { name: 'Register Supplier', state: 'suppliers' },
                { name: 'Vehicle Details', state: 'vehicles' }
              ]
            }
        
          ]
        }
        
        
        }
        */
        //iconMenu=[
        this.iconMenuassessment = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            },
            {
                name: 'Vehicle',
                type: 'dropDown',
                tooltip: 'Vehicle',
                icon: 'format_line_spacing',
                state: 'vehicle',
                sub: [
                    { name: 'New Vehicle', state: 'newvehicle' },
                    // { name: 'Company Users', state: 'users' },
                    // { name: 'Register Supplier', state: 'suppliers' },
                    { name: 'Vehicle Details', state: 'vehicledetails' }
                ]
            },
            {
                name: 'Prospective Orders',
                type: 'link',
                tooltip: 'Prospective Orders',
                icon: 'person',
                state: 'prospectiveorders'
            },
            {
                name: 'Procure',
                type: 'link',
                tooltip: 'Procure',
                icon: 'format_line_spacing',
                state: 'procurement'
            }
        ];
        this.iconMenuassessmentAdvisor = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            },
            {
                name: 'Procure',
                type: 'link',
                tooltip: 'Procure',
                icon: 'format_line_spacing',
                state: 'procurement'
            }
        ];
        this.iconMenuprocurement = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            },
            {
                name: 'Orders',
                type: 'link',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'orders'
            },
            {
                name: 'Suppliers',
                type: 'link',
                tooltip: 'Suppliers',
                icon: 'person',
                state: 'compsuppliers'
            }
            /*  {
                name: 'Vehicle',
                type: 'dropDown',
                tooltip: 'Vehicle',
                icon: 'format_line_spacing',
                state: 'vehicle',
                sub: [
                  { name: 'New Vehicle', state: 'newvehicle' },
                 // { name: 'Company Users', state: 'users' },
                 // { name: 'Register Supplier', state: 'suppliers' },
                  { name: 'Vehicle Details', state: 'vehicledetails' }
                ]
              },
              {
                name: 'Procure',
                type: 'link',
                tooltip: 'Procure',
                icon: 'format_line_spacing',
                state: 'procurement'
              },
              {
                name: 'Orders',
                type: 'link',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'orders'
              }
          */
        ];
        this.iconMenuIT = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            },
            {
                name: 'Users',
                type: 'link',
                tooltip: 'Users',
                icon: 'person',
                state: 'companyusers'
            },
        ];
        this.iconMenuAdministration = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            }, {
                name: 'Vehicle',
                type: 'dropDown',
                tooltip: 'Vehicle',
                icon: 'format_line_spacing',
                state: 'vehicle',
                sub: [
                    { name: 'New Vehicle', state: 'newvehicle' },
                    // { name: 'Company Users', state: 'users' },
                    // { name: 'Register Supplier', state: 'suppliers' },
                    { name: 'Vehicle Details', state: 'vehicledetails' }
                ]
            },
            {
                name: 'Users',
                type: 'link',
                tooltip: 'Users',
                icon: 'person',
                state: 'companyusers'
            },
            {
                name: 'Suppliers',
                type: 'link',
                tooltip: 'Suppliers',
                icon: 'person',
                state: 'compsuppliers'
            },
            {
                name: 'Prospective Orders',
                type: 'link',
                tooltip: 'Prospective Orders',
                icon: 'person',
                state: 'prospectiveorders'
            },
            {
                name: 'Procure',
                type: 'link',
                tooltip: 'Procure',
                icon: 'format_line_spacing',
                state: 'procurement'
            },
            {
                name: 'Orders',
                type: 'link',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'orders'
            },
            {
                name: 'Finance',
                type: 'dropDown',
                tooltip: 'Finance',
                icon: 'adjust',
                state: 'finance',
                sub: [
                    { name: 'Open Bid', state: 'openbids' },
                    // { name: 'Company Users', state: 'users' },
                    // { name: 'Register Supplier', state: 'suppliers' },
                    { name: 'View Bidding Report', state: 'viewbiddingreport' }
                ]
            }
        ];
        this.iconMenusuperAdmin = [
            {
                name: 'HOME',
                type: 'iconw',
                tooltip: 'Home',
                icon: 'home',
                state: 'home'
            },
            {
                name: 'PROFILE',
                type: 'iconw',
                tooltip: 'Profile',
                icon: 'person',
                state: 'profile/overview'
            },
            {
                name: 'TOUR',
                type: 'iconw',
                tooltip: 'Tour',
                icon: 'flight_takeoff',
                state: 'tour'
            },
            {
                type: 'separator',
                name: 'Main Links'
            },
            {
                name: 'Vehicle',
                type: 'dropDown',
                tooltip: 'Vehicle',
                icon: 'format_line_spacing',
                state: 'vehicle',
                sub: [
                    { name: 'New Vehicle', state: 'newvehicle' },
                    // { name: 'Company Users', state: 'users' },
                    // { name: 'Register Supplier', state: 'suppliers' },
                    { name: 'Vehicle Details', state: 'vehicledetails' }
                ]
            },
            {
                name: 'Users',
                type: 'link',
                tooltip: 'Users',
                icon: 'person',
                state: 'companyusers'
            },
            {
                name: 'Suppliers',
                type: 'link',
                tooltip: 'Suppliers',
                icon: 'person',
                state: 'compsuppliers'
            },
            {
                name: 'Prospective Orders',
                type: 'link',
                tooltip: 'Prospective Orders',
                icon: 'person',
                state: 'prospectiveorders'
            },
            {
                name: 'Procure',
                type: 'link',
                tooltip: 'Procure',
                icon: 'format_line_spacing',
                state: 'procurement'
            },
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            },
            {
                name: 'Manage',
                type: 'dropDown',
                tooltip: 'Manage',
                icon: 'format_line_spacing',
                state: 'manage',
                sub: [
                    { name: 'Add Company', state: 'addcompany' },
                    { name: 'Company Users', state: 'users' },
                    { name: 'Register Supplier', state: 'suppliers' },
                    { name: 'Vehicle Details', state: 'vehicles' }
                ]
            },
            {
                name: 'Orders',
                type: 'link',
                tooltip: 'Documentation',
                icon: 'library_books',
                state: 'orders'
            },
            {
                name: 'Finance',
                type: 'dropDown',
                tooltip: 'Finance',
                icon: 'adjust',
                state: 'finance',
                sub: [
                    { name: 'Open Bid', state: 'openbids' },
                    // { name: 'Company Users', state: 'users' },
                    // { name: 'Register Supplier', state: 'suppliers' },
                    { name: 'View Bidding Report', state: 'viewbiddingreport' }
                ]
            }
        ];
        this.iconMenu = [
            {
                name: 'DASHBOARD',
                type: 'link',
                tooltip: 'Dashboard',
                icon: 'dashboard',
                state: 'others'
            }
        ];
        // Icon menu TITLE at the very top of navigation.
        // This title will appear if any icon type item is present in menu.
        this.iconTypeMenuTitle = 'Frequently Accessed';
        // sets iconMenu as default;
        //if(5===5){
        this.menuItems = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](this.iconMenu);
        // navigation component has subscribed to this Observable
        this.menuItems$ = this.menuItems.asObservable();
    }
    //}
    // Customizer component uses this method to change menu.
    // You can remove this method and customizer component.
    // Or you can customize this method to supply different menu for
    // different user type.  iconMenuassessmentAdvisor
    NavigationService.prototype.publishNavigationChange = function (menuType) {
        switch (menuType) {
            case 'Advisor':
                this.menuItems.next(this.iconMenuassessmentAdvisor);
                break;
            case 'Assessment':
                this.menuItems.next(this.iconMenuassessment);
                break;
            case 'Procurement':
                this.menuItems.next(this.iconMenuprocurement);
                break;
            case 'Administration':
                this.menuItems.next(this.iconMenuAdministration);
                break;
            case 'Super Administration':
                this.menuItems.next(this.iconMenusuperAdmin);
                break;
            default:
                this.menuItems.next(this.iconMenu);
        }
    };
    NavigationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [app_globals__WEBPACK_IMPORTED_MODULE_2__["Globals"]])
    ], NavigationService);
    return NavigationService;
}());



/***/ }),

/***/ "./src/app/shared/services/order.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/services/order.service.ts ***!
  \**************************************************/
/*! exports provided: OrderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderService", function() { return OrderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_add_operator_shareReplay__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/add/operator/shareReplay */ "./node_modules/rxjs-compat/_esm5/add/operator/shareReplay.js");
/* harmony import */ var rxjs_Rx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Rx */ "./node_modules/rxjs-compat/_esm5/Rx.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_add_observable_throw__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/add/observable/throw */ "./node_modules/rxjs-compat/_esm5/add/observable/throw.js");
/* harmony import */ var rxjs_add_operator_catch__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/add/operator/catch */ "./node_modules/rxjs-compat/_esm5/add/operator/catch.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/BehaviorSubject */ "./node_modules/rxjs-compat/_esm5/BehaviorSubject.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { map } from 'rxjs/operators';



//import { map, catchError } from 'rxjs/operators';






//import { UserLogin } from "../models/userlogin.model";
//import { Observable } from 'rxjs/Observable';
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({ 'Content-Type': 'application/json', })
};
var OrderService = /** @class */ (function () {
    function OrderService(http) {
        this.http = http;
        this._url = "https://allautospares.com:447/";
        this.groupSource = new rxjs_BehaviorSubject__WEBPACK_IMPORTED_MODULE_8__["BehaviorSubject"]({});
        this.group$ = this.groupSource.asObservable();
    }
    /*
    public getOrders(){
        return this.http.post(this._url+ 'get/orderlist',{}).map((response: Response) =>{
           // console.log (response.json());
          // response;
          // console.log (JSON.stringify(response));
            });
     }
    */
    OrderService.prototype.setMake = function (val) {
        this.allmakes = val;
    };
    OrderService.prototype.getMake = function () {
        return this.allmakes;
    };
    OrderService.prototype.setMakemodel = function (val) {
        this.allmakesmodels = val;
    };
    OrderService.prototype.getMakemodel = function () {
        return this.allmakesmodels;
    };
    OrderService.prototype.setModel = function (val) {
        this.allmodels = val;
    };
    OrderService.prototype.getModel = function () {
        return this.allmodels;
    };
    OrderService.prototype.setPart = function (val) {
        this.allparts = val;
    };
    OrderService.prototype.getPart = function () {
        return this.allparts;
    };
    OrderService.prototype.setPOrders = function (val) {
        this.porders = val;
    };
    OrderService.prototype.givePOrders = function () {
        // console.log(JSON.stringify(this.orders));
        return this.porders;
    };
    OrderService.prototype.setprospectivePOrders = function (val) {
        this.prospectivePOrders = val;
    };
    OrderService.prototype.giveprospectivePOrders = function () {
        // console.log(JSON.stringify(this.orders));
        return this.prospectivePOrders;
    };
    OrderService.prototype.setOrders = function (val) {
        this.orders = val;
    };
    OrderService.prototype.giveOrders = function () {
        // console.log(JSON.stringify(this.orders));
        return this.orders;
    };
    OrderService.prototype.setBids = function (val) {
        this.bids = val;
    };
    OrderService.prototype.giveBids = function () {
        // console.log(JSON.stringify(this.orders));
        return this.bids;
    };
    OrderService.prototype.getOrders = function () {
        //return this.http.get(this._url+ 'get/orderlist');
        return this.http.post(this._url + 'get/orderlist', { "order_sourceCompanyid": localStorage.getItem('cnumber') });
        /*   this._demoService.getFoods().subscribe(
              data => { this.foods = data},
              err => console.error(err),
             () => console.log('done loading foods')
           );  */
    };
    OrderService.prototype.getPOrders = function () {
        return this.http.post(this._url + 'get/porderlist', { "order_sourceCompanyid": localStorage.getItem('cnumber') });
    };
    OrderService.prototype.getprospectivePOrders = function () {
        return this.http.post(this._url + 'get/prospective/porderlist', { "order_sourceCompanyid": localStorage.getItem('cnumber') });
    };
    OrderService.prototype.getCountys = function () {
        //return this.http.get(this._url+ 'get/orderlist');
        return this.http.post(this._url + 'any/get/county', {}).map(function (res) { return console.log(res); })
            .take(1);
        /*   this._demoService.getFoods().subscribe(
              data => { this.foods = data},
              err => console.error(err),
             () => console.log('done loading foods')
           );  */
    };
    OrderService.prototype.getPartBids = function (partid) {
        //return this.http.get(this._url+ 'get/orderlist');
        return this.http.post(this._url + 'get/partbids', { partid: partid });
        /*   this._demoService.getFoods().subscribe(
              data => { this.foods = data},
              err => console.error(err),
             () => console.log('done loading foods')
           );  */
    };
    OrderService.prototype.closeopenBids = function (partid, awardcompanyId, bidOrderId, companyOfficer, part_status) {
        //return this.http.get(this._url+ 'get/orderlist');
        return this.http.post(this._url + 'bid/closeopen', { bidPartId: partid, awardcompanyId: awardcompanyId, bidOrderId: bidOrderId, companyOfficer: companyOfficer, part_status: part_status });
        /*   this._demoService.getFoods().subscribe(
              data => { this.foods = data},
              err => console.error(err),
             () => console.log('done loading foods')
           );  */
    };
    OrderService.prototype.gethistory = function (comclient_number, car_make, car_model, car_yom, partName) {
        //return this.http.get(this._url+ 'get/orderlist');
        return this.http.post(this._url + 'get/partpurchased/history', { comclient_number: comclient_number, car_make: car_make, car_model: car_model, car_yom: car_yom, partName: partName });
        /*   this._demoService.getFoods().subscribe(
              data => { this.foods = data},
              err => console.error(err),
             () => console.log('done loading foods')
           );  */
    };
    OrderService.prototype.postOrder = function (order) {
        /*  console.log ("q234567u8io==========================================================");
          return this.http.post<sparepartfromcorporate>(this._url+'corporate/order',order)
          .map(res => console.log(res))// ...and calling .json() on the response to return data
          .catch((error:any) => Observable.throw(error.json().error || 'Server error')) //...errors if
         // .subscribe()
          .shareReplay()
          .subscribe() */
        return this.http.post(this._url + 'corporate/order', order).map(function (res) { return console.log(res); })
            //.take(1);
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["shareReplay"])());
        //.map(res =>  console.log(res))
        //.take(1)
    };
    OrderService.prototype.postReiceivedVehicle = function (order) {
        /*  console.log ("q234567u8io==========================================================");
          return this.http.post<sparepartfromcorporate>(this._url+'corporate/order',order)
          .map(res => console.log(res))// ...and calling .json() on the response to return data
          .catch((error:any) => Observable.throw(error.json().error || 'Server error')) //...errors if
         // .subscribe()
          .shareReplay()
          .subscribe() */
        return this.http.post(this._url + 'corporate/receive/vehicle', order).map(function (res) { return console.log(res); });
        //.take(1);
        //.pipe(shareReplay())
        //.map(res =>  console.log(res))
        //.take(1)
        /*
        return this.http.post(this._url+'corporate/receive/vehicle', order,httpOptions).pipe(
        
          map(this.extractData),
        
          catchError(this.handleError)
        
        )
        */
    };
    OrderService.prototype.postAuthorisedParts = function (order) {
        /*  console.log ("q234567u8io==========================================================");
          return this.http.post<sparepartfromcorporate>(this._url+'corporate/order',order)
          .map(res => console.log(res))// ...and calling .json() on the response to return data
          .catch((error:any) => Observable.throw(error.json().error || 'Server error')) //...errors if
         // .subscribe()
          .shareReplay()
          .subscribe() */
        return this.http.post(this._url + 'push/authorised/parts', order).map(function (res) { return console.log(res); })
            //.take(1);
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["shareReplay"])());
        //.map(res =>  console.log(res))
        //.take(1)
    };
    OrderService.prototype.addCompany = function (company) {
        //  /corporate/register
        return this.http.post(this._url + 'corporate/register', company);
    };
    OrderService.prototype.createCompanyUser = function (user) {
        return this.http.post(this._url + 'corporate/users/create', user);
    };
    OrderService.prototype.getUsersCompany = function (companynumber) {
        //return this.http.get(this._url+ 'get/orderlist');
        return this.http.post(this._url + 'get/all/users', { "companynumber": companynumber });
        /*   this._demoService.getFoods().subscribe(
              data => { this.foods = data},
              err => console.error(err),
             () => console.log('done loading foods')
           );  */
    };
    OrderService.prototype.bidAward = function (bidawardh) {
        //  /corporate/register
        return this.http.post(this._url + 'bid/award', bidawardh);
    };
    OrderService.prototype.bidOffer = function (bidoffer) {
        //  /corporate/register
        return this.http.post(this._url + 'bid/offer', bidoffer);
    };
    OrderService.prototype.bidOffertt = function () {
        //  /corporate/register
        return this.http.post(this._url + 'add', { "hata": "hatari fire" }).map(function (res) { return console.log(res); })
            .take(1);
    };
    OrderService.prototype.getWondetails = function (partId) {
        //console.log(obj);
        return this.http.post(this._url + 'part/won/details', { "partId": partId });
        //.subscribe(res => console.log(res));
    };
    OrderService.prototype.postMakemodel = function (make, model) {
        //console.log(obj);
        return this.http.post(this._url + 'create/vehicle', { "carmake": make, "carmodel": model }).map(function (res) { return console.log(res); })
            .take(1);
        //.subscribe(res => console.log(res));
    };
    OrderService.prototype.postPart = function (partname, category) {
        //console.log(obj);
        return this.http.post(this._url + 'post/vehicle/parts', { "partname": partname, "category": category }).map(function (res) { return console.log(res); })
            .take(1);
        //.subscribe(res => console.log(res));
    };
    OrderService.prototype.getallPart = function () {
        return this.http.post(this._url + 'get/partall', {});
    };
    OrderService.prototype.getallVehiclefeed = function () {
        var _this = this;
        var sub = this.http.post(this._url + 'get/vehicleall', {}).subscribe(function (data) {
            sub.unsubscribe();
            // refresh the list
            //  this.setPart(data);
            _this.setMakemodel(data);
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    OrderService.prototype.getallPartfeed = function () {
        var _this = this;
        var sub = this.http.post(this._url + 'get/partall', {}).subscribe(function (data) {
            sub.unsubscribe();
            // refresh the list
            _this.setPart(data);
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    OrderService.prototype.getallVehicles = function () {
        //console.log(obj);
        return this.http.post(this._url + 'get/vehicleall', {});
        //.subscribe(res => console.log(res));
    };
    /*
      getpurchasehistory() {
        
        //console.log(obj);
       return this.http.post(this._url+'get/partpurchased/history',{});
          //.subscribe(res => console.log(res));
      } */
    OrderService.prototype.getsuppliers = function () {
        //console.log(obj);
        return this.http.post(this._url + 'get/suppliers', { "companyId": localStorage.getItem('cnumber') });
        //.subscribe(res => console.log(res));
    };
    OrderService.prototype.addsupplier = function (supplier) {
        //console.log(obj);
        return this.http.post(this._url + 'add/company/suppliers', supplier);
        //.subscribe(res => console.log(res));
    };
    OrderService.prototype.getSomeData = function () {
        if (this.someDataObservable) {
            return this.someDataObservable;
        }
        else {
            this.someDataObservable = this.http.post(this._url + 'add', { "hata": "hatari fire" }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["share"])());
            return this.someDataObservable;
        }
    };
    OrderService.prototype.createUser = function () {
        return this.http.post(this._url + 'add', { "hata": "hatari fire" }).map(function (res) { return res.json(); })
            .take(1);
    };
    OrderService.prototype.loginUserg = function () {
        return this.http.post(this._url + 'add', { "hata": "hatari fire" }, httpOptions).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["shareReplay"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["catchError"])(this.handleError));
    };
    // Private
    OrderService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    OrderService.prototype.handleError = function (error) {
        var errMsg;
        if (error instanceof Response) {
            var err = error || '';
            errMsg = error.status + " - " + (error.statusText || '') + " " + err;
        }
        else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return rxjs__WEBPACK_IMPORTED_MODULE_4__["Observable"].throw(errMsg);
    };
    OrderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]])
    ], OrderService);
    return OrderService;
}());



/***/ }),

/***/ "./src/app/shared/services/route-parts.service.ts":
/*!********************************************************!*\
  !*** ./src/app/shared/services/route-parts.service.ts ***!
  \********************************************************/
/*! exports provided: RoutePartsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RoutePartsService", function() { return RoutePartsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RoutePartsService = /** @class */ (function () {
    function RoutePartsService(router) {
        this.router = router;
    }
    RoutePartsService.prototype.ngOnInit = function () {
    };
    RoutePartsService.prototype.generateRouteParts = function (snapshot) {
        var routeParts = [];
        if (snapshot) {
            if (snapshot.firstChild) {
                routeParts = routeParts.concat(this.generateRouteParts(snapshot.firstChild));
            }
            if (snapshot.data['title'] && snapshot.url.length) {
                // console.log(snapshot.data['title'], snapshot.url)
                routeParts.push({
                    title: snapshot.data['title'],
                    breadcrumb: snapshot.data['breadcrumb'],
                    url: snapshot.url[0].path,
                    urlSegments: snapshot.url,
                    params: snapshot.params
                });
            }
        }
        return routeParts;
    };
    RoutePartsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], RoutePartsService);
    return RoutePartsService;
}());



/***/ }),

/***/ "./src/app/shared/services/theme.service.ts":
/*!**************************************************!*\
  !*** ./src/app/shared/services/theme.service.ts ***!
  \**************************************************/
/*! exports provided: ThemeService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThemeService", function() { return ThemeService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _helpers_url_helper__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../helpers/url.helper */ "./src/app/shared/helpers/url.helper.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var ThemeService = /** @class */ (function () {
    function ThemeService(document) {
        this.document = document;
        this.egretThemes = [{
                "name": "egret-dark-purple",
                "baseColor": "#9c27b0",
                "isActive": false
            }, {
                "name": "egret-dark-pink",
                "baseColor": "#e91e63",
                "isActive": false
            }, {
                "name": "egret-blue",
                "baseColor": "#247ba0",
                "isActive": false
            }, {
                "name": "egret-indigo",
                "baseColor": "#3f51b5",
                "isActive": true
            }];
    }
    // Invoked in AppComponent and apply 'activatedTheme' on startup
    ThemeService.prototype.applyMatTheme = function (r) {
        /*
        ****** (SET YOUR DEFAULT THEME HERE) *******
        * Assign new Theme to activatedTheme
        */
        // this.activatedTheme = this.egretThemes[0]; 
        this.activatedTheme = this.egretThemes[1];
        // this.activatedTheme = this.egretThemes[2]; 
        //this.activatedTheme = this.egretThemes[3];
        // *********** ONLY FOR DEMO **********
        this.setThemeFromQuery();
        // ************************************
        this.changeTheme(r, this.activatedTheme);
    };
    ThemeService.prototype.changeTheme = function (r, theme) {
        r.removeClass(this.document.body, this.activatedTheme.name);
        r.addClass(this.document.body, theme.name);
        this.flipActiveFlag(theme);
    };
    ThemeService.prototype.flipActiveFlag = function (theme) {
        var _this = this;
        this.egretThemes.forEach(function (t) {
            t.isActive = false;
            if (t.name === theme.name) {
                t.isActive = true;
                _this.activatedTheme = theme;
            }
        });
    };
    // *********** ONLY FOR DEMO **********
    ThemeService.prototype.setThemeFromQuery = function () {
        var themeStr = Object(_helpers_url_helper__WEBPACK_IMPORTED_MODULE_2__["getQueryParam"])('theme');
        try {
            this.activatedTheme = JSON.parse(themeStr);
            this.flipActiveFlag(this.activatedTheme);
        }
        catch (e) { }
    };
    ThemeService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_common__WEBPACK_IMPORTED_MODULE_1__["DOCUMENT"])),
        __metadata("design:paramtypes", [Document])
    ], ThemeService);
    return ThemeService;
}());



/***/ }),

/***/ "./src/app/shared/services/upload.service.ts":
/*!***************************************************!*\
  !*** ./src/app/shared/services/upload.service.ts ***!
  \***************************************************/
/*! exports provided: UploadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UploadService", function() { return UploadService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var url = 'http://localhost:8000';
var url2 = "https://allautospares.com:447/";
/*
{
  providedIn: 'root'
}*/
var UploadService = /** @class */ (function () {
    function UploadService(http) {
        this.http = http;
    }
    UploadService.prototype.upload = function (files, data) {
        var _this = this;
        // this will be the our resulting map
        var status = {};
        var i = 0;
        files.forEach(function (file) {
            // create a new multipart-form for every file
            var formData = new FormData();
            formData.append('file', file, file.name);
            //if(i ===  0){
            formData.append('field', JSON.stringify({ hi: "THIS IS MY MESSAGE", index: i }));
            //}
            // create a http-post request and pass the form
            // tell it to report the upload progress
            var req = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpRequest"]('POST', url + "/upload", formData, {
                reportProgress: true
            });
            // create a new progress-subject for every file
            var progress = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
            // send the http-request and subscribe for progress-updates
            _this.http.request(req).subscribe(function (event) {
                if (event.type === _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpEventType"].UploadProgress) {
                    // calculate the progress percentage
                    var percentDone = Math.round(100 * event.loaded / event.total);
                    // pass the percentage into the progress-stream
                    progress.next(percentDone);
                }
                else if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpResponse"]) {
                    // Close the progress-stream if we get an answer form the API
                    // The upload is complete
                    progress.complete();
                }
            });
            // Save every progress-observable in a map of all observables
            status[file.name] = {
                progress: progress.asObservable()
            };
            i += 1;
        });
        i = 0;
        // return the map of progress.observables
        return status;
    };
    UploadService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UploadService);
    return UploadService;
}());



/***/ }),

/***/ "./src/app/shared/shared.module.ts":
/*!*****************************************!*\
  !*** ./src/app/shared/shared.module.ts ***!
  \*****************************************/
/*! exports provided: SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedModule", function() { return SharedModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ngx-translate/core */ "./node_modules/@ngx-translate/core/esm5/ngx-translate-core.js");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _components_header_side_header_side_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/header-side/header-side.component */ "./src/app/shared/components/header-side/header-side.component.ts");
/* harmony import */ var _components_sidebar_side_sidebar_side_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/sidebar-side/sidebar-side.component */ "./src/app/shared/components/sidebar-side/sidebar-side.component.ts");
/* harmony import */ var _components_header_top_header_top_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/header-top/header-top.component */ "./src/app/shared/components/header-top/header-top.component.ts");
/* harmony import */ var _components_sidebar_top_sidebar_top_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/sidebar-top/sidebar-top.component */ "./src/app/shared/components/sidebar-top/sidebar-top.component.ts");
/* harmony import */ var _components_customizer_customizer_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/customizer/customizer.component */ "./src/app/shared/components/customizer/customizer.component.ts");
/* harmony import */ var _components_layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/layouts/admin-layout/admin-layout.component */ "./src/app/shared/components/layouts/admin-layout/admin-layout.component.ts");
/* harmony import */ var _components_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/layouts/auth-layout/auth-layout.component */ "./src/app/shared/components/layouts/auth-layout/auth-layout.component.ts");
/* harmony import */ var _components_notifications_notifications_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/notifications/notifications.component */ "./src/app/shared/components/notifications/notifications.component.ts");
/* harmony import */ var _components_sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/sidenav/sidenav.component */ "./src/app/shared/components/sidenav/sidenav.component.ts");
/* harmony import */ var _components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/breadcrumb/breadcrumb.component */ "./src/app/shared/components/breadcrumb/breadcrumb.component.ts");
/* harmony import */ var _services_app_confirm_app_confirm_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/app-confirm/app-confirm.component */ "./src/app/shared/services/app-confirm/app-confirm.component.ts");
/* harmony import */ var _services_app_loader_app_loader_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./services/app-loader/app-loader.component */ "./src/app/shared/services/app-loader/app-loader.component.ts");
/* harmony import */ var _directives_font_size_directive__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./directives/font-size.directive */ "./src/app/shared/directives/font-size.directive.ts");
/* harmony import */ var _directives_scroll_to_directive__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./directives/scroll-to.directive */ "./src/app/shared/directives/scroll-to.directive.ts");
/* harmony import */ var _directives_dropdown_directive__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./directives/dropdown.directive */ "./src/app/shared/directives/dropdown.directive.ts");
/* harmony import */ var _directives_dropdown_anchor_directive__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./directives/dropdown-anchor.directive */ "./src/app/shared/directives/dropdown-anchor.directive.ts");
/* harmony import */ var _directives_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./directives/dropdown-link.directive */ "./src/app/shared/directives/dropdown-link.directive.ts");
/* harmony import */ var _directives_egret_side_nav_toggle_directive__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./directives/egret-side-nav-toggle.directive */ "./src/app/shared/directives/egret-side-nav-toggle.directive.ts");
/* harmony import */ var _pipes_relative_time_pipe__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! ./pipes/relative-time.pipe */ "./src/app/shared/pipes/relative-time.pipe.ts");
/* harmony import */ var _pipes_excerpt_pipe__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! ./pipes/excerpt.pipe */ "./src/app/shared/pipes/excerpt.pipe.ts");
/* harmony import */ var _pipes_get_value_by_key_pipe__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! ./pipes/get-value-by-key.pipe */ "./src/app/shared/pipes/get-value-by-key.pipe.ts");
/* harmony import */ var _services_theme_service__WEBPACK_IMPORTED_MODULE_29__ = __webpack_require__(/*! ./services/theme.service */ "./src/app/shared/services/theme.service.ts");
/* harmony import */ var _services_layout_service__WEBPACK_IMPORTED_MODULE_30__ = __webpack_require__(/*! ./services/layout.service */ "./src/app/shared/services/layout.service.ts");
/* harmony import */ var _services_navigation_service__WEBPACK_IMPORTED_MODULE_31__ = __webpack_require__(/*! ./services/navigation.service */ "./src/app/shared/services/navigation.service.ts");
/* harmony import */ var _services_route_parts_service__WEBPACK_IMPORTED_MODULE_32__ = __webpack_require__(/*! ./services/route-parts.service */ "./src/app/shared/services/route-parts.service.ts");
/* harmony import */ var _services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_33__ = __webpack_require__(/*! ./services/auth/auth.guard */ "./src/app/shared/services/auth/auth.guard.ts");
/* harmony import */ var _services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_34__ = __webpack_require__(/*! ./services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var _services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_35__ = __webpack_require__(/*! ./services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _services_upload_service__WEBPACK_IMPORTED_MODULE_36__ = __webpack_require__(/*! ./services/upload.service */ "./src/app/shared/services/upload.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








// ONLY REQUIRED FOR **SIDE** NAVIGATION LAYOUT


// ONLY REQUIRED FOR **TOP** NAVIGATION LAYOUT


// ONLY FOR DEMO (Removable without changing any layout configuration)

// ALL TIME REQUIRED 







// DIRECTIVES






// PIPES



// SERVICES








/*
  Only Required if you want to use Angular Landing
  (https://themeforest.net/item/angular-landing-material-design-angular-app-landing-page/21198258)
*/
// import { LandingPageService } from '../shared/services/landing-page.service';
var classesToInclude = [
    _components_header_top_header_top_component__WEBPACK_IMPORTED_MODULE_10__["HeaderTopComponent"],
    _components_sidebar_top_sidebar_top_component__WEBPACK_IMPORTED_MODULE_11__["SidebarTopComponent"],
    _components_sidenav_sidenav_component__WEBPACK_IMPORTED_MODULE_16__["SidenavComponent"],
    _components_notifications_notifications_component__WEBPACK_IMPORTED_MODULE_15__["NotificationsComponent"],
    _components_sidebar_side_sidebar_side_component__WEBPACK_IMPORTED_MODULE_9__["SidebarSideComponent"],
    _components_header_side_header_side_component__WEBPACK_IMPORTED_MODULE_8__["HeaderSideComponent"],
    _components_layouts_admin_layout_admin_layout_component__WEBPACK_IMPORTED_MODULE_13__["AdminLayoutComponent"],
    _components_layouts_auth_layout_auth_layout_component__WEBPACK_IMPORTED_MODULE_14__["AuthLayoutComponent"],
    _components_breadcrumb_breadcrumb_component__WEBPACK_IMPORTED_MODULE_17__["BreadcrumbComponent"],
    _services_app_confirm_app_confirm_component__WEBPACK_IMPORTED_MODULE_18__["AppComfirmComponent"],
    _services_app_loader_app_loader_component__WEBPACK_IMPORTED_MODULE_19__["AppLoaderComponent"],
    _components_customizer_customizer_component__WEBPACK_IMPORTED_MODULE_12__["CustomizerComponent"],
    _directives_font_size_directive__WEBPACK_IMPORTED_MODULE_20__["FontSizeDirective"],
    _directives_scroll_to_directive__WEBPACK_IMPORTED_MODULE_21__["ScrollToDirective"],
    _directives_dropdown_directive__WEBPACK_IMPORTED_MODULE_22__["AppDropdownDirective"],
    _directives_dropdown_anchor_directive__WEBPACK_IMPORTED_MODULE_23__["DropdownAnchorDirective"],
    _directives_dropdown_link_directive__WEBPACK_IMPORTED_MODULE_24__["DropdownLinkDirective"],
    _directives_egret_side_nav_toggle_directive__WEBPACK_IMPORTED_MODULE_25__["EgretSideNavToggleDirective"],
    _pipes_relative_time_pipe__WEBPACK_IMPORTED_MODULE_26__["RelativeTimePipe"],
    _pipes_excerpt_pipe__WEBPACK_IMPORTED_MODULE_27__["ExcerptPipe"],
    _pipes_get_value_by_key_pipe__WEBPACK_IMPORTED_MODULE_28__["GetValueByKeyPipe"]
];
var SharedModule = /** @class */ (function () {
    function SharedModule() {
    }
    SharedModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["FlexLayoutModule"],
                _ngx_translate_core__WEBPACK_IMPORTED_MODULE_5__["TranslateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatOptionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_7__["MatDialogModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_6__["PerfectScrollbarModule"]
            ],
            entryComponents: [_services_app_confirm_app_confirm_component__WEBPACK_IMPORTED_MODULE_18__["AppComfirmComponent"], _services_app_loader_app_loader_component__WEBPACK_IMPORTED_MODULE_19__["AppLoaderComponent"]],
            providers: [
                _services_theme_service__WEBPACK_IMPORTED_MODULE_29__["ThemeService"],
                _services_layout_service__WEBPACK_IMPORTED_MODULE_30__["LayoutService"],
                _services_navigation_service__WEBPACK_IMPORTED_MODULE_31__["NavigationService"],
                _services_route_parts_service__WEBPACK_IMPORTED_MODULE_32__["RoutePartsService"],
                _services_auth_auth_guard__WEBPACK_IMPORTED_MODULE_33__["AuthGuard"],
                _services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_34__["AppConfirmService"],
                _services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_35__["AppLoaderService"],
                _services_upload_service__WEBPACK_IMPORTED_MODULE_36__["UploadService"]
                // LandingPageService
            ],
            declarations: classesToInclude,
            exports: classesToInclude
        })
    ], SharedModule);
    return SharedModule;
}());



/***/ }),

/***/ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/views/others/companyusers/users/user-popup/user-popup.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" >\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"username\"\n        [formControl]=\"itemForm.controls['username']\"\n        placeholder=\"Name\">\n      </mat-form-field>\n    </div>\n\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        type=\"email\"\n        name=\"email\"\n        [formControl]=\"itemForm.controls['email']\"\n        placeholder=\"Email\">\n      </mat-form-field>\n    </div>\n\n\n\n    <div fxFlex=\"50\"  class=\"pb-1\">\n      <mat-form-field class=\"full-width\">\n        <mat-label> User Department</mat-label>\n        <mat-select   (selectionChange)=\"changeClientdep($event.value)\" required>\n          <mat-option *ngFor=\"let department of departments\" [value]=\"department\">\n            {{department}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    &nbsp; &nbsp; &nbsp; &nbsp; \n    <div fxFlex=\"50\" class=\"pb-1\">\n      <mat-form-field class=\"full-width\">\n        <mat-label> User Authority</mat-label>\n        <mat-select   (selectionChange)=\"changeClientauth($event.value)\" required>\n          <mat-option *ngFor=\"let authority of authoritys\" [value]=\"authority\">\n            {{authority}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n     \n  </div>\n\n\n   \n\n    \n\n\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid\" (click)=\"submitb()\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.scss":
/*!**************************************************************************************!*\
  !*** ./src/app/views/others/companyusers/users/user-popup/user-popup.component.scss ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy9jb21wYW55dXNlcnMvdXNlcnMvdXNlci1wb3B1cC91c2VyLXBvcHVwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQWlCLCtCQUFzQyxFQUFHIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvb3RoZXJzL2NvbXBhbnl1c2Vycy91c2Vycy91c2VyLXBvcHVwL3VzZXItcG9wdXAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYmx1ZS1zbmFja2JhciB7IGJhY2tncm91bmQ6IHJnYigzMywgMjEyLCA5KSAhaW1wb3J0YW50O30iXX0= */"

/***/ }),

/***/ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/views/others/companyusers/users/user-popup/user-popup.component.ts ***!
  \************************************************************************************/
/*! exports provided: UserPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPopupComponent", function() { return UserPopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var UserPopupComponent = /** @class */ (function () {
    function UserPopupComponent(data, dialogRef, fb, orderservice, loader, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.loader = loader;
        this.snackBar = snackBar;
        this.departments = ["Advisor", "Assessment", "IT", "Procurement", "Audit", "Administration", "Human Resource", "Finance"];
        this.authoritys = ["Admin", "Advisor", "Assessor", "Procurement"];
        this.usercomp = {};
    }
    UserPopupComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    UserPopupComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            username: [item.username || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            email: [item.email || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    UserPopupComponent.prototype.submitb = function () {
        var _this = this;
        //.. this.dialogRef.close(this.itemForm.value)
        this.loader.open();
        this.usercomp.company_name = localStorage.getItem('cname');
        this.usercomp.companynumber = localStorage.getItem('cnumber');
        this.usercomp.userAuthority = this.selectedauth;
        this.usercomp.userDepartment = this.selecteddep;
        this.usercomp.userEmail = this.itemForm.value.email;
        this.usercomp.userName = this.itemForm.value.username;
        //this.usercomp.userPhone=
        this.subscription = this.orderservice.createCompanyUser(this.usercomp).subscribe(function (data) {
            // refresh the list
            _this.loader.close();
            _this.openSnackBar("Success");
            // this.items =this.temp = data;
            //   console.log(JSON.stringify( data )+"-------------------");
            _this.subscription.unsubscribe();
            // this.carsparesarray.car_make = null;
            _this.dialogRef.close();
            // this.clearObjectValues(this.carsparesarray);
            console.log(data + "----------------------=-===============-------jjjjjjjjj ");
            // this.basicForm.reset();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    UserPopupComponent.prototype.changeClientdep = function (data) {
        this.selecteddep = data;
    };
    UserPopupComponent.prototype.changeClientauth = function (data) {
        this.selectedauth = data;
    };
    UserPopupComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    UserPopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-popup',
            template: __webpack_require__(/*! ./user-popup.component.html */ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.html"),
            styles: [__webpack_require__(/*! ./user-popup.component.scss */ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__["AppLoaderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], UserPopupComponent);
    return UserPopupComponent;
}());



/***/ }),

/***/ "./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" >\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"username\"\n        [formControl]=\"itemForm.controls['username']\"\n        placeholder=\"Name\">\n      </mat-form-field>\n    </div>\n\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n      \n        name=\"phone\"\n        [formControl]=\"itemForm.controls['phone']\"\n        placeholder=\"Phone\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <mat-label> Status</mat-label>\n        <mat-select   (selectionChange)=\"changeClientstatus($event.value)\" required>\n          <mat-option *ngFor=\"let status of statuss\" [value]=\"status\">\n            {{status}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <mat-label> Supplies Category</mat-label>\n        <mat-select   (selectionChange)=\"changeClientcategory($event.value)\" required>\n          <mat-option *ngFor=\"let category of categorys\" [value]=\"category\">\n            {{category}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n   \n   \n  <div fxFlex=\"50\"  class=\"pr-1\">\n    <mat-form-field class=\"full-width\">\n      <input\n      matInput\n      name=\"location\"\n      [formControl]=\"itemForm.controls['location']\"\n      placeholder=\"Location\">\n    </mat-form-field>\n  </div>\n\n   \n\n    \n\n\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid\" (click)=\"submitb()\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9jb21wc3VwcGxpZXJzL3N1cHBsaWVycy9zdXBwbGllcnBvcHVwL3N1cHBsaWVycG9wdXAuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: SupplierpopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SupplierpopupComponent", function() { return SupplierpopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var SupplierpopupComponent = /** @class */ (function () {
    // usercomp: CompUser ={}
    //departments=["Assessment","Procurement","Audit","Administration","Finance"];
    //authoritys=["Admin","Advisor","Procurement"];
    // usercomp: CompUser ={}
    function SupplierpopupComponent(data, dialogRef, fb, orderservice, loader) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.loader = loader;
        this.supi = {};
        this.categorys = ["Bodyparts", "Mechanical", "Electrical"];
        this.statuss = ["Account", "No Account"];
    }
    SupplierpopupComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    SupplierpopupComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            username: [item.username || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            phone: [item.phone || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            location: [item.username || '',]
            //phone: [item.phone || '', Validators.required],
        });
    };
    SupplierpopupComponent.prototype.submitb = function () {
        var _this = this;
        //.. this.dialogRef.close(this.itemForm.value)
        this.loader.open();
        this.supi.companyId = localStorage.getItem('cnumber');
        this.supi.comsupplierName = this.itemForm.value.username;
        this.supi.comsupplierPhone = this.itemForm.value.phone;
        this.supi.comsupplierlocation = this.itemForm.value.location;
        this.supi.comsupplierCategory = this.selectedcategory;
        this.supi.comsupplierstatus = this.selectedstatus;
        this.subscription = this.orderservice.addsupplier(this.supi).subscribe(function (data) {
            // refresh the list
            _this.loader.close();
            // this.items =this.temp = data;
            //   console.log(JSON.stringify( data )+"-------------------");
            _this.subscription.unsubscribe();
            // this.carsparesarray.car_make = null;
            _this.dialogRef.close();
            // this.clearObjectValues(this.carsparesarray);
            // console.log(data+"----------------------=-===============-------jjjjjjjjj ");
            // this.basicForm.reset();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    SupplierpopupComponent.prototype.changeClientcategory = function (data) {
        this.selectedcategory = data;
    };
    SupplierpopupComponent.prototype.changeClientstatus = function (data) {
        this.selectedstatus = data;
    };
    SupplierpopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-supplierpopup',
            template: __webpack_require__(/*! ./supplierpopup.component.html */ "./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.html"),
            styles: [__webpack_require__(/*! ./supplierpopup.component.scss */ "./src/app/views/others/compsuppliers/suppliers/supplierpopup/supplierpopup.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__["AppLoaderService"]])
    ], SupplierpopupComponent);
    return SupplierpopupComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div  fxLayout=\"row\" fxLayoutWrap=\"wrap\" fxLayoutAlign=\"center center\">\n  <div fxFlex=\"80\" >\n    <mat-card class=\"p-0\">\n        <mat-card-title class=\"mat-bg-primary m-0\">\n        <div class=\"card-title-text\" style=\"width: 100%;text-align: center\">\n          <span style=\"width: 100%;text-align: center\" > Place Offer </span>\n          <span fxFlex></span>\n          \n        </div>\n      \n      </mat-card-title>\n\n      <mat-card-content >\n        <div fxFlex=\"100%\"  style=\"max-width: 100%\"  class=\"product-content-wrap\" [@animate]=\"{value:'*',params:{delay: '100ms', x:'50px'}}\">\n                   <div class=\"main-info mb-1\">\n            <p class=\"mb-05\" >DESCRIPTION</p>\n           \n            <div fxLayout=\"row\" style=\"display: flex; justify-content: space-between\">\n              <p class=\"text-muted p-line\" ><strong>Make: </strong> <span class=\"mat-color-default\">{{data.carmake}}</span></p>\n              <p class=\"text-muted p-line\"><strong>Y.O.M: </strong> <span class=\"mat-color-default\">{{data.caryom}}</span></p>\n              </div>\n              <div fxLayout=\"row\" style=\"display: flex; justify-content: space-between;\">\n                <p class=\"text-muted p-line\" ><strong>Model: </strong> <span class=\"mat-color-default\">{{data.carmodel}}</span></p>\n      \n                </div>\n\n                <div fxLayout=\"row\" style=\"display: flex;\n                justify-content: space-between;\">\n                   <p class=\"text-muted p-line\" ><strong>Part Name: </strong> <span class=\"mat-color-default\">{{data.partname}}</span></p>\n            \n                  <p class=\"text-muted p-line\"><strong>Supplier: </strong> <span class=\"mat-color-default\">{{data.suppliername}}</span></p>    \n                  </div>\n\n           \n            <div fxLayout=\"row\" style=\"display: flex;\n            justify-content: space-between;\">\n              <p class=\"text-muted p-line\" ><strong>Bid Price per Part: </strong> <span class=\"mat-color-default\">{{data.bidpriceper}}</span></p>\n              <p class=\"text-muted p-line\"><strong>Total Price: </strong> <span class=\"mat-color-default\">{{data.bidtotalprice}}</span></p>  \n              </div>\n\n            <p class=\"text-muted p-line\"><strong>Quantity: </strong> <span ><span class=\"mat-color-default\">{{data.quantity}}</span> </span></p>\n          \n          &nbsp;\n           &nbsp; \n            &nbsp; \n           \n          <p class=\"mb-05\" style=\"text-align: center\" >Offer Price per Item               Ksh: {{offerpricedata}}</p>\n\n          <input matInput [(ngModel)]=\"offerpricedata\" required>\n          &nbsp; \n          &nbsp; \n          &nbsp;    \n  <!--        <mat-form-field>\n            <input matInput [(ngModel)]=\"offerpricedata\" required>\n          </mat-form-field>\n\n          <form [formGroup]=\"basicFormo\">\n            <div fxLayout=\"row wrap\">\n                   <div fxFlex=\"100\" fxFlex.gt-xs=\"50\">\n                    <div class=\"pb-1\">\n                        <mat-form-field class=\"full-width\">\n                            <input \n                            matInput\n                            name=\"offerprice\"\n                            formControlName=\"offerprice\"\n                            placeholder=\"Enter your offer Price\" required>\n                        </mat-form-field>\n                        \n                    </div>\n      \n                         \n                </div>\n            </div>\n[disabled]=\"offerpricedata\"\n        </form>     [disabled]=\"basicFormo.invalid\"  -->\n      &nbsp; \n          &nbsp; \n          &nbsp;\n        <p class=\"mb-05\"  ></p>\n          <div fxLayout=\"row\" style=\"display: flex;\n          justify-content: space-between;\">\n            <button mat-raised-button class=\"mat-bg-warn\" (click)=close() >Back to Bids</button>\n            \n            <button mat-raised-button class=\"mat-bg-primary\" [disabled]=\"!offerpricedata\"  (click)= \"placeoffer()\">Place Offer</button>\n          </div>\n        </div>  \n      </div> \n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy9vcmRlcnMvcGFydGljdWxhcnBhcnQvYmlkb2ZmZXJkaWFsb2cvYmlkb2ZmZXJkaWFsb2cuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUM7RUFBaUIsK0JBQXNDLEVBQUciLCJmaWxlIjoic3JjL2FwcC92aWV3cy9vdGhlcnMvb3JkZXJzL3BhcnRpY3VsYXJwYXJ0L2JpZG9mZmVyZGlhbG9nL2JpZG9mZmVyZGlhbG9nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiIC5ibHVlLXNuYWNrYmFyIHsgYmFja2dyb3VuZDogcmdiKDMzLCAyMTIsIDkpICFpbXBvcnRhbnQ7fSJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: BidofferdialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BidofferdialogComponent", function() { return BidofferdialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var BidofferdialogComponent = /** @class */ (function () {
    function BidofferdialogComponent(data, dialogRef, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
        this.offerdata = {};
    }
    BidofferdialogComponent.prototype.ngOnInit = function () {
        this.basicFormo = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            offerprice: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required)
        });
        console.log(this.data.bidtotalprice + "---------------------------" + this.data.partname);
    };
    BidofferdialogComponent.prototype.placeoffer = function () {
        var _this = this;
        this.offerdata.bid_id = this.data.bid_id;
        this.offerdata.awardcompanyId = this.data.awardcompanyId;
        this.offerdata.bidSupplierId = this.data.bidSupplierId;
        this.offerdata.bidOrderId = this.data.bidOrderId;
        this.offerdata.bidPartId = this.data.bidPartId;
        this.offerdata.bidamount = this.data.bidamount;
        this.offerdata.carmake = this.data.carmake;
        this.offerdata.carmodel = this.data.carmodel;
        this.offerdata.caryom = this.data.caryom;
        this.offerdata.partname = this.data.partname;
        this.offerdata.bidpriceper = this.data.bidpriceper;
        this.offerdata.bidtotalprice = this.data.bidtotalprice;
        this.offerdata.suppliername = this.data.suppliername;
        this.offerdata.comment = this.data.comment;
        this.offerdata.quantity = this.data.quantity;
        this.offerdata.offeramount = this.offerpricedata;
        this.orderservice.bidOffer(this.offerdata).subscribe(function (data) {
            _this.openSnackBar("Success");
            // refresh the list
            // this.items =this.temp = data;
            //   this.orderservice.orders = data;
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            //  this.orderservice.setOrders("wewewewe");  
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    BidofferdialogComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    BidofferdialogComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, panelClass: ['blue-snackbar'], verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    BidofferdialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bidofferdialog',
            template: __webpack_require__(/*! ./bidofferdialog.component.html */ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.html"),
            styles: [__webpack_require__(/*! ./bidofferdialog.component.scss */ "./src/app/views/others/orders/particularpart/bidofferdialog/bidofferdialog.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_4__["egretAnimations"]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogRef"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"]])
    ], BidofferdialogComponent);
    return BidofferdialogComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div  fxLayout=\"row\" fxLayoutWrap=\"wrap\" fxLayoutAlign=\"center center\">\n  <div fxFlex=\"80\" >\n    <mat-card class=\"p-0\">\n        <mat-card-title class=\"mat-bg-primary m-0\">\n        <div class=\"card-title-text\" style=\"width: 100%;text-align: center\">\n          <span style=\"width: 100%;text-align: center\" > Confirm Purchase </span>\n          <span fxFlex></span>\n          \n        </div>\n      \n      </mat-card-title>\n<!--  [@animate]=\"{value:'*',params:{delay: '100ms', x:'50px'}} class=\"product-content-wrap\" \" -->\n      <mat-card-content >\n        <div fxFlex=\"100%\"  style=\"max-width: 100%\">\n                   <div class=\"main-info mb-1\" style=\"max-width: 100%\">\n            <p class=\"mb-05\" >DESCRIPTION</p>\n            <marquee direction=\"left\" speed=\"fast\" behavior=\"alternate\">{{data.comment}}</marquee>\n            <div fxLayout=\"row\" class=\"kk\" >\n            \n              <p class=\"text-muted p-line\" ><strong>Make: </strong> <span class=\"mat-color-default\">{{data.carmake}}</span></p>\n            \n            \n              <p class=\"text-muted p-line\"><strong>Y.O.M: </strong> <span class=\"mat-color-default\">{{data.caryom}}</span></p>\n            \n            </div>\n              <div fxLayout=\"row\" >\n                <p class=\"text-muted p-line\" ><strong>Model: </strong> <span class=\"mat-color-default\">{{data.carmodel}}</span></p>\n      \n                </div>\n\n                <div fxLayout=\"row\" class=\"kk\" >\n                   <p class=\"text-muted p-line\" ><strong>Part Name: </strong> <span class=\"mat-color-default\">{{data.partname}}</span></p>\n            \n                  <p class=\"text-muted p-line\"><strong>Supplier: </strong> <span class=\"mat-color-default\">{{data.suppliername}}</span></p>    \n                  </div>\n\n           \n            <div fxLayout=\"row\" style=\"display: flex;\n            justify-content: space-between;\">\n              <p class=\"text-muted p-line\" ><strong>Bid Price per Part: </strong> <span class=\"mat-color-default\">{{data.bidpriceper}}</span></p>\n              <p class=\"text-muted p-line\"><strong>Total Price: </strong> <span class=\"mat-color-default\">{{data.bidtotalprice}}</span></p>  \n              </div>\n\n            <p class=\"text-muted p-line\"><strong>Quantity: </strong> <span ><span class=\"mat-color-default\">{{data.quantity}}</span> </span></p>\n          </div>\n         \n          <div fxLayout=\"row\" style=\"display: flex;\n          justify-content: space-between;\">\n            <button mat-raised-button class=\"mat-bg-warn\" (click)=close() >Back to Bids</button>\n            \n            <button mat-raised-button class=\"mat-bg-primary\" (click)=awardbid() >Proceed to Purchase</button>\n          </div>\n        </div>\n\n\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n"

/***/ }),

/***/ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".kk{\n    display: flex; flex-direction: row;  justify-content: space-between;\n    align-items: center;\n}\n.blue-snackbar { background: rgb(33, 212, 9) !important;}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3Mvb3RoZXJzL29yZGVycy9wYXJ0aWN1bGFycGFydC9iaWRwdXJjaGFzZS1kaWFsb2cvYmlkLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGNBQWMsQ0FBQyxvQkFBb0IsRUFBRSwrQkFBK0I7SUFDcEUsb0JBQW9CO0NBQ3ZCO0FBQ0QsaUJBQWlCLHVDQUF1QyxDQUFDIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvb3RoZXJzL29yZGVycy9wYXJ0aWN1bGFycGFydC9iaWRwdXJjaGFzZS1kaWFsb2cvYmlkcHVyY2hhc2UtZGlhbG9nLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmtre1xuICAgIGRpc3BsYXk6IGZsZXg7IGZsZXgtZGlyZWN0aW9uOiByb3c7ICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cbi5ibHVlLXNuYWNrYmFyIHsgYmFja2dyb3VuZDogcmdiKDMzLCAyMTIsIDkpICFpbXBvcnRhbnQ7fSJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: BidpurchaseDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BidpurchaseDialogComponent", function() { return BidpurchaseDialogComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};





var BidpurchaseDialogComponent = /** @class */ (function () {
    function BidpurchaseDialogComponent(data, dialogRef, orderservice, loader, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.orderservice = orderservice;
        this.loader = loader;
        this.snackBar = snackBar;
    }
    BidpurchaseDialogComponent.prototype.ngOnInit = function () {
        console.log(this.data.bidtotalprice + "---------------------------" + this.data.partname);
    };
    /*
      bid_id:bid_id,
      awardcompanyId:this.Order.order_sourceCompanyid,
      bidSupplierId:bidSupplierId,
      bidOrderId:bidOrderId,
      bidPartId:bidPartId,
      bidamount:bidAmount,  */
    BidpurchaseDialogComponent.prototype.awardbid = function () {
        var _this = this;
        this.loader.open();
        this.subscription = this.orderservice.bidAward(this.data).subscribe(function (data) {
            _this.loader.close();
            _this.openSnackBar("Success");
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.subscription.unsubscribe();
            _this.dialogRef.close({ data: 'Success' });
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    BidpurchaseDialogComponent.prototype.close = function () {
        this.dialogRef.close({ data: 'Success' });
    };
    /*
    openSnackBar(text:string) {
      this.snackBar.open(text,'' ,{
        duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
      });
    }
    */
    BidpurchaseDialogComponent.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        // this.subscription.unsubscribe();
    };
    BidpurchaseDialogComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, panelClass: ['blue-snackbar'], verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    BidpurchaseDialogComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-bidpurchase-dialog',
            template: __webpack_require__(/*! ./bidpurchase-dialog.component.html */ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.html"),
            styles: [__webpack_require__(/*! ./bidpurchase-dialog.component.scss */ "./src/app/views/others/orders/particularpart/bidpurchase-dialog/bidpurchase-dialog.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_3__["egretAnimations"]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__["AppLoaderService"], _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"]])
    ], BidpurchaseDialogComponent);
    return BidpurchaseDialogComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/printer/printer.component.html":
/*!********************************************************************!*\
  !*** ./src/app/views/others/orders/printer/printer.component.html ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div  id=\"invoice\">\n\n    <div class=\"toolbar hidden-print\">\n        <div class=\"text-right\">\n            <button #myPrint id=\"printInvoice\" style=\"display:none\" class=\"btn btn-info\"><i class=\"fa fa-print\"></i> Print</button>\n      <!--    <button (click)=\"printComponent('myStuf')\" class=\"btn btn-info\"><i class=\"fa fa-file-pdf-o\"></i> Export as PDF</button> -->\n        </div>\n        <hr>\n    </div>\n\n\n    <div style=\"overflow-y:scroll; height:100%\" class=\"invoice \">\n        <div  id=\"myStuf\" style=\"min-width: 600px\">\n\n            <header>\n                <div class=\"row\">\n                    <div class=\"col\">\n                        <a target=\"_blank\" href=\"http://www.stantechmotors.co.ke\">\n                            <img src=\"http://www.stantechmotors.co.ke/assets/outputs/logo.png\" data-holder-rendered=\"true\" />\n                            </a>\n                    </div>\n                    <div class=\"col company-details\">\n                        <h2 class=\"name\">\n                            <a target=\"_blank\" href=\"https://lobianijs.com\">\n                            Stantech Motors\n                            </a>\n                        </h2>\n                        <div>Off Mombasa Road, Shimo la Tewa Rd</div>\n                        <div>Nairobi, Kenya</div>\n                        <div>P.O Box 78710-00507,</div>\n                        <div>Tel: +254 20 8070403</div>\n                        <div>http://www.stantechmotors.co.ke</div>\n\n\n\n                    </div>\n                </div>\n            </header>\n\n\n            <main>\n                <div class=\"row contacts\">\n                      <div class=\"col invoice-to\">\n\n              <!--          <div  class=\"text-gray-light\">Vehicle Details: </div> -->\n\n                        <h2 class=\"to\">{{Order.carReg_no}}</h2>\n                        <div style=\"color:black\" class=\"address\">Job Number: {{Order.jobcard_no}}</div>\n                        <div style=\"color:black\" class=\"email\">Source Officer : <a href=\"mailto:john@example.com\">{{Order.order_sourceOfficer}}</a></div>\n <div style=\"color:black\" class=\"date\">Date of Entry: {{Order.order_date}}</div>\n                    </div>\n\n\n\n\n                    <div class=\"col invoice-details\">\n                        <h1 class=\"invoice-id\">Purchased Part</h1>\n                        <div class=\"email\">Printed By:<a href=\"mailto:john@example.com\">{{personel}} </a></div>\n                    </div>\n\n\n\n                </div>\n                <table border=\"0\" cellspacing=\"0\" cellpadding=\"0\">\n                    <thead>\n                  <tr>\n                            <th>#</th>\n                            <th  style=\"color:black\" class=\"text-left\">Part Name</th>\n                            <th  style=\"color:black\" class=\"text-right\">Suplier</th>\n                             <th  style=\"color:black\" class=\"text-right\">Suplier Phone</th>\n                              <th  style=\"color:black\" class=\"text-right\">Order Status</th>\n                            <th  style=\"color:black\" class=\"text-right\">Price</th>\n                             <th  style=\"color:black\" class=\"text-right\">Quantity</th>\n                            <th  style=\"color:black\" class=\"text-right\">TOTAL</th>\n                        </tr>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"let mydata of bids let i = index\">\n                            <td class=\"no\">{{i}}</td>\n\n                            <td class=\"text-left\"><h3>\n                          <!--    <a target=\"_blank\" href=\"https://www.youtube.com/channel/UC_UMEcP_kF0z4E6KbxCpV1w\"> </a> -->  \n                               {{mydata.partname}}\n                                </h3>\n    \n                            </td>\n\n                            <td class=\"\">\n                            \n                             <h3>\n                                   {{mydata.supplier}}\n                                   </h3>\n                            </td>\n                            <td class=\"qty\">  {{mydata.supplier_num}}  </td>\n                            <td class=\"qty\">  {{mydata.confirmed}}   </td>\n\n                            <td class=\"qty\">  {{mydata.quantity}}   </td>\n                            <td class=\"qty\">  {{mydata.amount}}   </td>\n                            <td class=\"total\">  {{mydata.amount * mydata.quantity}}</td>\n                        </tr>\n                      \n                      \n                     <!-- \n                        <tr>\n                            <td class=\"no\">01</td>\n                            <td class=\"text-left\"><h3>Website Design</h3>Creating a recognizable design solution based on the company's existing visual identity</td>\n                            <td class=\"unit\">$40.00</td>\n                            <td class=\"qty\">30</td>\n                            <td class=\"total\">$1,200.00</td>\n                        </tr>\n                        <tr>\n                            <td class=\"no\">02</td>\n                            <td class=\"text-left\"><h3>Website Development</h3>Developing a Content Management System-based Website</td>\n                            <td class=\"unit\">$40.00</td>\n                            <td class=\"qty\">80</td>\n                            <td class=\"total\">$3,200.00</td>\n                        </tr>\n                        <tr>\n                            <td class=\"no\">03</td>\n                            <td class=\"text-left\"><h3>Search Engines Optimization</h3>Optimize the site for search engines (SEO)</td>\n                            <td class=\"unit\">$40.00</td>\n                            <td class=\"qty\">20</td>\n                            <td class=\"total\">$800.00</td>\n                        </tr>\n\n                        -->\n                    </tbody>\n\n\n                    <tfoot>\n                    <tr>\n                    </tr>\n\n                        <tr>\n                            <td colspan=\"2\"></td>\n                            <td colspan=\"2\">GRAND TOTAL</td>\n                            <td> {{totalAmount()}}  </td>\n                        </tr>\n\n                    </tfoot>\n                </table>\n\n\n\n                <div class=\"notices\">\n                    <div>NOTICE:</div>\n                    <div class=\"notice\">This is a system generated report.</div>\n                </div>\n            </main>\n\n\n\n            <footer>\n               This is a system generated report.\n            </footer>\n\n\n\n\n        </div>\n        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->\n        <div></div>\n    </div>\n\n</div>\n\n"

/***/ }),

/***/ "./src/app/views/others/orders/printer/printer.component.ts":
/*!******************************************************************!*\
  !*** ./src/app/views/others/orders/printer/printer.component.ts ***!
  \******************************************************************/
/*! exports provided: PrinterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrinterComponent", function() { return PrinterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material";


var PrinterComponent = /** @class */ (function () {
    function PrinterComponent(router2, route, orderservice) {
        var _this = this;
        this.router2 = router2;
        this.route = route;
        this.orderservice = orderservice;
        this.myData = [];
        this.completed = [];
        this.myParts = [];
        this.total = 0;
        this.bids = [];
        this.personel = "";
        this.company = "";
        console.log("IN THE NEW WINDOW.....");
        this.productID = this.route.snapshot.params['id'];
        console.log("ID IS....", this.productID);
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.Order = data;
            console.log(JSON.stringify(data) + "-------------------");
            _this.Order = _this.Order.find(function (x) { return x.order_id == _this.productID; });
            console.log("ORDERS ARE", _this.Order);
            _this.findByName2("Complete", _this.Order.autorised_parts).then(function (data) {
                var part_Length = 0;
                var i = 0;
                console.log("the complete are....", data);
                // console.log(data);
                _this.completed = data;
                part_Length = data.length;
                data.map(function (d, f, g) {
                    console.log("part...", d.part_id);
                    _this.orderservice.getPartBids(d.part_id).subscribe(function (data2) {
                        i += 1;
                        console.log("Bids are...", data2);
                        if (data2.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won confirmed"; })) {
                            var itemwon = data2.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won confirmed"; });
                            console.log("WON BID......", itemwon.bidAmount);
                            _this.bids.push({
                                partname: d.partName,
                                supplier: itemwon.bidSourceName,
                                supplier_num: itemwon.bidSupplierPhone,
                                confirmed: "Confirmed",
                                quantity: d.partQuantity,
                                amount: itemwon.bidAmount,
                            });
                        }
                        else {
                            var unconfurmed = data2.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won unconfirmed"; });
                            _this.bids.push({
                                partname: d.partName,
                                supplier: unconfurmed.bidSourceName,
                                supplier_num: unconfurmed.bidSupplierPhone,
                                confirmed: "Unconfirmed",
                                quantity: d.partQuantity,
                                amount: unconfurmed.bidAmount,
                            });
                        }
                        console.log("Number of partss.....", part_Length);
                        console.log("Sub number....", i);
                        console.log("Bids are...", _this.bids);
                        if (part_Length == i) {
                            _this.print2.nativeElement.click();
                            console.log("we can print now.....");
                        }
                    });
                });
            }).catch(function (err) {
                console.log("error occured");
                _this.completed = [];
            });
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        console.log("BIDS", this.bids);
    }
    PrinterComponent.prototype.ngOnInit = function () {
        this.personel = localStorage.getItem('uemail');
        this.company = localStorage.getItem('cnumber');
        //PRINT CLICK EVENT
        var that = this;
        $('#printInvoice').click(function () {
            /*
              var divToPrint = document.getElementById('myStuf');
            
              var newWin = window.open('','Print-Window');
            
              newWin.document.open();
            
              newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML +  '</body></html>');
            
              newWin.document.close();
            
              setTimeout(()=>{
            
                newWin.close();
            
              },   10)   //wimdow.close()
            
            
            */
            Popup($('#myStuf')[0].outerHTML);
            function Popup(data) {
                window.print();
                console.log("printing done....");
                return true;
            }
            console.log("printing done 235....");
            window.close();
            // that.router2.navigate(['/orders']);
            //Hide all other elements other than printarea.
            /*
            
               $("#myStuf").show();
               window.print();
            
            
            */
            /*
            
            $("#invoice").printThis({
              debug: true,               // show the iframe for debugging
              importCSS: true,            // import parent page css
              importStyle: true,         // import style tags
              printContainer: true,       // print outer container/$.selector
              loadCSS: "[]",   // path to additional css file - use an array [] for multiple
              pageTitle: "Print Complete Items",              // add title to print page
              removeInline: false,        // remove inline styles from print elements
              removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
              printDelay: 333,            // variable print delay
              header: null,               // prefix to html
              footer: null,               // postfix to html
              base: false,                // preserve the BASE tag or accept a string for the URL
              formValues: true,           // preserve input/form values
              canvas: false,              // copy canvas content
              doctypeString: '...',       // enter a different doctype for older markup
              removeScripts: false,       // remove script tags from print content
              copyTagClasses: false,      // copy classes from the html & body tag
              beforePrintEvent: null,     // function for printEvent in iframe
              beforePrint: null,          // function called before iframe is filled
              afterPrint: function (){
                console.log("done print....", this.router);
            
                
                this.router.navigate('/orders');
            
                
              },          // function called before iframe is removed
            });
            
            */
            /*
            var divToPrint=document.getElementById("myStuf");
            let newWin= window.open("");
            newWin.document.write(divToPrint.outerHTML);
            newWin.print();
            newWin.close();
            
            */
            /*
            var divToPrint = document.getElementById('myStuf');
            
            let htmlToPrint = divToPrint.outerHTML;
            
            let newWin = window.open("");
            //newWin.document.write("<h3 align='center'>Print Page</h3>");
            newWin.document.write(htmlToPrint);
            newWin.print();
            newWin.close();
            
            
            */
        });
    };
    PrinterComponent.prototype.findByName2 = function (searchKey, orders) {
        var key = searchKey.toUpperCase();
        return Promise.resolve(orders.filter(function (order) {
            return (order.part_status + ' ').toUpperCase().indexOf(key) > -1;
        }));
    };
    PrinterComponent.prototype.printComponent = function (cmpName) {
        var printContents = document.getElementById(cmpName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
    };
    PrinterComponent.prototype.donePrint = function () {
        //  this.router2.navigate(['/print',this.productID]);
    };
    PrinterComponent.prototype.totalAmount = function () {
        var total = 0;
        this.bids.forEach(function (item, index) {
            total += (item.amount * item.quantity);
            console.log("the total....", total);
        });
        return total;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('myPrint'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], PrinterComponent.prototype, "print2", void 0);
    PrinterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-printer',
            template: __webpack_require__(/*! ./printer.component.html */ "./src/app/views/others/orders/printer/printer.component.html"),
            styles: [__webpack_require__(/*! ./printer.css */ "./src/app/views/others/orders/printer/printer.css")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"]])
    ], PrinterComponent);
    return PrinterComponent;
}());



/***/ }),

/***/ "./src/app/views/others/orders/printer/printer.css":
/*!*********************************************************!*\
  !*** ./src/app/views/others/orders/printer/printer.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#invoice {\n    padding: 30px;\n}\n\n.invoice {\n    position: relative;\n    background-color: #FFF;\n    min-height: 680px;\n    padding: 15px\n}\n\n.invoice header {\n    padding: 10px 0;\n    margin-bottom: 20px;\n    border-bottom: 1px solid #3989c6\n}\n\n.invoice .company-details {\n    text-align: right\n}\n\n.invoice .company-details .name {\n    margin-top: 0;\n    margin-bottom: 0\n}\n\n.invoice .contacts {\n    margin-bottom: 20px\n}\n\n.invoice .invoice-to {\n    text-align: left\n}\n\n.invoice .invoice-to .to {\n    margin-top: 0;\n    margin-bottom: 0\n}\n\n.invoice .invoice-details {\n    text-align: right\n}\n\n.invoice .invoice-details .invoice-id {\n    margin-top: 0;\n    color: #3989c6\n}\n\n.invoice main {\n    padding-bottom: 50px\n}\n\n.invoice main .thanks {\n    margin-top: -100px;\n    font-size: 2em;\n    margin-bottom: 50px\n}\n\n.invoice main .notices {\n    padding-left: 6px;\n    border-left: 6px solid #3989c6\n}\n\n.invoice main .notices .notice {\n    font-size: 1.2em\n}\n\n.invoice table {\n    width: 100%;\n    border-collapse: collapse;\n    border-spacing: 0;\n    margin-bottom: 20px\n}\n\n.invoice table td,\n.invoice table th {\n    padding: 15px;\n    background: #eee;\n    border-bottom: 1px solid #fff\n}\n\n.invoice table th {\n    white-space: nowrap;\n    font-weight: 400;\n    font-size: 16px\n}\n\n.invoice table td h3 {\n    margin: 0;\n    font-weight: 400;\n    color: #3989c6;\n    font-size: 1.2em\n}\n\n.invoice table .qty,\n.invoice table .total,\n.invoice table .unit {\n    text-align: right;\n    font-size: 1.2em\n}\n\n.invoice table .no {\n    color: #fff;\n    font-size: 1.6em;\n    background: #3989c6\n}\n\n.invoice table .unit {\n    background: #ddd\n}\n\n.invoice table .total {\n    background: #3989c6;\n    color: #fff\n}\n\n.invoice table tbody tr:last-child td {\n    border: none\n}\n\n.invoice table tfoot td {\n    background: 0 0;\n    border-bottom: none;\n    white-space: nowrap;\n    text-align: right;\n    padding: 10px 20px;\n    font-size: 1.2em;\n    border-top: 1px solid #aaa\n}\n\n.invoice table tfoot tr:first-child td {\n    border-top: none\n}\n\n.invoice table tfoot tr:last-child td {\n    color: #3989c6;\n    font-size: 1.4em;\n    border-top: 1px solid #3989c6\n}\n\n.invoice table tfoot tr td:first-child {\n    border: none\n}\n\n.invoice footer {\n    width: 100%;\n    text-align: center;\n    color: #777;\n    border-top: 1px solid #aaa;\n    padding: 8px 0\n}\n\n@media print {\n    .invoice {\n        font-size: 11px!important;\n        overflow: hidden!important\n    }\n    .invoice footer {\n        position: absolute;\n        bottom: 10px;\n        page-break-after: always\n    }\n    .invoice>div:last-child {\n        page-break-before: always\n    }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3Mvb3RoZXJzL29yZGVycy9wcmludGVyL3ByaW50ZXIuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztDQUNqQjs7QUFFRDtJQUNJLG1CQUFtQjtJQUNuQix1QkFBdUI7SUFDdkIsa0JBQWtCO0lBQ2xCLGFBQWE7Q0FDaEI7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLGdDQUFnQztDQUNuQzs7QUFFRDtJQUNJLGlCQUFpQjtDQUNwQjs7QUFFRDtJQUNJLGNBQWM7SUFDZCxnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSxtQkFBbUI7Q0FDdEI7O0FBRUQ7SUFDSSxnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSxjQUFjO0lBQ2QsZ0JBQWdCO0NBQ25COztBQUVEO0lBQ0ksaUJBQWlCO0NBQ3BCOztBQUVEO0lBQ0ksY0FBYztJQUNkLGNBQWM7Q0FDakI7O0FBRUQ7SUFDSSxvQkFBb0I7Q0FDdkI7O0FBRUQ7SUFDSSxtQkFBbUI7SUFDbkIsZUFBZTtJQUNmLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLGtCQUFrQjtJQUNsQiw4QkFBOEI7Q0FDakM7O0FBRUQ7SUFDSSxnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osMEJBQTBCO0lBQzFCLGtCQUFrQjtJQUNsQixtQkFBbUI7Q0FDdEI7O0FBRUQ7O0lBRUksY0FBYztJQUNkLGlCQUFpQjtJQUNqQiw2QkFBNkI7Q0FDaEM7O0FBRUQ7SUFDSSxvQkFBb0I7SUFDcEIsaUJBQWlCO0lBQ2pCLGVBQWU7Q0FDbEI7O0FBRUQ7SUFDSSxVQUFVO0lBQ1YsaUJBQWlCO0lBQ2pCLGVBQWU7SUFDZixnQkFBZ0I7Q0FDbkI7O0FBRUQ7OztJQUdJLGtCQUFrQjtJQUNsQixnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSxZQUFZO0lBQ1osaUJBQWlCO0lBQ2pCLG1CQUFtQjtDQUN0Qjs7QUFFRDtJQUNJLGdCQUFnQjtDQUNuQjs7QUFFRDtJQUNJLG9CQUFvQjtJQUNwQixXQUFXO0NBQ2Q7O0FBRUQ7SUFDSSxZQUFZO0NBQ2Y7O0FBRUQ7SUFDSSxnQkFBZ0I7SUFDaEIsb0JBQW9CO0lBQ3BCLG9CQUFvQjtJQUNwQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLGlCQUFpQjtJQUNqQiwwQkFBMEI7Q0FDN0I7O0FBRUQ7SUFDSSxnQkFBZ0I7Q0FDbkI7O0FBRUQ7SUFDSSxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLDZCQUE2QjtDQUNoQzs7QUFFRDtJQUNJLFlBQVk7Q0FDZjs7QUFFRDtJQUNJLFlBQVk7SUFDWixtQkFBbUI7SUFDbkIsWUFBWTtJQUNaLDJCQUEyQjtJQUMzQixjQUFjO0NBQ2pCOztBQUVEO0lBQ0k7UUFDSSwwQkFBMEI7UUFDMUIsMEJBQTBCO0tBQzdCO0lBQ0Q7UUFDSSxtQkFBbUI7UUFDbkIsYUFBYTtRQUNiLHdCQUF3QjtLQUMzQjtJQUNEO1FBQ0kseUJBQXlCO0tBQzVCO0NBQ0oiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9vdGhlcnMvb3JkZXJzL3ByaW50ZXIvcHJpbnRlci5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjaW52b2ljZSB7XG4gICAgcGFkZGluZzogMzBweDtcbn1cblxuLmludm9pY2Uge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjRkZGO1xuICAgIG1pbi1oZWlnaHQ6IDY4MHB4O1xuICAgIHBhZGRpbmc6IDE1cHhcbn1cblxuLmludm9pY2UgaGVhZGVyIHtcbiAgICBwYWRkaW5nOiAxMHB4IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgIzM5ODljNlxufVxuXG4uaW52b2ljZSAuY29tcGFueS1kZXRhaWxzIHtcbiAgICB0ZXh0LWFsaWduOiByaWdodFxufVxuXG4uaW52b2ljZSAuY29tcGFueS1kZXRhaWxzIC5uYW1lIHtcbiAgICBtYXJnaW4tdG9wOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDBcbn1cblxuLmludm9pY2UgLmNvbnRhY3RzIHtcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4XG59XG5cbi5pbnZvaWNlIC5pbnZvaWNlLXRvIHtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0XG59XG5cbi5pbnZvaWNlIC5pbnZvaWNlLXRvIC50byB7XG4gICAgbWFyZ2luLXRvcDogMDtcbiAgICBtYXJnaW4tYm90dG9tOiAwXG59XG5cbi5pbnZvaWNlIC5pbnZvaWNlLWRldGFpbHMge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0XG59XG5cbi5pbnZvaWNlIC5pbnZvaWNlLWRldGFpbHMgLmludm9pY2UtaWQge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgY29sb3I6ICMzOTg5YzZcbn1cblxuLmludm9pY2UgbWFpbiB7XG4gICAgcGFkZGluZy1ib3R0b206IDUwcHhcbn1cblxuLmludm9pY2UgbWFpbiAudGhhbmtzIHtcbiAgICBtYXJnaW4tdG9wOiAtMTAwcHg7XG4gICAgZm9udC1zaXplOiAyZW07XG4gICAgbWFyZ2luLWJvdHRvbTogNTBweFxufVxuXG4uaW52b2ljZSBtYWluIC5ub3RpY2VzIHtcbiAgICBwYWRkaW5nLWxlZnQ6IDZweDtcbiAgICBib3JkZXItbGVmdDogNnB4IHNvbGlkICMzOTg5YzZcbn1cblxuLmludm9pY2UgbWFpbiAubm90aWNlcyAubm90aWNlIHtcbiAgICBmb250LXNpemU6IDEuMmVtXG59XG5cbi5pbnZvaWNlIHRhYmxlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItY29sbGFwc2U6IGNvbGxhcHNlO1xuICAgIGJvcmRlci1zcGFjaW5nOiAwO1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHhcbn1cblxuLmludm9pY2UgdGFibGUgdGQsXG4uaW52b2ljZSB0YWJsZSB0aCB7XG4gICAgcGFkZGluZzogMTVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZWVlO1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZmZmXG59XG5cbi5pbnZvaWNlIHRhYmxlIHRoIHtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIGZvbnQtd2VpZ2h0OiA0MDA7XG4gICAgZm9udC1zaXplOiAxNnB4XG59XG5cbi5pbnZvaWNlIHRhYmxlIHRkIGgzIHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgICBjb2xvcjogIzM5ODljNjtcbiAgICBmb250LXNpemU6IDEuMmVtXG59XG5cbi5pbnZvaWNlIHRhYmxlIC5xdHksXG4uaW52b2ljZSB0YWJsZSAudG90YWwsXG4uaW52b2ljZSB0YWJsZSAudW5pdCB7XG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XG4gICAgZm9udC1zaXplOiAxLjJlbVxufVxuXG4uaW52b2ljZSB0YWJsZSAubm8ge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMS42ZW07XG4gICAgYmFja2dyb3VuZDogIzM5ODljNlxufVxuXG4uaW52b2ljZSB0YWJsZSAudW5pdCB7XG4gICAgYmFja2dyb3VuZDogI2RkZFxufVxuXG4uaW52b2ljZSB0YWJsZSAudG90YWwge1xuICAgIGJhY2tncm91bmQ6ICMzOTg5YzY7XG4gICAgY29sb3I6ICNmZmZcbn1cblxuLmludm9pY2UgdGFibGUgdGJvZHkgdHI6bGFzdC1jaGlsZCB0ZCB7XG4gICAgYm9yZGVyOiBub25lXG59XG5cbi5pbnZvaWNlIHRhYmxlIHRmb290IHRkIHtcbiAgICBiYWNrZ3JvdW5kOiAwIDA7XG4gICAgYm9yZGVyLWJvdHRvbTogbm9uZTtcbiAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgICBmb250LXNpemU6IDEuMmVtO1xuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjYWFhXG59XG5cbi5pbnZvaWNlIHRhYmxlIHRmb290IHRyOmZpcnN0LWNoaWxkIHRkIHtcbiAgICBib3JkZXItdG9wOiBub25lXG59XG5cbi5pbnZvaWNlIHRhYmxlIHRmb290IHRyOmxhc3QtY2hpbGQgdGQge1xuICAgIGNvbG9yOiAjMzk4OWM2O1xuICAgIGZvbnQtc2l6ZTogMS40ZW07XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICMzOTg5YzZcbn1cblxuLmludm9pY2UgdGFibGUgdGZvb3QgdHIgdGQ6Zmlyc3QtY2hpbGQge1xuICAgIGJvcmRlcjogbm9uZVxufVxuXG4uaW52b2ljZSBmb290ZXIge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBjb2xvcjogIzc3NztcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgI2FhYTtcbiAgICBwYWRkaW5nOiA4cHggMFxufVxuXG5AbWVkaWEgcHJpbnQge1xuICAgIC5pbnZvaWNlIHtcbiAgICAgICAgZm9udC1zaXplOiAxMXB4IWltcG9ydGFudDtcbiAgICAgICAgb3ZlcmZsb3c6IGhpZGRlbiFpbXBvcnRhbnRcbiAgICB9XG4gICAgLmludm9pY2UgZm9vdGVyIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICBib3R0b206IDEwcHg7XG4gICAgICAgIHBhZ2UtYnJlYWstYWZ0ZXI6IGFsd2F5c1xuICAgIH1cbiAgICAuaW52b2ljZT5kaXY6bGFzdC1jaGlsZCB7XG4gICAgICAgIHBhZ2UtYnJlYWstYmVmb3JlOiBhbHdheXNcbiAgICB9XG59Il19 */"

/***/ }),

/***/ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/views/others/orders/vehiclepics/vehiclepics.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<!-- You can now use your library component in app.component.html \n<h1>\n  Image Viewer\n</h1>\n<app-image-viewer  [images]=\"['https://res.cloudinary.com/dw9pws08t/image/upload/v1562584054/ctymmdmdfbgbmp16nnuc.jpg']\"\n[idContainer]=\"'idOnHTML'\"\n[loadOnInit]=\"true\"></app-image-viewer>\n\n\n\n-->\n <!--\n\n\n <img style=\"height:100%\" src=\"https://res.cloudinary.com/dw9pws08t/image/upload/v1562584054/ctymmdmdfbgbmp16nnuc.jpg\" alt=\"Image 1\">\n\n-->\n\n<ngx-image-viewer   [src]=\"myData\"></ngx-image-viewer>\n\n <!-- This are the actions of the dialog, containing the primary and the cancel button\n  <mat-dialog-actions class=\"actions\">\n\n    <button  mat-button  mat-primary mat-dialog-close>Cancel</button>\n\n\n  </mat-dialog-actions>\n\n-->"

/***/ }),

/***/ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/views/others/orders/vehiclepics/vehiclepics.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9vcmRlcnMvdmVoaWNsZXBpY3MvdmVoaWNsZXBpY3MuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/views/others/orders/vehiclepics/vehiclepics.component.ts ***!
  \**************************************************************************/
/*! exports provided: VehiclepicsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclepicsComponent", function() { return VehiclepicsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};


var VehiclepicsComponent = /** @class */ (function () {
    function VehiclepicsComponent(dialogRef, data) {
        //
        //this.myData = ["https://res.cloudinary.com/dw9pws08t/image/upload/v1562584054/ctymmdmdfbgbmp16nnuc.jpg","https://res.cloudinary.com/dw9pws08t/image/upload/v1562584056/i4ktac2ue45ckqxmfdbt.jpg","https://res.cloudinary.com/dw9pws08t/image/upload/v1562584057/zgnqcn1dfrhwvko5uzee.jpg"]
        // this.myData = data
        var _this = this;
        this.dialogRef = dialogRef;
        this.myData = [];
        data.car_imgUrl.map(function (d, f, g) {
            console.log("d is..", d);
            console.log("f is..", f);
            console.log("g is..", g);
            _this.myData = d;
        });
        //  this.myData = data.car_imgUrl
    }
    VehiclepicsComponent.prototype.ngOnInit = function () {
    };
    VehiclepicsComponent.prototype.close = function () {
        this.dialogRef.close();
    };
    VehiclepicsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vehiclepics',
            template: __webpack_require__(/*! ./vehiclepics.component.html */ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.html"),
            styles: [__webpack_require__(/*! ./vehiclepics.component.scss */ "./src/app/views/others/orders/vehiclepics/vehiclepics.component.scss")]
        }),
        __param(1, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"], Object])
    ], VehiclepicsComponent);
    return VehiclepicsComponent;
}());



/***/ }),

/***/ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  No data to display.\n</p>\n"

/***/ }),

/***/ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9wcm9zcGVjdGl2ZW9yZGVycy9nZXRwcmV2aW91c3F1b3Rlcy9nZXRwcmV2aW91c3F1b3Rlcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: GetpreviousquotesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GetpreviousquotesComponent", function() { return GetpreviousquotesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var GetpreviousquotesComponent = /** @class */ (function () {
    function GetpreviousquotesComponent() {
    }
    GetpreviousquotesComponent.prototype.ngOnInit = function () {
    };
    GetpreviousquotesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-getpreviousquotes',
            template: __webpack_require__(/*! ./getpreviousquotes.component.html */ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.html"),
            styles: [__webpack_require__(/*! ./getpreviousquotes.component.scss */ "./src/app/views/others/prospectiveorders/getpreviousquotes/getpreviousquotes.component.scss")]
        }),
        __metadata("design:paramtypes", [])
    ], GetpreviousquotesComponent);
    return GetpreviousquotesComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.html":
/*!****************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.html ***!
  \****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input\n  type=\"file\"\n  name=\"file\"\n  #file\n  style=\"display: none\"\n  (change)=\"onFilesAdded($event)\"\n  multiple\n/>\n\n\n<div class=\"container\" fxLayout=\"column\" fxLayoutAlign=\"space-evenly stretch\">\n  <h1 mat-dialog-title>Upload Files</h1>\n  <div>\n\n\n    <button\n      [disabled]=\"uploading || uploadSuccessful\"\n      mat-raised-button\n      color=\"primary\"\n      class=\"add-files-btn\"\n      (click)=\"addFiles()\"\n    >\n      Add Files\n    </button>\n  </div>\n\n  <!-- This is the content of the dialog, containing a list of the files to upload -->\n  <mat-dialog-content fxFlex>\n\n    <mat-list>\n      <mat-list-item style=\"height:100px\" *ngFor=\"let url of urls let i = index \">\n      <img style=\"width:30%; height:100%\" [src]=\"url.url\" alt=\"...\">\n        <h4 mat-line>{{url.file.name}}</h4>\n        <mat-progress-bar\n          *ngIf=\"progress\"\n          mode=\"determinate\"\n          [value]=\"progress[url.file.name].progress | async\"\n        ></mat-progress-bar>\n        &nbsp;\n            &nbsp;\n      </mat-list-item>\n    </mat-list>\n\n\n  </mat-dialog-content>\n\n  <!-- This are the actions of the dialog, containing the primary and the cancel button-->\n  <mat-dialog-actions class=\"actions\">\n\n    <button  mat-button mat-dialog-close>Cancel</button>\n\n\n    <button\n      mat-raised-button\n      color=\"primary\"\n      [disabled]=\"!canBeClosed\"\n      (click)=\"closeDialog()\"\n    >\n      {{primaryButtonText}}\n    </button>\n\n\n  </mat-dialog-actions>\n</div>\n"

/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.ts ***!
  \**************************************************************************************/
/*! exports provided: CarimgpopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarimgpopupComponent", function() { return CarimgpopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CarimgpopupComponent = /** @class */ (function () {
    function CarimgpopupComponent(dialogRef) {
        this.dialogRef = dialogRef;
        this.files = new Set();
        this.canBeClosed = true;
        this.primaryButtonText = 'Upload';
        this.showCancelButton = true;
        this.uploading = false;
        this.uploadSuccessful = false;
        this.urls = new Array();
    }
    CarimgpopupComponent.prototype.ngOnInit = function () {
    };
    //auto click the input element to initialisze file selection
    CarimgpopupComponent.prototype.addFiles = function () {
        this.file.nativeElement.click();
    };
    //add files to our array
    CarimgpopupComponent.prototype.onFilesAdded = function (event) {
        //Display pic
        var _this = this;
        var files2 = event.target.files;
        this.myData = event.target.files;
        if (files2) {
            var _loop_1 = function (file) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //    let i = e.target.result.url.indexOf('/')
                    //      let url = e.target.result.url.slice(i);
                    _this.urls.push({ "url": e.target.result, "file": file }); ///
                    //   console.log("BASE64 URLS ARE......", this.urls[0].url);
                };
                reader.readAsDataURL(file);
            };
            for (var _i = 0, files2_1 = files2; _i < files2_1.length; _i++) {
                var file = files2_1[_i];
                _loop_1(file);
            }
        }
    };
    //close dialog
    CarimgpopupComponent.prototype.closeDialog = function () {
        this.primaryButtonText = 'Finish';
        return this.dialogRef.close(this.myData); //was this.urls
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('file'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], CarimgpopupComponent.prototype, "file", void 0);
    CarimgpopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-carimgpopup',
            template: __webpack_require__(/*! ./carimgpopup.component.html */ "./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.html"),
            styles: [__webpack_require__(/*! ./d.css */ "./src/app/views/others/vehicle/newvehicle/carimgpopup/d.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]])
    ], CarimgpopupComponent);
    return CarimgpopupComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/carimgpopup/d.css":
/*!*******************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/carimgpopup/d.css ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".add-files-btn {\n    float: right;\n}\n\n:host {\n    height: 100%;\n    display: flex;\n    flex: 1;\n    flex-direction: column;\n}\n\n.actions {\n    justify-content: flex-end;\n}\n\n.container {\n    height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3Mvb3RoZXJzL3ZlaGljbGUvbmV3dmVoaWNsZS9jYXJpbWdwb3B1cC9kLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7Q0FDaEI7O0FBRUQ7SUFDSSxhQUFhO0lBQ2IsY0FBYztJQUNkLFFBQVE7SUFDUix1QkFBdUI7Q0FDMUI7O0FBRUQ7SUFDSSwwQkFBMEI7Q0FDN0I7O0FBRUQ7SUFDSSxhQUFhO0NBQ2hCIiwiZmlsZSI6InNyYy9hcHAvdmlld3Mvb3RoZXJzL3ZlaGljbGUvbmV3dmVoaWNsZS9jYXJpbWdwb3B1cC9kLmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5hZGQtZmlsZXMtYnRuIHtcbiAgICBmbG9hdDogcmlnaHQ7XG59XG5cbjpob3N0IHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBmbGV4OiAxO1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG59XG5cbi5hY3Rpb25zIHtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xufVxuXG4uY29udGFpbmVyIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/newvehicle.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n  <!-- Inbox left side bar carsparesarray.car_parts.length()>=1-->\n  \n  <mat-sidenav *ngIf=\"carsparesarray.car_parts.length >= 1\" #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\">\n    <!-- Compose button -->\n    <button mat-raised-button class=\"mat-warn full-width\" >{{carsparesarray.carreg_no}}</button>\n    <!-- left side buttons -->\n    <div [perfectScrollbar] class=\"chat-sidebar-scroll\">\n    <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\n      <mat-list-item class=\"primary-imenu-item\"  routerLinkActive=\"open\" *ngFor=\"let item of carsparesarray.car_parts\">\n        <a fxLayout=\"row\">\n            {{item.partQuantity}} &nbsp;\n            <span> {{item.partName}} </span>\n        </a>\n      </mat-list-item>\n     \n      <mat-divider></mat-divider>\n\n    </mat-nav-list>\n  </div>\n  </mat-sidenav>\n\n\n\n\n<mat-card class=\"p-0\">\n<mat-card-title class=\"\">\n  <div class=\"card-title-text\">Fill Vehicle Details</div>\n  <mat-divider></mat-divider>\n</mat-card-title>\n<mat-card-content>\n  <form [formGroup]=\"basicForm\" (ngSubmit)=\"onSubmit()\">\n      <div fxLayout=\"row wrap\">\n          <div fxFlex=\"100\" fxFlex.gt-xs=\"50\" class=\"pr-1\">\n                  <div class=\"pb-1\">\n                          <mat-form-field class=\"full-width\">\n                              <input\n                              matInput\n                              name=\"jobcard_no\"\n                              formControlName=\"jobcard_no\"\n                              placeholder=\"JobCard No.\"\n                              value=\"\">\n                          </mat-form-field>\n                         \n                      </div>\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                      <input\n                      matInput\n                      name=\"carreg_no\"\n                      formControlName=\"carreg_no\"\n                      placeholder=\"Car Reg No.\"\n                      value=\"\" required>\n                  </mat-form-field>\n                 \n              </div>\n              <div class=\"pb-1\">\n                <mat-form-field class=\"full-width\">\n                    <input\n                    matInput\n                    name=\"chassis_no\"\n                    formControlName=\"chassis_no\"\n                    placeholder=\"Chassis No. (optional)\"\n                    value=\"\">\n                </mat-form-field>\n               \n            </div>\n\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                    <mat-label>Vehicle Make</mat-label>\n                    <mat-select   (selectionChange)=\"changeClientmake($event.value)\" required>\n                      <mat-option *ngFor=\"let make of makes\" [value]=\"make.vehicle_make\">\n                        {{make.vehicle_make}}\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                 \n              </div>\n\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                    <mat-label>Vehicle Model</mat-label>\n                    <mat-select    (selectionChange)=\"changeClientmodel($event.value)\" required>\n                      <mat-option *ngFor=\"let model of modelsgive()\" [value]=\"model\">\n                        {{model}}\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                  \n              </div> \n\n\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                    <mat-label>Year of Manufacture</mat-label>\n                    <mat-select    (selectionChange)=\"changeClientyom($event.value)\" required>\n                      <mat-option *ngFor=\"let yom of yoms\" [value]=\"yom\">\n                        {{yom}}\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                  \n              </div>\n             \n              <div class=\"pb-1\">\n             <mat-chip   \n             class=\"icon-chip\" \n             color=\"primary\" \n             selected=\"true\" (click)=\"uploadCarimg()\">Upload Car Images?</mat-chip>\n            </div>\n          </div>\n         \n          <div fxFlex=\"100\" fxFlex.gt-xs=\"50\">\n              <!-- <div class=\"m-333\"> -->\n                  <!-- <span fxFlex></span> -->\n               <!--   <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"opencarpartPopUp({}, true)\">Add Car Part</button>\n                </div> -->\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                    <mat-label>Part Name</mat-label>\n                    <mat-select  formControlName=\"part_name\"   (selectionChange)=\"changeClientpart($event.value)\" required>\n                      <mat-option *ngFor=\"let partname of partnames\" [value]=\"partname.part_name\">\n                        {{partname.part_name}}\n                      </mat-option>\n                    </mat-select>\n                  </mat-form-field>\n                  \n              </div>\n\n              <div class=\"pb-1\">\n                <mat-form-field class=\"full-width\">\n                <input \n                   \n                    name=\"part_number\"\n                    matInput\n                    formControlName=\"part_number\"\n                    placeholder=\"Part Number (optional)\" \n                    value=\"\" >\n                </mat-form-field>\n               \n            </div>\n\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                  <input \n                     \n                      name=\"quantity\"\n                      matInput\n                      formControlName=\"quantity\"\n                      placeholder=\"Quantity\" \n                      value=\"\" required>\n                  </mat-form-field>\n                 \n              </div>\n\n              <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                      <textarea name=\"description\" matInput formControlName=\"description\" placeholder=\"Description (eg. color, side, genuine, taiwan)\"></textarea>\n                  </mat-form-field>\n               \n              </div>\n\n              <div style=\"display:none\"  class=\"pb-1\">\n             <mat-chip   \n                class=\"icon-chip\" \n                color=\"primary\" \n                selected=\"true\" (click)=\"uploadCarpartimg()\">Upload Sample Part Images?</mat-chip>\n              </div>\n\n          <!--   <div class=\"pb-1\">\n                \n            [disabled]=\"basicForm.invalid\" \n            && basicForm.invalid \n                \n                <input #file type=\"file\" accept='image/*' (change)=\"preview(file.files)\" />\n                <img [src]=\"imgURL\" height=\"200\" *ngIf=\"imgURL\">\n            </div>\n       \"basicForm.controls['carreg_no'].invalid\"  \"!carsparesarray.carreg_no\"           (click)=pushcarsparesarraytoserver()-->\n\n              <button type=\"button\"\n      mat-raised-button \n      color=\"primary\" [disabled]=\"basicForm.invalid\"\n     (click)=addtocarsparesarray()>Add part</button>\n          </div>\n      </div>\n      <button \n      mat-raised-button \n      color=\"primary\" \n      [disabled]=  \"!carsparesarray.car_make && basicForm.invalid ||checkpartfield()\" type=\"submit\" >Submit Order</button>\n  </form>\n</mat-card-content>\n</mat-card>\n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/newvehicle.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL25ld3ZlaGljbGUvbmV3dmVoaWNsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFpQiwrQkFBc0MsRUFBRyIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL25ld3ZlaGljbGUvbmV3dmVoaWNsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibHVlLXNuYWNrYmFyIHsgYmFja2dyb3VuZDogcmdiKDMzLCAyMTIsIDkpICFpbXBvcnRhbnQ7fSJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/newvehicle.component.ts ***!
  \*************************************************************************/
/*! exports provided: NewvehicleComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NewvehicleComponent", function() { return NewvehicleComponent; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_globals__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/globals */ "./src/app/globals.ts");
/* harmony import */ var _carimgpopup_carimgpopup_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./carimgpopup/carimgpopup.component */ "./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.ts");
/* harmony import */ var _partimgpopup_partimgpopup_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./partimgpopup/partimgpopup.component */ "./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _vehicledetails_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../vehicledetails/makedialogue/makedialogue.component */ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.ts");
/* harmony import */ var _vehicledetails_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../vehicledetails/modeldialogue/modeldialogue.component */ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.ts");
/* harmony import */ var _vehicledetails_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../vehicledetails/partsdialogue/partsdialogue.component */ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var NewvehicleComponent = /** @class */ (function () {
    function NewvehicleComponent(media, orderservice, router, globals, snackBar, dialog, http, loader) {
        this.media = media;
        this.orderservice = orderservice;
        this.router = router;
        this.globals = globals;
        this.snackBar = snackBar;
        this.dialog = dialog;
        this.http = http;
        this.loader = loader;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
        this.formData = {};
        this.carregno = " ";
        this.console = console;
        this.carsparesarray = { car_parts: [] };
        this.allcarsparesarray = [];
        this.carpartsData = {};
        this.yoms = ["1980", "1981", "1982", "1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", "1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000",
            "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019"];
        this.carimg = [];
        this.partimg = [];
        //  setTimeout(()=>{ this.hasvehicles = true }, 10000);
    }
    NewvehicleComponent.prototype.ngOnInit = function () {
        this.getItemsval();
        this.getItemsp();
        this.inboxSideNavInit();
        this.yoms = this.yoms.reverse();
        this.basicForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            carreg_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            part_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            chassis_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            quantity: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            part_number: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            jobcard_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](''),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('')
        });
        this.makes = this.orderservice.getMakemodel();
        this.partnames = this.orderservice.getPart();
        //this.modelsgive(this.makes[1]);
        //console.log("=========-----------=========--------=======---------= "+this.modelsgive(this.makes[1]))
    };
    NewvehicleComponent.prototype.clearpartsection = function () {
        // this.basicForm.controls['part_name'].reset();
        this.basicForm.controls['part_number'].reset();
        this.basicForm.controls['quantity'].reset();
        this.basicForm.controls['description'].reset();
        this.basicForm.get('part_name').setValue(null);
    };
    NewvehicleComponent.prototype.checkpartfield = function () {
        if (this.basicForm.value.part_name) {
            return true;
        }
        else {
            return false;
        }
    };
    NewvehicleComponent.prototype.addtocarsparesarray = function () {
        if (this.carsparesarray.car_make) {
            this.carsparesarray.car_parts.push({
                'partName': this.selectedpart,
                'partQuantity': this.basicForm.value.quantity,
                'partDescription': this.basicForm.value.description,
                'partNumber': this.basicForm.value.part_number,
                'part_imgs': this.partimg
                //'partPhotosneeded':
            });
        }
        else {
            this.carsparesarray.requisition_no = this.basicForm.value.jobcard_no;
            this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
            this.carsparesarray.car_make = this.selectedmake;
            this.carsparesarray.car_model = this.selectedmodel;
            this.carsparesarray.car_yom = this.selectedyom;
            // this.carsparesarray.car_imgs = this.carimg;
            this.carsparesarray.chassis_no = this.basicForm.value.chassis_no;
            this.carsparesarray.order_sourceCompany = localStorage.getItem('cname');
            this.carsparesarray.order_sourceOfficer = this.globals.userdetails.userEmail;
            this.carsparesarray.order_sourceCompanyid = localStorage.getItem('cnumber');
            this.carpartsData.partName = this.selectedpart;
            this.carpartsData.partNumber = this.basicForm.value.part_number;
            this.carpartsData.partQuantity = this.basicForm.value.quantity;
            this.carpartsData.partDescription = this.basicForm.value.description;
            //  this.carpartsData.part_imgs=this.partimg;
            this.carsparesarray.car_parts.push(this.carpartsData);
        }
        this.clearpartsection();
        //console.log(this.carsparesarray.car_parts.length+"jjjjjjjjj");
    };
    NewvehicleComponent.prototype.onSubmit = function () {
        console.log(this.carsparesarray.car_make + "--------===============-------jjjjjjjjjthis.selectedpart " + this.selectedpart);
        this.loader.open();
        if (this.carsparesarray.car_make) {
            console.log("--------===============-------jjjjjjjjjthis.selectedpart2  22 ");
            this.basicUpload(this.carimg, this.carsparesarray);
            /*
      this.subscription = this.orderservice.postReiceivedVehicle(this.carsparesarray).subscribe(
      
           data => {
             // refresh the list
            // this.items =this.temp = data;
          //   console.log(JSON.stringify( data )+"-------------------");
      
      
         this.subscription.unsubscribe();
      
          this.carsparesarray.car_make = null;
      
          this.openSnackBar("Success")
      
      
          // this.clearObjectValues(this.carsparesarray);
      
         console.log(data+"----------------------=-===============-------jjjjjjjjj ");
      
           this.basicForm.reset();
      
            // return true;
           },
           error => {
             console.error("Error saving !");
           //  return Observable.throw(error);
          }
      
         );
      */
            //   }
        }
        else {
        }
    };
    NewvehicleComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    NewvehicleComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    NewvehicleComponent.prototype.modelsgive = function () {
        var _this = this;
        if (this.makes && this.selectedmake) {
            var itm = this.makes.find(function (x) { return x.vehicle_make == _this.selectedmake; });
            return itm.vehicle_model;
        }
        else {
            // this.getItemsval();
            return ["Select Vechile Make"];
        }
        //..console.log("er5432345fcdcsert   ");
    };
    NewvehicleComponent.prototype.checkselect = function () {
        if (!this.selectedmake) {
            return true;
        }
        else {
            return false;
        }
    };
    NewvehicleComponent.prototype.changeClientmake = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmake = data;
        // this.matSelectmake = data.source;
        //..alert("selected --->"+data);
    };
    NewvehicleComponent.prototype.changeClientmodel = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmodel = data;
        // this.selectedmod=data;
        //..alert("selected --->"+data);
    };
    NewvehicleComponent.prototype.changeClientyom = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedyom = data;
        // this.selectedmod=data;
        //..alert("selected --->"+data);
    };
    NewvehicleComponent.prototype.changeClientpart = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedpart = data;
        // this.selectedmod=data;
        //..alert("selected --->"+data);
    };
    NewvehicleComponent.prototype.getItemsval = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv = this.orderservice.getallVehicles().subscribe(function (data) {
            subv.unsubscribe;
            // refresh the list
            //  this.itemsv =this.tempv = 
            // data;
            _this.orderservice.setMakemodel(data);
            //.. this.modelsgive();
            //.. this.orderservice.setMake(data["vehicle_make"]);
            //.. this.orderservice.setModel(data["vehicle_model"]);
            //   this.itemcarmethod(data);
            //   console.log("-------------------"+JSON.stringify( data));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    NewvehicleComponent.prototype.getItemsp = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var sub = this.orderservice.getallPart().subscribe(function (data) {
            sub.unsubscribe();
            // refresh the list
            _this.orderservice.setPart(data);
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    NewvehicleComponent.prototype.uploadCarpartimg = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_partimgpopup_partimgpopup_component__WEBPACK_IMPORTED_MODULE_9__["PartimgpopupComponent"], {
            width: '50%',
            height: '50%'
        });
        dialogRef.afterClosed().subscribe(function (data) {
            console.log(data);
            for (var _i = 0, data_1 = data; _i < data_1.length; _i++) {
                data = data_1[_i];
                var url = data.url;
                var i = url.indexOf(',') + 1;
                console.log(url.slice(i));
                _this.partimg.push(url.slice(i));
            }
        });
    };
    NewvehicleComponent.prototype.uploadCarimg = function () {
        var _this = this;
        var dialogRef = this.dialog.open(_carimgpopup_carimgpopup_component__WEBPACK_IMPORTED_MODULE_8__["CarimgpopupComponent"], {
            width: '50%',
            height: '50%'
        });
        dialogRef.afterClosed().subscribe(function (data) {
            console.log("Dialog close data", data);
            _this.carimg = data;
            /*       for(data of data){
     
                     let url = data.url
     
                     let i = url.indexOf(',')+1
                     console.log(url.slice(i));
                     
                  this.carimg.push(url.slice(i));
     
     
                   } */
        });
    };
    NewvehicleComponent.prototype.ngOnDestroy = function () {
        /*if(this.subscription){
       this.subscription.unsubscribe();
        }*/
    };
    NewvehicleComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, panelClass: ['blue-snackbar'], verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    NewvehicleComponent.prototype.basicUpload = function (myFiles, data) {
        var _this = this;
        var files = myFiles;
        var formData = new FormData();
        Array.from(files).forEach(function (f) { return formData.append('file', f); });
        console.log("FORM DATA IS......", formData);
        formData.append('field', JSON.stringify(data));
        this.http.post(this.orderservice._url + 'corporate/receive/vehicle2', formData)
            .subscribe(function (event) {
            console.log('done');
            _this.loader.close();
            _this.router.navigate(['others']);
        });
    };
    NewvehicleComponent.prototype.openmakePopUp = function (data, isNew) {
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Make' : 'Update Vehicle Make';
        var dialogRef = this.dialog.open(_vehicledetails_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_11__["MakedialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                //  this.getItemsv();
            }
            // this.getItemsp();
            // this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    NewvehicleComponent.prototype.openmodelPopUp = function (data, isNew) {
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Model' : 'Update Vehicle Model';
        var dialogRef = this.dialog.open(_vehicledetails_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_12__["ModeldialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                //this.getItemsv();
            }
            // this.getItemsp();
            //  this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    NewvehicleComponent.prototype.opencarpartPopUp = function (data, isNew) {
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Part' : 'Update Vehicle Part';
        var dialogRef = this.dialog.open(_vehicledetails_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_13__["PartsdialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                // this.getItemsv();
                //..  this.getItemsp();
            }
            // this.loader.open(); 
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenav"])
    ], NewvehicleComponent.prototype, "sideNav", void 0);
    NewvehicleComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-newvehicle',
            template: __webpack_require__(/*! ./newvehicle.component.html */ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.html"),
            styles: [__webpack_require__(/*! ./newvehicle.component.scss */ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["ObservableMedia"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"],
            app_globals__WEBPACK_IMPORTED_MODULE_7__["Globals"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"],
            _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"], _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpClient"], app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_10__["AppLoaderService"]])
    ], NewvehicleComponent);
    return NewvehicleComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.html":
/*!******************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<input\n  type=\"file\"\n  name=\"file\"\n  #file\n  style=\"display: none\"\n  (change)=\"onFilesAdded($event)\"\n  multiple\n/>\n\n\n<div class=\"container\" fxLayout=\"column\" fxLayoutAlign=\"space-evenly stretch\">\n  <h1 mat-dialog-title>Upload Files</h1>\n  <div>\n\n\n    <button\n      [disabled]=\"uploading || uploadSuccessful\"\n      mat-raised-button\n      color=\"primary\"\n      class=\"add-files-btn\"\n      (click)=\"addFiles()\"\n    >\n      Add Files\n    </button>\n  </div>\n\n  <!-- This is the content of the dialog, containing a list of the files to upload -->\n  <mat-dialog-content fxFlex>\n\n    <mat-list>\n      <mat-list-item style=\"height:100px\" *ngFor=\"let url of urls let i = index \">\n      <img style=\"width:30%; height:100%\" [src]=\"url.url\" alt=\"...\">\n        <h4 mat-line>{{url.file.name}}</h4>\n        <mat-progress-bar\n          *ngIf=\"progress\"\n          mode=\"determinate\"\n          [value]=\"progress[url.file.name].progress | async\"\n        ></mat-progress-bar>\n        &nbsp;\n            &nbsp;\n      </mat-list-item>\n    </mat-list>\n\n\n  </mat-dialog-content>\n\n  <!-- This are the actions of the dialog, containing the primary and the cancel button-->\n  <mat-dialog-actions class=\"actions\">\n\n    <button  mat-button mat-dialog-close>Cancel</button>\n\n\n    <button\n      mat-raised-button\n      color=\"primary\"\n      [disabled]=\"!canBeClosed\"\n      (click)=\"closeDialog()\"\n    >\n      {{primaryButtonText}}\n    </button>\n\n\n  </mat-dialog-actions>\n</div>\n"

/***/ }),

/***/ "./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.ts":
/*!****************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.ts ***!
  \****************************************************************************************/
/*! exports provided: PartimgpopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartimgpopupComponent", function() { return PartimgpopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PartimgpopupComponent = /** @class */ (function () {
    function PartimgpopupComponent(dialogRef) {
        this.dialogRef = dialogRef;
        this.files = new Set();
        this.canBeClosed = true;
        this.primaryButtonText = 'Upload';
        this.showCancelButton = true;
        this.uploading = false;
        this.uploadSuccessful = false;
        this.urls = new Array();
    }
    PartimgpopupComponent.prototype.ngOnInit = function () {
    };
    //auto click the input element to initialisze file selection
    PartimgpopupComponent.prototype.addFiles = function () {
        this.file.nativeElement.click();
    };
    //add files to our array
    PartimgpopupComponent.prototype.onFilesAdded = function (event) {
        //Display pic
        var _this = this;
        var files2 = event.target.files;
        if (files2) {
            var _loop_1 = function (file) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    //    let i = e.target.result.url.indexOf('/')
                    //      let url = e.target.result.url.slice(i);
                    _this.urls.push({ "url": e.target.result, "file": file }); ///
                    //   console.log("BASE64 URLS ARE......", this.urls[0].url);
                };
                reader.readAsDataURL(file);
            };
            for (var _i = 0, files2_1 = files2; _i < files2_1.length; _i++) {
                var file = files2_1[_i];
                _loop_1(file);
            }
        }
    };
    //close dialog
    PartimgpopupComponent.prototype.closeDialog = function () {
        this.primaryButtonText = 'Finish';
        return this.dialogRef.close(this.urls);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])('file'),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ElementRef"])
    ], PartimgpopupComponent.prototype, "file", void 0);
    PartimgpopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-partimgpopup',
            template: __webpack_require__(/*! ./partimgpopup.component.html */ "./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.html"),
            styles: [__webpack_require__(/*! ../carimgpopup/d.css */ "./src/app/views/others/vehicle/newvehicle/carimgpopup/d.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialogRef"]])
    ], PartimgpopupComponent);
    return PartimgpopupComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/vehicle.module.ts":
/*!********************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicle.module.ts ***!
  \********************************************************/
/*! exports provided: VehicleModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleModule", function() { return VehicleModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var app_shared_shared_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _newvehicle_newvehicle_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./newvehicle/newvehicle.component */ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.ts");
/* harmony import */ var _vehicle_routing__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./vehicle.routing */ "./src/app/views/others/vehicle/vehicle.routing.ts");
/* harmony import */ var _vehicledetails_vehicledetails_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./vehicledetails/vehicledetails.component */ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.ts");
/* harmony import */ var _newvehicle_carimgpopup_carimgpopup_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./newvehicle/carimgpopup/carimgpopup.component */ "./src/app/views/others/vehicle/newvehicle/carimgpopup/carimgpopup.component.ts");
/* harmony import */ var _newvehicle_partimgpopup_partimgpopup_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./newvehicle/partimgpopup/partimgpopup.component */ "./src/app/views/others/vehicle/newvehicle/partimgpopup/partimgpopup.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















var VehicleModule = /** @class */ (function () {
    function VehicleModule() {
    }
    VehicleModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__["PerfectScrollbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                app_shared_shared_module__WEBPACK_IMPORTED_MODULE_10__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_vehicle_routing__WEBPACK_IMPORTED_MODULE_12__["VehicleRoutes"])
            ],
            declarations: [
                _newvehicle_newvehicle_component__WEBPACK_IMPORTED_MODULE_11__["NewvehicleComponent"],
                _vehicledetails_vehicledetails_component__WEBPACK_IMPORTED_MODULE_13__["VehicledetailsComponent"],
                _newvehicle_carimgpopup_carimgpopup_component__WEBPACK_IMPORTED_MODULE_14__["CarimgpopupComponent"],
                _newvehicle_partimgpopup_partimgpopup_component__WEBPACK_IMPORTED_MODULE_15__["PartimgpopupComponent"]
            ],
            exports: [
                _newvehicle_newvehicle_component__WEBPACK_IMPORTED_MODULE_11__["NewvehicleComponent"],
                _vehicledetails_vehicledetails_component__WEBPACK_IMPORTED_MODULE_13__["VehicledetailsComponent"],
                _newvehicle_carimgpopup_carimgpopup_component__WEBPACK_IMPORTED_MODULE_14__["CarimgpopupComponent"],
                _newvehicle_partimgpopup_partimgpopup_component__WEBPACK_IMPORTED_MODULE_15__["PartimgpopupComponent"]
            ],
            entryComponents: [_newvehicle_carimgpopup_carimgpopup_component__WEBPACK_IMPORTED_MODULE_14__["CarimgpopupComponent"],
                _newvehicle_partimgpopup_partimgpopup_component__WEBPACK_IMPORTED_MODULE_15__["PartimgpopupComponent"]]
        })
    ], VehicleModule);
    return VehicleModule;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/vehicle.routing.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicle.routing.ts ***!
  \*********************************************************/
/*! exports provided: VehicleRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicleRoutes", function() { return VehicleRoutes; });
/* harmony import */ var _newvehicle_newvehicle_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./newvehicle/newvehicle.component */ "./src/app/views/others/vehicle/newvehicle/newvehicle.component.ts");
/* harmony import */ var _vehicledetails_vehicledetails_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./vehicledetails/vehicledetails.component */ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.ts");


var VehicleRoutes = [
    {
        path: '',
        //component: NewvehicleComponent,
        // data: { title: 'Blank', breadcrumb: 'BLANK' }
        children: [{
                path: 'newvehicle',
                component: _newvehicle_newvehicle_component__WEBPACK_IMPORTED_MODULE_0__["NewvehicleComponent"],
                data: { title: 'Blank', breadcrumb: 'BLANK' }
            },
            {
                path: 'vehicledetails',
                component: _vehicledetails_vehicledetails_component__WEBPACK_IMPORTED_MODULE_1__["VehicledetailsComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            }
            /*,
            {
              path: 'suppliers',
                component: SuppliersComponent,
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            } ,
            {
              path: 'vehicles',
                component: VehiclesComponent,
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            } */
        ]
        /*,
        {
          path: 'showorderparts/:id',
            component: ShoworderpartsComponent,
            data: { title: 'Basic', breadcrumb: 'BASIC' }
        }  ,
        {
          path: 'particularpart/:id',
            component: ParticularpartComponent,
            data: { title: 'Basic', breadcrumb: 'BASIC' }
        }
        */
    }
];


/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"carmake\"\n        [formControl]=\"itemForm.controls['carmake']\"\n        placeholder=\"Car Make\">\n      </mat-form-field>\n    </div>\n\n\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL3ZlaGljbGVkZXRhaWxzL21ha2VkaWFsb2d1ZS9tYWtlZGlhbG9ndWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBaUIsK0JBQXNDLEVBQUciLCJmaWxlIjoic3JjL2FwcC92aWV3cy9vdGhlcnMvdmVoaWNsZS92ZWhpY2xlZGV0YWlscy9tYWtlZGlhbG9ndWUvbWFrZWRpYWxvZ3VlLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsdWUtc25hY2tiYXIgeyBiYWNrZ3JvdW5kOiByZ2IoMzMsIDIxMiwgOSkgIWltcG9ydGFudDt9Il19 */"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.ts ***!
  \********************************************************************************************/
/*! exports provided: MakedialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MakedialogueComponent", function() { return MakedialogueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var MakedialogueComponent = /** @class */ (function () {
    function MakedialogueComponent(data, dialogRef, fb, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
    }
    MakedialogueComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    MakedialogueComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            carmake: [item.carmake || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    MakedialogueComponent.prototype.submit = function () {
        // this.dialogRef.close(this.itemForm.value)
        var _this = this;
        this.subscription = this.orderservice.postMakemodel(this.itemForm.value.carmake, "").subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.subscription.unsubscribe();
            _this.getItemsval();
            _this.openSnackBar("Success");
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    MakedialogueComponent.prototype.getItemsval = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv = this.orderservice.getallVehicles().subscribe(function (data) {
            subv.unsubscribe;
            // refresh the list
            //  this.itemsv =this.tempv = 
            // data;
            _this.orderservice.setMakemodel(data);
            //.. this.orderservice.setMake(data["vehicle_make"]);
            //.. this.orderservice.setModel(data["vehicle_model"]);
            //   this.itemcarmethod(data);
            //   console.log("-------------------"+JSON.stringify( data));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    MakedialogueComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    MakedialogueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-makedialogue',
            template: __webpack_require__(/*! ./makedialogue.component.html */ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.html"),
            styles: [__webpack_require__(/*! ./makedialogue.component.scss */ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], MakedialogueComponent);
    return MakedialogueComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field>\n        <mat-label>Vehicle Make</mat-label>\n        <mat-select    (selectionChange)=\"changeClient($event.value)\">\n          <mat-option *ngFor=\"let make of makes\" [value]=\"make.vehicle_make\">\n            {{make.vehicle_make}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        \n        name=\"carmodel\"\n        [formControl]=\"itemForm.controls['carmodel']\"\n        placeholder=\"Car Model\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid || checkselect()\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL3ZlaGljbGVkZXRhaWxzL21vZGVsZGlhbG9ndWUvbW9kZWxkaWFsb2d1ZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFpQiwrQkFBc0MsRUFBRyIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL3ZlaGljbGVkZXRhaWxzL21vZGVsZGlhbG9ndWUvbW9kZWxkaWFsb2d1ZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibHVlLXNuYWNrYmFyIHsgYmFja2dyb3VuZDogcmdiKDMzLCAyMTIsIDkpICFpbXBvcnRhbnQ7fSJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: ModeldialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModeldialogueComponent", function() { return ModeldialogueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ModeldialogueComponent = /** @class */ (function () {
    function ModeldialogueComponent(data, dialogRef, fb, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
    }
    ModeldialogueComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
        this.makes = this.orderservice.getMakemodel();
        console.log("-------------------" + JSON.stringify(this.makes));
    };
    ModeldialogueComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            carmodel: [item.carmodel || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    ModeldialogueComponent.prototype.submit = function () {
        // this.dialogRef.close(this.itemForm.value)
        var _this = this;
        this.subscription = this.orderservice.postMakemodel(this.selectedmod, this.itemForm.value.carmodel).subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.orderservice.getallVehiclefeed();
            _this.openSnackBar("Success");
            _this.subscription.unsubscribe();
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    ModeldialogueComponent.prototype.checkselect = function () {
        if (!this.selectedmod) {
            return true;
        }
        else {
            return false;
        }
    };
    ModeldialogueComponent.prototype.changeClient = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmod = data;
        //..alert("selected --->"+data);
    };
    ModeldialogueComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    ModeldialogueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modeldialogue',
            template: __webpack_require__(/*! ./modeldialogue.component.html */ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.html"),
            styles: [__webpack_require__(/*! ./modeldialogue.component.scss */ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], ModeldialogueComponent);
    return ModeldialogueComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.html":
/*!************************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n   \n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field>\n        <mat-label>Part Category</mat-label>\n        <mat-select  (selectionChange)=\"changeClient($event.value)\">\n          <mat-option *ngFor=\"let partcategory of partcategorys\" [value]=\"partcategory\">\n            {{partcategory}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"partname\"\n        [formControl]=\"itemForm.controls['partname']\"\n        placeholder=\"Part Name\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid || checkselect()\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.scss":
/*!************************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.scss ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL3ZlaGljbGVkZXRhaWxzL3BhcnRzZGlhbG9ndWUvcGFydHNkaWFsb2d1ZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUFpQiwrQkFBc0MsRUFBRyIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL3ZlaGljbGVkZXRhaWxzL3BhcnRzZGlhbG9ndWUvcGFydHNkaWFsb2d1ZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5ibHVlLXNuYWNrYmFyIHsgYmFja2dyb3VuZDogcmdiKDMzLCAyMTIsIDkpICFpbXBvcnRhbnQ7fSJdfQ== */"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.ts":
/*!**********************************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.ts ***!
  \**********************************************************************************************/
/*! exports provided: PartsdialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartsdialogueComponent", function() { return PartsdialogueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var PartsdialogueComponent = /** @class */ (function () {
    function PartsdialogueComponent(data, dialogRef, fb, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
        this.partcategorys = ["Body Part", "Electrical Part", "Mechanical Part", "Accessory"];
    }
    PartsdialogueComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    PartsdialogueComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            partname: [item.partname || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    PartsdialogueComponent.prototype.submit = function () {
        // this.dialogRef.close(this.itemForm.value)
        var _this = this;
        this.subscription = this.orderservice.postPart(this.itemForm.value.partname, this.selectedmod).subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.orderservice.getallPartfeed();
            _this.openSnackBar("Success");
            _this.subscription.unsubscribe();
            // this.orderservice.setPart(this.orderservice.getPart().push(this.itemForm.value.partname));
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    PartsdialogueComponent.prototype.checkselect = function () {
        if (!this.selectedmod) {
            return true;
        }
        else {
            return false;
        }
    };
    PartsdialogueComponent.prototype.changeClient = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmod = data;
        //..alert("selected --->"+data);
    };
    PartsdialogueComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    PartsdialogueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-partsdialogue',
            template: __webpack_require__(/*! ./partsdialogue.component.html */ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.html"),
            styles: [__webpack_require__(/*! ./partsdialogue.component.scss */ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], PartsdialogueComponent);
    return PartsdialogueComponent;
}());



/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n\n  <mat-sidenav  #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\">\n   \n      <mat-card-title class=\"\">\n          <div class=\"card-title-text\">\n            <span>Select Option</span>\n          </div>\n          <mat-divider></mat-divider>\n        </mat-card-title>\n        <mat-card-content>\n          \n           <div style=\"width: 100%;text-align: center\">\n            <div style=\"box-sizing: border-box;\n            padding: 10px\">\n            <span> \n               \n                <button mat-raised-button   [ngStyle]=\"{'background-color': returnBol('car_make')? 'red' : 'LIGHTSLATEGRAY'}\"  (click)=\"getClick('car_make')\">Vehicle Make</button>\n            </span>\n            &nbsp;\n            &nbsp;\n          </div>\n          <div style=\"box-sizing: border-box;\n          padding: 10px\">\n            <span> \n                <button mat-raised-button  [ngStyle]=\"{'background-color': returnBol('car_model')? 'red' : 'LIGHTSLATEGRAY'}\"  (click)=\"getClick('car_model')\">Vehicle Model</button>\n            </span>\n            &nbsp;\n            &nbsp;\n          </div>\n          <div  style=\"box-sizing: border-box;\n          padding: 10px\">\n            <span> \n                <button mat-raised-button  [ngStyle]=\"{'background-color': returnBol('car_parts')? 'red' : 'LIGHTSLATEGRAY'}\" (click)=\"getClick('car_parts')\" >Vehicle   Parts</button>\n            </span>\n            &nbsp;\n          </div>\n          </div>\n        </mat-card-content>\n\n    </mat-sidenav>\n\n   \n<mat-card class=\"p-0\">\n<div class=\"messages-wrap\"   *ngIf=\"selectedValue === 'car_make'\" >\n<div class=\"m-333\">\n<!-- <span fxFlex></span>        color=\"primary\"   [disabled]= \"!returnBol('car_make')\"                  -->\n<button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"openmakePopUp({}, true)\">Add Car Make</button>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n<mat-card-content class=\"p-0\">\n  <ngx-datatable\n        class=\"material ml-0 mr-0\"\n        [rows]=\"itemsv\"\n        [columnMode]=\"'flex'\"\n        [headerHeight]=\"50\"\n        [footerHeight]=\"50\"\n        [limit]=\"10\"\n        [rowHeight]=\"'auto'\">\n        <ngx-datatable-column name=\"Car Make\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.vehicle_make }}\n          </ng-template>\n        </ngx-datatable-column>\n      \n        <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n            <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n          </ng-template>\n        </ngx-datatable-column>\n      </ngx-datatable>\n</mat-card-content>\n</mat-card>\n</div>\n\n<!-- ================================================== -->\n\n<div class=\"messages-wrap\" *ngIf=\"selectedValue === 'car_model'\">\n<div class=\"m-333\">\n  <!-- <span fxFlex></span> -->\n  <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"openmodelPopUp({}, true)\">Add Car Model</button>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n  <mat-card-content class=\"p-0\">\n    <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"itemsv\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\"\n          [limit]=\"10\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Car Make\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.vehicle_make }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Car Model\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.vehicle_model}}\n            </ng-template>\n          </ngx-datatable-column>\n        \n          <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n              <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n            </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>\n  </mat-card-content>\n</mat-card>\n</div>\n\n<!--=============================================================-->\n\n<div class=\"messages-wrap\" *ngIf=\"selectedValue === 'car_parts'\">\n  <div class=\"m-333\">\n    <!-- <span fxFlex></span> -->\n    <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"opencarpartPopUp({}, true)\">Add Car Part</button>\n  </div>\n  <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n    <mat-card-content class=\"p-0\">\n      <ngx-datatable\n            class=\"material ml-0 mr-0\"\n            [rows]=\"itemsp\"\n            [columnMode]=\"'flex'\"\n            [headerHeight]=\"50\"\n            [footerHeight]=\"50\"\n            [limit]=\"10\"\n            [rowHeight]=\"'auto'\">\n            <ngx-datatable-column name=\"Part Name\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.part_name }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Part Category\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.part_category }}\n              </ng-template>\n            </ngx-datatable-column>\n           \n            <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n                <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n              </ng-template>\n            </ngx-datatable-column>\n          </ngx-datatable>\n    </mat-card-content>\n  </mat-card>\n  </div>\n  \n\n\n</mat-card>\n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy92ZWhpY2xlL3ZlaGljbGVkZXRhaWxzL3ZlaGljbGVkZXRhaWxzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.ts ***!
  \*********************************************************************************/
/*! exports provided: VehicledetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehicledetailsComponent", function() { return VehicledetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./makedialogue/makedialogue.component */ "./src/app/views/others/vehicle/vehicledetails/makedialogue/makedialogue.component.ts");
/* harmony import */ var _modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modeldialogue/modeldialogue.component */ "./src/app/views/others/vehicle/vehicledetails/modeldialogue/modeldialogue.component.ts");
/* harmony import */ var _partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./partsdialogue/partsdialogue.component */ "./src/app/views/others/vehicle/vehicledetails/partsdialogue/partsdialogue.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var VehicledetailsComponent = /** @class */ (function () {
    function VehicledetailsComponent(media, dialog, snack, orderservice, confirmService, loader) {
        this.media = media;
        this.dialog = dialog;
        this.snack = snack;
        this.orderservice = orderservice;
        this.confirmService = confirmService;
        this.loader = loader;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
        this.itemcars = {};
        this.itemcarsarr = [];
    }
    VehicledetailsComponent.prototype.ngOnInit = function () {
        this.inboxSideNavInit();
        this.selectedValue = "car_make";
        this.getItemsv();
        this.getItemsp();
        // this.getItems()
    };
    VehicledetailsComponent.prototype.getClick = function (sele) {
        this.selectedValue = sele;
    };
    VehicledetailsComponent.prototype.returnBol = function (ret) {
        if (this.selectedValue === ret) {
            return true;
        }
        else {
            return false;
        }
    };
    VehicledetailsComponent.prototype.itemcarmethod = function (item) {
        var _this = this;
        item.forEach(function (item1, index) {
            console.log("900000======== " + item1); // 9, 2, 5
            for (var j = 0; j <= item1.vehicle_model.length; j++) {
                console.log("90000011======== " + item1.vehicle_model[j]);
                _this.itemcars.model = item1.vehicle_model[j];
                _this.itemcars.make = item1.vehicle_make;
                _this.itemcarsarr.push(_this.itemcars);
                // this.itemcars.model=null;
                // this.itemcars.make=null;
            }
        });
        /*
          item.forEach((item1, indexs) => {
            console.log("900000======== "+item1); // 9, 2, 5
          //  console.log("-------------------"+JSON.stringify( item1));
          //  console.log(index); // 0, 1, 2
        
           item1.vehicle_model.forEach((item2, index) => {
             // console.log(item); // 9, 2, 5
              //console.log(index); // 0, 1, 2
           //   console.log("-------============="+JSON.stringify( item2));
         this.itemcars.model=item2[index];
          this.itemcars.make=item1.vehicle_make[indexs];
          this.itemcarsarr.push(this.itemcars);
          
          
          });
        
        });
        */
        //console.log("-------============="+JSON.stringify( this.itemcars));
        //console.log("!!!!!!!!!!!!!!!!!!!!"+JSON.stringify(this.itemcarsarr));
    };
    VehicledetailsComponent.prototype.getItemsp = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var sub = this.orderservice.getallPart().subscribe(function (data) {
            sub.unsubscribe();
            // refresh the list
            if (data) {
                _this.itemsp = _this.tempp = data;
            }
            // this.orderservice.orders = data;
            // this.orderservice.setOrders(data);
            //   console.log("-------------------"+JSON.stringify( data));
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    VehicledetailsComponent.prototype.getItemsv = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv = this.orderservice.getallVehicles().subscribe(function (data) {
            subv.unsubscribe;
            // refresh the list
            if (data) {
                _this.itemsv = _this.tempv = data;
            }
            //   this.itemcarmethod(data);
            //   console.log("-------------------"+JSON.stringify( data));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    VehicledetailsComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    /*  getItems() {
        this.getItemSub = this.crudService.getItems()
          .subscribe(data => {
            this.items = data;
          })
      }
     */
    VehicledetailsComponent.prototype.openmakePopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Make' : 'Update Vehicle Make';
        var dialogRef = this.dialog.open(_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_7__["MakedialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                _this.getItemsv();
            }
            // this.getItemsp();
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    VehicledetailsComponent.prototype.openmodelPopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Model' : 'Update Vehicle Model';
        var dialogRef = this.dialog.open(_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_8__["ModeldialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                _this.getItemsv();
            }
            // this.getItemsp();
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    VehicledetailsComponent.prototype.opencarpartPopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Part' : 'Update Vehicle Part';
        var dialogRef = this.dialog.open(_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_9__["PartsdialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                // this.getItemsv();
                _this.getItemsp();
            }
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    VehicledetailsComponent.prototype.deleteItem = function (row) {
        /* this.confirmService.confirm({message: `Delete ${row.name}?`})
           .subscribe(res => {
             if (res) {
               this.loader.open();
            /*   this.crudService.removeItem(row)
                 .subscribe(data => {
                   this.items = data;
                   this.loader.close();
                   this.snack.open('Member deleted!', 'OK', { duration: 4000 })
                 })  */
        //   }
        //  }) 
    };
    VehicledetailsComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    VehicledetailsComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"])
    ], VehicledetailsComponent.prototype, "sideNav", void 0);
    VehicledetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vehicledetails',
            template: __webpack_require__(/*! ./vehicledetails.component.html */ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.html"),
            styles: [__webpack_require__(/*! ./vehicledetails.component.scss */ "./src/app/views/others/vehicle/vehicledetails/vehicledetails.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__["ObservableMedia"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_6__["OrderService"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__["AppLoaderService"]])
    ], VehicledetailsComponent);
    return VehicledetailsComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    apiURL: 'developmentApi'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/nems/devProjects/allautospares/purchasing system/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map