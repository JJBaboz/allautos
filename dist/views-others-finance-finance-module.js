(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-others-finance-finance-module"],{

/***/ "./src/app/views/others/finance/finance.module.ts":
/*!********************************************************!*\
  !*** ./src/app/views/others/finance/finance.module.ts ***!
  \********************************************************/
/*! exports provided: FinanceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceModule", function() { return FinanceModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _finance_routing__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./finance.routing */ "./src/app/views/others/finance/finance.routing.ts");
/* harmony import */ var _openbids_openbids_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./openbids/openbids.component */ "./src/app/views/others/finance/openbids/openbids.component.ts");
/* harmony import */ var _viewbidreports_viewbidreports_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./viewbidreports/viewbidreports.component */ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.ts");
/* harmony import */ var _partopenbid_partopenbid_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./partopenbid/partopenbid.component */ "./src/app/views/others/finance/partopenbid/partopenbid.component.ts");
/* harmony import */ var _viewbidreports_viewpartbids_viewpartbids_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./viewbidreports/viewpartbids/viewpartbids.component */ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var FinanceModule = /** @class */ (function () {
    function FinanceModule() {
    }
    FinanceModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                //BrowserAnimationsModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_10__["PerfectScrollbarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_finance_routing__WEBPACK_IMPORTED_MODULE_11__["FinanceRoutes"])
            ],
            declarations: [_openbids_openbids_component__WEBPACK_IMPORTED_MODULE_12__["OpenbidsComponent"], _viewbidreports_viewbidreports_component__WEBPACK_IMPORTED_MODULE_13__["ViewbidreportsComponent"], _partopenbid_partopenbid_component__WEBPACK_IMPORTED_MODULE_14__["PartopenbidComponent"], _viewbidreports_viewpartbids_viewpartbids_component__WEBPACK_IMPORTED_MODULE_15__["ViewpartbidsComponent"]]
        })
    ], FinanceModule);
    return FinanceModule;
}());



/***/ }),

/***/ "./src/app/views/others/finance/finance.routing.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/others/finance/finance.routing.ts ***!
  \*********************************************************/
/*! exports provided: FinanceRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinanceRoutes", function() { return FinanceRoutes; });
/* harmony import */ var _openbids_openbids_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./openbids/openbids.component */ "./src/app/views/others/finance/openbids/openbids.component.ts");
/* harmony import */ var _viewbidreports_viewbidreports_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./viewbidreports/viewbidreports.component */ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.ts");
/* harmony import */ var _partopenbid_partopenbid_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./partopenbid/partopenbid.component */ "./src/app/views/others/finance/partopenbid/partopenbid.component.ts");
/* harmony import */ var _viewbidreports_viewpartbids_viewpartbids_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./viewbidreports/viewpartbids/viewpartbids.component */ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.ts");




var FinanceRoutes = [
    {
        path: '',
        //component: NewvehicleComponent,
        // data: { title: 'Blank', breadcrumb: 'BLANK' }   
        children: [{
                path: 'openbids',
                component: _openbids_openbids_component__WEBPACK_IMPORTED_MODULE_0__["OpenbidsComponent"],
                data: { title: 'Blank', breadcrumb: 'BLANK' }
            },
            {
                path: 'partopenbid/:id',
                component: _partopenbid_partopenbid_component__WEBPACK_IMPORTED_MODULE_2__["PartopenbidComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            },
            {
                path: 'viewbiddingreport',
                component: _viewbidreports_viewbidreports_component__WEBPACK_IMPORTED_MODULE_1__["ViewbidreportsComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            },
            {
                path: 'viewpartbids/:id',
                component: _viewbidreports_viewpartbids_viewpartbids_component__WEBPACK_IMPORTED_MODULE_3__["ViewpartbidsComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            }
        ]
    }
];


/***/ }),

/***/ "./src/app/views/others/finance/openbids/openbids.component.html":
/*!***********************************************************************!*\
  !*** ./src/app/views/others/finance/openbids/openbids.component.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-333\">\n  <!-- <span fxFlex></span> -->\n  <mat-form-field class=\"margin-333\" style=\"width: 100%\">\n    <input \n    matInput \n    placeholder=\"Type to search from all columns eg Registration Number of a Car\" \n    value=\"\"\n    (keyup)='updateFilter($event)'>\n  </mat-form-field>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n  <mat-card-content class=\"p-0\">\n    <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"items\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\"\n          [limit]=\"10\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Jobcard No.\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.jobcard_no }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Car Registration No.\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.carReg_no }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Car Model\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.car_model }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Order date\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.order_date }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Order Status\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                \n                    {{ row?.order_status }}\n                 \n             <!-- <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'Complete' : 'Bidding'}}</mat-chip>\n      -->      </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n           <!--   <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" ><mat-icon>edit</mat-icon></button>\n              <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n            \n             [routerLink]=\"['orders/showorderparts', row.car_model]\" (click)= openOrder()   [routerLink]=\"['/orders/showorderparts', row.order_id]\"\n            -->\n            <div>\n                \n              <div   fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                  <button mat-raised-button color=\"primary\" (click)= openOrdertoOpenbid(row.order_id)  >\n                      <mat-icon>open_with</mat-icon>\n                      OPEN\n                  </button>\n                   \n                    </div>  \n                  \n            </div>  \n            </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>\n  </mat-card-content>\n</mat-card>\n"

/***/ }),

/***/ "./src/app/views/others/finance/openbids/openbids.component.scss":
/*!***********************************************************************!*\
  !*** ./src/app/views/others/finance/openbids/openbids.component.scss ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9maW5hbmNlL29wZW5iaWRzL29wZW5iaWRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/finance/openbids/openbids.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/views/others/finance/openbids/openbids.component.ts ***!
  \*********************************************************************/
/*! exports provided: OpenbidsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OpenbidsComponent", function() { return OpenbidsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-navigation-with-data */ "./node_modules/ngx-navigation-with-data/fesm5/ngx-navigation-with-data.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var OpenbidsComponent = /** @class */ (function () {
    function OpenbidsComponent(dialog, snack, 
    // private tableService: TableService,
    confirmService, loader, router, orderservice, 
    // private socketService: SocketService,
    navCtrl) {
        this.dialog = dialog;
        this.snack = snack;
        this.confirmService = confirmService;
        this.loader = loader;
        this.router = router;
        this.orderservice = orderservice;
        this.navCtrl = navCtrl;
    }
    OpenbidsComponent.prototype.ngOnInit = function () {
        this.getItems2();
        //this.initIoConnection()
    };
    /*
    private initIoConnection(): void {
      this.socketService.initSocket();
  
      this.ioConnection = this.socketService.onBid()
        .subscribe((bid: Bid) => {
          this.bids.push(bid);
        });
  
  /*
      this.socketService.onEvent(Event.CONNECT)
        .subscribe(() => {
          console.log('connected');
        });
  
      this.socketService.onEvent(Event.DISCONNECT)
        .subscribe(() => {
          console.log('disconnected');
        }); */
    // }
    OpenbidsComponent.prototype.acceptBid = function (message) {
        if (!message) {
            return;
        }
        /* this.socketService.send({
           from: this.user,
           content: message
         }); */
        //  this.messageContent = null;
    };
    OpenbidsComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    OpenbidsComponent.prototype.getItems2 = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            _this.orderservice.orders = data;
            _this.orderservice.setOrders(data);
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    OpenbidsComponent.prototype.getItems = function () {
        var _this = this;
        /* this.getItemSub = this.orderservice.getOrders()
         //this.getItemSub = this.orderservice.postgetOrder()
             .subscribe(data => {
              this.items =this.temp = data;
             // console.log(data+"-------------------fghjklkjhgfdsdfghjkjhdsasdfghjhgfdsasdfghjk");
             })
           //..  console.log (JSON.stringify(this.items )+"----------------------------"+this.getItemSub);
          */
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            console.log(JSON.stringify(data) + "-------------------");
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    OpenbidsComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var columns = Object.keys(this.temp[0]);
        // Removes last "$$index" from "column"
        columns.splice(columns.length - 1);
        // console.log(columns);
        if (!columns.length)
            return;
        var rows = this.temp.filter(function (d) {
            for (var i = 0; i <= columns.length; i++) {
                var column = columns[i];
                // console.log(d[column]);
                if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
                    return true;
                }
            }
        });
        this.items = rows;
    };
    //JSON.stringify(this.navCtrl.data)
    OpenbidsComponent.prototype.openOrdertoOpenbid = function (id) {
        //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
        //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
        var ite = this.items.find(function (x) { return x.order_id == id; });
        this.router.navigate(['finance/partopenbid', ite.order_id]);
    };
    OpenbidsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-openbids',
            template: __webpack_require__(/*! ./openbids.component.html */ "./src/app/views/others/finance/openbids/openbids.component.html"),
            styles: [__webpack_require__(/*! ./openbids.component.scss */ "./src/app/views/others/finance/openbids/openbids.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_2__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__["AppLoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__["OrderService"],
            ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_6__["NgxNavigationWithDataComponent"]])
    ], OpenbidsComponent);
    return OpenbidsComponent;
}());



/***/ }),

/***/ "./src/app/views/others/finance/partopenbid/partopenbid.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/views/others/finance/partopenbid/partopenbid.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n \n\n\n  <mat-sidenav  #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\" >\n    <!-- Compose button -->\n    <button mat-raised-button class=\"mat-warn full-width\" >{{Order.carReg_no}}</button>\n    <!-- left side buttons           -->\n    <div [perfectScrollbar] class=\"chat-sidebar-scroll\">\n    <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\n      <mat-list-item class=\"primary-imenu-item\"  routerLinkActive=\"open\" *ngFor=\"let item of Order.autorised_parts ; let i = index\" \n      [style.background]=\"selected==i?  '#e91e63':'black'\" (click)=\"selected=i\"    (click)= onclickpart(item.part_id) >\n        <a fxLayout=\"row\">\n           \n            <span> {{item.partName}} </span>\n        </a>\n      \n        <mat-divider></mat-divider>\n      </mat-list-item>\n     \n      \n\n    </mat-nav-list>\n  </div>\n  </mat-sidenav>\n\n\n\n<mat-card class=\"p-0\" style=\"height: 370px;\">\n  <mat-card-title class=\"\">\n    <div class=\"card-title-text\">Vehicle Part Status</div>\n    <mat-divider></mat-divider>\n  </mat-card-title>\n  <mat-card-content>\n     <!--  *ngIf=\"item.part_status == 'Closed'\"  \n  <div  *ngIf=\"partstatus() == 'Closed'\">\n    <button type=\"button\"\n    mat-raised-button \n    color=\"primary\" \n    (click)=openBidding(selectedid)>Open Bidding</button>\n  </div>  -->\n\n  <div  *ngIf=\"partstatus() != 'Bidding' ; then thenBlock; else elseBlock\"> </div>\n  <ng-template #thenBlock>\n      <button type=\"button\"\n      mat-raised-button \n      color=\"primary\" \n      (click)=openBidding(selectedid)>Open Bidding</button>\n                     \n  </ng-template>\n  <ng-template #elseBlock>\n\n      <p>The Status for this part is {{ partstatus() }}</p>\n    </ng-template>\n\n  </mat-card-content>\n  </mat-card>\n  \n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/finance/partopenbid/partopenbid.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/views/others/finance/partopenbid/partopenbid.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9maW5hbmNlL3BhcnRvcGVuYmlkL3BhcnRvcGVuYmlkLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/finance/partopenbid/partopenbid.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/views/others/finance/partopenbid/partopenbid.component.ts ***!
  \***************************************************************************/
/*! exports provided: PartopenbidComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartopenbidComponent", function() { return PartopenbidComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PartopenbidComponent = /** @class */ (function () {
    function PartopenbidComponent(media, route, orderservice, router) {
        //  console.log(JSON.stringify(this.navCtrl.data)); 
        this.media = media;
        this.route = route;
        this.orderservice = orderservice;
        this.router = router;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
    }
    PartopenbidComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.inboxSideNavInit();
        this.productID = this.route.snapshot.params['id'];
        //console.log(this.productID ); 
        //this.Order = JSON.parse(this.productID);
        this.Order = this.orderservice.giveOrders().find(function (x) { return x.order_id == _this.productID; });
        this.selectedid = this.Order.autorised_parts[0].part_id;
        // console.log("-------------------"+JSON.stringify(this.orderservice.giveOrders()));
        //this.Order =  this.getorderfrom(this.productID);
        //  console.log(this.orderservice.giveOrders());
    };
    PartopenbidComponent.prototype.onclickpart = function (id) {
        //  this.getbids(id);
        // this.initc=false;
        var ptname = this.Order.autorised_parts.find(function (x) { return x.part_id == id; }).partName;
        this.selectedid = id;
        // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
    };
    PartopenbidComponent.prototype.partstatus = function () {
        var _this = this;
        //  this.getbids(id);
        // this.initc=false;
        var ptname = this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; }).part_status;
        return ptname;
        // this.selectedid=id;
        // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
    };
    PartopenbidComponent.prototype.openBidding = function (id) {
        var _this = this;
        this.Order.autorised_parts.find(function (x) { return x.part_id == id; });
        //this.items =this.temp =null;
        this.orderservice.closeopenBids(id, localStorage.getItem('cnumber'), this.productID, localStorage.getItem('uemail'), "Bidding").subscribe(function (data) {
            if (data["result"] == "Success") {
                _this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; }).part_status = 'Bidding';
            }
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            // return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    PartopenbidComponent.prototype.getorderfrom = function (id) {
        //return this.orderservice.orders.find(x=>x.order_id == id)
    };
    PartopenbidComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    PartopenbidComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSidenav"])
    ], PartopenbidComponent.prototype, "sideNav", void 0);
    PartopenbidComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-partopenbid',
            template: __webpack_require__(/*! ./partopenbid.component.html */ "./src/app/views/others/finance/partopenbid/partopenbid.component.html"),
            styles: [__webpack_require__(/*! ./partopenbid.component.scss */ "./src/app/views/others/finance/partopenbid/partopenbid.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_4__["ObservableMedia"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], PartopenbidComponent);
    return PartopenbidComponent;
}());



/***/ }),

/***/ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/views/others/finance/viewbidreports/viewbidreports.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-333\">\n        <!-- <span fxFlex></span> -->\n        <mat-form-field class=\"margin-333\" style=\"width: 100%\">\n          <input \n          matInput \n          placeholder=\"Type to search from all columns eg Registration Number of a Car\" \n          value=\"\"\n          (keyup)='updateFilter($event)'>\n        </mat-form-field>\n      </div>\n      <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n        <mat-card-content class=\"p-0\">\n          <ngx-datatable\n                class=\"material ml-0 mr-0\"\n                [rows]=\"items\"\n                [columnMode]=\"'flex'\"\n                [headerHeight]=\"50\"\n                [footerHeight]=\"50\"\n                [limit]=\"10\"\n                [rowHeight]=\"'auto'\">\n                <ngx-datatable-column name=\"Jobcard No.\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                    {{ row?.jobcard_no }}\n                  </ng-template>\n                </ngx-datatable-column>\n                <ngx-datatable-column name=\"Car Registration No.\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                    {{ row?.carReg_no }}\n                  </ng-template>\n                </ngx-datatable-column>\n                <ngx-datatable-column name=\"Car Model\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                    {{ row?.car_model }}\n                  </ng-template>\n                </ngx-datatable-column>\n                <ngx-datatable-column name=\"Order date\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                    {{ row?.order_date }}\n                  </ng-template>\n                </ngx-datatable-column>\n                <ngx-datatable-column name=\"Order Status\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                      \n                          {{ row?.order_status }}\n                       \n                   <!-- <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'Complete' : 'Bidding'}}</mat-chip>\n            -->      </ng-template>\n                </ngx-datatable-column>\n                <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n                  <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                 <!--   <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" ><mat-icon>edit</mat-icon></button>\n                    <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n                  \n                   [routerLink]=\"['orders/showorderparts', row.car_model]\" (click)= openOrder()   [routerLink]=\"['/orders/showorderparts', row.order_id]\"\n                  -->\n                  <div>\n                      \n                    <div   fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                        <button mat-raised-button color=\"primary\" (click)= openOrder(row.order_id)  >\n                            <mat-icon>open_with</mat-icon>\n                            OPEN\n                        </button>\n                         \n                          </div>  \n                        \n                  </div>  \n                  </ng-template>\n                </ngx-datatable-column>\n              </ngx-datatable>\n        </mat-card-content>\n      </mat-card>\n      "

/***/ }),

/***/ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/views/others/finance/viewbidreports/viewbidreports.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9maW5hbmNlL3ZpZXdiaWRyZXBvcnRzL3ZpZXdiaWRyZXBvcnRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/views/others/finance/viewbidreports/viewbidreports.component.ts ***!
  \*********************************************************************************/
/*! exports provided: ViewbidreportsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewbidreportsComponent", function() { return ViewbidreportsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ngx-navigation-with-data */ "./node_modules/ngx-navigation-with-data/fesm5/ngx-navigation-with-data.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ViewbidreportsComponent = /** @class */ (function () {
    function ViewbidreportsComponent(dialog, snack, confirmService, loader, router, orderservice, navCtrl) {
        this.dialog = dialog;
        this.snack = snack;
        this.confirmService = confirmService;
        this.loader = loader;
        this.router = router;
        this.orderservice = orderservice;
        this.navCtrl = navCtrl;
        this.bids = [];
    }
    ViewbidreportsComponent.prototype.ngOnInit = function () {
        this.getItems2();
        //this.initIoConnection()
    };
    ViewbidreportsComponent.prototype.acceptBid = function (message) {
        if (!message) {
            return;
        }
        /* this.socketService.send({
           from: this.user,
           content: message
         }); */
        //  this.messageContent = null;
    };
    ViewbidreportsComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    ViewbidreportsComponent.prototype.getItems2 = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            _this.orderservice.orders = data;
            _this.orderservice.setOrders(data);
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    ViewbidreportsComponent.prototype.getItems = function () {
        var _this = this;
        /* this.getItemSub = this.orderservice.getOrders()
         //this.getItemSub = this.orderservice.postgetOrder()
             .subscribe(data => {
              this.items =this.temp = data;
             // console.log(data+"-------------------fghjklkjhgfdsdfghjkjhdsasdfghjhgfdsasdfghjk");
             })
           //..  console.log (JSON.stringify(this.items )+"----------------------------"+this.getItemSub);
          */
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            console.log(JSON.stringify(data) + "-------------------");
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    ViewbidreportsComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var columns = Object.keys(this.temp[0]);
        // Removes last "$$index" from "column"
        columns.splice(columns.length - 1);
        // console.log(columns);
        if (!columns.length)
            return;
        var rows = this.temp.filter(function (d) {
            for (var i = 0; i <= columns.length; i++) {
                var column = columns[i];
                // console.log(d[column]);
                if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
                    return true;
                }
            }
        });
        this.items = rows;
    };
    //JSON.stringify(this.navCtrl.data)
    ViewbidreportsComponent.prototype.openOrder = function (id) {
        //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
        //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
        var ite = this.items.find(function (x) { return x.order_id == id; });
        this.router.navigate(['finance/viewpartbids', ite.order_id]);
    };
    ViewbidreportsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-viewbidreports',
            template: __webpack_require__(/*! ./viewbidreports.component.html */ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.html"),
            styles: [__webpack_require__(/*! ./viewbidreports.component.scss */ "./src/app/views/others/finance/viewbidreports/viewbidreports.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_2__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__["AppLoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__["OrderService"],
            ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_6__["NgxNavigationWithDataComponent"]])
    ], ViewbidreportsComponent);
    return ViewbidreportsComponent;
}());



/***/ }),

/***/ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.html":
/*!**********************************************************************************************!*\
  !*** ./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.html ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n\n    <mat-sidenav  #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\" >\n      <!-- Compose button -->\n      <button mat-raised-button class=\"mat-warn full-width\" >{{Order.carReg_no}}</button>\n      <!-- left side buttons           -->\n      <div [perfectScrollbar] class=\"chat-sidebar-scroll\">\n      <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\n        <mat-list-item class=\"primary-imenu-item\"  routerLinkActive=\"open\" *ngFor=\"let item of Order.autorised_parts ; let i = index\" \n        [style.background]=\"selected==i?  '#e91e63':'black'\" (click)=\"selected=i\"    (click)= onclickpart(item.part_id) >\n          <a fxLayout=\"row\">\n             \n              <span> {{item.partName}} </span>\n          </a>\n        \n          <mat-divider></mat-divider>\n        </mat-list-item>\n       \n        \n  \n      </mat-nav-list>\n    </div>\n    </mat-sidenav>\n  \n    <div  *ngIf=\"partstatus() == 'Complete' ; then thenBlock; else elseBlock\"> </div>\n    <ng-template #thenBlock>\n    \n        \n  <mat-card class=\"p-0\" >\n      <mat-card-title class=\"mat-bg-primary m-0\">\n          <div class=\"card-title-text\">\n            <span style=\"width: 100%;text-align: center\" >Awarded Bid</span>\n            <span fxFlex></span>\n            \n          </div>\n        \n        </mat-card-title>\n    <mat-card-content>\n   \n        <ngx-datatable\n        class=\"material ml-0 mr-0\"\n        [rows]=\"itemswon\"\n        [columnMode]=\"'flex'\"\n        [headerHeight]=\"50\"\n        [footerHeight]=\"50\"\n        [limit]=\"10\"\n        [rowHeight]=\"'auto'\">\n        <ngx-datatable-column name=\"Supplier Name\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidSourceName }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Supplier Phone No.\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidSupplierPhone }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Amount per Item\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidAmount }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Supplier Status\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidSupplyStatus }}\n          </ng-template>\n        </ngx-datatable-column>\n     <!--  <ngx-datatable-column name=\"Order Status\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'Complete' : 'Bidding'}}</mat-chip>\n          </ng-template>  \n        </ngx-datatable-column>--> \n       \n      </ngx-datatable>\n          \n  \n    </mat-card-content>\n    </mat-card>\n    \n     \n  <mat-card class=\"p-0\" style=\"height: 320px;\">\n      <mat-card-title class=\"mat-bg-primary m-0\">\n          <div class=\"card-title-text\">\n            <span style=\"width: 100%;text-align: center\" >All Bids</span>\n            <span fxFlex></span>\n            \n          </div>\n        \n        </mat-card-title>\n    <mat-card-content>\n   \n      \n        <ngx-datatable\n        class=\"material ml-0 mr-0\"\n        [rows]=\"items\"\n        [columnMode]=\"'flex'\"\n        [headerHeight]=\"50\"\n        [footerHeight]=\"50\"\n        [limit]=\"10\"\n        [rowHeight]=\"'auto'\">\n        <ngx-datatable-column name=\"Supplier Name\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidSourceName }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Supplier Phone No.\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidSupplierPhone }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Amount per Item\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidAmount }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Supplier Status\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.bidSupplyStatus }}\n          </ng-template>\n        </ngx-datatable-column>\n\n       \n      </ngx-datatable>\n          \n          \n  \n    </mat-card-content>\n    </mat-card>\n        \n      \n\n    </ng-template>\n    <ng-template #elseBlock>\n        <mat-card class=\"p-0\" style=\"height: 370px;\">\n            <mat-card-title class=\"mat-bg-primary m-0\">\n                <div class=\"card-title-text\">\n                  <span style=\"width: 100%;text-align: center\" >Part Status</span>\n                  <span fxFlex></span>\n                  \n                </div>\n              \n              </mat-card-title>\n          <mat-card-content>\n         \n      <p>The Purchase process of this part is not Complete</p>\n\n    </mat-card-content>\n  </mat-card>\n                       \n    </ng-template>\n\n  </mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.scss":
/*!**********************************************************************************************!*\
  !*** ./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.scss ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9maW5hbmNlL3ZpZXdiaWRyZXBvcnRzL3ZpZXdwYXJ0Ymlkcy92aWV3cGFydGJpZHMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.ts":
/*!********************************************************************************************!*\
  !*** ./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.ts ***!
  \********************************************************************************************/
/*! exports provided: ViewpartbidsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewpartbidsComponent", function() { return ViewpartbidsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ViewpartbidsComponent = /** @class */ (function () {
    function ViewpartbidsComponent(media, route, orderservice, router) {
        //  console.log(JSON.stringify(this.navCtrl.data)); 
        this.media = media;
        this.route = route;
        this.orderservice = orderservice;
        this.router = router;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
    }
    ViewpartbidsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.inboxSideNavInit();
        this.productID = this.route.snapshot.params['id'];
        //console.log(this.productID ); 
        //this.Order = JSON.parse(this.productID);
        this.Order = this.orderservice.giveOrders().find(function (x) { return x.order_id == _this.productID; });
        this.getbids(this.Order.autorised_parts[0].part_id);
        this.selectedid = this.Order.autorised_parts[0].part_id;
        // console.log("-------------------"+JSON.stringify(this.orderservice.giveOrders()));
        //this.Order =  this.getorderfrom(this.productID);
        //  console.log(this.orderservice.giveOrders());
    };
    ViewpartbidsComponent.prototype.onclickpart = function (id) {
        //  this.getbids(id);
        // this.initc=false;
        this.itemswon = null;
        this.items = this.temp = null;
        var ptname = this.Order.autorised_parts.find(function (x) { return x.part_id == id; }).partName;
        this.selectedid = id;
        this.getbids(id);
        // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
    };
    ViewpartbidsComponent.prototype.partstatus = function () {
        var _this = this;
        //  this.getbids(id);
        // this.initc=false;
        var ptname = this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; }).part_status;
        return ptname;
        // this.selectedid=id;
        // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
    };
    ViewpartbidsComponent.prototype.openBidding = function (id) {
        var _this = this;
        this.Order.autorised_parts.find(function (x) { return x.part_id == id; });
        //this.items =this.temp =null;
        this.orderservice.closeopenBids(id, localStorage.getItem('cnumber'), this.productID, localStorage.getItem('uemail'), "Bidding").subscribe(function (data) {
            if (data["result"] == "Success") {
                _this.Order.autorised_parts.find(function (x) { return x.part_id == _this.selectedid; }).part_status = 'Bidding';
            }
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            // return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    ViewpartbidsComponent.prototype.getorderfrom = function (id) {
        //return this.orderservice.orders.find(x=>x.order_id == id)
    };
    ViewpartbidsComponent.prototype.getWonBid = function () {
        //  itemswon;
        //this.Order.autorised_parts.find(x=>x.part_id == id)
        //console.log("========================= = = =  "+JSON.stringify( this.items));
        // console.log("========================= = = =  "+JSON.stringify(this.items.find(x=>x.bidAmount== "5800")));
        //  console.log("========================= = = =  "+JSON.stringify(this.items.find(x=>x.bidStatus[x.bidStatus.length-1]== "won unconfirmed")));
        if (this.items.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won confirmed"; })) {
            this.itemswon = [this.items.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won confirmed"; })];
        }
        else {
            this.itemswon = [this.items.find(function (x) { return x.bidStatus[x.bidStatus.length - 1] == "won unconfirmed"; })];
        }
        //this.itemswon = this.items.find(x=>x.bidStatus[this.items.bidStatus.length] == "won unconfirmed");
        //this.items =this.temp =null;
        /*  this.orderservice.closeopenBids(id,localStorage.getItem('cnumber'),this.productID,localStorage.getItem('uemail'),"Bidding").subscribe(
            data => {
              if(data["result"]=="Success"){
                this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status='Bidding'
        
              }
            
             },
             error => {
               console.error("Error saving !");
             //  return Observable.throw(error);
            }
          );  */
    };
    ViewpartbidsComponent.prototype.getbids = function (partid) {
        var _this = this;
        this.items = this.temp = null;
        this.orderservice.getPartBids(partid).subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            _this.getWonBid();
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    ViewpartbidsComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    ViewpartbidsComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"])
    ], ViewpartbidsComponent.prototype, "sideNav", void 0);
    ViewpartbidsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-viewpartbids',
            template: __webpack_require__(/*! ./viewpartbids.component.html */ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.html"),
            styles: [__webpack_require__(/*! ./viewpartbids.component.scss */ "./src/app/views/others/finance/viewbidreports/viewpartbids/viewpartbids.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_1__["ObservableMedia"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], ViewpartbidsComponent);
    return ViewpartbidsComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-others-finance-finance-module.js.map