(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-others-manage-manage-module"],{

/***/ "./src/app/views/others/manage/addcompany/addcompany.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/views/others/manage/addcompany/addcompany.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"p-0\">\n  <mat-card-title class=\"\">\n    <div class=\"card-title-text\">ADD COMPANY</div>\n    <mat-divider></mat-divider>\n  </mat-card-title>\n  <mat-card-content>\n    <mat-horizontal-stepper [linear]=\"true\">\n      <mat-step [stepControl]=\"firstFormGroup\">\n        <form [formGroup]=\"firstFormGroup\">\n          <ng-template matStepLabel>Fill Company Details</ng-template>\n          <div fxLayout=\"row wrap\">\n            <div fxFlex=\"100\" fxFlex.gt-xs=\"50\" class=\"pr-1\">\n                    <div class=\"pb-1\">\n                            <mat-form-field class=\"full-width\">\n                                <input\n                                matInput\n                                name=\"company_name\"\n                                formControlName=\"company_name\"\n                                placeholder=\"Company Name\"\n                                value=\"\">\n                            </mat-form-field>\n                           \n                        </div>\n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input\n                        matInput\n                        name=\"company_postaladdress\"\n                        formControlName=\"company_postaladdress\"\n                        placeholder=\"Postal Address\"\n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input\n                        matInput\n                        name=\"company_phonenumber\"\n                        formControlName=\"company_phonenumber\"\n                        placeholder=\"Phone No.\"\n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                    <input\n                        matInput\n                        \n                        name=\"company_email\"\n                        formControlName=\"company_email\"\n                        placeholder=\"Email Address\"\n                        value=\"\" required>\n                    </mat-form-field>\n                    \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                    <input\n                        matInput\n                        name=\"company_website\"\n                        formControlName=\"company_website\"\n                        placeholder=\"Website\"\n                        value=\"\"  required>\n                    </mat-form-field>\n                    \n                </div>\n\n\n                <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                  <input\n                      matInput\n                      name=\"location_county\"\n                      formControlName=\"location_county\"\n                      placeholder=\"Location County\"\n                      value=\"\"  required>\n                  </mat-form-field>\n                  \n              </div>\n\n  \n               \n            </div>\n  \n          </div>\n          <div>\n            <button mat-raised-button color=\"primary\" matStepperNext>Next</button>\n          </div>\n        </form>\n      </mat-step>\n      <mat-step [stepControl]=\"secondFormGroup\">\n        <form [formGroup]=\"secondFormGroup\">\n          <ng-template matStepLabel>Register Admin</ng-template>\n          <div fxLayout=\"row wrap\">\n            <div fxFlex=\"100\" fxFlex.gt-xs=\"50\" class=\"pr-1\">\n                    <div class=\"pb-1\">\n                            <mat-form-field class=\"full-width\">\n                                <input\n                                matInput\n                                name=\"user_name\"\n                                formControlName=\"user_name\"\n                                placeholder=\"User Name\"\n                                value=\"\">\n                            </mat-form-field>\n                           \n                        </div>\n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input\n                        matInput\n                        name=\"user_phoneno\"\n                        formControlName=\"user_phoneno\"\n                        placeholder=\"Phone Number\"\n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input\n                        matInput\n                        name=\"user_email\"\n                        formControlName=\"user_email\"\n                        placeholder=\"Email Address\"\n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n             \n  \n               \n            </div>\n  \n          </div>\n          <div fxLayout=\"row\">\n            <button mat-raised-button color=\"accent\" matStepperPrevious>Back</button>\n            <span fxFlex=\"8px\"></span>\n            <button mat-raised-button color=\"primary\" matStepperNext>Next</button>\n          </div>\n        </form>\n      </mat-step>\n      <mat-step>\n        <ng-template matStepLabel>Done</ng-template>\n        <div>\n          <mat-icon class=\"pt-1\" [style.fontSize]=\"'36px'\">check</mat-icon>\n        </div>\n        <div class=\"pb-1 mb-1\"> You Are Done.</div>\n        <div fxLayout=\"row\">\n          <button mat-raised-button color=\"accent\" matStepperPrevious>Back</button>\n          <span fxFlex=\"8px\"></span>\n          <button mat-raised-button color=\"primary\" (click)=sendCompreg() >Submit</button>\n        </div>\n      </mat-step>\n    </mat-horizontal-stepper>\n  </mat-card-content>\n</mat-card>"

/***/ }),

/***/ "./src/app/views/others/manage/addcompany/addcompany.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/others/manage/addcompany/addcompany.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvYWRkY29tcGFueS9hZGRjb21wYW55LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/manage/addcompany/addcompany.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/others/manage/addcompany/addcompany.component.ts ***!
  \************************************************************************/
/*! exports provided: AddcompanyComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddcompanyComponent", function() { return AddcompanyComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AddcompanyComponent = /** @class */ (function () {
    function AddcompanyComponent(fb, orderservice, router) {
        this.fb = fb;
        this.orderservice = orderservice;
        this.router = router;
        this.compreg = {};
    }
    AddcompanyComponent.prototype.ngOnInit = function () {
        this.firstFormGroup = this.fb.group({
            company_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            company_postaladdress: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            company_phonenumber: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            company_email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            company_website: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            location_county: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        }),
            this.secondFormGroup = this.fb.group({
                user_name: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                user_phoneno: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
                user_email: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
            });
    };
    AddcompanyComponent.prototype.sendCompreg = function () {
        var _this = this;
        console.log("-------------------" + this.firstFormGroup.value.company_name);
        this.compreg.company_name = this.firstFormGroup.value.company_name;
        this.compreg.company_postaladdress = this.firstFormGroup.value.company_postaladdress;
        this.compreg.company_phonenumber = this.firstFormGroup.value.company_phonenumber;
        this.compreg.company_email = this.firstFormGroup.value.company_email;
        this.compreg.company_website = this.firstFormGroup.value.company_website;
        this.compreg.location_county = this.firstFormGroup.value.location_county;
        this.compreg.user_name = this.secondFormGroup.value.user_name;
        this.compreg.user_phoneno = this.secondFormGroup.value.user_phoneno;
        this.compreg.user_email = this.secondFormGroup.value.user_email;
        console.log(this.compreg);
        this.subscription = this.orderservice.addCompany(this.compreg).subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            // this.orderservice.orders = data;
            // this.orderservice.setOrders(data);
            _this.subscription.unsubscribe();
            if (data["result"] == "Success") {
                _this.firstFormGroup.reset();
                _this.secondFormGroup.reset();
                _this.router.navigate(['others']);
            }
            else {
            }
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    AddcompanyComponent.prototype.ngOnDestroy = function () {
        //Called once, before the instance is destroyed.
        //Add 'implements OnDestroy' to the class.
        //  this.subscription.unsubscribe();
    };
    AddcompanyComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-addcompany',
            providers: [app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"]],
            template: __webpack_require__(/*! ./addcompany.component.html */ "./src/app/views/others/manage/addcompany/addcompany.component.html"),
            styles: [__webpack_require__(/*! ./addcompany.component.scss */ "./src/app/views/others/manage/addcompany/addcompany.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AddcompanyComponent);
    return AddcompanyComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/manage.module.ts":
/*!******************************************************!*\
  !*** ./src/app/views/others/manage/manage.module.ts ***!
  \******************************************************/
/*! exports provided: ManageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageModule", function() { return ManageModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _addcompany_addcompany_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./addcompany/addcompany.component */ "./src/app/views/others/manage/addcompany/addcompany.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./users/users.component */ "./src/app/views/others/manage/users/users.component.ts");
/* harmony import */ var _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./suppliers/suppliers.component */ "./src/app/views/others/manage/suppliers/suppliers.component.ts");
/* harmony import */ var _manage_routing__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./manage.routing */ "./src/app/views/others/manage/manage.routing.ts");
/* harmony import */ var _vehicles_vehicles_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./vehicles/vehicles.component */ "./src/app/views/others/manage/vehicles/vehicles.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var ManageModule = /** @class */ (function () {
    function ManageModule() {
    }
    ManageModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                //BrowserAnimationsModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_10__["PerfectScrollbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatToolbarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_manage_routing__WEBPACK_IMPORTED_MODULE_14__["ManageRoutes"])
            ],
            declarations: [_addcompany_addcompany_component__WEBPACK_IMPORTED_MODULE_11__["AddcompanyComponent"], _users_users_component__WEBPACK_IMPORTED_MODULE_12__["UsersComponent"], _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_13__["SuppliersComponent"], _vehicles_vehicles_component__WEBPACK_IMPORTED_MODULE_15__["VehiclesComponent"]]
        })
    ], ManageModule);
    return ManageModule;
}());



/***/ }),

/***/ "./src/app/views/others/manage/manage.routing.ts":
/*!*******************************************************!*\
  !*** ./src/app/views/others/manage/manage.routing.ts ***!
  \*******************************************************/
/*! exports provided: ManageRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageRoutes", function() { return ManageRoutes; });
/* harmony import */ var _addcompany_addcompany_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./addcompany/addcompany.component */ "./src/app/views/others/manage/addcompany/addcompany.component.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users/users.component */ "./src/app/views/others/manage/users/users.component.ts");
/* harmony import */ var _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./suppliers/suppliers.component */ "./src/app/views/others/manage/suppliers/suppliers.component.ts");
/* harmony import */ var _vehicles_vehicles_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./vehicles/vehicles.component */ "./src/app/views/others/manage/vehicles/vehicles.component.ts");




var ManageRoutes = [
    {
        path: '',
        // component: ManageComponent,
        children: [{
                path: 'addcompany',
                component: _addcompany_addcompany_component__WEBPACK_IMPORTED_MODULE_0__["AddcompanyComponent"],
                data: { title: 'Blank', breadcrumb: 'BLANK' }
            },
            {
                path: 'users',
                component: _users_users_component__WEBPACK_IMPORTED_MODULE_1__["UsersComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            },
            {
                path: 'suppliers',
                component: _suppliers_suppliers_component__WEBPACK_IMPORTED_MODULE_2__["SuppliersComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            },
            {
                path: 'vehicles',
                component: _vehicles_vehicles_component__WEBPACK_IMPORTED_MODULE_3__["VehiclesComponent"],
                data: { title: 'Basic', breadcrumb: 'BASIC' }
            }
        ]
    }
];


/***/ }),

/***/ "./src/app/views/others/manage/suppliers/suppliers.component.html":
/*!************************************************************************!*\
  !*** ./src/app/views/others/manage/suppliers/suppliers.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"m-333\">\n  <!-- <span fxFlex></span> -->\n  <mat-form-field class=\"margin-333\" style=\"width: 100%\">\n    <input \n    matInput \n    placeholder=\"Type to search from all columns\" \n    value=\"\"\n    (keyup)='updateFilter($event)'>\n  </mat-form-field>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n  <mat-card-content class=\"p-0\">\n    <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"items\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\"\n          [limit]=\"10\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Supplier Name\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.requisition_no }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Phone No.\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.carReg_no }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Status\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.car_model }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Action\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.order_date }}\n            </ng-template>\n          </ngx-datatable-column>\n       <!--\n          <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n         \n            <div>\n                \n              <div   fxLayout=\"row\" style=\"flex-direction: row; display: flex;\">\n                  <button mat-raised-button color=\"primary\" (click)= openOrder(row.order_id)  >\n                      <mat-icon>open_with</mat-icon>\n                      OPEN\n                  </button>\n                   \n                    </div>  \n                  \n            </div>  \n            </ng-template>\n          </ngx-datatable-column>\n\n        -->\n        </ngx-datatable>\n  </mat-card-content>\n</mat-card>\n"

/***/ }),

/***/ "./src/app/views/others/manage/suppliers/suppliers.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/others/manage/suppliers/suppliers.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2Uvc3VwcGxpZXJzL3N1cHBsaWVycy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/others/manage/suppliers/suppliers.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/others/manage/suppliers/suppliers.component.ts ***!
  \**********************************************************************/
/*! exports provided: SuppliersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SuppliersComponent", function() { return SuppliersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var app_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! app/shared/services/socket.service */ "./src/app/shared/services/socket.service.ts");
/* harmony import */ var ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ngx-navigation-with-data */ "./node_modules/ngx-navigation-with-data/fesm5/ngx-navigation-with-data.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


//import { TableService } from '../../orders/tables.service';






var SuppliersComponent = /** @class */ (function () {
    function SuppliersComponent(dialog, snack, 
    // private tableService: TableService,
    confirmService, loader, router, orderservice, socketService, navCtrl) {
        this.dialog = dialog;
        this.snack = snack;
        this.confirmService = confirmService;
        this.loader = loader;
        this.router = router;
        this.orderservice = orderservice;
        this.socketService = socketService;
        this.navCtrl = navCtrl;
    }
    SuppliersComponent.prototype.ngOnInit = function () {
        this.getItems2();
    };
    SuppliersComponent.prototype.getItems2 = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        this.orderservice.getOrders().subscribe(function (data) {
            // refresh the list
            _this.items = _this.temp = data;
            _this.orderservice.orders = data;
            _this.orderservice.setOrders(data);
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    SuppliersComponent.prototype.updateFilter = function (event) {
        var val = event.target.value.toLowerCase();
        var columns = Object.keys(this.temp[0]);
        // Removes last "$$index" from "column"
        columns.splice(columns.length - 1);
        // console.log(columns);
        if (!columns.length)
            return;
        var rows = this.temp.filter(function (d) {
            for (var i = 0; i <= columns.length; i++) {
                var column = columns[i];
                // console.log(d[column]);
                if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
                    return true;
                }
            }
        });
        this.items = rows;
    };
    SuppliersComponent.prototype.deleteItem = function (row) {
        var _this = this;
        this.confirmService.confirm({ message: "Delete " + row.name + "?" })
            .subscribe(function (res) {
            if (res) {
                _this.loader.open();
                /*  this.tableService.removeItem(row)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member deleted!', 'OK', { duration: 4000 })
                    })  */
            }
        });
    };
    SuppliersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-suppliers',
            template: __webpack_require__(/*! ./suppliers.component.html */ "./src/app/views/others/manage/suppliers/suppliers.component.html"),
            styles: [__webpack_require__(/*! ./suppliers.component.scss */ "./src/app/views/others/manage/suppliers/suppliers.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_2__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_3__["AppLoaderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_5__["OrderService"],
            app_shared_services_socket_service__WEBPACK_IMPORTED_MODULE_6__["SocketService"],
            ngx_navigation_with_data__WEBPACK_IMPORTED_MODULE_7__["NgxNavigationWithDataComponent"]])
    ], SuppliersComponent);
    return SuppliersComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/users/user-popup/user-popup.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/views/others/manage/users/user-popup/user-popup.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"username\"\n        [formControl]=\"itemForm.controls['name']\"\n        placeholder=\"Name\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        type=\"number\"\n        name=\"age\"\n        [formControl]=\"itemForm.controls['age']\"\n        placeholder=\"Age\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        type=\"email\"\n        name=\"email\"\n        [formControl]=\"itemForm.controls['email']\"\n        placeholder=\"Email\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"company\"\n        [formControl]=\"itemForm.controls['company']\"\n        placeholder=\"Company\">\n      </mat-form-field>\n    </div>\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"phone\"\n        [formControl]=\"itemForm.controls['phone']\"\n        placeholder=\"Phone\">\n      </mat-form-field>\n    </div>\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"address\"\n        [formControl]=\"itemForm.controls['address']\"\n        placeholder=\"address\">\n      </mat-form-field>\n    </div>\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"balance\"\n        [formControl]=\"itemForm.controls['balance']\"\n        placeholder=\"Balance\">\n      </mat-form-field>\n    </div>\n    <div fxFlex=\"50\"  class=\"pt-1 pr-1\">\n      <mat-slide-toggle [formControl]=\"itemForm.controls['isActive']\">Active Customer</mat-slide-toggle>\n    </div>\n\n\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/manage/users/user-popup/user-popup.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/views/others/manage/users/user-popup/user-popup.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvdXNlcnMvdXNlci1wb3B1cC91c2VyLXBvcHVwLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/manage/users/user-popup/user-popup.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/views/others/manage/users/user-popup/user-popup.component.ts ***!
  \******************************************************************************/
/*! exports provided: UserPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserPopupComponent", function() { return UserPopupComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};



var UserPopupComponent = /** @class */ (function () {
    function UserPopupComponent(data, dialogRef, fb) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
    }
    UserPopupComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    UserPopupComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            name: [item.name || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required],
            age: [item.age || ''],
            email: [item.email || ''],
            company: [item.company || ''],
            phone: [item.phone || ''],
            address: [item.address || ''],
            balance: [item.balance || ''],
            isActive: [item.isActive || false]
        });
    };
    UserPopupComponent.prototype.submit = function () {
        this.dialogRef.close(this.itemForm.value);
    };
    UserPopupComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-user-popup',
            template: __webpack_require__(/*! ./user-popup.component.html */ "./src/app/views/others/manage/users/user-popup/user-popup.component.html"),
            styles: [__webpack_require__(/*! ./user-popup.component.scss */ "./src/app/views/others/manage/users/user-popup/user-popup.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"]])
    ], UserPopupComponent);
    return UserPopupComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/users/users.component.html":
/*!****************************************************************!*\
  !*** ./src/app/views/others/manage/users/users.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n\n    <mat-sidenav  #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\">\n     \n        <mat-card-title class=\"\">\n            <div class=\"card-title-text\">\n              <span>Select Company</span>\n            </div>\n            <mat-divider></mat-divider>\n          </mat-card-title>\n          <mat-card-content>\n            \n            <form class=\"pt-1\">\n                <mat-select id=\"langToggle\" placeholder=\"\" [(ngModel)]=\"selectedValue\" name=\"food\" class=\"mb-1\">\n                  <mat-option *ngFor=\"let food of foods\" [value]=\"food.value\" ngDefaultControl>\n                    {{food.viewValue}}\n                  </mat-option>\n                </mat-select>\n                <p> Selected Company: {{selectedValue}} </p>\n        \n            <!--    <button mat-button color=\"primary\" class=\"mr-1\">Primary</button>   -->\n            </form>\n        \n          </mat-card-content>\n\n      </mat-sidenav>\n\n\n<mat-card class=\"p-0\">\n<div class=\"messages-wrap\">\n<div class=\"m-333\">\n  <!-- <span fxFlex></span> -->\n  <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"openPopUp({}, true)\">Add User</button>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n  <mat-card-content class=\"p-0\">\n    <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"items\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\"\n          [limit]=\"10\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Name\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.name }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Department\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.age }}\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Role\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.balance | currency }}\n            </ng-template>\n          </ngx-datatable-column>\n        \n          <ngx-datatable-column name=\"Status\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row.isActive\">{{row.isActive ? 'active' : 'inactive'}}</mat-chip>\n            </ng-template>\n          </ngx-datatable-column>\n          <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n              <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n            </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>\n  </mat-card-content>\n</mat-card>\n</div>\n</mat-card>\n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/manage/users/users.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/views/others/manage/users/users.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvdXNlcnMvdXNlcnMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/others/manage/users/users.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/views/others/manage/users/users.component.ts ***!
  \**************************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./user-popup/user-popup.component */ "./src/app/views/others/manage/users/user-popup/user-popup.component.ts");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var UsersComponent = /** @class */ (function () {
    function UsersComponent(media, dialog, snack, 
    // private crudService: CrudService,
    confirmService, loader) {
        this.media = media;
        this.dialog = dialog;
        this.snack = snack;
        this.confirmService = confirmService;
        this.loader = loader;
        this.selectedValue = 'Stantech Motors';
        this.foods = [
            { value: 'Stantech Motors', viewValue: 'Stantech Motors' },
            { value: 'Top Quality Motors', viewValue: 'Top Quality Motors' },
            { value: 'Excellent Garage', viewValue: 'Excellent Garage' },
            { value: 'Motor Craft Garage', viewValue: 'Motor Craft Garage' }
        ];
        this.hasvehicles = false;
        this.isSidenavOpen = true;
    }
    UsersComponent.prototype.ngOnInit = function () {
        this.inboxSideNavInit();
        // this.getItems()
    };
    UsersComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    /*  getItems() {
        this.getItemSub = this.crudService.getItems()
          .subscribe(data => {
            this.items = data;
          })
      }
     */
    UsersComponent.prototype.openPopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add new User' : 'Update User';
        var dialogRef = this.dialog.open(_user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_2__["UserPopupComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    UsersComponent.prototype.deleteItem = function (row) {
        var _this = this;
        this.confirmService.confirm({ message: "Delete " + row.name + "?" })
            .subscribe(function (res) {
            if (res) {
                _this.loader.open();
                /*   this.crudService.removeItem(row)
                     .subscribe(data => {
                       this.items = data;
                       this.loader.close();
                       this.snack.open('Member deleted!', 'OK', { duration: 4000 })
                     })  */
            }
        });
    };
    UsersComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    UsersComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSidenav"])
    ], UsersComponent.prototype, "sideNav", void 0);
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/views/others/manage/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/views/others/manage/users/users.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_5__["egretAnimations"]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_6__["ObservableMedia"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_1__["MatSnackBar"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_3__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_4__["AppLoaderService"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"carmake\"\n        [formControl]=\"itemForm.controls['carmake']\"\n        placeholder=\"Car Make\">\n      </mat-form-field>\n    </div>\n\n\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvdmVoaWNsZXMvbWFrZWRpYWxvZ3VlL21ha2VkaWFsb2d1ZS5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.ts ***!
  \*************************************************************************************/
/*! exports provided: MakedialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MakedialogueComponent", function() { return MakedialogueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var MakedialogueComponent = /** @class */ (function () {
    function MakedialogueComponent(data, dialogRef, fb, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
    }
    MakedialogueComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    MakedialogueComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            carmake: [item.carmake || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    MakedialogueComponent.prototype.submit = function () {
        // this.dialogRef.close(this.itemForm.value)
        var _this = this;
        this.subscription = this.orderservice.postMakemodel(this.itemForm.value.carmake, "").subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.subscription.unsubscribe();
            _this.getItemsval();
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    MakedialogueComponent.prototype.getItemsval = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv = this.orderservice.getallVehicles().subscribe(function (data) {
            subv.unsubscribe;
            // refresh the list
            //  this.itemsv =this.tempv = 
            // data;
            _this.orderservice.setMakemodel(data);
            //.. this.orderservice.setMake(data["vehicle_make"]);
            //.. this.orderservice.setModel(data["vehicle_model"]);
            //   this.itemcarmethod(data);
            //   console.log("-------------------"+JSON.stringify( data));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    MakedialogueComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    MakedialogueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-makedialogue',
            template: __webpack_require__(/*! ./makedialogue.component.html */ "./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.html"),
            styles: [__webpack_require__(/*! ./makedialogue.component.scss */ "./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], MakedialogueComponent);
    return MakedialogueComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field>\n        <mat-label>Vehicle Make</mat-label>\n        <mat-select    (selectionChange)=\"changeClient($event.value)\">\n          <mat-option *ngFor=\"let make of makes\" [value]=\"make.vehicle_make\">\n            {{make.vehicle_make}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        \n        name=\"carmodel\"\n        [formControl]=\"itemForm.controls['carmodel']\"\n        placeholder=\"Car Model\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid || checkselect()\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvdmVoaWNsZXMvbW9kZWxkaWFsb2d1ZS9tb2RlbGRpYWxvZ3VlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.ts ***!
  \***************************************************************************************/
/*! exports provided: ModeldialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModeldialogueComponent", function() { return ModeldialogueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var ModeldialogueComponent = /** @class */ (function () {
    function ModeldialogueComponent(data, dialogRef, fb, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
    }
    ModeldialogueComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
        this.makes = this.orderservice.getMakemodel();
        console.log("-------------------" + JSON.stringify(this.makes));
    };
    ModeldialogueComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            carmodel: [item.carmodel || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    ModeldialogueComponent.prototype.submit = function () {
        // this.dialogRef.close(this.itemForm.value)
        var _this = this;
        this.subscription = this.orderservice.postMakemodel(this.selectedmod, this.itemForm.value.carmodel).subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.subscription.unsubscribe();
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    ModeldialogueComponent.prototype.checkselect = function () {
        if (!this.selectedmod) {
            return true;
        }
        else {
            return false;
        }
    };
    ModeldialogueComponent.prototype.changeClient = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmod = data;
        //..alert("selected --->"+data);
    };
    ModeldialogueComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    ModeldialogueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-modeldialogue',
            template: __webpack_require__(/*! ./modeldialogue.component.html */ "./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.html"),
            styles: [__webpack_require__(/*! ./modeldialogue.component.scss */ "./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], ModeldialogueComponent);
    return ModeldialogueComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.html":
/*!*****************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.html ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1 matDialogTitle>{{data.title}}</h1>\n  <form [formGroup]=\"itemForm\" (ngSubmit)=\"submit()\">\n  <div fxLayout=\"row wrap\" fxLayout.lt-sm=\"column\">\n   \n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field>\n        <mat-label>Part Category</mat-label>\n        <mat-select  (selectionChange)=\"changeClient($event.value)\">\n          <mat-option *ngFor=\"let partcategory of partcategorys\" [value]=\"partcategory\">\n            {{partcategory}}\n          </mat-option>\n        </mat-select>\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"50\"  class=\"pr-1\">\n      <mat-form-field class=\"full-width\">\n        <input\n        matInput\n        name=\"partname\"\n        [formControl]=\"itemForm.controls['partname']\"\n        placeholder=\"Part Name\">\n      </mat-form-field>\n    </div>\n\n    <div fxFlex=\"100\" class=\"mt-1\">\n      <button mat-raised-button color=\"primary\" [disabled]=\"itemForm.invalid || checkselect()\">Save</button>\n      <span fxFlex></span>\n      <button mat-button color=\"warn\" type=\"button\" (click)=\"dialogRef.close(false)\">Cancel</button>\n    </div>\n  </div>\n  </form>"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.scss ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvdmVoaWNsZXMvcGFydHNkaWFsb2d1ZS9wYXJ0c2RpYWxvZ3VlLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.ts ***!
  \***************************************************************************************/
/*! exports provided: PartsdialogueComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PartsdialogueComponent", function() { return PartsdialogueComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (undefined && undefined.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};




var PartsdialogueComponent = /** @class */ (function () {
    function PartsdialogueComponent(data, dialogRef, fb, orderservice, snackBar) {
        this.data = data;
        this.dialogRef = dialogRef;
        this.fb = fb;
        this.orderservice = orderservice;
        this.snackBar = snackBar;
        this.partcategorys = ["Body Part", "Electrical Part", "Mechanical Part", "Accessory"];
    }
    PartsdialogueComponent.prototype.ngOnInit = function () {
        this.buildItemForm(this.data.payload);
    };
    PartsdialogueComponent.prototype.buildItemForm = function (item) {
        this.itemForm = this.fb.group({
            partname: [item.partname || '', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]
        });
    };
    PartsdialogueComponent.prototype.submit = function () {
        // this.dialogRef.close(this.itemForm.value)
        var _this = this;
        this.subscription = this.orderservice.postPart(this.itemForm.value.partname, this.selectedmod).subscribe(function (data) {
            // refresh the list
            // this.items =this.temp = data;
            /*    this.orderservice.orders = data;
                //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
                this.orderservice.setOrders("wewewewe");  */
            _this.subscription.unsubscribe();
            _this.dialogRef.close();
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
    };
    PartsdialogueComponent.prototype.checkselect = function () {
        if (!this.selectedmod) {
            return true;
        }
        else {
            return false;
        }
    };
    PartsdialogueComponent.prototype.changeClient = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmod = data;
        //..alert("selected --->"+data);
    };
    PartsdialogueComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    PartsdialogueComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-partsdialogue',
            template: __webpack_require__(/*! ./partsdialogue.component.html */ "./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.html"),
            styles: [__webpack_require__(/*! ./partsdialogue.component.scss */ "./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.scss")]
        }),
        __param(0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        __metadata("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_3__["OrderService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"]])
    ], PartsdialogueComponent);
    return PartsdialogueComponent;
}());



/***/ }),

/***/ "./src/app/views/others/manage/vehicles/vehicles.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/vehicles.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n\n    <mat-sidenav  #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\">\n     \n        <mat-card-title class=\"\">\n            <div class=\"card-title-text\">\n              <span>Select Option</span>\n            </div>\n            <mat-divider></mat-divider>\n          </mat-card-title>\n          <mat-card-content>\n            \n             <div style=\"width: 100%;text-align: center\">\n              <div style=\"box-sizing: border-box;\n              padding: 10px\">\n              <span> \n                 \n                  <button mat-raised-button   [ngStyle]=\"{'background-color': returnBol('car_make')? 'red' : 'LIGHTSLATEGRAY'}\"  (click)=\"getClick('car_make')\">Vehicle Make</button>\n              </span>\n              &nbsp;\n              &nbsp;\n            </div>\n            <div style=\"box-sizing: border-box;\n            padding: 10px\">\n              <span> \n                  <button mat-raised-button  [ngStyle]=\"{'background-color': returnBol('car_model')? 'red' : 'LIGHTSLATEGRAY'}\"  (click)=\"getClick('car_model')\">Vehicle Model</button>\n              </span>\n              &nbsp;\n              &nbsp;\n            </div>\n            <div  style=\"box-sizing: border-box;\n            padding: 10px\">\n              <span> \n                  <button mat-raised-button  [ngStyle]=\"{'background-color': returnBol('car_parts')? 'red' : 'LIGHTSLATEGRAY'}\" (click)=\"getClick('car_parts')\" >Vehicle   Parts</button>\n              </span>\n              &nbsp;\n            </div>\n            </div>\n          </mat-card-content>\n\n      </mat-sidenav>\n\n     \n<mat-card class=\"p-0\">\n<div class=\"messages-wrap\"   *ngIf=\"selectedValue === 'car_make'\" >\n<div class=\"m-333\">\n  <!-- <span fxFlex></span>        color=\"primary\"   [disabled]= \"!returnBol('car_make')\"                  -->\n  <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"openmakePopUp({}, true)\">Add Car Make</button>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n  <mat-card-content class=\"p-0\">\n    <ngx-datatable\n          class=\"material ml-0 mr-0\"\n          [rows]=\"itemsv\"\n          [columnMode]=\"'flex'\"\n          [headerHeight]=\"50\"\n          [footerHeight]=\"50\"\n          [limit]=\"10\"\n          [rowHeight]=\"'auto'\">\n          <ngx-datatable-column name=\"Car Make\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              {{ row?.vehicle_make }}\n            </ng-template>\n          </ngx-datatable-column>\n        \n          <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n            <ng-template let-row=\"row\" ngx-datatable-cell-template>\n              <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n              <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n            </ng-template>\n          </ngx-datatable-column>\n        </ngx-datatable>\n  </mat-card-content>\n</mat-card>\n</div>\n\n<!-- ================================================== -->\n\n<div class=\"messages-wrap\" *ngIf=\"selectedValue === 'car_model'\">\n  <div class=\"m-333\">\n    <!-- <span fxFlex></span> -->\n    <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"openmodelPopUp({}, true)\">Add Car Model</button>\n  </div>\n  <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n    <mat-card-content class=\"p-0\">\n      <ngx-datatable\n            class=\"material ml-0 mr-0\"\n            [rows]=\"itemsv\"\n            [columnMode]=\"'flex'\"\n            [headerHeight]=\"50\"\n            [footerHeight]=\"50\"\n            [limit]=\"10\"\n            [rowHeight]=\"'auto'\">\n            <ngx-datatable-column name=\"Car Make\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.vehicle_make }}\n              </ng-template>\n            </ngx-datatable-column>\n            <ngx-datatable-column name=\"Car Model\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                {{ row?.vehicle_model}}\n              </ng-template>\n            </ngx-datatable-column>\n          \n            <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n              <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n                <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n              </ng-template>\n            </ngx-datatable-column>\n          </ngx-datatable>\n    </mat-card-content>\n  </mat-card>\n  </div>\n\n  <!--=============================================================-->\n\n  <div class=\"messages-wrap\" *ngIf=\"selectedValue === 'car_parts'\">\n    <div class=\"m-333\">\n      <!-- <span fxFlex></span> -->\n      <button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"opencarpartPopUp({}, true)\">Add Car Part</button>\n    </div>\n    <mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n      <mat-card-content class=\"p-0\">\n        <ngx-datatable\n              class=\"material ml-0 mr-0\"\n              [rows]=\"itemsp\"\n              [columnMode]=\"'flex'\"\n              [headerHeight]=\"50\"\n              [footerHeight]=\"50\"\n              [limit]=\"10\"\n              [rowHeight]=\"'auto'\">\n              <ngx-datatable-column name=\"Part Name\" [flexGrow]=\"1\">\n                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                  {{ row?.part_name }}\n                </ng-template>\n              </ngx-datatable-column>\n              <ngx-datatable-column name=\"Part Category\" [flexGrow]=\"1\">\n                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                  {{ row?.part_category }}\n                </ng-template>\n              </ngx-datatable-column>\n             \n              <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n                <ng-template let-row=\"row\" ngx-datatable-cell-template>\n                  <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\" (click)=\"openPopUp(row)\"><mat-icon>edit</mat-icon></button>\n                  <button mat-icon-button mat-sm-button color=\"warn\" (click)=\"deleteItem(row)\"><mat-icon>delete</mat-icon></button>\n                </ng-template>\n              </ngx-datatable-column>\n            </ngx-datatable>\n      </mat-card-content>\n    </mat-card>\n    </div>\n    \n\n\n</mat-card>\n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/vehicles.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/vehicles.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9tYW5hZ2UvdmVoaWNsZXMvdmVoaWNsZXMuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/views/others/manage/vehicles/vehicles.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/others/manage/vehicles/vehicles.component.ts ***!
  \********************************************************************/
/*! exports provided: VehiclesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclesComponent", function() { return VehiclesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./makedialogue/makedialogue.component */ "./src/app/views/others/manage/vehicles/makedialogue/makedialogue.component.ts");
/* harmony import */ var _modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modeldialogue/modeldialogue.component */ "./src/app/views/others/manage/vehicles/modeldialogue/modeldialogue.component.ts");
/* harmony import */ var _partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./partsdialogue/partsdialogue.component */ "./src/app/views/others/manage/vehicles/partsdialogue/partsdialogue.component.ts");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var VehiclesComponent = /** @class */ (function () {
    function VehiclesComponent(media, dialog, snack, orderservice, confirmService, loader) {
        this.media = media;
        this.dialog = dialog;
        this.snack = snack;
        this.orderservice = orderservice;
        this.confirmService = confirmService;
        this.loader = loader;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
        this.itemcars = {};
        this.itemcarsarr = [];
    }
    VehiclesComponent.prototype.ngOnInit = function () {
        this.inboxSideNavInit();
        this.selectedValue = "car_make";
        this.getItemsv();
        this.getItemsp();
        // this.getItems()
    };
    VehiclesComponent.prototype.getClick = function (sele) {
        this.selectedValue = sele;
    };
    VehiclesComponent.prototype.returnBol = function (ret) {
        if (this.selectedValue === ret) {
            return true;
        }
        else {
            return false;
        }
    };
    VehiclesComponent.prototype.itemcarmethod = function (item) {
        var _this = this;
        item.forEach(function (item1, index) {
            console.log("900000======== " + item1); // 9, 2, 5
            for (var j = 0; j <= item1.vehicle_model.length; j++) {
                console.log("90000011======== " + item1.vehicle_model[j]);
                _this.itemcars.model = item1.vehicle_model[j];
                _this.itemcars.make = item1.vehicle_make;
                _this.itemcarsarr.push(_this.itemcars);
                // this.itemcars.model=null;
                // this.itemcars.make=null;
            }
        });
        /*
          item.forEach((item1, indexs) => {
            console.log("900000======== "+item1); // 9, 2, 5
          //  console.log("-------------------"+JSON.stringify( item1));
          //  console.log(index); // 0, 1, 2
        
           item1.vehicle_model.forEach((item2, index) => {
             // console.log(item); // 9, 2, 5
              //console.log(index); // 0, 1, 2
           //   console.log("-------============="+JSON.stringify( item2));
         this.itemcars.model=item2[index];
          this.itemcars.make=item1.vehicle_make[indexs];
          this.itemcarsarr.push(this.itemcars);
          
          
          });
        
        });
        */
        console.log("-------=============" + JSON.stringify(this.itemcars));
        console.log("!!!!!!!!!!!!!!!!!!!!" + JSON.stringify(this.itemcarsarr));
    };
    VehiclesComponent.prototype.getItemsp = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var sub = this.orderservice.getallPart().subscribe(function (data) {
            sub.unsubscribe();
            // refresh the list
            _this.itemsp = _this.tempp = data;
            // this.orderservice.orders = data;
            // this.orderservice.setOrders(data);
            //   console.log("-------------------"+JSON.stringify( data));
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    VehiclesComponent.prototype.getItemsv = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv = this.orderservice.getallVehicles().subscribe(function (data) {
            subv.unsubscribe;
            // refresh the list
            _this.itemsv = _this.tempv = data;
            //   this.itemcarmethod(data);
            //   console.log("-------------------"+JSON.stringify( data));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    VehiclesComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    /*  getItems() {
        this.getItemSub = this.crudService.getItems()
          .subscribe(data => {
            this.items = data;
          })
      }
     */
    VehiclesComponent.prototype.openmakePopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Make' : 'Update Vehicle Make';
        var dialogRef = this.dialog.open(_makedialogue_makedialogue_component__WEBPACK_IMPORTED_MODULE_6__["MakedialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                _this.getItemsv();
            }
            // this.getItemsp();
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    VehiclesComponent.prototype.openmodelPopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Model' : 'Update Vehicle Model';
        var dialogRef = this.dialog.open(_modeldialogue_modeldialogue_component__WEBPACK_IMPORTED_MODULE_7__["ModeldialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                _this.getItemsv();
            }
            // this.getItemsp();
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    VehiclesComponent.prototype.opencarpartPopUp = function (data, isNew) {
        var _this = this;
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add Vehicle Part' : 'Update Vehicle Part';
        var dialogRef = this.dialog.open(_partsdialogue_partsdialogue_component__WEBPACK_IMPORTED_MODULE_8__["PartsdialogueComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        dialogRef.afterClosed()
            .subscribe(function (res) {
            if (!res) {
                // If user press cancel
                return;
            }
            else {
                // this.getItemsv();
                _this.getItemsp();
            }
            _this.loader.open();
            if (isNew) {
                /*    this.crudService.addItem(res)
                      .subscribe(data => {
                        this.items = data;
                        this.loader.close();
                        this.snack.open('Member Added!', 'OK', { duration: 4000 })
                      }) */
            }
            else {
                /*  this.crudService.updateItem(data._id, res)
                    .subscribe(data => {
                      this.items = data;
                      this.loader.close();
                      this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                    }) */
            }
        });
    };
    VehiclesComponent.prototype.deleteItem = function (row) {
        /* this.confirmService.confirm({message: `Delete ${row.name}?`})
           .subscribe(res => {
             if (res) {
               this.loader.open();
            /*   this.crudService.removeItem(row)
                 .subscribe(data => {
                   this.items = data;
                   this.loader.close();
                   this.snack.open('Member deleted!', 'OK', { duration: 4000 })
                 })  */
        //   }
        //  }) 
    };
    VehiclesComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    VehiclesComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"])
    ], VehiclesComponent.prototype, "sideNav", void 0);
    VehiclesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-vehicles',
            template: __webpack_require__(/*! ./vehicles.component.html */ "./src/app/views/others/manage/vehicles/vehicles.component.html"),
            styles: [__webpack_require__(/*! ./vehicles.component.scss */ "./src/app/views/others/manage/vehicles/vehicles.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__["ObservableMedia"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_9__["OrderService"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__["AppLoaderService"]])
    ], VehiclesComponent);
    return VehiclesComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-others-manage-manage-module.js.map