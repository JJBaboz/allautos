(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-others-home-home-module"],{

/***/ "./src/app/views/others/home/home.module.ts":
/*!**************************************************!*\
  !*** ./src/app/views/others/home/home.module.ts ***!
  \**************************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _purchasescreen_purchasescreen_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./purchasescreen/purchasescreen.component */ "./src/app/views/others/home/purchasescreen/purchasescreen.component.ts");
/* harmony import */ var _homelanding_homelanding_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./homelanding/homelanding.component */ "./src/app/views/others/home/homelanding/homelanding.component.ts");
/* harmony import */ var _home_routing__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./home.routing */ "./src/app/views/others/home/home.routing.ts");
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};














//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                //BrowserAnimationsModule,
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _shared_shared_module__WEBPACK_IMPORTED_MODULE_9__["SharedModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_13__["PerfectScrollbarModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(_home_routing__WEBPACK_IMPORTED_MODULE_12__["HomeRoutes"])
            ],
            declarations: [_homelanding_homelanding_component__WEBPACK_IMPORTED_MODULE_11__["HomelandingComponent"], _purchasescreen_purchasescreen_component__WEBPACK_IMPORTED_MODULE_10__["PurchasescreenComponent"]]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ }),

/***/ "./src/app/views/others/home/home.routing.ts":
/*!***************************************************!*\
  !*** ./src/app/views/others/home/home.routing.ts ***!
  \***************************************************/
/*! exports provided: HomeRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutes", function() { return HomeRoutes; });
/* harmony import */ var _homelanding_homelanding_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./homelanding/homelanding.component */ "./src/app/views/others/home/homelanding/homelanding.component.ts");
/* harmony import */ var _purchasescreen_purchasescreen_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./purchasescreen/purchasescreen.component */ "./src/app/views/others/home/purchasescreen/purchasescreen.component.ts");


var HomeRoutes = [
    {
        path: '',
        component: _homelanding_homelanding_component__WEBPACK_IMPORTED_MODULE_0__["HomelandingComponent"],
        data: { title: 'Blank', breadcrumb: 'BLANK' }
    },
    {
        path: 'sparefield',
        component: _purchasescreen_purchasescreen_component__WEBPACK_IMPORTED_MODULE_1__["PurchasescreenComponent"],
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    }
];


/***/ }),

/***/ "./src/app/views/others/home/homelanding/homelanding.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/views/others/home/homelanding/homelanding.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <button mat-raised-button color=\"primary\" class=\"mr-1\">Place Order Now</button> -->\n\n<!-- Dashboard card row -->\n<div fxLayout=\"row wrap\">\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{scale:'.9',delay:'300ms'}}\">\n    <mat-card>\n      <mat-card-title fxLayoutAlign=\"start center\">\n        <small class=\"text-muted\">Parts Raised</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"primary\" \n        selected=\"true\"><mat-icon>live</mat-icon>live</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">0.0</h3>\n       <!-- <small class=\"text-muted\">Monthly</small> -->\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n    <mat-card>\n      <mat-card-title>\n        <small class=\"text-muted\">Parts Authorized</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"accent\" \n        selected=\"true\"><mat-icon>live</mat-icon>live</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">0.0</h3>\n       <!-- <small class=\"text-muted\">Monthly</small> -->\n      </mat-card-content>\n    </mat-card>\n  </div>\n\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n    <mat-card>\n      <mat-card-title>\n        <small class=\"text-muted\">No. of Bids</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"accent\" \n        selected=\"true\"><mat-icon>live</mat-icon>live</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">0.0</h3>\n      <!--  <small class=\"text-muted\">Monthly</small> -->\n      </mat-card-content>\n    </mat-card>\n  </div>\n  <div fxFlex=\"100\" fxFlex.gt-sm=\"25\" fxFlex.sm=\"50\" [@animate]=\"{value:'*',params:{delay:'300ms',scale:'.9'}}\">\n    <mat-card>\n      <mat-card-title>\n        <small class=\"text-muted\">Purchased Parts</small>\n        <span fxFlex></span>\n        <mat-chip\n        class=\"icon-chip\" \n        color=\"primary\" \n        selected=\"true\"><mat-icon>live </mat-icon>live</mat-chip>\n      </mat-card-title>\n      <mat-card-content>\n        <h3 class=\"m-0 font-normal\">0.0</h3>\n      <!--  <small class=\"text-muted\">Monthly</small> -->\n      </mat-card-content>\n    </mat-card>\n  </div>\n</div>\n\n \n  <!-- Fullwidth chart class=\"p-0\"-->\n<div  fxLayout=\"row\" fxLayoutWrap=\"wrap\" fxLayoutAlign=\"center center\">\n  <div fxFlex=\"80\" >\n    <mat-card class=\"p-0\">\n        <mat-card-title class=\"mat-bg-primary m-0\">\n        <div class=\"card-title-text\">\n          <span style=\"width: 100%;text-align: center\" >Real-Time Puchasing System</span>\n          <span fxFlex></span>\n          \n        </div>\n      \n      </mat-card-title>\n\n      <mat-card-content >\n       \n        <section class=\"home-section section-introhome text-center\">\n          <div class=\"container\">\n              <!--      (click)=sendtoform()    -->\n              <button mat-raised-button mat-lg-button color=\"primary\" (click)=sendtoform() >Click Here to Enter Vehicle Details</button>\n              <img src=\"assets/images/5-star.png\" alt=\"\">\n             \n          </div>\n        </section>\n\n      </mat-card-content>\n    </mat-card>\n  </div>\n  \n</div>\n\n\n  "

/***/ }),

/***/ "./src/app/views/others/home/homelanding/homelanding.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/others/home/homelanding/homelanding.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9ob21lL2hvbWVsYW5kaW5nL2hvbWVsYW5kaW5nLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/home/homelanding/homelanding.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/others/home/homelanding/homelanding.component.ts ***!
  \************************************************************************/
/*! exports provided: HomelandingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomelandingComponent", function() { return HomelandingComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! app/shared/services/auth/auth.service */ "./src/app/shared/services/auth/auth.service.ts");
/* harmony import */ var app_globals__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/globals */ "./src/app/globals.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomelandingComponent = /** @class */ (function () {
    function HomelandingComponent(router, orderservice, authService, globals) {
        this.router = router;
        this.orderservice = orderservice;
        this.authService = authService;
        this.globals = globals;
    }
    HomelandingComponent.prototype.ngOnInit = function () {
        this.getItemsp();
        this.getItemsval();
        this.authService.removetoken();
        // console.log(this.globals.userName+"  ================================---------------------  "+this.globals.userdetails);
    };
    HomelandingComponent.prototype.getItemsp = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var sub = this.orderservice.getallPart().subscribe(function (data) {
            sub.unsubscribe();
            // refresh the list
            //.. this.itemsp =this.tempp = 
            //..   data;
            _this.orderservice.setPart(data);
            // this.orderservice.orders = data;
            // this.orderservice.setOrders(data);
            //   console.log("-------------------"+JSON.stringify( data));
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    HomelandingComponent.prototype.getItemsval = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv = this.orderservice.getallVehicles().subscribe(function (data) {
            subv.unsubscribe;
            // refresh the list
            //  this.itemsv =this.tempv = 
            // data;
            _this.orderservice.setMakemodel(data);
            //.. this.orderservice.setMake(data["vehicle_make"]);
            //.. this.orderservice.setModel(data["vehicle_model"]);
            //   this.itemcarmethod(data);
            //   console.log("-------------------"+JSON.stringify( data));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    HomelandingComponent.prototype.sendtoform = function () {
        this.router.navigate(['vehicle/newvehicle']);
    };
    HomelandingComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-homelanding',
            template: __webpack_require__(/*! ./homelanding.component.html */ "./src/app/views/others/home/homelanding/homelanding.component.html"),
            styles: [__webpack_require__(/*! ./homelanding.component.scss */ "./src/app/views/others/home/homelanding/homelanding.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_2__["OrderService"],
            app_shared_services_auth_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"],
            app_globals__WEBPACK_IMPORTED_MODULE_4__["Globals"]])
    ], HomelandingComponent);
    return HomelandingComponent;
}());



/***/ }),

/***/ "./src/app/views/others/home/purchasescreen/purchasescreen.component.html":
/*!********************************************************************************!*\
  !*** ./src/app/views/others/home/purchasescreen/purchasescreen.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n    <!-- Inbox left side bar carsparesarray.car_parts.length()>=1-->\n    \n    <mat-sidenav *ngIf=\"carsparesarray.car_parts.length >= 1\" #sidenav [opened]=\"isSidenavOpen\" mode=\"side\" class=\"inbox-sidenav\">\n      <!-- Compose button -->\n      <button mat-raised-button class=\"mat-warn full-width\" >{{carsparesarray.carreg_no}}</button>\n      <!-- left side buttons -->\n      <div [perfectScrollbar] class=\"chat-sidebar-scroll\">\n      <mat-nav-list class=\"inbox-nav-list\" role=\"list\">\n        <mat-list-item class=\"primary-imenu-item\"  routerLinkActive=\"open\" *ngFor=\"let item of carsparesarray.car_parts\">\n          <a fxLayout=\"row\">\n              {{item.partQuantity}} &nbsp;\n              <span> {{item.partName}} </span>\n          </a>\n        </mat-list-item>\n       \n        <mat-divider></mat-divider>\n  \n      </mat-nav-list>\n    </div>\n    </mat-sidenav>\n  \n  \n  \n  \n  <mat-card class=\"p-0\">\n  <mat-card-title class=\"\">\n    <div class=\"card-title-text\">Fill Order Details</div>\n    <mat-divider></mat-divider>\n  </mat-card-title>\n  <mat-card-content>\n    <form [formGroup]=\"basicForm\" (ngSubmit)=\"onSubmit()\">\n        <div fxLayout=\"row wrap\">\n            <div fxFlex=\"100\" fxFlex.gt-xs=\"50\" class=\"pr-1\">\n  \n  \n  \n                    <div class=\"pb-1\">\n                            <mat-form-field class=\"full-width\">\n                                <input\n                                matInput\n                                name=\"requisition_no\"\n                                formControlName=\"requisition_no\"\n                                placeholder=\"Requisition No.\"\n                                value=\"\">\n                            </mat-form-field>\n                           \n                        </div>\n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input\n                        matInput\n                        name=\"carreg_no\"\n                        formControlName=\"carreg_no\"\n                        placeholder=\"Car Reg No.\"\n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input\n                        matInput\n                        name=\"car_make\"\n                        formControlName=\"car_make\"\n                        placeholder=\"Car Make\"\n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                    <input\n                        matInput\n                        \n                        name=\"car_model\"\n                        formControlName=\"car_model\"\n                        placeholder=\"Car Model\"\n                        value=\"\" required>\n                    </mat-form-field>\n                    \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                    <input\n                        matInput\n                        name=\"yom\"\n                        formControlName=\"yom\"\n                        placeholder=\"Year of Manufacture\"\n                        value=\"\"  required>\n                    </mat-form-field>\n                    \n                </div>\n  \n               \n            </div>\n  \n            <div fxFlex=\"100\" fxFlex.gt-xs=\"50\">\n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <input \n                        matInput\n                        name=\"part_name\"\n                        formControlName=\"part_name\"\n                        placeholder=\"Part Name\" required>\n                    </mat-form-field>\n                    \n                </div>\n  \n                <div class=\"pb-1\">\n                  <mat-form-field class=\"full-width\">\n                  <input \n                     \n                      name=\"part_number\"\n                      matInput\n                      formControlName=\"part_number\"\n                      placeholder=\"Part Number\" \n                      value=\"\" >\n                  </mat-form-field>\n                 \n              </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                    <input \n                       \n                        name=\"quantity\"\n                        matInput\n                        formControlName=\"quantity\"\n                        placeholder=\"Quantity\" \n                        value=\"\" required>\n                    </mat-form-field>\n                   \n                </div>\n  \n                <div class=\"pb-1\">\n                    <mat-form-field class=\"full-width\">\n                        <textarea name=\"description\" matInput formControlName=\"description\" placeholder=\"Description (eg. color, side)\"></textarea>\n                    </mat-form-field>\n                 \n                </div>\n  \n                <div class=\"pb-1\">\n                    \n                    <mat-radio-group \n                    name=\"sample_photos\" \n                    formControlName=\"sample_photos\">\n                        \n                        <mat-radio-button value=\"yes\">Part photos needed?</mat-radio-button>\n                    </mat-radio-group>\n                </div>\n            <!--   <div class=\"pb-1\">\n                    \n                  \n                  <input #file type=\"file\" accept='image/*' (change)=\"preview(file.files)\" />\n                  <img [src]=\"imgURL\" height=\"200\" *ngIf=\"imgURL\">\n              </div>\n         \"basicForm.controls['carreg_no'].invalid\"  \"!carsparesarray.carreg_no\"           (click)=pushcarsparesarraytoserver()-->\n  \n                <button type=\"button\"\n        mat-raised-button \n        color=\"primary\" \n        [disabled]=\"basicForm.invalid\" (click)=addtocarsparesarray()>Add part</button>\n            </div>\n        </div>\n        <button \n        mat-raised-button \n        color=\"primary\" \n        [disabled]= \"!carsparesarray.car_make && basicForm.invalid ||checkpartfield()\" type=\"submit\" >Submit Order</button>\n    </form>\n  </mat-card-content>\n  </mat-card>\n  \n  </mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/home/purchasescreen/purchasescreen.component.scss":
/*!********************************************************************************!*\
  !*** ./src/app/views/others/home/purchasescreen/purchasescreen.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL290aGVycy9ob21lL3B1cmNoYXNlc2NyZWVuL3B1cmNoYXNlc2NyZWVuLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/views/others/home/purchasescreen/purchasescreen.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/views/others/home/purchasescreen/purchasescreen.component.ts ***!
  \******************************************************************************/
/*! exports provided: PurchasescreenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PurchasescreenComponent", function() { return PurchasescreenComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PurchasescreenComponent = /** @class */ (function () {
    function PurchasescreenComponent(media, orderservice, router) {
        this.media = media;
        this.orderservice = orderservice;
        this.router = router;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
        this.formData = {};
        this.carregno = "KCP 393D";
        this.console = console;
        this.carsparesarray = { car_parts: [] };
        this.allcarsparesarray = [];
        this.carpartsData = {};
        //  setTimeout(()=>{ this.hasvehicles = true }, 10000);
    }
    PurchasescreenComponent.prototype.ngOnInit = function () {
        this.inboxSideNavInit();
        this.basicForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormGroup"]({
            carreg_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            car_make: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            car_model: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            yom: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            part_name: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            quantity: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('', _angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required),
            part_number: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            requisition_no: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"](''),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormControl"]('')
        });
        //this.makes=this.orderservice.getMakemodel();
        /*
            this.subscription=this.orderservice.bidOffertt().subscribe( data => {
              // this.router.navigate(['list-user']);
             //... this.subscription.unsubscribe();
             }); */
    };
    PurchasescreenComponent.prototype.clearpartsection = function () {
        this.basicForm.controls['part_name'].reset();
        this.basicForm.controls['part_number'].reset();
        this.basicForm.controls['quantity'].reset();
        this.basicForm.controls['description'].reset();
    };
    PurchasescreenComponent.prototype.checkpartfield = function () {
        if (this.basicForm.value.part_name) {
            return true;
        }
        else {
            return false;
        }
    };
    PurchasescreenComponent.prototype.addtocarsparesarray = function () {
        if (this.carsparesarray.car_make) {
            //  this.carsparesarray.requisition_no = this.basicForm.value.requisition_no;
            //  this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
            // this.carsparesarray.car_make = this.basicForm.value.car_make;
            //  this.carsparesarray.car_model = this.basicForm.value.car_model;
            //this.carsparesarray.car_yom = this.basicForm.value.yom;
            this.carsparesarray.car_parts.push({
                'partName': this.basicForm.value.part_name,
                'partQuantity': this.basicForm.value.quantity,
                'partDescription': this.basicForm.value.description,
                'partNumber': this.basicForm.value.part_number
                //'partPhotosneeded':
            });
            console.log(JSON.stringify(this.carsparesarray) + "-------------------------ALREADY tttttttttttttt-----------------------------------------------" + this.carpartsData);
            //..  console.log(this.carsparesarray.car_make);
        }
        else {
            this.carsparesarray.requisition_no = this.basicForm.value.requisition_no;
            this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
            this.carsparesarray.car_make = this.basicForm.value.car_make;
            this.carsparesarray.car_model = this.basicForm.value.car_model;
            this.carsparesarray.car_yom = this.basicForm.value.yom;
            this.carsparesarray.order_sourceCompany = "STANTECH MOTORS";
            this.carsparesarray.order_sourceOfficer = "STANTECH MOTORS staff";
            this.carsparesarray.order_sourceCompanyid = "555112";
            this.carpartsData.partName = this.basicForm.value.part_name;
            this.carpartsData.partNumber = this.basicForm.value.part_number;
            this.carpartsData.partQuantity = this.basicForm.value.quantity;
            this.carpartsData.partDescription = this.basicForm.value.description;
            this.carsparesarray.car_parts.push(this.carpartsData);
            /*   this.carsparesarray.car_parts.push({
        'partName':  this.basicForm.value.part_name,
        'partQuantity': this.basicForm.value.quantity,
        'partDescription': this.basicForm.value.description,
        'partNumber': this.basicForm.value.part_number
        //'partPhotosneeded':
            });  */
            // this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
            console.log(JSON.stringify(this.carsparesarray) + "-------------------------tttttttttttttt-----------------------------------------------" + this.carpartsData);
        }
        /*
        this.subscription=this.orderservice.bidOffertt("ertyui")
        .subscribe( data => {
         // this.router.navigate(['list-user']);
        //... this.subscription.unsubscribe();
        }); */
        this.clearpartsection();
        //console.log(this.carsparesarray.car_parts.length+"jjjjjjjjj");
    };
    //,,..pushcarsparesarraytoserver(){
    PurchasescreenComponent.prototype.onSubmit = function () {
        // this.subscription=
        /*   this.orderservice.bidOffertt().subscribe( data => {
             // this.router.navigate(['list-user']);
            //... this.subscription.unsubscribe();
            });  */
        var _this = this;
        if (this.basicForm.value.part_name === null) {
            if (this.carsparesarray.car_make) {
                ///  this.carsparesarray.order_sourceCompany = "STANTECH MOTORS";
                //  this.orderservice.postOrder(this.carsparesarray);
                console.log("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffjjjjjjjjj");
                /*this.subscription=
                this.orderservice.bidOffertt().subscribe( data => {
                 // this.router.navigate(['list-user']);
                //... this.subscription.unsubscribe();
                });   */
                this.subscription = this.orderservice.postOrder(this.carsparesarray).subscribe(function (data) {
                    // refresh the list
                    // this.items =this.temp = data;
                    //   console.log(JSON.stringify( data )+"-------------------");
                    _this.subscription.unsubscribe();
                    _this.carsparesarray.car_make = null;
                    // this.clearObjectValues(this.carsparesarray);
                    console.log(data + "----------------------=-===============-------jjjjjjjjj ");
                    _this.basicForm.reset();
                    return true;
                }, function (error) {
                    console.error("Error saving !");
                    //  return Observable.throw(error);
                });
                // this.subscription.unsubscribe();
                /*  this.orderservice.postOrder(this.carsparesarray).subscribe(
                    data => {
                       // refresh the list
                      // this.items =this.temp = data;
                    //   console.log(JSON.stringify( data )+"-------------------");
                    this.carsparesarray.car_make = null;
                  
                    // this.clearObjectValues(this.carsparesarray);
                   console.log(data+"----------------------=-===============-------jjjjjjjjj ");
                     this.basicForm.reset();
                       return true;
                     },
                     error => {
                       console.error("Error saving !");
                     //  return Observable.throw(error);
                    }
                  );
            */
                /*   this.carsparesarray.car_make = null;
                   
                  // this.clearObjectValues(this.carsparesarray);
                  console.log("----------------------=-===============-------jjjjjjjjj");
                   this.basicForm.reset(); */
                this.router.navigate(['others']);
            }
        }
        else {
            /*
               this.carsparesarray.requisition_no = this.basicForm.value.requisition_no;
               this.carsparesarray.order_sourceCompany = "STANTECH MOTORS";
               this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
               this.carsparesarray.car_make = this.basicForm.value.car_make;
               this.carsparesarray.car_model = this.basicForm.value.car_model;
               this.carsparesarray.car_yom = this.basicForm.value.yom;
               this.carpartsData.partName = this.basicForm.value.part_name;
               this.carpartsData.partNumber = this.basicForm.value.part_number;
               this.carpartsData.partQuantity = this.basicForm.value.quantity;
               this.carpartsData.partDescription = this.basicForm.value.description;
               this.carsparesarray.car_parts.push(this.carpartsData);
              
               
             this.orderservice.postOrder(this.carsparesarray);
             this.carsparesarray.car_make = null;
             console.log("----------------------=-===============-------jjjjjjjjj ZERO "+ JSON.stringify(this.carsparesarray));
            // this.clearObjectValues(this.carsparesarray);
             this.basicForm.reset();
            */
        }
        //..this.router.navigate(['others']);
    };
    PurchasescreenComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    PurchasescreenComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    PurchasescreenComponent.prototype.onSubmitu = function () {
        //..console.log("er5432345fcdcsert   ");
    };
    PurchasescreenComponent.prototype.checkselect = function () {
        if (!this.selectedmod) {
            return true;
        }
        else {
            return false;
        }
    };
    PurchasescreenComponent.prototype.changeClient = function (data) {
        //  console.error("============================================ changeClient !"+data);
        this.selectedmod = data;
        //..alert("selected --->"+data);
    };
    PurchasescreenComponent.prototype.ngOnDestroy = function () {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"])
    ], PurchasescreenComponent.prototype, "sideNav", void 0);
    PurchasescreenComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-purchasescreen',
            template: __webpack_require__(/*! ./purchasescreen.component.html */ "./src/app/views/others/home/purchasescreen/purchasescreen.component.html"),
            styles: [__webpack_require__(/*! ./purchasescreen.component.scss */ "./src/app/views/others/home/purchasescreen/purchasescreen.component.scss")]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__["ObservableMedia"], app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_4__["OrderService"], _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], PurchasescreenComponent);
    return PurchasescreenComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-others-home-home-module.js.map