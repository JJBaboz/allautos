(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-others-companyusers-companyusers-module"],{

/***/ "./src/app/views/others/companyusers/companyusers.module.ts":
/*!******************************************************************!*\
  !*** ./src/app/views/others/companyusers/companyusers.module.ts ***!
  \******************************************************************/
/*! exports provided: CompanyusersModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyusersModule", function() { return CompanyusersModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @swimlane/ngx-datatable */ "./node_modules/@swimlane/ngx-datatable/release/index.js");
/* harmony import */ var _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ng2-file-upload/ng2-file-upload */ "./node_modules/ng2-file-upload/ng2-file-upload.js");
/* harmony import */ var ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ngx-perfect-scrollbar */ "./node_modules/ngx-perfect-scrollbar/dist/ngx-perfect-scrollbar.es5.js");
/* harmony import */ var _companyusers_routing__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./companyusers.routing */ "./src/app/views/others/companyusers/companyusers.routing.ts");
/* harmony import */ var app_shared_shared_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./users/users.component */ "./src/app/views/others/companyusers/users/users.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};













//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
var CompanyusersModule = /** @class */ (function () {
    function CompanyusersModule() {
    }
    CompanyusersModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatProgressBarModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_5__["FlexLayoutModule"],
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_7__["ChartsModule"],
                ng2_file_upload_ng2_file_upload__WEBPACK_IMPORTED_MODULE_8__["FileUploadModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatInputModule"],
                //BrowserAnimationsModule,
                _swimlane_ngx_datatable__WEBPACK_IMPORTED_MODULE_6__["NgxDatatableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatStepperModule"],
                ngx_perfect_scrollbar__WEBPACK_IMPORTED_MODULE_9__["PerfectScrollbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                app_shared_shared_module__WEBPACK_IMPORTED_MODULE_11__["SharedModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forChild(_companyusers_routing__WEBPACK_IMPORTED_MODULE_10__["CompanyusersRoutes"])
            ],
            declarations: [
                _users_users_component__WEBPACK_IMPORTED_MODULE_12__["UsersComponent"]
            ]
        })
    ], CompanyusersModule);
    return CompanyusersModule;
}());



/***/ }),

/***/ "./src/app/views/others/companyusers/companyusers.routing.ts":
/*!*******************************************************************!*\
  !*** ./src/app/views/others/companyusers/companyusers.routing.ts ***!
  \*******************************************************************/
/*! exports provided: CompanyusersRoutes */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyusersRoutes", function() { return CompanyusersRoutes; });
/* harmony import */ var _users_users_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./users/users.component */ "./src/app/views/others/companyusers/users/users.component.ts");

var CompanyusersRoutes = [
    {
        path: '',
        component: _users_users_component__WEBPACK_IMPORTED_MODULE_0__["UsersComponent"],
        data: { title: 'Blank', breadcrumb: 'BLANK' }
    }
    /*,{
        path: 'procureh/:id',
        component: ProcureComponent,
        data: { title: 'Blank', breadcrumb: 'BLANK' }
      } */
];


/***/ }),

/***/ "./src/app/views/others/companyusers/users/users.component.html":
/*!**********************************************************************!*\
  !*** ./src/app/views/others/companyusers/users/users.component.html ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<mat-sidenav-container>\n\n\n<mat-card class=\"p-0\">\n<div class=\"messages-wrap\">\n<div class=\"m-333\">\n<!-- <span fxFlex></span> -->\n<button mat-raised-button class=\"mb-05\" color=\"primary\" (click)=\"openPopUp({}, true)\">Add User</button>\n</div>\n<mat-card class=\"p-0\" [@animate]=\"{value:'*',params:{y:'50px',delay:'300ms'}}\">\n<mat-card-content class=\"p-0\">\n  <ngx-datatable\n        class=\"material ml-0 mr-0\"\n        [rows]=\"itemzz\"\n        [columnMode]=\"'flex'\"\n        [headerHeight]=\"50\"\n        [footerHeight]=\"50\"\n        [limit]=\"10\"\n        [rowHeight]=\"'auto'\">\n        <ngx-datatable-column name=\"Name\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.userName }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Department\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.userDepartment }}\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Role\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            {{ row?.userAuthority }}\n          </ng-template>\n        </ngx-datatable-column>\n      \n        <ngx-datatable-column name=\"Status\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            <mat-chip mat-sm-chip [color]=\"'primary'\" [selected]=\"row\">{{row.isActive ? 'active' : 'active'}}</mat-chip>\n          </ng-template>\n        </ngx-datatable-column>\n        <ngx-datatable-column name=\"Actions\" [flexGrow]=\"1\">\n          <ng-template let-row=\"row\" ngx-datatable-cell-template>\n            <button mat-icon-button mat-sm-button color=\"primary\" class=\"mr-1\"><mat-icon>edit</mat-icon></button>\n            <button mat-icon-button mat-sm-button color=\"warn\" ><mat-icon>delete</mat-icon></button>\n          </ng-template>\n        </ngx-datatable-column>\n      </ngx-datatable>\n</mat-card-content>\n</mat-card>\n</div>\n</mat-card>\n\n</mat-sidenav-container>"

/***/ }),

/***/ "./src/app/views/others/companyusers/users/users.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/views/others/companyusers/users/users.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".blue-snackbar {\n  background: #21d409 !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL25lbXMvZGV2UHJvamVjdHMvYWxsYXV0b3NwYXJlcy9wdXJjaGFzaW5nIHN5c3RlbS9zcmMvYXBwL3ZpZXdzL290aGVycy9jb21wYW55dXNlcnMvdXNlcnMvdXNlcnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFBaUIsK0JBQXNDLEVBQUciLCJmaWxlIjoic3JjL2FwcC92aWV3cy9vdGhlcnMvY29tcGFueXVzZXJzL3VzZXJzL3VzZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJsdWUtc25hY2tiYXIgeyBiYWNrZ3JvdW5kOiByZ2IoMzMsIDIxMiwgOSkgIWltcG9ydGFudDt9Il19 */"

/***/ }),

/***/ "./src/app/views/others/companyusers/users/users.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/views/others/companyusers/users/users.component.ts ***!
  \********************************************************************/
/*! exports provided: UsersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersComponent", function() { return UsersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! app/shared/animations/egret-animations */ "./src/app/shared/animations/egret-animations.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! app/shared/services/app-confirm/app-confirm.service */ "./src/app/shared/services/app-confirm/app-confirm.service.ts");
/* harmony import */ var app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! app/shared/services/app-loader/app-loader.service */ "./src/app/shared/services/app-loader/app-loader.service.ts");
/* harmony import */ var _user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-popup/user-popup.component */ "./src/app/views/others/companyusers/users/user-popup/user-popup.component.ts");
/* harmony import */ var app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! app/shared/services/order.service */ "./src/app/shared/services/order.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var UsersComponent = /** @class */ (function () {
    function UsersComponent(media, dialog, snackBar, orderservice, 
    // private crudService: CrudService,
    confirmService, loader) {
        this.media = media;
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.orderservice = orderservice;
        this.confirmService = confirmService;
        this.loader = loader;
        this.hasvehicles = false;
        this.isSidenavOpen = true;
    }
    UsersComponent.prototype.ngOnInit = function () {
        this.inboxSideNavInit();
        this.getItemsall();
        // this.getItems()
    };
    UsersComponent.prototype.ngOnDestroy = function () {
        if (this.getItemSub) {
            this.getItemSub.unsubscribe();
        }
    };
    /*  getItems() {
        this.getItemSub = this.crudService.getItems()
          .subscribe(data => {
            this.items = data;
          })
      }
     */
    UsersComponent.prototype.getItemsall = function () {
        var _this = this;
        //this.items =this.temp =this.orderservice.getOrders2();
        this.orderservice.getUsersCompany(localStorage.getItem('cnumber')).subscribe(function (data) {
            // refresh the list
            _this.itemzz = data;
            // this.orderservice.orders = data;
            // this.orderservice.setOrders(data);
            //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
            return true;
        }, function (error) {
            console.error("Error saving !");
            //  return Observable.throw(error);
        });
        // console.log(  +"-------------------");
    };
    UsersComponent.prototype.openPopUp = function (data, isNew) {
        if (data === void 0) { data = {}; }
        var title = isNew ? 'Add new User' : 'Update User';
        var dialogRef = this.dialog.open(_user_popup_user_popup_component__WEBPACK_IMPORTED_MODULE_6__["UserPopupComponent"], {
            width: '720px',
            disableClose: true,
            data: { title: title, payload: data }
        });
        //  dialogRef.afterClosed()
        //.subscribe(res => {
        //  if(!res) {
        // If user press cancel
        //   return;
        // }
        //this.loader.open();
        if (isNew) {
            /*    this.crudService.addItem(res)
                  .subscribe(data => {
                    this.items = data;
                    this.loader.close();
                    this.snack.open('Member Added!', 'OK', { duration: 4000 })
                  }) */
        }
        else {
            /*  this.crudService.updateItem(data._id, res)
                .subscribe(data => {
                  this.items = data;
                  this.loader.close();
                  this.snack.open('Member Updated!', 'OK', { duration: 4000 })
                }) */
        }
        //  })
    };
    UsersComponent.prototype.deleteItem = function (row) {
        var _this = this;
        this.confirmService.confirm({ message: "Delete " + row.name + "?" })
            .subscribe(function (res) {
            if (res) {
                _this.loader.open();
                /*   this.crudService.removeItem(row)
                     .subscribe(data => {
                       this.items = data;
                       this.loader.close();
                       this.snack.open('Member deleted!', 'OK', { duration: 4000 })
                     })  */
            }
        });
    };
    UsersComponent.prototype.updateSidenav = function () {
        var self = this;
        setTimeout(function () {
            self.isSidenavOpen = !self.isMobile;
            self.sideNav.mode = self.isMobile ? 'over' : 'side';
        });
    };
    UsersComponent.prototype.inboxSideNavInit = function () {
        var _this = this;
        this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
        this.updateSidenav();
        this.screenSizeWatcher = this.media.subscribe(function (change) {
            _this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
            _this.updateSidenav();
        });
    };
    UsersComponent.prototype.openSnackBar = function (text) {
        this.snackBar.open(text, '', {
            duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"]),
        __metadata("design:type", _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenav"])
    ], UsersComponent.prototype, "sideNav", void 0);
    UsersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-users',
            template: __webpack_require__(/*! ./users.component.html */ "./src/app/views/others/companyusers/users/users.component.html"),
            styles: [__webpack_require__(/*! ./users.component.scss */ "./src/app/views/others/companyusers/users/users.component.scss")],
            animations: app_shared_animations_egret_animations__WEBPACK_IMPORTED_MODULE_1__["egretAnimations"]
        }),
        __metadata("design:paramtypes", [_angular_flex_layout__WEBPACK_IMPORTED_MODULE_3__["ObservableMedia"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialog"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"],
            app_shared_services_order_service__WEBPACK_IMPORTED_MODULE_7__["OrderService"],
            app_shared_services_app_confirm_app_confirm_service__WEBPACK_IMPORTED_MODULE_4__["AppConfirmService"],
            app_shared_services_app_loader_app_loader_service__WEBPACK_IMPORTED_MODULE_5__["AppLoaderService"]])
    ], UsersComponent);
    return UsersComponent;
}());



/***/ })

}]);
//# sourceMappingURL=views-others-companyusers-companyusers-module.js.map