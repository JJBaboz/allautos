export class Companyreg {
    public company_name?: string;
    public company_postaladdress?: string;
    public company_phonenumber?: string;
    public company_email?: string;
    public company_website?: string;
    public location_county?: string;
    public user_name?: string;
    public user_phoneno?: string;
    public user_email?: string;
  }
