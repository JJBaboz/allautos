import {carparts} from './carparts.model';
export class sparepartfromcorporate {
  public requisition_no?: string;
  public order_sourceCompany?: string;
  public order_sourceOfficer?: string;
  public order_sourceCompanyid?: string;
  public carreg_no?: string;
  public chassis_no?: string;
  public car_make?: string;
  public car_model?: string;
  public car_yom?: string;
  public car_imgs?: [string];
  public car_parts: carparts[];
}



