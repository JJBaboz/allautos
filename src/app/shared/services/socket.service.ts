import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
//import { Message } from '../model/message';
//import { Event } from '../model/event';

import * as socketIo from 'socket.io-client';
import { Bid } from '../models/bid.model';

//const SERVER_URL = 'http://localhost:8080';

const SERVER_URL = "https://allautospares.com:447/";

@Injectable()
export class SocketService {
    private socket;

    public initSocket(): void {
        this.socket = socketIo(SERVER_URL);
    }
/*
    public send(message: Message): void {
        this.socket.emit('message', message);
    }

    public onMessage(): Observable<Message> {
        return new Observable<Message>(observer => {
            this.socket.on('message', (data: Message) => observer.next(data));
        });
    }   */

    public onBid(): Observable<Bid> {
        return new Observable<Bid>(observer => {
            this.socket.on('bid', (data: Bid) => observer.next(data));
        });
    }

    public onEvent(event: Event): Observable<any> {
        return new Observable<Event>(observer => {
            this.socket.on(event, () => observer.next());
        });
    }
}