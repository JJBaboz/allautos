import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { OrderService } from 'app/shared/services/order.service';
import { Subscription } from 'rxjs';

import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';


@Component({
  selector: 'app-modeldialogue',
  templateUrl: './modeldialogue.component.html',
  styleUrls: ['./modeldialogue.component.scss']
})





export class ModeldialogueComponent implements OnInit {
  public itemForm: FormGroup;
  //makes=["AUDI","TOYOTA","SUBARU"];
  makes;
  subscription:Subscription;
  selectedmod:string;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ModeldialogueComponent>,
    private fb: FormBuilder,
    private orderservice: OrderService ,private snackBar: MatSnackBar,private loader: AppLoaderService
  ) { }

  ngOnInit() {
    this.loader.close();

    this.buildItemForm(this.data.payload)
    this.getMakes()


   console.log("-------------------"+JSON.stringify( this.makes));
  }
  buildItemForm(item) {
    this.itemForm = this.fb.group({
      carmodel: [item.carmodel || '', Validators.required]
    })
  }




  getMakes() {

    //this.items =this.temp =this.orderservice.getOrders2();
    var subv= this.orderservice.getallVehicles().subscribe(
      data => {
        subv.unsubscribe;
         // refresh the list

        this.orderservice.setMakemodel(data);
  
        this.makes= data
  

         
         return true;
       },
       error => {
         console.error("Error saving !");
       
      }
    );
  
    }






  submit() {
    // this.dialogRef.close(this.itemForm.value)
 
    this.subscription= this.orderservice.postMakemodel(this.selectedmod, this.itemForm.value.carmodel).subscribe(
     data => {
        // refresh the list
       // this.items =this.temp = data;
    /*    this.orderservice.orders = data;
        //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
        this.orderservice.setOrders("wewewewe");  */
        this.orderservice.getallVehiclefeed();
        this.openSnackBar("Success")
        this.subscription.unsubscribe();



        this.dialogRef.close({cancel:false,vehicle_make:this.selectedmod,vehicle_model:this.itemForm.value.carmodel});
        return true;
      },
      error => {
        console.error("Error saving !");
      //  return Observable.throw(error);
     }
   );
 
   }
 

   checkselect(){
     if(!this.selectedmod){
       return true;
     }else{
       return false;
     }
   }
   changeClient(data){
  //  console.error("============================================ changeClient !"+data);
    this.selectedmod=data;
    //..alert("selected --->"+data);
  }



   openSnackBar(text:string) {
    this.snackBar.open(text,'' ,{
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
    });
  }


}
