import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModeldialogueComponent } from './modeldialogue.component';

describe('ModeldialogueComponent', () => {
  let component: ModeldialogueComponent;
  let fixture: ComponentFixture<ModeldialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModeldialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModeldialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
