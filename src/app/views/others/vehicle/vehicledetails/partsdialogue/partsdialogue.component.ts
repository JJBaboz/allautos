import { Component, OnInit, Inject } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { OrderService } from 'app/shared/services/order.service';

import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-partsdialogue',
  templateUrl: './partsdialogue.component.html',
  styleUrls: ['./partsdialogue.component.scss']
})
export class PartsdialogueComponent implements OnInit {
  public itemForm: FormGroup;
  partcategorys=["Body Part","Electrical Part","Mechanical Part","Accessory"];
  subscription:any;
  selectedmod:string;
  
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<PartsdialogueComponent>,
    private fb: FormBuilder,
    private orderservice: OrderService ,private snackBar: MatSnackBar,private loader: AppLoaderService
  ) { }


  ngOnInit() {

    this.loader.close();
    this.buildItemForm(this.data.payload)
  }
  buildItemForm(item) {
    this.itemForm = this.fb.group({
      partname: [item.partname || '', Validators.required]
    
    })

  }

  submit() {
    // this.dialogRef.close(this.itemForm.value)
 
    this.subscription= this.orderservice.postPart(this.itemForm.value.partname ,this.selectedmod).subscribe(
     data => {
        // refresh the list
       // this.items =this.temp = data;
    /*    this.orderservice.orders = data;
        //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
        this.orderservice.setOrders("wewewewe");  */
        this.orderservice.getallPartfeed();
        this.openSnackBar("Success")
        this.subscription.unsubscribe();
        
       // this.orderservice.setPart(this.orderservice.getPart().push(this.itemForm.value.partname));
        this.dialogRef.close({cancle:false, part_name:this.itemForm.value.partname, part_category:this.selectedmod});
        return true;
      },
      error => {
        console.error("Error saving !");
      //  return Observable.throw(error);
     }
   );
 
   }
 





   checkselect(){
     if(!this.selectedmod){
       return true;
     }else{
       return false;
     }
   }
   changeClient(data){
  //  console.error("============================================ changeClient !"+data);
    this.selectedmod=data;
    //..alert("selected --->"+data);
  }



   openSnackBar(text:string) {
    this.snackBar.open(text,'' ,{
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
    });
  }


}