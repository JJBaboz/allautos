import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartsdialogueComponent } from './partsdialogue.component';

describe('PartsdialogueComponent', () => {
  let component: PartsdialogueComponent;
  let fixture: ComponentFixture<PartsdialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartsdialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartsdialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
