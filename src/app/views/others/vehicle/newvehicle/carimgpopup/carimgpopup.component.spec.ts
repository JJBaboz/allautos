import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarimgpopupComponent } from './carimgpopup.component';

describe('CarimgpopupComponent', () => {
  let component: CarimgpopupComponent;
  let fixture: ComponentFixture<CarimgpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarimgpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarimgpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
