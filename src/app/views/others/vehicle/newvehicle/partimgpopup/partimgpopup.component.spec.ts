import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartimgpopupComponent } from './partimgpopup.component';

describe('PartimgpopupComponent', () => {
  let component: PartimgpopupComponent;
  let fixture: ComponentFixture<PartimgpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartimgpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartimgpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
