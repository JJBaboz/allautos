import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompsuppliersComponent } from './compsuppliers.component';

describe('CompsuppliersComponent', () => {
  let component: CompsuppliersComponent;
  let fixture: ComponentFixture<CompsuppliersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompsuppliersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompsuppliersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
