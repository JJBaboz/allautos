import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierpopupComponent } from './supplierpopup.component';

describe('SupplierpopupComponent', () => {
  let component: SupplierpopupComponent;
  let fixture: ComponentFixture<SupplierpopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupplierpopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierpopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
