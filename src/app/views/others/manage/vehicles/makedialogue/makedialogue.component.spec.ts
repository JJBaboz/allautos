import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MakedialogueComponent } from './makedialogue.component';

describe('MakedialogueComponent', () => {
  let component: MakedialogueComponent;
  let fixture: ComponentFixture<MakedialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MakedialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MakedialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
