import { Routes } from "@angular/router";
import { AddcompanyComponent } from "./addcompany/addcompany.component";
import { UsersComponent } from "./users/users.component";
import { SuppliersComponent } from "./suppliers/suppliers.component";
import { ManageComponent } from "./manage.component";
import { VehiclesComponent } from "./vehicles/vehicles.component";






export const ManageRoutes: Routes = [
  {
    path: '',
   // component: ManageComponent,
    children: [{
      path: 'addcompany',
      component: AddcompanyComponent,
      data: { title: 'Blank', breadcrumb: 'BLANK' }
    }, 
    {
      path: 'users',
        component: UsersComponent,
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    } , 
    {
      path: 'suppliers',
        component: SuppliersComponent,
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    } , 
    {
      path: 'vehicles',
        component: VehiclesComponent,
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    }
  
  ]
  }
];

