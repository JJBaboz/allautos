import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog, MatSnackBar } from '@angular/material';
//import { TableService } from '../../orders/tables.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { SocketService } from 'app/shared/services/socket.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.scss']
})
export class SuppliersComponent implements OnInit {
  public getItemSub: Subscription;
  public items;
  temp;
  constructor(
    private dialog: MatDialog,
    private snack: MatSnackBar,
   // private tableService: TableService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService,
    private router: Router,
    private orderservice: OrderService,
    private socketService: SocketService,
    public navCtrl: NgxNavigationWithDataComponent
  ) { }

  ngOnInit() {
    this.getItems2();
  }

  getItems2() {
    //this.items =this.temp =this.orderservice.getOrders2();
    this.orderservice.getOrders().subscribe(
      data => {
         // refresh the list
         this.items =this.temp = data;
         this.orderservice.orders = data;
         this.orderservice.setOrders(data);
         //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
         
         return true;
       },
       error => {
         console.error("Error saving !");
       //  return Observable.throw(error);
      }
    );
     
     // console.log(  +"-------------------");

      }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function(d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.items = rows;

  }


  deleteItem(row) {
    this.confirmService.confirm({message: `Delete ${row.name}?`})
      .subscribe(res => {
        if (res) {
          this.loader.open();
        /*  this.tableService.removeItem(row)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            })  */
        }
      })
  }
}
