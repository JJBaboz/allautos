import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Companyreg } from 'app/shared/models/companyreg.model';
import { OrderService } from 'app/shared/services/order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addcompany',
  providers: [OrderService],
  templateUrl: './addcompany.component.html',
  styleUrls: ['./addcompany.component.scss']
})
export class AddcompanyComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  compreg:Companyreg = {};
  subscription:any;
  constructor(private fb: FormBuilder,private orderservice: OrderService , private router: Router) { }

  ngOnInit() {
    this.firstFormGroup = this.fb.group({
      company_name: ['', Validators.required],
      company_postaladdress: ['', Validators.required],
      company_phonenumber: ['', Validators.required],
      company_email: ['', Validators.required],
      company_website: ['', Validators.required],
      location_county: ['', Validators.required]
    }),



    this.secondFormGroup = this.fb.group({
      user_name: ['', Validators.required],
      user_phoneno: ['', Validators.required],
      user_email: ['', Validators.required]

    });


  }
    
  sendCompreg() {
    console.log("-------------------"+ this.firstFormGroup.value.company_name);

    this.compreg.company_name=this.firstFormGroup.value.company_name;
this.compreg.company_postaladdress=this.firstFormGroup.value.company_postaladdress;
this.compreg.company_phonenumber=this.firstFormGroup.value.company_phonenumber;
this.compreg.company_email=this.firstFormGroup.value.company_email;
this.compreg.company_website=this.firstFormGroup.value.company_website;
this.compreg.location_county=this.firstFormGroup.value.location_county;
this.compreg.user_name=this.secondFormGroup.value.user_name;
this.compreg.user_phoneno=this.secondFormGroup.value.user_phoneno;
this.compreg.user_email=this.secondFormGroup.value.user_email;
    console.log(this.compreg);
  
    this.subscription= this.orderservice.addCompany(this.compreg).subscribe(
    data => {
       // refresh the list
      // this.items =this.temp = data;
      // this.orderservice.orders = data;
      // this.orderservice.setOrders(data);
      this.subscription.unsubscribe();
if(data["result"]=="Success"){
  this.firstFormGroup.reset();
  this.secondFormGroup.reset();
  this.router.navigate(['others']);
}else{


}


       
       return true;
     },
     error => {
       console.error("Error saving !");
     //  return Observable.throw(error);
    }
  );

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
  //  this.subscription.unsubscribe();
  }
  
}
