import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchasescreenComponent } from './purchasescreen.component';

describe('PurchasescreenComponent', () => {
  let component: PurchasescreenComponent;
  let fixture: ComponentFixture<PurchasescreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PurchasescreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchasescreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
