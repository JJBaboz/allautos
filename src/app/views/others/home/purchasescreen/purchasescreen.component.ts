import { Component, OnInit, ViewChild  } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { MatSidenav, MatDialog } from '@angular/material';
import { Subscription } from 'rxjs';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { sparepartfromcorporate } from 'app/shared/models/sparepartfromcorporate.model';
import { carparts } from 'app/shared/models/carparts.model';
import { OrderService } from 'app/shared/services/order.service';
import { Router } from '@angular/router';
import { share } from 'rxjs/operators';



@Component({
  selector: 'app-purchasescreen',
  //providers: [OrderService],
  templateUrl: './purchasescreen.component.html',
  styleUrls: ['./purchasescreen.component.scss']
})
export class PurchasescreenComponent implements OnInit {
  isMobile;
   hasvehicles: Boolean = false;
  screenSizeWatcher: Subscription;
  isSidenavOpen: Boolean = true;
  formData = {}
  carregno = "KCP 393D";
  console = console;
  carsparesarray: sparepartfromcorporate = {car_parts:[]};
  allcarsparesarray =[];
  basicForm: FormGroup;
  carpartsData: carparts = {}; 
  subscription: any;
  selectedmod:string;
  makes;

  @ViewChild(MatSidenav) private sideNav: MatSidenav;
  constructor(private media: ObservableMedia, private orderservice: OrderService ,private router: Router) { 

  //  setTimeout(()=>{ this.hasvehicles = true }, 10000);
  }

  ngOnInit() {
    this.inboxSideNavInit();
    this.basicForm = new FormGroup({
      carreg_no: new FormControl('', 
        Validators.required
      ),
      car_make: new FormControl('', 
        Validators.required
      ),
      car_model: new FormControl('', 
        Validators.required
      ),
      yom: new FormControl('', 
        Validators.required
      ),
      part_name: new FormControl('', 
        Validators.required
      ),
      quantity: new FormControl('', 
        Validators.required
      ) ,
      part_number: new FormControl('', 
        
      ),
      requisition_no: new FormControl('', 
        
      ), 
      description: new FormControl('', 
        
      ) 
     
    })
//this.makes=this.orderservice.getMakemodel();
/*
    this.subscription=this.orderservice.bidOffertt().subscribe( data => {
      // this.router.navigate(['list-user']);
     //... this.subscription.unsubscribe();
     }); */
 

  }

   clearpartsection(){
   
     this.basicForm.controls['part_name'].reset();
     this.basicForm.controls['part_number'].reset();
     this.basicForm.controls['quantity'].reset();
     this.basicForm.controls['description'].reset();
      }

      checkpartfield(){
   
      if(this.basicForm.value.part_name){
        return true;
      }else{
        return false;
      }

         }


  addtocarsparesarray() {
    if(this.carsparesarray.car_make){
  //  this.carsparesarray.requisition_no = this.basicForm.value.requisition_no;
  //  this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
   // this.carsparesarray.car_make = this.basicForm.value.car_make;
  //  this.carsparesarray.car_model = this.basicForm.value.car_model;
    //this.carsparesarray.car_yom = this.basicForm.value.yom;
    this.carsparesarray.car_parts.push({
'partName':  this.basicForm.value.part_name,
'partQuantity': this.basicForm.value.quantity,
'partDescription': this.basicForm.value.description,
'partNumber': this.basicForm.value.part_number
//'partPhotosneeded':
    });
    console.log(JSON.stringify(this.carsparesarray)+"-------------------------ALREADY tttttttttttttt-----------------------------------------------"+this.carpartsData);
  
  //..  console.log(this.carsparesarray.car_make);
  }else{  
    this.carsparesarray.requisition_no = this.basicForm.value.requisition_no;
    this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
    this.carsparesarray.car_make = this.basicForm.value.car_make;
    this.carsparesarray.car_model = this.basicForm.value.car_model;
    this.carsparesarray.car_yom = this.basicForm.value.yom;
    this.carsparesarray.order_sourceCompany = "STANTECH MOTORS";
    this.carsparesarray.order_sourceOfficer = "STANTECH MOTORS staff";
    this.carsparesarray.order_sourceCompanyid = "555112";
    this.carpartsData.partName = this.basicForm.value.part_name;
    this.carpartsData.partNumber = this.basicForm.value.part_number;
    this.carpartsData.partQuantity = this.basicForm.value.quantity;
    this.carpartsData.partDescription = this.basicForm.value.description;
    this.carsparesarray.car_parts.push(this.carpartsData);
   
    /*   this.carsparesarray.car_parts.push({
'partName':  this.basicForm.value.part_name,
'partQuantity': this.basicForm.value.quantity,
'partDescription': this.basicForm.value.description,
'partNumber': this.basicForm.value.part_number
//'partPhotosneeded':
    });  */
   // this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
  
    console.log(JSON.stringify(this.carsparesarray)+"-------------------------tttttttttttttt-----------------------------------------------"+this.carpartsData);
  }
  /*
  this.subscription=this.orderservice.bidOffertt("ertyui")
  .subscribe( data => {
   // this.router.navigate(['list-user']);
  //... this.subscription.unsubscribe();
  }); */
 
  
  this.clearpartsection();
  //console.log(this.carsparesarray.car_parts.length+"jjjjjjjjj");
  }

//,,..pushcarsparesarraytoserver(){
  onSubmit(){
   // this.subscription=
 /*   this.orderservice.bidOffertt().subscribe( data => {
      // this.router.navigate(['list-user']);
     //... this.subscription.unsubscribe();
     });  */
 
    if(this.basicForm.value.part_name===null){
      if(this.carsparesarray.car_make){
      ///  this.carsparesarray.order_sourceCompany = "STANTECH MOTORS";
    //  this.orderservice.postOrder(this.carsparesarray);
  
    console.log("ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffjjjjjjjjj");
   
    /*this.subscription=
    this.orderservice.bidOffertt().subscribe( data => {
     // this.router.navigate(['list-user']);
    //... this.subscription.unsubscribe();
    });   */


    this.subscription= this.orderservice.postOrder(this.carsparesarray).subscribe(
      data => {
        // refresh the list
       // this.items =this.temp = data;
     //   console.log(JSON.stringify( data )+"-------------------");
    this.subscription.unsubscribe();
     this.carsparesarray.car_make = null;
   
     // this.clearObjectValues(this.carsparesarray);
    console.log(data+"----------------------=-===============-------jjjjjjjjj ");
      this.basicForm.reset();
        return true;
      },
      error => {
        console.error("Error saving !");
      //  return Observable.throw(error);
     }
    );   
  // this.subscription.unsubscribe();
    /*  this.orderservice.postOrder(this.carsparesarray).subscribe(
        data => {
           // refresh the list
          // this.items =this.temp = data;
        //   console.log(JSON.stringify( data )+"-------------------");
        this.carsparesarray.car_make = null;
      
        // this.clearObjectValues(this.carsparesarray);
       console.log(data+"----------------------=-===============-------jjjjjjjjj ");
         this.basicForm.reset();
           return true;
         },
         error => {
           console.error("Error saving !");
         //  return Observable.throw(error);
        }
      );
*/

   /*   this.carsparesarray.car_make = null;
      
     // this.clearObjectValues(this.carsparesarray);
     console.log("----------------------=-===============-------jjjjjjjjj");
      this.basicForm.reset(); */
      this.router.navigate(['others']);
      }
    }else{
     /*
        this.carsparesarray.requisition_no = this.basicForm.value.requisition_no;
        this.carsparesarray.order_sourceCompany = "STANTECH MOTORS";
        this.carsparesarray.carreg_no = this.basicForm.value.carreg_no;
        this.carsparesarray.car_make = this.basicForm.value.car_make;
        this.carsparesarray.car_model = this.basicForm.value.car_model;
        this.carsparesarray.car_yom = this.basicForm.value.yom;
        this.carpartsData.partName = this.basicForm.value.part_name;
        this.carpartsData.partNumber = this.basicForm.value.part_number;
        this.carpartsData.partQuantity = this.basicForm.value.quantity;
        this.carpartsData.partDescription = this.basicForm.value.description;
        this.carsparesarray.car_parts.push(this.carpartsData);
       
        
      this.orderservice.postOrder(this.carsparesarray);
      this.carsparesarray.car_make = null;
      console.log("----------------------=-===============-------jjjjjjjjj ZERO "+ JSON.stringify(this.carsparesarray));
     // this.clearObjectValues(this.carsparesarray);
      this.basicForm.reset();
     */
    }
    //..this.router.navigate(['others']);
  }



  updateSidenav() {
    let self = this;
    setTimeout(() => {
      self.isSidenavOpen = !self.isMobile;
      self.sideNav.mode = self.isMobile ? 'over' : 'side';
    })
  }
  inboxSideNavInit() {
    this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
    this.updateSidenav();
    this.screenSizeWatcher = this.media.subscribe((change: MediaChange) => {
      this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
      this.updateSidenav();
    });
  }


  onSubmitu(){
    //..console.log("er5432345fcdcsert   ");
  }


  checkselect(){
    if(!this.selectedmod){
      return true;
    }else{
      return false;
    }
  }
  changeClient(data){
 //  console.error("============================================ changeClient !"+data);
   this.selectedmod=data;
   //..alert("selected --->"+data);
 }


  ngOnDestroy(){
    if(this.subscription){
   this.subscription.unsubscribe();
    }
  }


}