import { Routes } from "@angular/router";
import { HomelandingComponent } from "./homelanding/homelanding.component";
import { PurchasescreenComponent } from "./purchasescreen/purchasescreen.component";

export const HomeRoutes: Routes = [
  
    {
      path: '',
      component: HomelandingComponent,
      data: { title: 'Blank', breadcrumb: 'BLANK' }
    }, 
    {
      path: 'sparefield',
        component: PurchasescreenComponent,
        data: { title: 'Basic', breadcrumb: 'BASIC' }
    }   
  
];