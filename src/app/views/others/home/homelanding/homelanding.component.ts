import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { Globals } from 'app/globals';

@Component({
  selector: 'app-homelanding',
  templateUrl: './homelanding.component.html',
  styleUrls: ['./homelanding.component.scss']
})
export class HomelandingComponent implements OnInit {

  constructor(private router: Router,
    private orderservice: OrderService,
    private authService: AuthService,
    private globals: Globals
    
    ) { }

  ngOnInit() {
    this.getItemsp();
    this.getItemsval();
   
    this.authService.removetoken();
   // console.log(this.globals.userName+"  ================================---------------------  "+this.globals.userdetails);
  }



   
  getItemsp() {
    
    //this.items =this.temp =this.orderservice.getOrders2();
    var sub=this.orderservice.getallPart().subscribe(
      data => {
        sub.unsubscribe()
         // refresh the list
        //.. this.itemsp =this.tempp = 
      //..   data;
         this.orderservice.setPart(data);
        // this.orderservice.orders = data;
        // this.orderservice.setOrders(data);
     //   console.log("-------------------"+JSON.stringify( data));
         //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
         
         return true;
       },
       error => {
         console.error("Error saving !");
       //  return Observable.throw(error);
      }
    );
     
     // console.log(  +"-------------------");

      }



      getItemsval() {
        //this.items =this.temp =this.orderservice.getOrders2();
        var subv= this.orderservice.getallVehicles().subscribe(
          data => {
            subv.unsubscribe;
             // refresh the list
                          
           //  this.itemsv =this.tempv = 
            // data;
            this.orderservice.setMakemodel(data);
            //.. this.orderservice.setMake(data["vehicle_make"]);
            //.. this.orderservice.setModel(data["vehicle_model"]);
          //   this.itemcarmethod(data);
          //   console.log("-------------------"+JSON.stringify( data));
             
             return true;
           },
           error => {
             console.error("Error saving !");
           //  return Observable.throw(error);
          }
        );
         
         // console.log(  +"-------------------");
    
          }


          



  sendtoform() {
    
    this.router.navigate(['vehicle/newvehicle']);
  }
}
