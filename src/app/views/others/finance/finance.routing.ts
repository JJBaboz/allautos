import { Routes } from "@angular/router";
import { OpenbidsComponent } from "./openbids/openbids.component";
import { ViewbidreportsComponent } from "./viewbidreports/viewbidreports.component";
import { PartopenbidComponent } from "./partopenbid/partopenbid.component";
import { ViewpartbidsComponent } from "./viewbidreports/viewpartbids/viewpartbids.component";


export const FinanceRoutes: Routes = [
    {
      path: '',
      //component: NewvehicleComponent,
     // data: { title: 'Blank', breadcrumb: 'BLANK' }   
      children: [{
        path: 'openbids',
        component: OpenbidsComponent,
        data: { title: 'Blank', breadcrumb: 'BLANK' }
      }, 
      {
        path: 'partopenbid/:id',
          component:  PartopenbidComponent,
          data: { title: 'Basic', breadcrumb: 'BASIC' }
      },
      {
        path: 'viewbiddingreport',
          component:  ViewbidreportsComponent,
          data: { title: 'Basic', breadcrumb: 'BASIC' }
      }  , 
      {
        path: 'viewpartbids/:id',
          component:  ViewpartbidsComponent,
          data: { title: 'Basic', breadcrumb: 'BASIC' }
      }  
      
    
    ]
 
  
  }
 
  ];