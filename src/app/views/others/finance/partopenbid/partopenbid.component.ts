import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { MatSidenav } from '@angular/material';
import { Subscription } from 'rxjs';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';

@Component({
  selector: 'app-partopenbid',
  templateUrl: './partopenbid.component.html',
  styleUrls: ['./partopenbid.component.scss']
})
export class PartopenbidComponent implements OnInit {
  isMobile;
  hasvehicles: Boolean = false;
 screenSizeWatcher: Subscription;
 isSidenavOpen: Boolean = true;
  public productID;
  Order;
  selectedid;

  @ViewChild(MatSidenav) private sideNav: MatSidenav;
  
  constructor( private media: ObservableMedia,private route: ActivatedRoute ,private orderservice: OrderService,   private router: Router) { 

  //  console.log(JSON.stringify(this.navCtrl.data)); 

  }

  ngOnInit() {
    this.inboxSideNavInit();
    this.productID = this.route.snapshot.params['id'];
    //console.log(this.productID ); 
    //this.Order = JSON.parse(this.productID);
    this.Order= this.orderservice.giveOrders().find(x=>x.order_id == this.productID)
    this.selectedid=this.Order.autorised_parts[0].part_id;
   // console.log("-------------------"+JSON.stringify(this.orderservice.giveOrders()));
    //this.Order =  this.getorderfrom(this.productID);
  //  console.log(this.orderservice.giveOrders());
  }
  onclickpart(id){
  //  this.getbids(id);
   // this.initc=false;
    var ptname=this.Order.autorised_parts.find(x=>x.part_id == id).partName;
    this.selectedid=id;
   // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);

  }
  partstatus(){
    //  this.getbids(id);
     // this.initc=false;
      var ptname=this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status;
      return ptname;
     // this.selectedid=id;
     // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
  
    }
    
  openBidding(id) {
   
     this.Order.autorised_parts.find(x=>x.part_id == id)
      //this.items =this.temp =null;
      this.orderservice.closeopenBids(id,localStorage.getItem('cnumber'),this.productID,localStorage.getItem('uemail'),"Bidding").subscribe(
        data => {
          if(data["result"]=="Success"){
            this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status='Bidding'
    
          }
           // refresh the list
          // this.items =this.temp = data;
       /*    this.orderservice.orders = data;
           //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
           this.orderservice.setOrders("wewewewe");  */
          // return true;
         },
         error => {
           console.error("Error saving !");
         //  return Observable.throw(error);
        }
      );
           


  
    }

  getorderfrom(id){

    //return this.orderservice.orders.find(x=>x.order_id == id)
  }



  updateSidenav() {
    let self = this;
    setTimeout(() => {
      self.isSidenavOpen = !self.isMobile;
      self.sideNav.mode = self.isMobile ? 'over' : 'side';
    })
  }
  inboxSideNavInit() {
    this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
    this.updateSidenav();
    this.screenSizeWatcher = this.media.subscribe((change: MediaChange) => {
      this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
      this.updateSidenav();
    });
  }


}
