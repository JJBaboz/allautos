import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PartopenbidComponent } from './partopenbid.component';

describe('PartopenbidComponent', () => {
  let component: PartopenbidComponent;
  let fixture: ComponentFixture<PartopenbidComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PartopenbidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PartopenbidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
