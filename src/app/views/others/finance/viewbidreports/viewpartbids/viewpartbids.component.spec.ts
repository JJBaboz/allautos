import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpartbidsComponent } from './viewpartbids.component';

describe('ViewpartbidsComponent', () => {
  let component: ViewpartbidsComponent;
  let fixture: ComponentFixture<ViewpartbidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpartbidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpartbidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
