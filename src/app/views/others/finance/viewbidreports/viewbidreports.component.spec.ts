import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewbidreportsComponent } from './viewbidreports.component';

describe('ViewbidreportsComponent', () => {
  let component: ViewbidreportsComponent;
  let fixture: ComponentFixture<ViewbidreportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewbidreportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewbidreportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
