import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenbidsComponent } from './openbids.component';

describe('OpenbidsComponent', () => {
  let component: OpenbidsComponent;
  let fixture: ComponentFixture<OpenbidsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpenbidsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenbidsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
