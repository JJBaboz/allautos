import { Routes } from "@angular/router";
import { ProcureComponent } from "./procure/procure.component";
import { AlltoprocureComponent } from "./alltoprocure/alltoprocure.component";





export const ProcurementRoutes: Routes = [
    {
      path: '',
      component: AlltoprocureComponent,
      data: { title: 'Blank', breadcrumb: 'BLANK' }
    },{
        path: 'procureh/:id',
        component: ProcureComponent,
        data: { title: 'Blank', breadcrumb: 'BLANK' }
      }

]