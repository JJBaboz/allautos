import { Component, OnInit } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { MatDialog, MatSnackBar } from '@angular/material';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';

@Component({
  selector: 'app-alltoprocure',
  templateUrl: './alltoprocure.component.html',
  styleUrls: ['./alltoprocure.component.scss'],
  animations: egretAnimations,
})
export class AlltoprocureComponent implements OnInit {
  public items;
  temp;

  constructor(
    private dialog: MatDialog,
    private snack: MatSnackBar,

    private confirmService: AppConfirmService,
    private loader: AppLoaderService,
    private router: Router,
    private orderservice: OrderService,
  
    public navCtrl: NgxNavigationWithDataComponent
  ) { }

  ngOnInit() {
    this.getItems2()
  }



  getItems2() {
    //this.items =this.temp =this.orderservice.getOrders2();
    this.orderservice.getPOrders().subscribe(
      data => {
         // refresh the list
         this.items =this.temp = data;
       //..  this.orderservice.orders = data;
         this.orderservice.setPOrders(data);
         //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
         
         return true;
       },
       error => {
         console.error("Error saving !");
       //  return Observable.throw(error);
      }
    );
     
     // console.log(  +"-------------------");

      }



      updateFilter(event) {
        const val = event.target.value.toLowerCase();
        var columns = Object.keys(this.temp[0]);
        // Removes last "$$index" from "column"
        columns.splice(columns.length - 1);
    
        // console.log(columns);
        if (!columns.length)
          return;
    
        const rows = this.temp.filter(function(d) {
          for (let i = 0; i <= columns.length; i++) {
            let column = columns[i];
            // console.log(d[column]);
            if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
              return true;
            }
          }
        });
    
        this.items = rows;
    
      }
      //JSON.stringify(this.navCtrl.data)
      openOrder(id) {
      //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
      //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
     var ite= this.items.find(x=>x.order_id == id)
        this.router.navigate(['procurement/procureh',ite.order_id]);
    

      }



}
