import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlltoprocureComponent } from './alltoprocure.component';

describe('AlltoprocureComponent', () => {
  let component: AlltoprocureComponent;
  let fixture: ComponentFixture<AlltoprocureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlltoprocureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlltoprocureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
