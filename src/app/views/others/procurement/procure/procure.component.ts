import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';

export interface authorisedData{
  order_sourceCompanyid?:string;
  orderId?:string;
  authorisedparts?:any;

}

@Component({
  selector: 'app-procure',
  templateUrl: './procure.component.html',
  styleUrls: ['./procure.component.scss']
})
export class ProcureComponent implements OnInit {

  public productID;
  Order;
  interestFormGroup : FormGroup;
  interests:any;
  subscription:any;
  authorisedDatar:authorisedData={};
    constructor( private route: ActivatedRoute ,private orderservice: OrderService,   private router: Router, private formBuilder: FormBuilder) { 
 
   }

  ngOnInit() {
    this.productID = this.route.snapshot.params['id'];

    this.Order= this.orderservice.givePOrders().find(x=>x.order_id == this.productID)

    this.interestFormGroup = this.formBuilder.group({
      interests: this.formBuilder.array([])
       
    });

  }
  onChange(event) {
    const interests = this.interestFormGroup.get('interests') as FormArray;

    if(event.checked) {
      interests.push(new FormControl(event.source.value))
    } else {
      const i = interests.controls.findIndex(x => x.value === event.source.value);
      interests.removeAt(i);
    }
  }


  submit() {
    if(this.interestFormGroup.value.interests.length>=1){
      this.authorisedDatar.order_sourceCompanyid=localStorage.getItem('cnumber');
      this.authorisedDatar.orderId=this.productID;
      this.authorisedDatar.authorisedparts=this.interestFormGroup.value.interests;
     

      this.subscription= this.orderservice.postAuthorisedParts(this.authorisedDatar).subscribe(
        data => {
          // refresh the list
         // this.items =this.temp = data;
       //   console.log(JSON.stringify( data )+"-------------------");
      this.subscription.unsubscribe();
     //  this.carsparesarray.car_make = null;
     
       // this.clearObjectValues(this.carsparesarray);
    //  console.log(data+"----------------------=-===============-------jjjjjjjjj ");
      //  this.basicForm.reset();
          return true;
        },
        error => {
          console.error("Error saving !");
        //  return Observable.throw(error);
       }
      );  
      this.router.navigate(['others']);
  //  console.log(this.interestFormGroup.value.interests);
  }
  }

}
