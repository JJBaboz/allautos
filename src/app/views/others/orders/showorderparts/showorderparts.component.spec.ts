import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShoworderpartsComponent } from './showorderparts.component';

describe('ShoworderpartsComponent', () => {
  let component: ShoworderpartsComponent;
  let fixture: ComponentFixture<ShoworderpartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShoworderpartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShoworderpartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
