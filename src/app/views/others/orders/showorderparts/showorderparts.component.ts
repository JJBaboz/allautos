import { VehiclepicsComponent } from './../vehiclepics/vehiclepics.component';
import { Component, OnInit } from '@angular/core';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { FormBuilder, FormGroup, FormArray, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material';

@Component({

  selector: 'app-showorderparts',
  templateUrl: './showorderparts.component.html',
  styleUrls: ['./showorderparts.component.scss'],
  providers: []
})
export class ShoworderpartsComponent implements OnInit {

  public productID;
  Order;
  itemswontoprint = [] as  any;
  itmw;
  somedata:boolean= false;
  interestFormGroup : FormGroup;
  interests:any;
  public orderID;

  constructor(private dialog: MatDialog, private route: ActivatedRoute ,private orderservice: OrderService,   private router: Router, private formBuilder: FormBuilder) { 

  //  console.log(JSON.stringify(this.navCtrl.data));

  }

  ngOnInit() {

    this.orderID =this.productID = this.route.snapshot.params['id'] 
    //console.log(this.productID ); 
    //this.Order = JSON.parse(this.productID);
    this.Order= this.orderservice.giveOrders().find(x=>x.order_id == this.productID)
   // this.getbids(this.Order.autorised_parts[0].part_id);
    
   // console.log("-------------------"+JSON.stringify(this.orderservice.giveOrders()));
    //this.Order =  this.getorderfrom(this.productID);
  //  console.log(this.orderservice.giveOrders());
  this.interestFormGroup = this.formBuilder.group({
    interests: this.formBuilder.array([])
     
  });
  }

  onChange(event) {
    const interests = this.interestFormGroup.get('interests') as FormArray;

    if(event.checked) {
      this.getbids(event.source.value);
      interests.push(new FormControl(event.source.value))
    } else {
      const i = interests.controls.findIndex(x => x.value === event.source.value);
      interests.removeAt(i);
      this.itemswontoprint.pop(this.itemswontoprint.find(x=>x.bidPartId== event.source.value));
    }
  }


  openPopup(){

    const dialogRef = this.dialog.open( VehiclepicsComponent, {
      width: '80%',
      height: '80%',
      data:this.Order
    });

    //console.log("data is", this.Order.car_imgUrl)

  }
  openParticular() {
   
      this.router.navigate(['/orders/particularpart',this.Order.order_id]);
  
    }

    

  getorderfrom(id){

    //return this.orderservice.orders.find(x=>x.order_id == id)
  }

  printPage() {
   // window.print();
   // this.router.navigate(['orders/printpage']);
  }

  getWonBid(data) {
    //  itemswon;
   // this.itemswontoprint.push(this.Order.autorised_parts[0]);
    
      //this.Order.autorised_parts.find(x=>x.part_id == id)
      //console.log("========================= = = =  "+JSON.stringify( this.items));
     // console.log("========================= = = =  "+JSON.stringify(this.items.find(x=>x.bidAmount== "5800")));
    //  console.log("========================= = = =  "+JSON.stringify(data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed")));
   if(data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed")){
    //  this.itemswon = [data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed")];
   // var da = ;
  // this.itmw=data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed");
      this.itemswontoprint.push(data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won confirmed"));
    //  console.log("========================= = = =  "+JSON.stringify( this.itemswontoprint));
   }else{
   // this.itemswon = [data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won unconfirmed")];
  // this.itmw=data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won unconfirmed");
    this.itemswontoprint.push(data.find(x=>x.bidStatus[x.bidStatus.length-1]== "won unconfirmed"));
  
   }
   this.somedata=true;
    //this.itemswon = this.items.find(x=>x.bidStatus[this.items.bidStatus.length] == "won unconfirmed");
       //this.items =this.temp =null;
     /*  this.orderservice.closeopenBids(id,localStorage.getItem('cnumber'),this.productID,localStorage.getItem('uemail'),"Bidding").subscribe(
         data => {
           if(data["result"]=="Success"){
             this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status='Bidding'
     
           }
         
          },
          error => {
            console.error("Error saving !");
          //  return Observable.throw(error);
         }
       );  */
         
     }
  
  
    getbids(partid:string) {
      console.log("--------get bids for -----------"+partid);
     // this.items =this.temp =null;
      this.orderservice.getPartBids(partid).subscribe(
        data => {
           // refresh the list
         //  this.items =this.temp = data;
         console.log("--------arrived bids for -----------"+partid);
       //  this.itemswontoprint.push(data)
           this.getWonBid(data);
           
       /*    this.orderservice.orders = data;
           //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
           this.orderservice.setOrders("wewewewe");  */
           return true;
         },
         error => {
           console.error("Error saving !");
         //  return Observable.throw(error);
        }
      );
       
       // console.log(  +"-------------------");
  
        }

        partidtoname(id){

         return this.Order.autorised_parts.find(x=>x.part_id == id).partName;


        }


}
