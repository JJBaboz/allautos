import { Component, OnInit } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
  animations: egretAnimations
})
export class OrdersComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
