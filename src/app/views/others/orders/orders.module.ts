import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  MatListModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatChipsModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTabsModule,
  MatInputModule,
  MatProgressBarModule,
 
  MatDatepickerModule, 
  MatNativeDateModule,
  
  MatStepperModule,
  MatSidenavModule,
  MatTooltipModule,
  MatDialogModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatSelectModule
  
 
 } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { DisplayallordersComponent } from './displayallorders/displayallorders.component';
import { ShoworderpartsComponent } from './showorderparts/showorderparts.component';
import { OrdersRoutes } from './orders.routing';
import { SharedModule } from 'app/shared/shared.module';
import { ParticularpartComponent } from './particularpart/particularpart.component';
import { BidpurchaseDialogComponent } from './particularpart/bidpurchase-dialog/bidpurchase-dialog.component';
import { BidofferdialogComponent } from './particularpart/bidofferdialog/bidofferdialog.component';
import { PrintpageComponent } from './printpage/printpage.component';
import {NgxPrintModule} from 'ngx-print'
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatGridListModule,
    MatChipsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatDialogModule,
    MatTabsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatProgressBarModule,
    FlexLayoutModule,
    NgxDatatableModule,
    ChartsModule,
    FileUploadModule,
    CommonModule,
    MatInputModule,
    NgxDatatableModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatStepperModule,
    PerfectScrollbarModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    NgxPrintModule,
   // BrowserAnimationsModule,
    SharedModule,
    RouterModule.forChild(OrdersRoutes)
  ],
  declarations: [
   
   DisplayallordersComponent,ShoworderpartsComponent, ParticularpartComponent, PrintpageComponent
  ]
})
export class OrdersModule { }
