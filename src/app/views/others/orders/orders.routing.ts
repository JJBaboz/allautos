import { Routes } from '@angular/router';
import { DisplayallordersComponent } from './displayallorders/displayallorders.component';
import { ShoworderpartsComponent } from './showorderparts/showorderparts.component';
import { ParticularpartComponent } from './particularpart/particularpart.component';
import { PrintpageComponent } from './printpage/printpage.component';


export const OrdersRoutes: Routes = [
  {
    path: '',
    component: DisplayallordersComponent,
    data: { title: 'Blank', breadcrumb: 'BLANK' }
  }, 
  {
    path: 'showorderparts/:id',
      component: ShoworderpartsComponent,
      data: { title: 'Basic', breadcrumb: 'BASIC' }
  }  , 
  {
    path: 'particularpart/:id',
      component: ParticularpartComponent,
      data: { title: 'Basic', breadcrumb: 'BASIC' }
  }   
  , 
  {
    path: 'printpage',
      component: PrintpageComponent,
      data: { title: 'Basic', breadcrumb: 'BASIC' }
  }  
];