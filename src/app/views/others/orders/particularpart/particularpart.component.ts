import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';
import { sparepartfromcorporate } from 'app/shared/models/sparepartfromcorporate.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { MatSidenav, MatDialog, MatDialogRef,MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { OrderService } from 'app/shared/services/order.service';
import { BidpurchaseDialogComponent } from './bidpurchase-dialog/bidpurchase-dialog.component';
import { BidofferdialogComponent } from './bidofferdialog/bidofferdialog.component';
import { egretAnimations } from 'app/shared/animations/egret-animations';


export interface DialogData {
  bid_id:string;
  awardcompanyId:string;
  bidSupplierId:string;
  bidOrderId:string;
  bidPartId:string;
  bidamount:string;
  carmake: string;
  carmodel: string;
  caryom:string;
  partname:string;
  bidpriceper: any;
  bidtotalprice: any;
  suppliername:string;
  quantity:any;
  comment: string;
  
}



@Component({
  selector: 'app-particularpart',
  templateUrl: './particularpart.component.html',
  styleUrls: ['./particularpart.component.scss'],
  animations: egretAnimations
})
export class ParticularpartComponent implements OnInit {
  [x: string]: any;
  public items;itemshistory;
  initc:Boolean;
  temp;
  isMobile;
  selectedid;
  screenSizeWatcher: Subscription;
  isSidenavOpen: Boolean = true;
  console = console;
    public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
    public hasBaseDropZoneOver: boolean = false;
    carsparesarray: sparepartfromcorporate = {car_parts:[]};  //will be removed
    public productID;
    Order;

    @ViewChild(MatSidenav) private sideNav: MatSidenav;

    constructor(private media: ObservableMedia,private route: ActivatedRoute, 
      private orderservice: OrderService, public dialog: MatDialog,private snackBar: MatSnackBar) { }
  
    ngOnInit() {
      this.initc=true;
      //selected=0;
      this.inboxSideNavInit();
      this.productID = this.route.snapshot.params['id'];
    //console.log(this.productID ); 
    this.Order= this.orderservice.giveOrders().find(x=> x.order_id == this.productID)
  //  this.Order = JSON.parse(this.productID);
    this.getbids(this.Order.autorised_parts[0].part_id);
    this.selectedid=this.Order.autorised_parts[0].part_id;
   // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,this.Order.order_parts[0].partName)
   //this.getbids("partid:string");
    
  }
    public fileOverBase(e: any): void {
      this.hasBaseDropZoneOver = e;
    }


    openSnackBar(text:string) {
      this.snackBar.open(text,'' ,{
        duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
      });
    }

  
    onclickpart(id){
      this.getbids(id);
      this.initc=false;
      var ptname=this.Order.autorised_parts.find(x=>x.part_id == id).partName;
      this.selectedid=id;
     // this.getparthistory(this.Order.order_sourceCompanyid,this.Order.car_make,this.Order.car_model,this.Order.car_yom,ptname);
 
    }

    isItwon(id){

    var stat=  this.Order.autorised_parts.find(x=>x.part_id == id).part_status;

if(stat==='Complete'){
return true;
}
else{
return false;
}


    }

    



///row.bidSourceName,row.bidAmount,row.bidPartId,row.bidOrderId
    openDialogPurchase(bid_id:string,bidSupplierId:string,bidSourceName:string,bidAmount:any,bidPartId:string,bidOrderId:string): void {
       console.log(bidSourceName+bidAmount);
     var part = this.Order.autorised_parts.find(x=>x.part_id == bidPartId)

      const dialogRef = this.dialog.open(BidpurchaseDialogComponent, {
        width: '600px',
        data: {
          bid_id:bid_id,
          awardcompanyId:this.Order.order_sourceCompanyid,
          bidSupplierId:bidSupplierId,
          bidOrderId:bidOrderId,
          bidPartId:bidPartId,
          bidamount:bidAmount,
          carmake: this.Order.car_make,
          carmodel: this.Order.car_model,
          caryom:this.Order.car_yom,
          partname:part.partName,
          bidpriceper: bidAmount,
          bidtotalprice: part.partQuantity*bidAmount,
          suppliername:bidSourceName,
          comment: 'super cool',
          quantity:part.partQuantity   } 
      }).afterClosed()
      .subscribe(response => {
        this.Order.autorised_parts.find(x=>x.part_id == bidPartId).part_status='Complete'
        console.log(response);

      }); 
    }
    
    openDialogOffer(bid_id:string,bidSupplierId:string,bidSourceName:string,bidAmount:any,bidPartId:string,bidOrderId:string): void {
     // console.log(bidSourceName+bidAmount);
    var part = this.Order.autorised_parts.find(x=>x.part_id == bidPartId)

     const dialogRef = this.dialog.open(BidofferdialogComponent, {
       width: '600px',
       data: {
         bid_id:bid_id,
         awardcompanyId:this.Order.order_sourceCompanyid,
         bidSupplierId:bidSupplierId,
         bidOrderId:bidOrderId,
         bidPartId:bidPartId,
         bidamount:bidAmount,
         carmake: this.Order.car_make,
         carmodel: this.Order.car_model,
         caryom:this.Order.car_yom,
         partname:part.partName,
         bidpriceper: bidAmount,
         bidtotalprice: part.partQuantity*bidAmount,
         suppliername:bidSourceName,
         comment: 'super cool',
         quantity:part.partQuantity   } 
     });  
   }


    getbids(partid:string) {
      this.items =this.temp =null;
      this.orderservice.getPartBids(partid).subscribe(
        data => {
           // refresh the list
           this.items =this.temp = data;
       /*    this.orderservice.orders = data;
           //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
           this.orderservice.setOrders("wewewewe");  */
           return true;
         },
         error => {
           console.error("Error saving !");
         //  return Observable.throw(error);
        }
      );
       
       // console.log(  +"-------------------");
  
        }

        getparthistory(comclient_number:string,car_make:string,car_model:string,car_yom:string,partName:string) {
          this.itemshistory =null;

     //    gethistory(comclient_number:string,car_make:string,car_model:string,car_yom:string,partName:string)

          this.orderservice.gethistory(comclient_number,car_make,car_model,car_yom,partName).subscribe(
            data => {
               // refresh the list
               this.itemshistory = data;
           /*    this.orderservice.orders = data;
               //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
               this.orderservice.setOrders("wewewewe");  */
               return true;
             },
             error => {
               console.error("Error saving !");
             //  return Observable.throw(error);
            }
          );
           
           // console.log(  +"-------------------");
      
            }


getStatusreturn(vat){
 
  var ite= this.Order.autorised_parts.find(x=>x.part_id == vat).part_status;
  //console.log( JSON.stringify(ite) +"-------------------"+vat+ite.part_status);
  return ite;
}
selectedStatusreturn(){
 
  var ite= this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status;
  //console.log( JSON.stringify(ite) +"-------------------"+vat+ite.part_status);
  return ite;
}
closeBidding(){
  this.Order.autorised_parts.find(x=>x.part_id == this.selectedid)
  //this.items =this.temp =null;
  this.orderservice.closeopenBids(this.selectedid,localStorage.getItem('cnumber'),this.productID,localStorage.getItem('uemail'),"Closed").subscribe(
    data => {
      if(data["result"]=="Success"){
        this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status='Closed'

      }
       // refresh the list
      // this.items =this.temp = data;
   /*    this.orderservice.orders = data;
       //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
       this.orderservice.setOrders("wewewewe");  */
      // return true;
     },
     error => {
       console.error("Error saving !");
     //  return Observable.throw(error);
    }
  );
   

  
  //console.log("-------------------"+this.Order.autorised_parts.find(x=>x.part_id == this.selectedid).part_status);


}

    updateSidenav() {
      let self = this;
      setTimeout(() => {
        self.isSidenavOpen = !self.isMobile;
        self.sideNav.mode = self.isMobile ? 'over' : 'side';
      })
    }
    inboxSideNavInit() {
      this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
      this.updateSidenav();
      this.screenSizeWatcher = this.media.subscribe((change: MediaChange) => {
        this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
        this.updateSidenav();
      });
    }

  }
  

  // closeopenBids(partid:string,awardcompanyId:string,bidOrderId:string,companyOfficer:string,part_status:string)