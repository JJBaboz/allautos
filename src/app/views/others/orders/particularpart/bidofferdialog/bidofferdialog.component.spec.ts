import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidofferdialogComponent } from './bidofferdialog.component';

describe('BidofferdialogComponent', () => {
  let component: BidofferdialogComponent;
  let fixture: ComponentFixture<BidofferdialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidofferdialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidofferdialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
