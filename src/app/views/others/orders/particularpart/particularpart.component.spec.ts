import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticularpartComponent } from './particularpart.component';

describe('ParticularpartComponent', () => {
  let component: ParticularpartComponent;
  let fixture: ComponentFixture<ParticularpartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticularpartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticularpartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
