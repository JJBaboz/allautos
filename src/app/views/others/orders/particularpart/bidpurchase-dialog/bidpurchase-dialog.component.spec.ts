import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidpurchaseDialogComponent } from './bidpurchase-dialog.component';

describe('BidpurchaseDialogComponent', () => {
  let component: BidpurchaseDialogComponent;
  let fixture: ComponentFixture<BidpurchaseDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidpurchaseDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidpurchaseDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
