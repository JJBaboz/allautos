import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclepicsComponent } from './vehiclepics.component';

describe('VehiclepicsComponent', () => {
  let component: VehiclepicsComponent;
  let fixture: ComponentFixture<VehiclepicsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehiclepicsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehiclepicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
