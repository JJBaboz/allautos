import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayallordersComponent } from './displayallorders.component';

describe('DisplayallordersComponent', () => {
  let component: DisplayallordersComponent;
  let fixture: ComponentFixture<DisplayallordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayallordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayallordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
