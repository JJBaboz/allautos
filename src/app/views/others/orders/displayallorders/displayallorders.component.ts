import { Component, OnInit, OnDestroy } from '@angular/core';

import { egretAnimations } from 'app/shared/animations/egret-animations';
import { Subscription } from 'rxjs';
import { MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { TableService } from '../tables.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { SocketService } from 'app/shared/services/socket.service';
import { Bid } from 'app/shared/models/bid.model';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-displayallorders',
  templateUrl: './displayallorders.component.html',
  styleUrls: ['./displayallorders.component.scss'],
  animations: egretAnimations,
  providers: [TableService,SocketService]
})
export class DisplayallordersComponent implements OnInit, OnDestroy {
  //public items: any[];
  public items;
  temp;
  ioConnection: any;
  bids: Bid[] = [];
  /*  accepbidata = {
    corporate_id:String,

  } */
  //temp = [];
  public getItemSub: Subscription;
  constructor(
    private dialog: MatDialog,
    private snack: MatSnackBar,
    private tableService: TableService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService,
    private router: Router,
    private orderservice: OrderService,
    private socketService: SocketService,
    public navCtrl: NgxNavigationWithDataComponent
  ) { }

  ngOnInit() {
    this.getItems2()
    //this.initIoConnection()
  }

  private initIoConnection(): void {
    this.socketService.initSocket();

    this.ioConnection = this.socketService.onBid()
      .subscribe((bid: Bid) => {
        this.bids.push(bid);
      });

/*
    this.socketService.onEvent(Event.CONNECT)
      .subscribe(() => {
        console.log('connected');
      });

    this.socketService.onEvent(Event.DISCONNECT)
      .subscribe(() => {
        console.log('disconnected');
      }); */
  }

  public acceptBid(message: string): void {
    if (!message) {
      return;
    }

   /* this.socketService.send({
      from: this.user,
      content: message
    }); */
    //  this.messageContent = null;
  }



  ngOnDestroy() {
    if (this.getItemSub) {
      this.getItemSub.unsubscribe()
    }
  }



  getItems2() {
    //this.items =this.temp =this.orderservice.getOrders2();
    this.orderservice.getOrders().subscribe(
      data => {
         // refresh the list
         this.items =this.temp = data;
         this.orderservice.orders = data;
         this.orderservice.setOrders(data);
         //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
         
         return true;
       },
       error => {
         console.error("Error saving !");
       //  return Observable.throw(error);
      }
    );
     
     // console.log(  +"-------------------");

      }















  getItems() {
  /* this.getItemSub = this.orderservice.getOrders()
   //this.getItemSub = this.orderservice.postgetOrder() 
       .subscribe(data => {
        this.items =this.temp = data;
       // console.log(data+"-------------------fghjklkjhgfdsdfghjkjhdsasdfghjhgfdsasdfghjk");
       })
     //..  console.log (JSON.stringify(this.items )+"----------------------------"+this.getItemSub);
    */
   this.orderservice.getOrders().subscribe(
          data => {
             // refresh the list
             this.items =this.temp = data;
             console.log(JSON.stringify( data )+"-------------------");
          
             return true;
           },
           error => {
             console.error("Error saving !");
           //  return Observable.throw(error);
          }
        );
    
    }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function(d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.items = rows;

  }
  //JSON.stringify(this.navCtrl.data)
  openOrder(id) {
  //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
  //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
 var ite= this.items.find(x=>x.order_id == id)
    this.router.navigate(['orders/showorderparts',ite.order_id]);

  }

/*  openPopUp(data: any = {}, isNew?) {
    let title = isNew ? 'Add new member' : 'Update member';
    let dialogRef: MatDialogRef<any> = this.dialog.open(NgxTablePopupComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if(!res) {
          // If user press cancel
          return;
        }
        this.loader.open();
        if (isNew) {
          this.crudService.addItem(res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Added!', 'OK', { duration: 4000 })
            })
        } else {
          this.crudService.updateItem(data._id, res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Updated!', 'OK', { duration: 4000 })
            })
        }
      })
  }  */
  deleteItem(row) {
    this.confirmService.confirm({message: `Delete ${row.name}?`})
      .subscribe(res => {
        if (res) {
          this.loader.open();
          this.tableService.removeItem(row)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            })
        }
      })
  }
}