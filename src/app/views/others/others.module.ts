import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { 
  MatListModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatMenuModule,
  MatSlideToggleModule,
  MatGridListModule,
  MatChipsModule,
  MatCheckboxModule,
  MatRadioModule,
  MatTabsModule,
  MatInputModule,
  MatProgressBarModule,
 
  MatDatepickerModule, 
  MatNativeDateModule,
 
  MatStepperModule,
  MatSidenavModule
 } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';
import { SharedModule } from './../../shared/shared.module';

import { AppGalleryComponent } from './app-gallery/app-gallery.component';
import { AppPricingComponent } from './app-pricing/app-pricing.component';
import { AppUsersComponent } from './app-users/app-users.component';
import { AppBlankComponent } from './app-blank/app-blank.component';
import { OthersRoutes } from "./others.routing";
import { Nested1Component } from './nested1/nested1.component';
import { Nested2Component } from './nested2/nested2.component';
import { Nested3Component } from './nested3/nested3.component';
import { AppBlankComponent2 } from './app-blank2/app-blank2.component';
import { BasicFormComponent } from './basic-form/basic-form.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { HomeComponent } from './home/home.component';
import { HomelandingComponent } from './home/homelanding/homelanding.component';
import { PurchasescreenComponent } from './home/purchasescreen/purchasescreen.component';
import { OrdersComponent } from './orders/orders.component';
import { ReportsComponent } from './reports/reports.component';
import { ManageComponent } from './manage/manage.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { NewvehicleComponent } from './vehicle/newvehicle/newvehicle.component';
import { ProcurementComponent } from './procurement/procurement.component';
import { CompanyusersComponent } from './companyusers/companyusers.component';

import { UsersComponent } from './companyusers/users/users.component';
import { CompsuppliersComponent } from './compsuppliers/compsuppliers.component';
import { SuppliersComponent } from './compsuppliers/suppliers/suppliers.component';
import { SupplierpopupComponent } from './compsuppliers/suppliers/supplierpopup/supplierpopup.component';
import { ProspectiveordersComponent } from './prospectiveorders/prospectiveorders.component';
import { ViewallpordersComponent } from './prospectiveorders/viewallporders/viewallporders.component'

import { FinanceComponent } from './finance/finance.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatGridListModule,
    MatChipsModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTabsModule,
    MatInputModule,
    MatProgressBarModule,
    FlexLayoutModule,
    NgxDatatableModule,
    ChartsModule,
    FileUploadModule,
    SharedModule,
    MatDatepickerModule, 
    MatNativeDateModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatStepperModule,
    PerfectScrollbarModule,
    RouterModule.forChild(OthersRoutes)
  ],
  declarations: [
    AppGalleryComponent, 
    AppPricingComponent, 
    AppUsersComponent, 
    AppBlankComponent2,
    BasicFormComponent,
    AppBlankComponent, Nested1Component, Nested2Component, Nested3Component, 
    HomeComponent,   OrdersComponent, ReportsComponent, ManageComponent,
     VehicleComponent, ProcurementComponent, CompanyusersComponent, 
     CompsuppliersComponent, SuppliersComponent, SupplierpopupComponent, 
     ProspectiveordersComponent, ViewallpordersComponent ]

//HomelandingComponent,PurchasescreenComponent, NewvehicleComponent,, UsersComponent 
  })
export class OthersModule { }
