import { Component, OnInit, ViewChild } from '@angular/core';
import { egretAnimations } from 'app/shared/animations/egret-animations';
import { Subscription } from 'rxjs';
import { MatSidenav, MatDialog, MatSnackBar, MatDialogRef } from '@angular/material';
import { ObservableMedia, MediaChange } from '@angular/flex-layout';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { UserPopupComponent } from './user-popup/user-popup.component';
import { OrderService } from 'app/shared/services/order.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: egretAnimations
})
export class UsersComponent implements OnInit {
  isMobile;
  hasvehicles: Boolean = false;
 screenSizeWatcher: Subscription;
 isSidenavOpen: Boolean = true;
  public items: any[];
  itemzz;
  public getItemSub: Subscription;

  @ViewChild(MatSidenav) private sideNav: MatSidenav;
  constructor(
    private media: ObservableMedia,
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private orderservice: OrderService, 
   // private crudService: CrudService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService
  ) { }

  ngOnInit() {
    this.inboxSideNavInit();
    this.getItemsall();
   // this.getItems()
  }
  ngOnDestroy() {
    if (this.getItemSub) {
      this.getItemSub.unsubscribe()
    }
  }
/*  getItems() {
    this.getItemSub = this.crudService.getItems()
      .subscribe(data => {
        this.items = data;
      })
  }
 */


  getItemsall() {
    //this.items =this.temp =this.orderservice.getOrders2();
    this.orderservice.getUsersCompany(localStorage.getItem('cnumber')).subscribe(
      data => {
         // refresh the list
         this.itemzz = data;
        // this.orderservice.orders = data;
        // this.orderservice.setOrders(data);
         //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
         
         return true;
       },
       error => {
         console.error("Error saving !");
       //  return Observable.throw(error);
      }
    );
     
     // console.log(  +"-------------------");

      }




  openPopUp(data: any = {}, isNew?) {
    let title = isNew ? 'Add new User' : 'Update User';
    let dialogRef: MatDialogRef<any> = this.dialog.open(UserPopupComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
  //  dialogRef.afterClosed()
      //.subscribe(res => {
      //  if(!res) {
          // If user press cancel
       //   return;
       // }
        //this.loader.open();
        if (isNew) {
      /*    this.crudService.addItem(res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Added!', 'OK', { duration: 4000 })
            }) */
        } else {
        /*  this.crudService.updateItem(data._id, res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Updated!', 'OK', { duration: 4000 })
            }) */
        }
    //  })
  }
  deleteItem(row) {
    this.confirmService.confirm({message: `Delete ${row.name}?`})
      .subscribe(res => {
        if (res) {
          this.loader.open();
       /*   this.crudService.removeItem(row)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            })  */
        }
      })
  }



  updateSidenav() {
    let self = this;
    setTimeout(() => {
      self.isSidenavOpen = !self.isMobile;
      self.sideNav.mode = self.isMobile ? 'over' : 'side';
    })
  }
  inboxSideNavInit() {
    this.isMobile = this.media.isActive('xs') || this.media.isActive('sm');
    this.updateSidenav();
    this.screenSizeWatcher = this.media.subscribe((change: MediaChange) => {
      this.isMobile = (change.mqAlias == 'xs') || (change.mqAlias == 'sm');
      this.updateSidenav();
    });
  }


  openSnackBar(text:string) {
    this.snackBar.open(text,'' ,{
      duration: 3000, verticalPosition: 'top', horizontalPosition: 'end'
    });
  }


}