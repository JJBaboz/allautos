import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatDialog, MatSnackBar } from '@angular/material';
import { TableService } from '../../orders/tables.service';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { SocketService } from 'app/shared/services/socket.service';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
import { Bid } from 'app/shared/models/bid.model';

@Component({
  selector: 'app-viewallporders',
  templateUrl: './viewallporders.component.html',
  styleUrls: ['./viewallporders.component.scss']
})
export class ViewallpordersComponent implements OnInit {

  public items;
  temp;
  ioConnection: any;
  bids: Bid[] = [];
  /*  accepbidata = {
    corporate_id:String,

  } */
  //temp = [];
  public getItemSub: Subscription;
  constructor(
    private dialog: MatDialog,
    private snack: MatSnackBar,
   
    private loader: AppLoaderService,
    private router: Router,
    private orderservice: OrderService,
    
    public navCtrl: NgxNavigationWithDataComponent
  ) { }
  ngOnInit() {
    this.getItems2()
  }


 
  getItems2() {
    //this.items =this.temp =this.orderservice.getOrders2();
    this.orderservice.getprospectivePOrders().subscribe(
      data => {
         // refresh the list
         this.items =this.temp = data;
       //..  this.orderservice.orders = data;
        this.orderservice.setprospectivePOrders(data);
         //console.log("-------------------"+JSON.stringify( this.orderservice.orders));
         
         return true;
       },
       error => {
         console.error("Error saving !");
       //  return Observable.throw(error);
      }
    );
     
     // console.log(  +"-------------------");

      }




  getItems() {
  /* this.getItemSub = this.orderservice.getOrders()
   //this.getItemSub = this.orderservice.postgetOrder() 
       .subscribe(data => {
        this.items =this.temp = data;
       // console.log(data+"-------------------fghjklkjhgfdsdfghjkjhdsasdfghjhgfdsasdfghjk");
       })
     //..  console.log (JSON.stringify(this.items )+"----------------------------"+this.getItemSub);
    */
  /* this.orderservice.getOrders().subscribe(
          data => {
             // refresh the list
             this.items =this.temp = data;
             console.log(JSON.stringify( data )+"-------------------");
          
             return true;
           },
           error => {
             console.error("Error saving !");
           //  return Observable.throw(error);
          }
        );
    */
    }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function(d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.items = rows;

  }
  //JSON.stringify(this.navCtrl.data)
  openOrder(id) {
  //  this.router.navigateByUrl('/123', { state: { hello: 'world' } });
  //  this.router.navigateByUrl('orders/showorderparts',{ "hello": 'world'  });JSON.stringify(ite)
 var ite= this.items.find(x=>x.order_id == id)
   this.router.navigate(['prospectiveorders/getquotes',ite.order_id]);

  }

/*  openPopUp(data: any = {}, isNew?) {
    let title = isNew ? 'Add new member' : 'Update member';
    let dialogRef: MatDialogRef<any> = this.dialog.open(NgxTablePopupComponent, {
      width: '720px',
      disableClose: true,
      data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if(!res) {
          // If user press cancel
          return;
        }
        this.loader.open();
        if (isNew) {
          this.crudService.addItem(res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Added!', 'OK', { duration: 4000 })
            })
        } else {
          this.crudService.updateItem(data._id, res)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member Updated!', 'OK', { duration: 4000 })
            })
        }
      })
  }  */
 /* deleteItem(row) {
    this.confirmService.confirm({message: `Delete ${row.name}?`})
      .subscribe(res => {
        if (res) {
          this.loader.open();
          this.tableService.removeItem(row)
            .subscribe(data => {
              this.items = data;
              this.loader.close();
              this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            })
        }
      })
  } */
}