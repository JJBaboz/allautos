import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewallpordersComponent } from './viewallporders.component';

describe('ViewallpordersComponent', () => {
  let component: ViewallpordersComponent;
  let fixture: ComponentFixture<ViewallpordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewallpordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewallpordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
