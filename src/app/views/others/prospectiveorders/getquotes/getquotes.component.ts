import { Component, OnInit } from '@angular/core';
import { NgxNavigationWithDataComponent } from 'ngx-navigation-with-data';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'app/shared/services/order.service';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { GetpreviousquotesComponent } from '../getpreviousquotes/getpreviousquotes.component';
import { MatDialog } from '@angular/material';


export interface toquoteData{
  order_sourceCompanyid?:string;
  orderId?:string;
  toquoteparts?:any;

}

@Component({
  selector: 'app-getquotes',
  templateUrl: './getquotes.component.html',
  styleUrls: ['./getquotes.component.scss']
})
export class GetquotesComponent implements OnInit {

 public productID;
  Order;
  interestFormGroup : FormGroup;
  interests:any;
  subscription:any;
  toquoteDatar:toquoteData={};

  constructor( private route: ActivatedRoute ,private orderservice: OrderService,   private router: Router, private formBuilder: FormBuilder,public dialog: MatDialog) { 

  //  console.log(JSON.stringify(this.navCtrl.data)); 

  }

  ngOnInit() {
    this.productID = this.route.snapshot.params['id'];
    //console.log(this.productID ); 
    //this.Order = JSON.parse(this.productID);
   this.Order= this.orderservice.giveprospectivePOrders().find(x=>x.order_id == this.productID)
   // console.log("-------------------"+JSON.stringify(this.orderservice.giveprospectivePOrders()));
    //this.Order =  this.getorderfrom(this.productID);
  //  console.log(this.orderservice.giveOrders());
  this.interestFormGroup = this.formBuilder.group({
    interests: this.formBuilder.array([])
     
  });

}
  openParticular() {
   
     // this.router.navigate(['/orders/particularpart',this.Order.order_id]);
  
    }

  getorderfrom(id){

    //return this.orderservice.orders.find(x=>x.order_id == id)
  }
  onChange(event) {
    const interests = this.interestFormGroup.get('interests') as FormArray;

    if(event.checked) {
      interests.push(new FormControl(event.source.value))
    } else {
      const i = interests.controls.findIndex(x => x.value === event.source.value);
      interests.removeAt(i);
    }
  }


  submit() {
    if(this.interestFormGroup.value.interests.length>=1){
      this.toquoteDatar.order_sourceCompanyid=localStorage.getItem('cnumber');
      this.toquoteDatar.orderId=this.productID;
      this.toquoteDatar.toquoteparts=this.interestFormGroup.value.interests;
     

      this.subscription= this.orderservice.postAuthorisedParts(this.toquoteDatar).subscribe(
        data => {
          // refresh the list
         // this.items =this.temp = data;
       //   console.log(JSON.stringify( data )+"-------------------");
      this.subscription.unsubscribe();
     //  this.carsparesarray.car_make = null;
     
       // this.clearObjectValues(this.carsparesarray);
    //  console.log(data+"----------------------=-===============-------jjjjjjjjj ");
      //  this.basicForm.reset();
          return true;
        },
        error => {
          console.error("Error saving !");
        //  return Observable.throw(error);
       }
      );  
      this.router.navigate(['others']);
  //  console.log(this.interestFormGroup.value.interests);
  }
  }


  openDialogQuoate(): void {
    //console.log(bidSourceName+bidAmount);
 // var part = this.Order.autorised_parts.find(x=>x.part_id == bidPartId)

   const dialogRef = this.dialog.open(GetpreviousquotesComponent, {
     width: '600px',
   /*  data: {
       bid_id:bid_id,
       awardcompanyId:this.Order.order_sourceCompanyid,
       bidSupplierId:bidSupplierId,
       bidOrderId:bidOrderId,
       bidPartId:bidPartId,
       bidamount:bidAmount,
       carmake: this.Order.car_make,
       carmodel: this.Order.car_model,
       caryom:this.Order.car_yom,
       partname:part.partName,
       bidpriceper: bidAmount,
       bidtotalprice: part.partQuantity*bidAmount,
       suppliername:bidSourceName,
       comment: 'super cool',
       quantity:part.partQuantity   }  */
   });  
 }
 



}
