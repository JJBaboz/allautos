import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetpreviousquotesComponent } from './getpreviousquotes.component';

describe('GetpreviousquotesComponent', () => {
  let component: GetpreviousquotesComponent;
  let fixture: ComponentFixture<GetpreviousquotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetpreviousquotesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetpreviousquotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
