import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProspectiveordersComponent } from './prospectiveorders.component';

describe('ProspectiveordersComponent', () => {
  let component: ProspectiveordersComponent;
  let fixture: ComponentFixture<ProspectiveordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProspectiveordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProspectiveordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
