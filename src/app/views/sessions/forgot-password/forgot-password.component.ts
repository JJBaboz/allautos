import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  userEmail;
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;
  signinFormf: FormGroup;
  constructor() { }

  ngOnInit() {
    this.signinFormf = new FormGroup({
      username: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      companyname: new FormControl('', Validators.required),
      companynumber: new FormControl('', Validators.required)
    })
  }
  submitEmail() {
    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
  }
}
