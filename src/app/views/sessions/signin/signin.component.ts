import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton, MatSnackBar } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'app/shared/services/auth/auth.service';
import { Router } from '@angular/router';
import { UserLogin } from 'app/shared/services/auth/userlogin.model';
import { NavigationService } from 'app/shared/services/navigation.service';


@Component({
  selector: 'app-signin',
  providers: [AuthService],
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar) progressBar: MatProgressBar;
  @ViewChild(MatButton) submitButton: MatButton;

  signinForm: FormGroup;
  userlogin: UserLogin={};
  cname;
  constructor(private router: Router,private authService: AuthService,private snackBar: MatSnackBar,  private navService: NavigationService) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      //username: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      companyname: new FormControl('', Validators.required),
      companynumber: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    })
    this.feedfilleds();
  // this.authService.setUserdetails(localStorage.getItem('token'));

  }

 /* signinty() {
    const signinData = this.signinForm.value
    console.log(signinData);

    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    this.authService.login({
      email: this.signinForm.value.username,
      password: this.signinForm.value.password
    })
    this.router.navigate(['/others']);

  }
*/

feedfilleds(){
 /* this.signinForm.setValue({
    email:  "localStorage.getItem('uemail')", 
    companyname: "localStorage.getItem('cname')",
    companynumber: "localStorage.getItem('cnumber')",
    password: "localStorage.getItem('pas')"

  }); */
  //4controls['companyname']

//this.cname="selected";
  this.signinForm.patchValue({"companyname":localStorage.getItem('cname')});
  this.signinForm.patchValue({"companynumber":localStorage.getItem('cnumber')});
  this.signinForm.patchValue({"email":localStorage.getItem('uemail')});
//  this.signinForm.patchValue({"password":localStorage.getItem('pas')});
}

 authenticateUser(){
  this.submitButton.disabled = true;
  this.progressBar.mode = 'indeterminate';
   this.userlogin.companyname=this.signinForm.value.companyname;
   this.userlogin.companynumber=this.signinForm.value.companynumber;
   this.userlogin.email=this.signinForm.value.email;
   this.userlogin.password=this.signinForm.value.password;

   //userLogin(userlog:UserLogin)
   this.authService.userLogin(this.userlogin).subscribe(
    data => {
       // refresh the list
      // this.items =this.temp = data;
      // this.orderservice.orders = data;
      // this.orderservice.setOrders(data);
    
if(data["result"]=="Success"){
  this.authService.setUserdetails(data["usertoken"])
 // this.signinty();
 //this.navService.setIconMenu();
 //this.authService.giveUserName();
 // this.signinForm.reset();
 //this.router.navigate(['/others']);
  this.submitButton.disabled = false;
  //this.secondFormGroup.reset();
  localStorage.setItem('pas',this.userlogin.password);
  localStorage.setItem('cname',this.userlogin.companyname);
  localStorage.setItem('cnumber',this.userlogin.companynumber);
  localStorage.setItem('uemail',this.userlogin.email);
  
  this.router.navigate(['others']);
 // this.router.navigate(['vehicle/vehicledetails']);
 // /
}else{
  this.submitButton.disabled = false;
  this.openSnackBar(data["result"])

  //this.progressBar.[hidden]="true"
}

     
       return true;
     },
     error => {
       console.error("Error saving !");
       this.submitButton.disabled = false;
     //  return Observable.throw(error);
    }
   );   
  //userLogin(userlog:UserLogin)

 }
 openSnackBar(text) {
  this.snackBar.open(text,'Ok' ,{
    duration: 3000
  });
}


}
//[disabled]="signinForm.invalid"
